#!/bin/bash

USER=$5 # the user to run as
GROUP=$6 # the group to run as
NAME="$3" # Name of the application
DJANGODIR=/home/$USER/iris_backend/ # Django project directory
LOGFILE=/home/$USER/iris_backend/logs/gunicorn/$2
LOGDIR=$(dirname $LOGFILE)
LOGFILEDJANGO=/home/$USER/iris_backend/logs/django/$7
LOGDIRDJANGO=$(dirname $LOGFILE)
NUM_WORKERS=9 # how many worker processes should Gunicorn spawn
DJANGO_WSGI_MODULE=config.wsgi
DJANGO_SETTINGS=$4
PORT=$1

echo "Starting $NAME as `whoami`"


# Activate the virtual environment
#source /home/devel/sites/environments/sentinel/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
cd /home/$USER/iris_backend/Iris/

RUNDIR=$(dirname $LOGFILE)
test -d $RUNDIR || mkdir -p $RUNDIR


RUNDIR=$(dirname $LOGFILEDJANGO)
test -d $RUNDIR || mkdir -p $RUNDIR

exec /home/$USER/iris_env/bin/gunicorn --env DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS $DJANGO_WSGI_MODULE \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
-b 0.0.0.0:$PORT \
--log-level=debug \
--log-file=$LOGFILE 2>>$LOGFILE \
--timeout 300 \
--capture-output
