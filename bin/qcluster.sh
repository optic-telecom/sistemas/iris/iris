#!/bin/bash

USER=$1 # the user to run as
DJANGO_SETTINGS=$2

cd /home/$USER/iris_backend/Iris/

exec python3 manage.py qcluster --settings="$DJANGO_SETTINGS" 2>> /home/$USER/iris_backend/logs/qcluster/qcluster_output.log
