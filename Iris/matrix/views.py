import re
from functools import reduce
from typing import List, Dict, Any, Mapping

import requests
import unidecode
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from dynamic_preferences.registries import global_preferences_registry
from fuzzywuzzy import fuzz
from rest_framework import viewsets
import json


def proxy(request) -> JsonResponse:

    """

        This function return information of matrix request

        :param request: Values of user request
        :type request: request

        :returns: Response data user
        :rtype: JsonResponse
    
    """

    global_preferences: Dict[str, any] = global_preferences_registry.manager()

    origin_url: str = request.build_absolute_uri().split('/api/v1/matrix/')
    url: str = global_preferences['general__matrix_domain'] + origin_url[1]

    headers: Dict[str, str] = {
        'AUTHORIZATION': global_preferences['matrix__matrix_token'],
        'content-type': 'application/json'
    }

    result: List[Dict[str, Any]] = []
    status_code: int = 500

    if request.method == 'GET':

        response = requests.get(
            url=url,
            headers=headers,
            verify=False,
            params={'limit': 1000000, 'offset': 0}
        )

        status_code: int = response.status_code

        if response.status_code == 200:
            
            response = response.json()

            if type(response) != list and 'results' in response.keys():

                result: List[Dict[str, Any]] = response['results']

            else:

                result: List[Dict[str, Any]] = response

        else:
            
            result: List[Dict[str, Any]] = []

        result: JsonResponse = JsonResponse(data=result, safe=False, status=status_code)

    else:

        if request.method == 'POST':

            response = requests.post(
                url=url,
                headers=headers,
                verify=False,
                data=request.body.decode('utf-8')
            )

        status_code: int = response.status_code

        try:

            result: JsonResponse = JsonResponse(data=response.json(), safe=False, status=status_code)

        except:

            result: JsonResponse = JsonResponse(safe=False, status=status_code, data={})

    return result
