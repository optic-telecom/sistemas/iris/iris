from django.apps import AppConfig


class MatrixConfig(AppConfig):

    """

        Class define documentation app Matrix.
    
    """

    name: str = 'matrix'
