from django.urls import path, re_path
from typing import List, Callable

from .views import proxy

urlpatterns: List[Callable] = [
    re_path(r'$', proxy)
]
