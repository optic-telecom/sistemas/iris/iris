from django.core.management.base import BaseCommand
from customers.models import LeadCustomerModel, LeadletCustomerModel, LeadletContactModel

class Command(BaseCommand):

    help = 'Delete lead and leadlet'

    def handle(self, **options):

        LeadletContactModel.objects.all().delete()
        LeadletCustomerModel.objects.all().delete()
        LeadCustomerModel.objects.all().delete()