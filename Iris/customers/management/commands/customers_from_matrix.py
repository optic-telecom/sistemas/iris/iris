import requests
from typing import IO, List, Union, Dict, Any
from openpyxl.workbook import Workbook
from openpyxl.styles import PatternFill

from django.core.management.base import BaseCommand
from dynamic_preferences.registries import global_preferences_registry
from customers.models import CustomerModel, CustomerPhoneModel, CustomerEmailModel

class Command(BaseCommand):

    help = 'Creates new customers based on Matrix data'

    def handle(self, **options):

        global_preferences: Dict[str, any] = global_preferences_registry.manager()

        failed_items = []

        customer_list = []

        next_url: str = global_preferences['general__matrix_domain'] + "customers/"

        while next_url != None:

            headers: Dict[str, str] = { 'AUTHORIZATION': global_preferences['matrix__matrix_token']}

            response = requests.get(
                url=next_url,
                headers=headers,
                verify=False,
            ).json()
            
            next_url = response.get("next", None)

            customers = response['results']

            for customer in customers:

                operator = 1

                for service in customer['service_set']:

                    try:

                        service_response = requests.get(
                            url=service + "?operator=2",
                            headers=headers,
                            verify=False,
                        ).json()

                        if 'id' in service_response: operator = 2

                    except: 

                        operator = 2


                customer['operator'] = operator

            customer_list += customers


        for customer in customer_list:

            if "+" not in customer['phone']:
                customer['phone'] = "+" + customer['phone']

            if "-" in customer['phone']:
                customer['phone'] = customer['phone'].replace("-","")

            try:

                phone_list = []

                phone_instance = CustomerPhoneModel.objects.create(
                    phone=customer['phone'],
                    operator=customer['operator']
                )

                phone_list.append(phone_instance)

                email_list = []

                email_instance = CustomerEmailModel.objects.create(
                    email=customer['email'],
                    operator=customer['operator']
                )

                email_list.append(email_instance)

                customer_inst = CustomerModel.objects.create(
                    rut=customer['rut'],
                    operator=customer['operator']
                )

                customer_inst.phones.set(phone_list)
                customer_inst.emails.set(email_list)
                
            
            except:

                failed_items.append([customer['rut'], customer['email'], customer['phone']])

        if len(failed_items) > 0:
            failed_wb = Workbook()

            ws1 = failed_wb.active
            
            for item in failed_items:
                ws1.append(item)
                
            failed_wb.save(filename="Registos Fallidos.xlsx")
