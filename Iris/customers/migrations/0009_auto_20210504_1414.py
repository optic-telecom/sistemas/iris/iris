# Generated by Django 2.1.7 on 2021-05-04 18:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0008_auto_20210504_1201'),
        ('operators', '0003_update_operators')
    ]

    operations = [
        migrations.RemoveField(
            model_name='customeremailmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='customerphonemodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='feasibilitytablesbase',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalcustomeremailmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalcustomermodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalcustomerphonemodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalcustomertablesmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalfeasibilitytablesbase',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalleadcustomermodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalleadletcontactmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalleadletcustomermodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalleadlettablesmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalleadtablesmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='leadcustomermodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='leadletcontactmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='leadletcustomermodel',
            name='operator',
        ),
        migrations.AlterUniqueTogether(
            name='customermodel',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='customermodel',
            name='operator',
        ),
    ]
