import threading
from datetime import datetime
from typing import Any, Dict, List, Set, Union
from unicodedata import normalize
import requests
from common.utilitarian import *
from common.utilitarian import get_token
from common.viewsets import BaseViewSet
from common.models import BaseModel
from common.serializers import BaseSerializer
from operators.models import OperatorModel
from communications.api import get_customer_communications
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from dynamic_preferences.registries import global_preferences_registry
from openpyxl.styles import PatternFill
from openpyxl.workbook import Workbook
from openpyxl.writer.excel import save_virtual_workbook
from rest_framework.decorators import action
from rest_framework.serializers import (BooleanField, CharField, ChoiceField,
                                        DateField, EmailField, IntegerField,
                                        Serializer, ValidationError)

from .datatables import (CustomerDatatable, LeadCustomerDatatable,
                         LeadletCustomerDatatable,
                         LeadletCustomerOpportunityDatatable, CustomerTablesDatatableBase, LeadTablesDatatable, LeadletTablesDatatable)
from .documentation import (DocumentationCustomerView,
                            DocumentationLeadletView, DocumentationLeadView)
from .models import (CustomerEmailModel, CustomerModel, CustomerPhoneModel,
                     LeadCustomerModel, LeadletContactModel, LeadChannelModel, LeadletChannelModel,
                     LeadletCustomerModel, CustomerTablesModel, LeadTablesModel, LeadletTablesModel,
                     )
from .serializers import (CustomerSerializer, LeadCustomerSerializer, LeadChannelSerializer, LeadletChannelSerializer,
                          LeadletContactSerializer, LeadletCustomerSerializer, CustomerTablesSerializer, LeadTablesSerializer, LeadletTablesSerializer,)


class CustomerView(BaseViewSet):

    queryset: QuerySet = CustomerModel.objects.filter(deleted=False)
    serializer_class = CustomerSerializer
    documentation_class = DocumentationCustomerView()

    @method_decorator(permission_required('customers.add_customermodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_customermodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_customermodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.delete_customermodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('customers.view_customermodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    @documentation_class.documentation_communications_history()
    def communications_history(self, request, pk) -> JsonResponse:

        #Get customer
        try:

            customer: CustomerModel = CustomerModel.objects.get(ID=pk)

        except Exception as e:

            raise ValidationError({
                'Customer error': 'Don`t exist customer'    
            })        

        result = get_customer_communications(customer)

        return JsonResponse( result, safe=False )

    @method_decorator(permission_required('customers.change_customermodel',raise_exception=True))
    @documentation_class.documentation_remove_phone()
    @action(detail=True, methods=['post'])
    def delete_phone(self, request, pk) -> JsonResponse:

        try:

            instance: CustomerSerializer = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error pk":"Don`t exist item"
            })

        if not 'phone' in request.POST.keys():

            raise ValidationError({
                "Error phone":"'phone' fields is required"
            })

        phone: str = request.POST['phone']

        instance.phones.filter(phone=phone).delete()

        return JsonResponse({}, safe=True)

    @method_decorator(permission_required('customers.change_customermodel',raise_exception=True))
    @documentation_class.documentation_remove_email()
    @action(detail=True, methods=['post'])
    def delete_email(self, request, pk) -> JsonResponse:

        try:

            instance: CustomerSerializer = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error pk":"Don`t exist item"
            })

        if not 'email' in request.POST.keys():

            raise ValidationError({
                "Error email":"'email' fields is required"
            })

        email: str = request.POST['email']

        instance.emails.filter(email=email).delete()

        return JsonResponse({}, safe=True)

    @method_decorator(permission_required('customers.change_customermodel',raise_exception=True))
    @documentation_class.documentation_add_phone()
    @action(detail=True, methods=['post'])
    def add_phone(self, request, pk):

        try:

            instance: CustomerSerializer = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error pk":"Don`t exist item"
            })

        phone_instance: CustomerPhoneModel = CustomerPhoneModel()

        if not 'phone' in request.data.keys():

            raise ValidationError({
                "Error phone":"'phone' fields is required"
            })

        phone: str = request.data['phone']

        if phone in [ phone[0] for phone in instance.phones.values_list('phone') ]:

            raise ValidationError({
                "Error phone repeat":"Repeated phone"
            })

        phone_instance.phone: str = phone
        phone_instance.operator: int = instance.operator

        try:

            phone_instance.full_clean()
            phone_instance.save()
            instance.phones.add(phone_instance)

        except:

            raise ValidationError({
                "Error phone":"Invalid phone format"
            })

        return JsonResponse({}, safe=False)

    @method_decorator(permission_required('customers.change_customermodel',raise_exception=True))
    @documentation_class.documentation_add_email()
    @action(detail=True, methods=['post'])
    def add_email(self, request, pk):

        try:

            instance: CustomerSerializer = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error pk":"Don`t exist item"
            })

        email_instance: CustomerEmailModel = CustomerEmailModel()

        if not 'email' in request.data.keys():

            raise ValidationError({
                "Error email":"'email' fields is required"
            })

        email: str = request.data['email']

        if email in [ email[0] for email in instance.emails.values_list('email') ]:

            raise ValidationError({
                "Error email repeat":"Repeated email"
            })

        email_instance.email: str = email
        email_instance.operator: int = instance.operator

        try:

            email_instance.full_clean()
            email_instance.save()
            instance.emails.add(email_instance)

        except:

            raise ValidationError({
                "Error email":"Invalid email format"
            })

        return JsonResponse({}, safe=False)

    @method_decorator(permission_required('customers.view_customermodel',raise_exception=True))
    @documentation_class.documentation_datables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(CustomerDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('customers.view_customermodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return struct data

            :param self: Instance of Class CustomerView
            :type self: CustomerView
            :param request: data of request
            :type request: request
        
        """
        
        return CustomerDatatable(request).get_data()

class LeadCustomerView(BaseViewSet):

    queryset: QuerySet = LeadCustomerModel.objects.filter(deleted=False)
    serializer_class = LeadCustomerSerializer
    documentation_class = DocumentationLeadView()

    @documentation_class.documentation_create()
    @method_decorator(permission_required('customers.add_leadcustomermodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @documentation_class.documentation_update()
    @method_decorator(permission_required('customers.change_leadcustomermodel',raise_exception=True))
    def update(self, request, *args, **kwargs):       
        return super().update(request, *args, **kwargs)
    
    @method_decorator(permission_required('customers.change_leadcustomermodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.delete_leadcustomermodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('customers.view_leadcustomermodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifyView
            :type self: TypifyView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request):
            
            lead_datatables = LeadCustomerDatatable(request)
            lead_datatables.ID_TABLE = pk
            lead_datatables.download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request]
        ).start()

        return JsonResponse(data={})

    @method_decorator(permission_required(['customers.add_leadletcustomermodel', 'customers.change_leadcustomermodel'],raise_exception=True))
    @action(detail=True, methods=['post'])
    @documentation_class.documentation_change_to_leadlet()
    def change_to_leadlet(self, request, pk) -> JsonResponse:

        try:

            instance: LeadCustomerModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Item error": "Don`t exist instance"
            })

        if instance.status == 2:

            raise ValidationError({
                "Intance is leadlef": "Instance has been converted into leadlef"
            })

        def feasibility(street_location: int, operator: int):

            global_preferences: Dict[str, Any] = global_preferences_registry.manager()

            #Get headers and token
            headers: Dict[str, str] = {
                'AUTHORIZATION': global_preferences['matrix__matrix_token']
            }

            #Get url of pulso
            url: str = global_preferences['pulso__domain_url'] + f'/api/v1/factibility/'


            params: Dict[str, Union[str, int]] = {
                'street_location': street_location,
                'operator': operator,
            }

            #Check feasibility
            response_feasibility: Dict[str, str] = requests.get(
                url=url,
                headers=headers,
                verify=False,
                params=params,
            )

            response_feasibility_data: Dict[str, str] = response_feasibility.json()

            if response_feasibility_data.get('factibility', 'NO') != 'SI':

                raise ValidationError({
                    'Error address feasibility': "must have feasibility to change leadlet"
                })

        feasibility(instance.street_location, instance.operator)

        leadlet_instance: LeadletCustomerModel = LeadletCustomerModel()

        leadlet_instance.creator: User = request.user
        leadlet_instance.updater: User = request.user
        leadlet_instance.operator: int = instance.operator
        leadlet_instance.rut: Union[str, None] = self.request.data.get('rut', None)
        leadlet_instance.address: int = instance.address
        leadlet_instance.street_location: int = instance.street_location
        leadlet_instance.name: str = instance.name
        leadlet_instance.primary_email: str = instance.primary_email
        leadlet_instance.secundary_email: str = instance.secundary_email
        leadlet_instance.primary_phone: str = instance.primary_phone
        leadlet_instance.secundary_phone: str = instance.secundary_phone
        leadlet_instance.origin_company: int = instance.origin_company
        leadlet_instance.lead: int = instance
        leadlet_instance.save()

        instance.status: int = 2
        instance.save()

        return JsonResponse({}, safe=False)

    @method_decorator(permission_required('customers.view_leadcustomermodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    @documentation_class.documentation_datables_struct()
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadCustomerView
            :type self: LeadCustomerView
            :param request: data of request
            :type request: request
        
        """

        lead_datatable = LeadCustomerDatatable(request)
        lead_datatable.ID_TABLE = pk

        return JsonResponse(lead_datatable.get_struct(), safe=True)

    @method_decorator(permission_required('customers.view_leadcustomermodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    @documentation_class.documentation_datables()
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadCustomerView
            :type self: LeadCustomerView
            :param request: data of request
            :type request: request
        
        """

        lead_datatable = LeadCustomerDatatable(request)
        lead_datatable.ID_TABLE = pk

        return lead_datatable.get_data()

class LeadletCustomerView(BaseViewSet):

    queryset: QuerySet = LeadletCustomerModel.objects.filter(deleted=False)
    serializer_class = LeadletCustomerSerializer
    documentation_class = DocumentationLeadletView()

    @method_decorator(permission_required('customers.add_leadletcustomermodel',raise_exception=True))
    @documentation_class.documentation_create()
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_leadletcustomermodel',raise_exception=True))
    @documentation_class.documentation_update()
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_leadletcustomermodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.delete_leadletcustomermodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('customers.view_leadletcustomermodel',raise_exception=True))
    @documentation_class.documentation_datables_struct()
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadCustomerView
            :type self: LeadCustomerView
            :param request: data of request
            :type request: request
        
        """

        leadlet_datatable = LeadletCustomerDatatable(request)
        leadlet_datatable.ID_TABLE = pk

        return JsonResponse(leadlet_datatable.get_struct(), safe=True)

    @method_decorator(permission_required('customers.view_leadletcustomermodel',raise_exception=True))
    @documentation_class.documentation_datables_struct_opportunity()
    @action(detail=False, methods=['get'])
    def datatables_opportunity_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadletCustomerView
            :type self: LeadletCustomerView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(LeadletCustomerOpportunityDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('customers.view_leadletcustomermodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadCustomerView
            :type self: LeadCustomerView
            :param request: data of request
            :type request: request
        
        """

        leadlet_datatable = LeadletCustomerDatatable(request)
        leadlet_datatable.ID_TABLE = pk

        return leadlet_datatable.get_data()

    @method_decorator(permission_required('customers.view_leadletcustomermodel',raise_exception=True))
    @documentation_class.documentation_datables_opportunity()
    @action(detail=False, methods=['post'])
    def datatables_opportunity(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadletCustomerView
            :type self: LeadletCustomerView
            :param request: data of request
            :type request: request
        
        """

        return LeadletCustomerOpportunityDatatable(request).get_data()

    @method_decorator(permission_required('customers.view_leadletcustomermodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifyView
            :type self: TypifyView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request):
            leadlet_datatables = LeadletCustomerDatatable(request)
            leadlet_datatables.ID_TABLE = pk
            leadlet_datatables.download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request]
        ).start()

        return JsonResponse(data={})

    @method_decorator(permission_required('customers.view_leadletcustomermodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def download_data_opportunity(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifyView
            :type self: TypifyView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request):

            LeadletCustomerOpportunityDatatable(request).download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request]
        ).start()

        return JsonResponse(data={})

    @documentation_class.create_customer()
    @method_decorator(permission_required(['customers.add_customermodel','customers.change_leadletcustomermodel'],raise_exception=True))
    @action(detail=True, methods=['post'])
    def create_customer(self, request, pk: int) -> JsonResponse:

        COMMUNE_CHOICES = {
            u'01101': 'Iquique', u'01107': 'Alto Hospicio', u'01401': 'Pozo Almonte', u'01402': 'Camiña', u'01403': 'Colchane', 
            u'01404': 'Huara', u'01405': 'Pica', u'02101': 'Antofagasta', u'02102': 'Mejillones', u'02103': 'Sierra Gorda', 
            u'02104': 'Taltal', u'02201': 'Calama', u'02202': 'Ollagüe', u'02203': 'San Pedro de Atacama', u'02301': 'Tocopilla',
            u'02302': 'María Elena', u'03101': 'Copiapó', u'03102': 'Caldera', u'03103': 'Tierra Amarilla', u'03201': 'Chañaral',
            u'03202': 'Diego de Almagro', u'03301': 'Vallenar', u'03302': 'Alto del Carmen', u'03303': 'Freirina', 
            u'03304': 'Huasco', u'04101': 'La Serena', u'04102': 'Coquimbo', u'04103': 'Andacollo', u'04104': 'La Higuera',
            u'04105': 'Paiguano', u'04106': 'Vicuña', u'04201': 'Illapel', u'04202': 'Canela', u'04203': 'Los Vilos',
            u'04204': 'Salamanca', u'04301': 'Ovalle', u'04302': 'Combarbalá', u'04303': 'Monte Patria', u'04304': 'Punitaqui',
            u'04305': 'Réo Hurtado', u'05101': 'Valparaíso', u'05102': 'Casablanca', u'05103': 'Concón', u'05104': 'Juan Fernández',
            u'05105': 'Puchuncaví', u'05107': 'Quintero', u'05109': 'Viña del Mar', u'05201': 'Isla de Pascua', 
            u'05301': 'Los Andes', u'05302': 'Calle Larga', u'05303': 'Rinconada', u'05304': 'San Esteban', u'05401': 'La Ligua',
            u'05402': 'Cabildo', u'05403': 'Papudo', u'05404': 'Petorca', u'05405': 'Zapallar', u'05501': 'Quillota',
            u'05502': 'Calera', u'05503': 'Hijuelas', u'05504': 'La Cruz', u'05506': 'Nogales', u'05601': 'San Antonio',
            u'05602': 'Algarrobo', u'05603': 'Cartagena', u'05604': 'El Quisco', u'05605': 'El Tabo', u'05606': 'Santo Domingo',
            u'05701': 'San Felipe', u'05702': 'Catemu', u'05703': 'Llaillay', u'05704': 'Panquehue', u'05705': 'Putaendo',
            u'05706': 'Santa María', u'05801': 'Quilpué', u'05802': 'Limache', u'05803': 'Olmué', u'05804': 'Villa Alemana',
            u'06101': 'Rancagua', u'06102': 'Codegua', u'06103': 'Coinco', u'06104': 'Coltauco', u'06105': 'Doñihue',
            u'06106': 'Graneros', u'06107': 'Las Cabras', u'06108': 'Machalí', u'06109': 'Malloa', u'06110': 'Mostazal',
            u'06111': 'Olivar', u'06112': 'Peumo', u'06113': 'Pichidegua', u'06114': 'Quinta de Tilcoco', u'06115': 'Rengo',
            u'06116': 'Requínoa', u'06117': 'San Vicente', u'06201': 'Pichilemu', u'06202': 'La Estrella', u'06203': 'Litueche',
            u'06204': 'Marchihue', u'06205': 'Navidad', u'06206': 'Paredones', u'06301': 'San Fernando', u'06302': 'Chépica',
            u'06303': 'Chimbarongo', u'06304': 'Lolol', u'06305': 'Nancagua', u'06306': 'Palmilla', u'06307': 'Peralillo',
            u'06308': 'Placilla', u'06309': 'Pumanque', u'06310': 'Santa Cruz', u'07101': 'Talca', u'07102': 'Constitución',
            u'07103': 'Curepto', u'07104': 'Empedrado', u'07105': 'Maule', u'07106': 'Pelarco', u'07107': 'Pencahue',
            u'07108': 'Río Claro', u'07109': 'San Clemente', u'07110': 'San Rafael', u'07201': 'Cauquenes', u'07202': 'Chanco',
            u'07203': 'Pelluhue', u'07301': 'Curicó', u'07302': 'Hualañé', u'07303': 'Licantén', u'07304': 'Molina',
            u'07305': 'Rauco', u'07306': 'Romeral', u'07307': 'Sagrada Familia', u'07308': 'Teno', u'07309': 'Vichuquén',
            u'07401': 'Linares', u'07402': 'Colbún', u'07403': 'Longaví', u'07404': 'Parral', u'07405': 'Retiro',
            u'07406': 'San Javier', u'07407': 'Villa Alegre', u'07408': 'Yerbas Buenas', u'08101': 'Concepción', u'08102': 'Coronel',
            u'08103': 'Chiguayante', u'08104': 'Florida', u'08105': 'Hualqui', u'08106': 'Lota', u'08107': 'Penco',
            u'08108': 'San Pedro de la Paz', u'08109': 'Santa Juana', u'08110': 'Talcahuano', u'08111': 'Tomé', u'08112': 'Hualpén',
            u'08201': 'Lebu', u'08202': 'Arauco', u'08203': 'Cañete', u'08204': 'Contulmo', u'08205': 'Curanilahue',
            u'08206': 'Los Álamos', u'08207': 'Tirúa', u'08301': 'Los Ángeles', u'08302': 'Antuco', u'08303': 'Cabrero',
            u'08304': 'Laja', u'08305': 'Mulchén', u'08306': 'Nacimiento', u'08307': 'Negrete', u'08308': 'Quilaco',
            u'08309': 'Quilleco', u'08310': 'San Rosendo', u'08311': 'Santa Bárbara', u'08312': 'Tucapel', u'08313': 'Yumbel',
            u'08314': 'Alto Biobío', u'08401': 'Chillán', u'08402': 'Bulnes', u'08403': 'Cobquecura', u'08404': 'Coelemu',
            u'08405': 'Coihueco', u'08406': 'Chillán Viejo', u'08407': 'El Carmen', u'08408': 'Ninhue', u'08409': 'Ñiquén',
            u'08410': 'Pemuco', u'08411': 'Pinto', u'08412': 'Portezuelo', u'08413': 'Quillón', u'08414': 'Quirihue',
            u'08415': 'Ránquil', u'08416': 'San Carlos', u'08417': 'San Fabián', u'08418': 'San Ignacio', u'08419': 'San Nicolás',
            u'08420': 'Treguaco', u'08421': 'Yungay', u'09101': 'Temuco', u'09102': 'Carahue', u'09103': 'Cunco',
            u'09104': 'Curarrehue', u'09105': 'Freire', u'09106': 'Galvarino', u'09107': 'Gorbea', u'09108': 'Lautaro',
            u'09109': 'Loncoche', u'09110': 'Melipeuco', u'09111': 'Nueva Imperial', u'09112': 'Padre las Casas', u'09113': 'Perquenco',
            u'09114': 'Pitrufquén', u'09115': 'Pucón', u'09116': 'Saavedra', u'09117': 'Teodoro Schmidt', u'09118': 'Toltén',
            u'09119': 'Vilcún', u'09120': 'Villarrica', u'09121': 'Cholchol', u'09201': 'Angol', u'09202': 'Collipulli',
            u'09203': 'Curacautín', u'09204': 'Ercilla', u'09205': 'Lonquimay', u'09206': 'Los Sauces', u'09207': 'Lumaco',
            u'09208': 'Purén', u'09209': 'Renaico', u'09210': 'Traiguén', u'09211': 'Victoria', u'10101': 'Puerto Montt',
            u'10102': 'Calbuco', u'10103': 'Cochamó', u'10104': 'Fresia', u'10105': 'Frutillar', u'10106': 'Los Muermos',
            u'10107': 'Llanquihue', u'10108': 'Maullín', u'10109': 'Puerto Varas', u'10201': 'Castro', u'10202': 'Ancud',
            u'10203': 'Chonchi', u'10204': 'Curaco de Vélez', u'10205': 'Dalcahue', u'10206': 'Puqueldón', u'10207': 'Queilén',
            u'10208': 'Quellón', u'10209': 'Quemchi', u'10210': 'Quinchao', u'10301': 'Osorno', u'10302': 'Puerto Octay',
            u'10303': 'Purranque', u'10304': 'Puyehue', u'10305': 'Río Negro', u'10306': 'San Juan de la Costa', u'10307': 'San Pablo',
            u'10401': 'Chaitén', u'10402': 'Futaleufú', u'10403': 'Hualaihué', u'10404': 'Palena', u'11101': 'Coihaique',
            u'11102': 'Lago Verde', u'11201': 'Aisén', u'11202': 'Cisnes', u'11203': 'Guaitecas', u'11301': 'Cochrane',
            u'11302': 'OHiggins', u'11303': 'Tortel', u'11401': 'Chile Chico', u'11402': 'Río Ibáñez', u'12101': 'Punta Arenas',
            u'12102': 'Laguna Blanca', u'12103': 'Río Verde', u'12104': 'San Gregorio', u'12201': 'Cabo de Hornos (Ex Navarino)', 
            u'12202': 'Antártica', u'12301': 'Porvenir', u'12302': 'Primavera', u'12303': 'Timaukel', u'12401': 'Natales',
            u'12402': 'Torres del Paine', u'13101': 'Santiago', u'13102': 'Cerrillos', u'13103': 'Cerro Navia', u'13104': 'Conchalí',
            u'13105': 'El Bosque', u'13106': 'Estación Central', u'13107': 'Huechuraba', u'13108': 'Independencia',
            u'13109': 'La Cisterna', u'13110': 'La Florida', u'13111': 'La Granja', u'13112': 'La Pintana', u'13113': 'La Reina',
            u'13114': 'Las Condes', u'13115': 'Lo Barnechea', u'13116': 'Lo Espejo', u'13117': 'Lo Prado', u'13118': 'Macul',
            u'13119': 'Maipú', u'13120': 'Ñuñoa', u'13121': 'Pedro Aguirre Cerda', u'13122': 'Peñalolén', u'13123': 'Providencia',
            u'13124': 'Pudahuel', u'13125': 'Quilicura', u'13126': 'Quinta Normal', u'13127': 'Recoleta', u'13128': 'Renca',
            u'13129': 'San Joaín', u'13130': 'San Miguel', u'13131': 'San Ramón', u'13132': 'Vitacura', u'13201': 'Puente Alto',
            u'13202': 'Pirque', u'13203': 'San José de Maipo', u'13301': 'Colina', u'13302': 'Lampa', u'13303': 'Tiltil',
            u'13401': 'San Bernardo', u'13402': 'Buin', u'13403': 'Calera de Tango', u'13404': 'Paine', u'13501': 'Melipilla',
            u'13502': 'Alhué', u'13503': 'Curacaví', u'13504': 'María Pinto', u'13505': 'San Pedro', u'13601': 'Talagante',
            u'13602': 'El Monte', u'13603': 'Isla de Maipo', u'13604': 'Padre Hurtado', u'13605': 'Peñaflor', u'14101': 'Valdivia',
            u'14102': 'Corral', u'14103': 'Lanco', u'14104': 'Los Lagos', u'14105': 'Máfil', u'14106': 'Mariquina',
            u'14107': 'Paillaco', u'14108': 'Panguipulli', u'14201': 'La Unión', u'14202': 'Futrono', u'14203': 'Lago Ranco',
            u'14204': 'Río Bueno', u'15101': 'Arica', u'15102': 'Camarones', u'15201': 'Putre', u'15202': 'General Lagos'
        }

        class SerializerCustomer(Serializer):

            class Meta:

                extra_kwargs: Dict[str, Dict[str, Any]] = {
                    'send_email': {
                        'required': False
                    },
                    'send_snail_mail': {
                        'required': False
                    },
                    'default_due_day': {
                        'required': False
                    },
                    'unified_billing': {
                        'required': False
                    },
                    'full_process': {
                        'required': False
                    },
                    'notes': {
                        'required': False
                    },
                    'birth_date': {
                        'required': False
                    },
                    'first_category_activity': {
                        'required': False
                    },
                    'document_type': {
                        'required': False
                    },
                    'technology_kind': {
                        'required': False
                    },
                }

            HOME = 1
            COMPANY = 2
            TYPE_CHOICES = ((HOME, HOME), (COMPANY, COMPANY))

            NETWORK_CABLE = 1
            GPON = 2
            GEPON = 3
            ANTENNA = 4

            TECHNOLOGY_KIND_CHOICES = (
                (NETWORK_CABLE, ("UTP")),
                (GPON, ("GPON")),
                (GEPON, ("GEPON")),
                (ANTENNA, ("antenna")),
            )

            BOLETA = 1
            FACTURA = 2
            NOTA_DE_VENTA = 3

            DOCUMENT_CHOICES = (
                (BOLETA, ("boleta")),
                (FACTURA, ("factura")),
                (NOTA_DE_VENTA, ("nota de venta")),
            )

            send_email = BooleanField(default=False)
            send_snail_mail = BooleanField(default=False)
            rut: str = CharField(max_length=20)
            first_name: str = CharField(max_length=255)
            last_name: str = CharField(max_length=255)
            kind: int = ChoiceField(choices=TYPE_CHOICES, default=1)
            commercial_activity: int = IntegerField(allow_null=True)
            email: str = EmailField()
            street = CharField(max_length=75, default="")
            house_number = CharField(max_length=75, default="")
            apartment_number = CharField(max_length=75, default="")
            tower = CharField(max_length=75, default="")
            location: int = CharField()
            phone: str = CharField(max_length=75)
            default_due_day: int = CharField(default=5)
            unified_billing = BooleanField(default=False)
            full_process = BooleanField(default=True)
            notes: str = CharField(allow_null=True, default="_")
            birth_date: str = DateField(allow_null=True, default=None)
            first_category_activity = BooleanField(default=True)
            document_type: int = ChoiceField(choices=DOCUMENT_CHOICES, default=1)
            plan: int = IntegerField()
            technology_kind: int = ChoiceField(choices=TECHNOLOGY_KIND_CHOICES, default=1)
            operator: int = IntegerField(default=2)

        try:

            instance: LeadletCustomerModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Item error": "Don`t exist instance"
            })

        if instance.customer or instance.is_customer:

            raise ValidationError({
                "Customer error": "Customer has been created"
            })

        validator = SerializerCustomer(data=request.data)

        if validator.is_valid():

            data = validator.validated_data

            commune_name = request.data['commune']
            trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
            commune_name = normalize('NFKC', normalize('NFKD', commune_name).translate(trans_tab))

            for code, commune in COMMUNE_CHOICES.items():
                commune = normalize('NFKC', normalize('NFKD', commune).translate(trans_tab))

                if commune.lower() == commune_name.lower():
                    data['location'] = code

            global_preferences: Dict[str, any] = global_preferences_registry.manager()

            headers: Dict[str, str] = {
                'AUTHORIZATION': global_preferences['matrix__matrix_token']
            }

            response = requests.post(
                url= global_preferences['general__matrix_domain'] + "create_customerservice/",
                data=data,
                headers=headers,
                verify=False,
            )

            if isinstance(response.json(), list):

                raise ValidationError(
                    response.json()
                )

            response: Dict[str, Any] = response.json()
            
            #Get id customer
            instance.customer: int = response['customer']['id']
            instance.status: int = 4
            instance.conversion_date: datetime = datetime.now()
            instance.is_customer: bool = True 
            instance.service: int = response['service']['number']
            if 'address' in request.data: instance.address = request.data['address']
            instance.save()

            try:

                operator_instance = OperatorModel.objects.get(ID=int(request.data['operator']))

                customer_instance = CustomerModel.objects.create(
                    rut=request.data['rut'],
                    operator=operator_instance,
                    creator=request.user,
                    updater=request.user
                )

                if request.data['email']:
                    email_instance = CustomerEmailModel.objects.create(
                    email=request.data['email'],
                    operator= operator_instance
                    )
                    customer_instance.emails.add(email_instance)

                if request.data['secundary_email']:
                    secundary_email_instance = CustomerEmailModel.objects.create(
                    email=request.data['secundary_email'],
                    operator= operator_instance
                    )
                    customer_instance.emails.add(secundary_email_instance)
                    
                if request.data['phone']:
                    phone_instance = CustomerPhoneModel.objects.create(
                    phone=request.data['phone'],
                    operator= operator_instance
                    )
                    customer_instance.phones.add(phone_instance)

                if request.data['secundary_phone']:
                    secundary_phone_instance = CustomerPhoneModel.objects.create(
                    phone=request.data['secundary_phone'],
                    operator= operator_instance
                    )
                    customer_instance.phones.add(secundary_phone_instance)
                    

            except:

                pass

        else:

            raise ValidationError({
                "Error": validator.errors
            })

        return JsonResponse(response, safe=False)

class LeadletContactView(BaseViewSet):

    queryset: QuerySet = LeadletContactModel.objects.filter(deleted=False)
    serializer_class = LeadletContactSerializer

class CustomerTableView(BaseViewSet):

    """

        Class define ViewSet of CustomerTablesModel.
    
    """

    queryset: BaseModel = CustomerTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = CustomerTablesSerializer
    
    @method_decorator(permission_required('customers.add_customertablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_customertablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_customertablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.delete_customertablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('formercustomersCustomers.view_customertablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {

            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "RUT":{
                    "type":"str",
                    "name":"rut",
                },
            },
            "columns":  {
                'creator': "Creador",
                'updater': "Último en actualizar",
                'created': "Fecha de creación",
                'updated': "Fecha de actualización",
                "rut": "Rut",
                "phones": "Teléfonos",
                "emails": "Correos",
                "web": "Web",
                "facebook": "Facebook",
                "instagram": "Instagram",
                "twitter": "Twitter"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('customers.view_customertablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class CustomerTableView
            :type self: CustomerTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(CustomerTablesDatatableBase(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('customers.view_customertablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class CustomerTableView
            :type self: CustomerTableView
            :param request: data of request
            :type request: request
        
        """

        return CustomerTablesDatatableBase(request).get_data()

class LeadTableView(BaseViewSet):

    """

        Class define ViewSet of LeadTablesModel.
    
    """

    queryset: BaseModel = LeadTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = LeadTablesSerializer
    
    @method_decorator(permission_required('customers.add_leadtablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_leadtablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_leadtablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.delete_leadtablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('formercustomersCustomers.view_leadtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {

            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Estado":{
                    "type":"choices",
                    "name":"status",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "Lead",
                            1: "Prospecto",
                        }
                    }
                },
                "Compañía de Origen":{
                    "type":"choices",
                    "name":"origin_company",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "ENTEL",
                            1: "GTD",
                            2: "MOVISTAR",
                            3: "CLARO",
                            4: "VTR",
                            5: "NINGUNA"
                        }
                    }
                },
            },
            "columns":  {
                'creator': "Creador",
                'created': "Fecha de creación",
                "name": "Nombre",
                "primary_email": "Correo",
                "primary_phone": "Teléfono",
                "origin_company": "Compañía de Origen",
                "status": "Estado",
                "str_address": "Dirección",
                "commentaries": "Comentarios"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('customers.view_leadtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadTableView
            :type self: LeadTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(LeadTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('customers.view_leadtablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadTableView
            :type self: LeadTableView
            :param request: data of request
            :type request: request
        
        """

        return LeadTablesDatatable(request).get_data()

class LeadletTableView(BaseViewSet):

    """

        Class define ViewSet of LeadletTablesModel.
    
    """

    queryset: BaseModel = LeadletTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = LeadletTablesSerializer
    
    @method_decorator(permission_required('customers.add_leadlettablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_leadlettablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_leadlettablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('customers.delete_leadlettablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('customers.view_leadlettablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {

            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "RUT":{
                    "type":"str",
                    "name":"rut",
                },
                "Es cliente":{
                    "type":"bool",
                    "name":"is_customer",
                },
                "Compañía de Origen":{
                    "type":"choices",
                    "name":"origin_company",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "ENTEL",
                            1: "GTD",
                            2: "MOVISTAR",
                            3: "CLARO",
                            4: "VTR",
                            5: "NINGUNA"
                        }
                    }
                },
            },
            "columns":  {
                'creator': "Creador",
                'created': "Fecha de creación",
                "name": "Nombre",
                "primary_email": "Correo",
                "primary_phone": "Teléfono",
                "origin_company": "Compañía de Origen",
                "str_address": "Dirección",
                "commentaries": "Comentarios"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('customers.view_leadlettablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadletTableView
            :type self: LeadletTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(LeadletTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('customers.view_leadlettablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class LeadletTableView
            :type self: LeadletTableView
            :param request: data of request
            :type request: request
        
        """

        return LeadletTablesDatatable(request).get_data()

class LeadChannelView(BaseViewSet):

    """

        Class define ViewSet of LeadChannelModel.
    
    """

    queryset: BaseModel = LeadChannelModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = LeadChannelSerializer
    
    @method_decorator(permission_required('customers.add_leadchannelmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_leadchannelmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

class LeadletChannelView(BaseViewSet):

    """

        Class define ViewSet of LeadletChannelModel.
    
    """

    queryset: BaseModel = LeadletChannelModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = LeadletChannelSerializer
    
    @method_decorator(permission_required('customers.add_leadletchannelmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('customers.change_leadletchannelmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)