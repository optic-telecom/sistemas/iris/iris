from datetime import datetime
import random
import requests
from typing import Any, Dict, List, Union
from slack import WebClient

from common.serializers import BaseSerializer
from rest_framework import serializers
from rest_framework.serializers import ValidationError
from dynamic_preferences.registries import global_preferences_registry
from common.models import BaseModel
from communications.models import ChatModel
from users.models import UserSlackIDModel
from .models import (CustomerEmailModel, CustomerModel, CustomerPhoneModel,
                     LeadCustomerModel, LeadletContactModel,
                     LeadletCustomerModel, CustomerTablesModel, FeasibilityTablesBase, LeadTablesModel, LeadletTablesModel,
                     LeadChannelModel, LeadletChannelModel)


class LeadletContactSerializer(BaseSerializer):

    class Meta:
        model = LeadletContactModel
        fields: Union[List[str], str] = '__all__'
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class CustomerSerializer(BaseSerializer):

    def to_representation(self, instance):

        data: Dict[str, Any] = super().to_representation(instance)
        
        phones: List[str] = [ phone for phone in CustomerPhoneModel.objects.filter(ID__in=data['phones']).values_list('phone', flat=True) ]
        emails: List[str] = [ email for email in CustomerEmailModel.objects.filter(ID__in=data['emails']).values_list('email', flat=True) ]

        data['phones']: List[str] = phones
        data['emails']: List[str] = emails

        return data

    class Meta:

        model = CustomerModel
        fields: Union[List[str], str] = '__all__'
        read_only_fields: Union[List[str], str] = ['phones', 'emails']

        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'phones': {
                'required':False,
            },
            'emails': {
                'required': False,
            },
        }

class LeadCustomerSerializer(BaseSerializer):

    def create(self, validated_data: Dict[str, Any]) -> LeadCustomerModel:

        """
        
            This funtion validate and create instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        def slack_message(instance):

            global_preferences: Dict[str] = global_preferences_registry.manager()

            pulso_token: str = global_preferences['pulso__pulso_check_token']
            pulso_url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility-iris/'

            _random: int = random.randint(5000, 5000000)

            street_location = requests.post(
                url=f"{pulso_url}?no_cache={_random}",
                json={
                    "street_location":[instance.street_location],
                },
                headers={
                    'Authorization':pulso_token
                },
                verify=False,
            ).json()

            address = street_location[0]['street_location'] if len(street_location) > 0 else ''

            token: str = instance.operator.slack_token

            client = WebClient(token=token)

            channel_query: str = LeadChannelModel.objects.filter(operator=instance.operator)
            if len(channel_query) == 0:
                return None
            
            channel: str = channel_query.first().channel

            user_uid_query: str = UserSlackIDModel.objects.filter(user=instance.creator,operator=instance.operator)
            user_uid = "<@" + str(user_uid_query[0].uid) + ">" if len(user_uid_query) > 0 else ''

            creator = instance.creator if instance.creator.username != "api@api.com" else "Desde la página web se"
            blocks = [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f"{creator} {user_uid} ha creado un lead. ID: {str(instance.ID)}"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*Nombre*: " + instance.name + "\n" + 
                        "*Teléfono*: +" + str(instance.primary_phone.country_code) + " " + str(instance.primary_phone.national_number) + "\n" +
                        "*Correo*: " + instance.primary_email + "\n" +
                        "*Dirección*" + address
                    }
                }
            ]

            result = client.chat_postMessage(
                token=token,
                channel=channel,
                blocks=blocks,
                text=f"{creator} {user_uid} ha creado un lead. ID: {str(instance.ID)}"
            )

            return True

        result = super().create(validated_data)
        chatInstance = ChatModel.objects.create(operator=result.operator)
        result.chat = chatInstance.ID
        result.save()
        slack_message(result)
        return result

    class Meta:
        model = LeadCustomerModel
        fields: Union[List[str], str] = '__all__'
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False,
            },
            'updater': {
                'required': False,
            },
            'status': {
                'read_only': True,
            },
            'customer': {
                'read_only': True,
            },
            'plan': {
                'read_only': True,
            },
            'conversion_date': {
                'read_only': True,
            },
            'seller': {
                'read_only': True,
            },
            'secundary_email': {
                'required': False,
            },
            'secundary_phone': {
                'required': False,
            },
            'street_location': {
                'required': True,
            },
        }

class LeadletCustomerSerializer(BaseSerializer):

    def create(self, validated_data: Dict[str, Any]) -> LeadletCustomerModel:

        """
        
            This funtion validate and create instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        def slack_message(instance):

            global_preferences: Dict[str] = global_preferences_registry.manager()

            pulso_token: str = global_preferences['pulso__pulso_check_token']
            pulso_url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility-iris/'

            _random: int = random.randint(5000, 5000000)

            street_location = requests.post(
                url=f"{pulso_url}?no_cache={_random}",
                json={
                    "street_location":[instance.street_location],
                },
                headers={
                    'Authorization':pulso_token
                },
                verify=False,
            ).json()

            address = street_location[0]['street_location'] if len(street_location) > 0 else ''

            token: str = instance.operator.slack_token

            client = WebClient(token=token)

            channel_query: str = LeadletChannelModel.objects.filter(operator=instance.operator)
            if len(channel_query) == 0:
                return None
            
            channel: str = channel_query.first().channel

            user_uid_query: str = UserSlackIDModel.objects.filter(user=instance.creator,operator=instance.operator)
            user_uid = "<@" + str(user_uid_query[0].uid) + ">" if len(user_uid_query) > 0 else ''

            creator = instance.creator if instance.creator.username != "api@api.com" else "Desde la página web se"

            blocks = [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f"{creator} {user_uid} ha creado un prospecto. ID: {str(instance.ID)}"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*Nombre*: " + instance.name + "\n" + 
                        "*Teléfono*: +" + str(instance.primary_phone.country_code) + " " + str(instance.primary_phone.national_number) + "\n" +
                        "*Correo*: " + instance.primary_email + "\n" +
                        "*Dirección*" + address
                    }
                }
            ]

            result = client.chat_postMessage(
                token=token,
                channel=channel,
                blocks=blocks,
                text=f"{creator} {user_uid} ha creado un prospecto. ID: {str(instance.ID)}"
            )

            return True

        result = super().create(validated_data)
        chatInstance = ChatModel.objects.create(operator=result.operator)
        result.chat = chatInstance.ID
        result.save()
        slack_message(result)
        return result

    class Meta:
        model = LeadletCustomerModel
        fields: Union[List[str], str] = '__all__'
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'read_only': True,
            },
            'updater': {
                'read_only': True,
            },
            'customer': {
                'read_only': True,
            },
            'conversion_date': {
                'read_only': True,
            },
            'seller': {
                'read_only': True,
            },
            'lead': {
                'read_only': True,
            },
            'is_customer': {
                'read_only': True,
            }
        }

class CustomerTablesSerializer(BaseSerializer):

    """

        Class define serializer of RemoveEquipamentView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = FeasibilityTablesBase
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: FeasibilityTablesBase, validated_data: Dict[str, Any]) -> FeasibilityTablesBase:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class LeadTablesSerializer(BaseSerializer):

    """

        Class define serializer of RemoveEquipamentView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = LeadTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: LeadTablesModel, validated_data: Dict[str, Any]) -> LeadTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class LeadletTablesSerializer(BaseSerializer):

    """

        Class define serializer of RemoveEquipamentView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = LeadletTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: LeadletTablesModel, validated_data: Dict[str, Any]) -> LeadletTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class LeadChannelSerializer(BaseSerializer):

    
    """

        Class define serializer of LeadChannelView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = LeadChannelModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def create(self, validated_data: Dict[str, Any]) -> LeadChannelModel:

        same_op_channel = LeadChannelModel.objects.filter(operator = validated_data['operator'])

        if len(same_op_channel) > 0:

            raise ValidationError({
                    'Error same operator': str(same_op_channel[0].ID)
                })

        return super().create(validated_data)

class LeadletChannelSerializer(BaseSerializer):

    
    """

        Class define serializer of LeadletChannelView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = LeadletChannelModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def create(self, validated_data: Dict[str, Any]) -> LeadletChannelModel:

        same_op_channel = LeadletChannelModel.objects.filter(operator = validated_data['operator'])

        if len(same_op_channel) > 0:

            raise ValidationError({
                    'Error same operator': str(same_op_channel[0].ID)
                })

        return super().create(validated_data)