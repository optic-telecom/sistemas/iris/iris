from typing import List

from django.contrib import admin

from .models import CustomerModel, LeadCustomerModel, LeadletCustomerModel, CustomerTablesModel, LeadTablesModel, LeadletTablesModel


class CustomerAdmin(admin.ModelAdmin):

    fields: List[str] = ["rut", "phones", "emails", "web", "facebook", "instagram", "twitter","operator"]
    list_display: List[str] = ["ID","rut", "web", "facebook", "instagram", "twitter","operator"]
    search_fields: List[str] = ["ID","rut", "phones", "emails", "web", "facebook", "instagram", "twitter"]
    ordering: List[str] = ["rut", "phones", "emails", "web", "facebook", "instagram", "twitter"]

class LeadletCustomerAdmin(admin.ModelAdmin):

    fields: List[str] = ["street_location", "name", "primary_email", "primary_phone", "secundary_phone", "origin_company", "rut", "lead","operator"]
    list_display: List[str] = ["ID","street_location", "name", "primary_email", "primary_phone", "origin_company", "rut","operator"]
    search_fields: List[str] = ["ID","name", "primary_email", "primary_phone", "origin_company", "rut"]
    ordering: List[str] = ["name", "primary_email", "primary_phone", "origin_company", "rut"]

class LeadCustomerAdmin(admin.ModelAdmin):

    fields: List[str] = ["street_location", "name", "primary_email", "primary_phone", "secundary_phone", "origin_company", "status","operator"]
    list_display: List[str] = ["ID","street_location", "name", "primary_email", "primary_phone", "origin_company", "status","operator"]
    search_fields: List[str] = ["ID","name", "primary_email", "primary_phone", "origin_company"]
    ordering: List[str] = ["name", "primary_email", "primary_phone", "origin_company", "status"]

class FeasibilityTablesBaseAdmin(admin.ModelAdmin):

    fields = ['name', 'columns', 'last_time', "last_time_type", "operator", "create_me", "assigned_to_me", "filters" ]

    #list of fields to display in django admin
    list_display = ['name']


    #if you want django admin to show the search bar, just add this line
    search_fields = ['name']

    #to define model data list ordering
    ordering = ['name']

class CustomerTablesAdmin(admin.ModelAdmin):
    pass

class LeadTablesAdmin(admin.ModelAdmin):
    list_display = ['ID','name','operator']
    search_fields = ['name','ID']
    pass

class LeadletTablesAdmin(admin.ModelAdmin):
    list_display = ['ID','name','operator']
    search_fields = ['ID','name']
    pass

admin.site.register(CustomerModel, CustomerAdmin)
admin.site.register(LeadletCustomerModel,LeadletCustomerAdmin)
admin.site.register(LeadCustomerModel,LeadCustomerAdmin)

admin.site.register(CustomerTablesModel,CustomerTablesAdmin)
admin.site.register(LeadTablesModel,LeadTablesAdmin)
admin.site.register(LeadletTablesModel,LeadletTablesAdmin)