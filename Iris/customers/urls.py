from rest_framework.routers import DefaultRouter

from .views import (CustomerView, LeadCustomerView, LeadletContactView,
                    LeadletCustomerView, CustomerTableView, LeadTableView, LeadletTableView,
                    LeadChannelView, LeadletChannelView)

router = DefaultRouter()

router.register(r'customer', CustomerView, basename='customer')

router.register(r'lead_customer', LeadCustomerView, basename='lead_customer')
router.register(r'leadlet_customer', LeadletCustomerView, basename='leadlet_customer')
router.register(r'leadlet_contact', LeadletContactView, basename='leadlet_contact')
router.register(r'customer_tables', CustomerTableView, basename='customer_tables')
router.register(r'lead_tables', LeadTableView, basename='lead_tables')
router.register(r'leadlet_tables', LeadletTableView, basename='leadlet_tables')
router.register(r'lead_channel', LeadChannelView, basename='lead_channel')
router.register(r'leadlet_channel', LeadletChannelView, basename='leadlet_channel')

urlpatterns = router.urls
