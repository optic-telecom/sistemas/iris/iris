import requests
import calendar
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from rest_framework.serializers import ValidationError
from dynamic_preferences.registries import global_preferences_registry
from common.models import BaseModel
from communications.models import ChatModel
from common.utilitarian import get_token, matrix_url
from typing import Dict, Any, List, Tuple, Union, Set
from datetime import datetime
from django.core.validators import URLValidator
from django.contrib.postgres.fields import JSONField
from django.utils.translation import gettext_lazy as _

def validate_instance(url: str, id: int):

    """
    
        This function, validate if exist instance
    
    """

    global_preferences: Dict[str, Any] = global_preferences_registry.manager()
    base_url: str = global_preferences['general__iris_backend_domain']

    response = requests.get(
        url=f'{base_url}{url}{id}/',
        verify=False,
    )

    if response.status_code != 200:

        raise ValidationError({
            "Error item": "Don`t exist item"
        })

def validate_domain(url: str, domain: str, name: str):

    """
        
        This function, validate domain name
    
    """

    try:

        URLValidator(url)

        if not domain in url:

            raise "Error"

    except:

        raise ValidationError({
            "Facebook error": f"Invalid {name} url"
        })


class CustomerModel(BaseModel):

    def validate_rut(value: str):

        """
        
            This function, validate customer rut
        
        """

        # url: str = matrix_url('customers') + f'?fields=id&rut={value}'
        # token: str = get_token()
        
        # response = requests.get(
        #     url=url,
        #     headers={
        #         'Authorization': f'JWT {token}'
        #     },
        #     verify=False,
        # ).json()

        # if not response:

        #     raise ValidationError({
        #         "Don't exist": "There is no customer with that rut"
        #     })

        return True

    def validate_facebook(value: str):

        """
        
            This function, validate facebook domain name
        
        """

        validate_domain(value, 'facebook.com', 'Facebook')

    def validate_instagram(value: str):

        """
        
            This function, validate instagram domain name
        
        """

        validate_domain(value, 'instagram.com', 'Instagram')

    def validate_twitter(value: str):

        """
        
            This function, validate twitter domain name
        
        """

        validate_domain(value, 'twitter.com', 'Twitter')

    rut: str = models.CharField(max_length=30, null=False, blank=False, validators=[validate_rut])
    phones: list = models.ManyToManyField('CustomerPhoneModel')
    emails: list = models.ManyToManyField('CustomerEmailModel')
    web: str = models.URLField(null=True, blank=True)
    facebook: str = models.URLField(null=True, blank=True, validators=[validate_facebook])
    instagram: str = models.URLField(null=True, blank=True, validators=[validate_instagram])
    twitter: str = models.URLField(null=True, blank=True, validators=[validate_twitter])

    class Meta:

        unique_together = ('rut','operator')

class CustomerPhoneModel(BaseModel):

    phone: str = PhoneNumberField(null=False, blank=False)

class CustomerEmailModel(BaseModel):

    email: str = models.EmailField(null=False, blank=False)

class FeasibilityCustomerBase(BaseModel):

    CURRENT_COMPANY_CHOICES: Tuple[Tuple[int, str]] = (
        (0, "ENTEL"),
        (1, "GTD"),
        (2, "MOVISTAR"),
        (3, "CLARO"),
        (4, "VTR"),
        (5, "NINGUNA"),
    )

    address: int = models.PositiveIntegerField(null=True)
    street_location: int = models.PositiveIntegerField(null=False)
    name: str = models.CharField(max_length=100, null=False, blank=False)
    primary_email: str = models.EmailField(blank=False, null=False)
    primary_phone: str = PhoneNumberField(blank=False, null=False)
    secundary_phone: str = PhoneNumberField(blank=True, null=True)
    origin_company: int = models.PositiveIntegerField(choices=CURRENT_COMPANY_CHOICES, default=5)
    chat =  models.IntegerField(null=True)

    class Meta:

        abstract: bool = True

class LeadCustomerModel(FeasibilityCustomerBase):

    def clean(self):

        global_preferences: Dict[str, Any] = global_preferences_registry.manager()

        #Get headers and token
        headers: Dict[str, str] = {
            'AUTHORIZATION': global_preferences['matrix__matrix_token']
        }


        url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility/'

        params: Dict[str, Union[str, int]] = {
            'street_location':self.street_location,
            'operator':self.operator.ID,
        }

        if self.address:

            params['address'] = self.address

        #Check feasibility
        response_feasibility: Dict[str, str] = requests.get(
            url=url,
            headers=headers,
            verify=False,
            params=params,
        )

        response_feasibility_data: Dict[str, str] = response_feasibility.json()
        response_feasibility_status: int = response_feasibility.status_code

        if response_feasibility_status != 200 or response_feasibility_data.get('error', ''):

            raise ValidationError({
                "Error address value": "Must be an existing address",
            })

        if response_feasibility_data.get('factibility', 'SI') == 'SI':

            raise ValidationError({
                'Error address feasibility': "must be a leadlet"
            })

    STATUS_CHOICES: Tuple[Tuple[int, str]] = (
        (0, "LEAD"),
        (1, "LEAFLET"),
    )

    status: int = models.PositiveIntegerField(choices=STATUS_CHOICES, default=0)

class LeadletCustomerModel(FeasibilityCustomerBase):

    def clean(self):

        global_preferences: Dict[str, Any] = global_preferences_registry.manager()

        #Get headers and token
        headers: Dict[str, str] = {
            'AUTHORIZATION': global_preferences['matrix__matrix_token']
        }
        
        url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility/'


        params: Dict[str, Union[str, int]] = {
            'street_location':self.street_location,
            'operator':self.operator.ID,
        }

        if self.address:

            params['address'] = self.address

        #Check feasibility
        response_feasibility: Dict[str, str] = requests.get(
            url=url,
            headers=headers,
            verify=False,
            params=params,
        )

        response_feasibility_data: Dict[str, str] = response_feasibility.json()
        response_feasibility_status: int = response_feasibility.status_code

        if response_feasibility_status != 200 or response_feasibility_data.get('error', ''):

            raise ValidationError({
                "Error address value": "Must be an existing address",
            })

        if response_feasibility_data.get('factibility', 'NO') != 'SI':

            raise ValidationError({
                'Error address feasibility': "must have feasibility"
            })

        if self.customer:

            self.is_customer = True

    def validation_sellers(value: int):

        validate_instance("api/v1/sellers/", value)

    def validate_customer(value: int):

        validate_instance("api/v1/customers/", value)

    rut: str = models.CharField(max_length=30, null=True)
    service: List[int] = models.PositiveIntegerField(null=True)
    customer: int = models.PositiveIntegerField(null=True, validators=[validate_customer])
    conversion_date = models.DateField(null=True)
    seller: int = models.PositiveIntegerField(null=True, validators=[validation_sellers])
    is_customer: bool = models.BooleanField(default=False)
    lead: int = models.ForeignKey(
        "LeadCustomerModel",
        on_delete=models.PROTECT,
        null=True,
    )

class LeadletContactModel(BaseModel):

    CHANNEL_COMMUNICATION_CHOICES: Tuple[Tuple[int, str]] = (
        (1, "WHATSAPP"),
        (2, "PHONE"),
        (3, "EMAIL"),
        (4, "FACEBOOK"),
        (5, "INSTAGRAM"),
    )

    leadted_customer: int = models.ForeignKey(
        "LeadletCustomerModel",
        on_delete=models.PROTECT,
    )
    contact_type: int = models.PositiveIntegerField(choices=CHANNEL_COMMUNICATION_CHOICES)
    message: str = models.TextField()

class FeasibilityTablesBase(BaseModel):

    """
                    
    Parent table for leads, leadlets and customers.

    """
    

    FIELDS_FILTERS: Dict[str, str] = {
        "creator__pk":"choices",
        "created":"datetime",
        "rut":"str",
    }

    VALID_COLUMNS : Set[str] =  set( [
            'creator', 'updater', 'created', 'updated', 'rut', 
            'phones', 'emails', 'web', 'facebook', 'instagram', 'twitter'
    ] )

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "creator__pk":"choices",
                    "created":"datetime",
                    "rut":"str",
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = self.FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        invalid_columns: List[str] = list( set( self.columns ) - self.VALID_COLUMNS )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

    # class Meta:

    #     abstract: bool = True


class CustomerTablesModel(FeasibilityTablesBase):

    FIELDS_FILTERS: Dict[str, str] = {
        "creator__pk":"choices",
        "created":"datetime",
        "rut":"str",
    }

    VALID_COLUMNS : Set[str] =  set( [
            'creator', 'updater', 'created', 'updated', 'rut', 
            'phones', 'emails', 'web', 'facebook', 'instagram', 'twitter'
    ] )

class LeadTablesModel(FeasibilityTablesBase):

    FIELDS_FILTERS: Dict[str, str] = {
        "creator__pk":"choices",
        "created":"datetime",
        "street_location":"location",
        "name": "str",
        "origin_company": "choices",
        "status": "choices"
    }

    VALID_COLUMNS : Set[str] =  set( [
            'creator', 'created', 'name', 'primary_email',
            'primary_phone', 'origin_company',"status", "str_address", 'commentaries'
    ] )

class LeadletTablesModel(FeasibilityTablesBase):

    FIELDS_FILTERS: Dict[str, str] = {
        "creator__pk":"choices",
        "created":"datetime",
        "name": "str",
        "origin_company": "choices",
        "is_customer": "bool"
    }

    VALID_COLUMNS : Set[str] =  set( [
            'creator', 'created', 'name', 'primary_email',
            'primary_phone', 'origin_company', "str_address", 'commentaries'
    ] )

class LeadChannelModel(BaseModel):
    
    """

        Class define model of LeadCustomer Channel.
    
    """

    
    #Slack channel to send notifications
    channel: str = models.TextField(blank=False, null=False, default='')

class LeadletChannelModel(BaseModel):
    
    """

        Class define model of LeadletCustomer Channel.
    
    """

    
    #Slack channel to send notifications
    channel: str = models.TextField(blank=False, null=False, default='')