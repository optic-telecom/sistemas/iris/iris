import random
from datetime import date, datetime, timedelta
from typing import Any, Dict, List, Union

import pytz
import requests
from common.dataTables import BaseDatatables
from common.models import BaseModel
from common.utilitarian import get_token, matrix_url
from dateutil.relativedelta import relativedelta
from django.db.models import Q, QuerySet
from django.http import JsonResponse
from django_datatables_view.base_datatable_view import BaseDatatableView
from dynamic_preferences.registries import global_preferences_registry

from .models import CustomerModel, LeadCustomerModel, LeadletCustomerModel, CustomerTablesModel, LeadTablesModel, LeadletTablesModel


class CustomerDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "rut", "created"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "rut":"str",
        "emails":"str",
        "phones":"str",
    }

    model: BaseModel = CustomerModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            if search.isdigit():

                qs: QuerySet = qs.filter( ID=int(search) )
            
            else:
                
                qs: QuerySet = qs.filter( 
                    Q(rut__icontains=search) | 
                    Q(phones__phone__icontains=search) |
                    Q(emails__email__icontains=search) |
                    Q(web__icontains=search) |
                    Q(facebook__icontains=search) |
                    Q(instagram__icontains=search) |
                    Q(twitter__icontains=search)
                ).distinct()


        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "ID": instance.ID,
            "created": instance.created,
            "rut": instance.rut,
            "phones": [ phone for phone in instance.phones.values_list('phone', flat=True) ],
            "emails": [ email for email in instance.emails.values_list('email', flat=True) ],
            "web": instance.web,
            "facebook": instance.facebook,
            "instagram": instance.instagram,
            "twitter": instance.twitter,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":800,
        }

        data_struct["fields"] = {
            "ID": "int",
            "created": "datetime",
            "rut": "str",
            "phones": "list",
            "emails": "list",
        }

        data_struct["columns"] = {
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Rut":{
                "field": "rut",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Telefonos":{
                "field": "phones",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Correos":{
                "field": "emails",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":'right'
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 6,
                "width":100,
                "fixed":'right'
            },
        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Rut":{
                "type":"str",
                "name":"rut"
            },

        }

        return data_struct


class LeadCustomerDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "created", "creator", "name", "updated", "updater", "primary_email", "primary_phone", "origin_company", "status"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
        "primary_email":"str",
        "primary_phone":"str",
        "origin_company":"choices",
        "status":"choices",
        "street_location":"location",
    }
    FIELDS_EXCEL: Dict[str, Dict[str, str]] = {
        "A":"Estado",
        "B":"Compañia de origen",
        "C":"Telefono primario",
        "D":"Telefono secundario",
        "E":"Correo primario",
        "F":"Correo secundario",
        "G":"Nombre",
        "H":"Dirección",
    }

    model: BaseModel = LeadCustomerModel

    def get_initial_params(self):

        super().get_initial_params()

        self.LOCATION_FILTER : List[List[Union[str, int, bool]]] = list(filter(lambda x: 'street_location' == x[0], self.REQUEST_FILTERS))
        self.REQUEST_FILTERS : List[List[Union[str, int, bool]]] = list(filter(lambda x: 'street_location' != x[0], self.REQUEST_FILTERS))

    def get_filtered_queryset(self) -> QuerySet:

        instance_tables_lead: QuerySet = LeadTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_lead.filters

        result : QuerySet = super().get_filtered_queryset()

        global_preferences: Dict[str, Any] = global_preferences_registry.manager()

        pulso_token: str = global_preferences['pulso__pulso_check_token']
        pulso_url: str = global_preferences['pulso__domain_url']

        #Created to request user
        if instance_tables_lead.create_me:

            result: QuerySet = result.filter(creator=self.request.user)

        #Time interval
        if instance_tables_lead.last_time_type and instance_tables_lead.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            result: QuerySet = result.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_lead.last_time_type] * instance_tables_lead.last_time) )

        if self.LOCATION_FILTER:

            street_location_pulso : List[int] = list( set( [ street_location[0] for street_location in result.values_list('street_location') ] ) )

            validated_locations: List[int] = []
            excluded_locations: List[int] = []

            for _filter in self.LOCATION_FILTER:

                location: str = _filter[2]
                _type: str = location.split(':')[0]
                location_search: str = int(location.split(':')[1])

                result_locations : List[int] = []

                if _type != 'streetLocation':
                    
                    _random: int = random.randint(5000, 5000000)
                    
                    response_locations = requests.post(
                        url=f"{pulso_url}/api/v1/locations-iris/?no_cache={_random}",
                        json={
                            "tipo":_type,
                            "location":location_search,
                            "locations":street_location_pulso,
                        },
                        headers={
                            'Authorization':pulso_token
                        },
                        verify=False,                        
                    )

                    result_locations : List[int] = response_locations.json()

                else:

                    result_locations : List[int] =  result_locations + [location_search]

                if _filter[1] == "equal":

                    validated_locations: List[int] = validated_locations + result_locations

                else:

                    excluded_locations: List[int] = excluded_locations + result_locations
        
            result: QuerySet = result.filter(street_location__in= list( set(validated_locations) - set(excluded_locations) ) )

        return result

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """
        
        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )
        qs_size = len(qs)
        
        if qs:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]

            global_preferences: Dict[str, Any] = global_preferences_registry.manager()

            pulso_token: str = global_preferences['pulso__pulso_check_token']
            pulso_url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility-iris/'

            _random: int = random.randint(5000, 5000000)
            street_locations: List[int] = list( set( [ street.street_location for street in qs ] ) )
            street_locations = requests.post(
                url=f"{pulso_url}?no_cache={_random}",
                json={
                    "street_location":street_locations,
                },
                headers={
                    'Authorization':pulso_token
                },
                verify=False,
            )
            if street_locations.status_code != 500:
                street_locations = street_locations.json()
                self.street_locations: Dict[int, str] = { location['street_location_id']:location['street_location'] for location in street_locations}
            else:
                self.street_locations = {}
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )

        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            if search.isdigit():

                qs: QuerySet = qs.filter( ID=int(search) )
        
            else:

                qs: QuerySet = qs.filter( 
                    Q(name__icontains=search) |
                    Q(primary_email__icontains=search) |
                    Q(primary_phone__icontains=search) |
                    Q(secundary_phone__icontains=search)
                )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username if instance.creator.username != "api@api.com" else "Página Web",
            "name": instance.name,
            "primary_email": instance.primary_email,
            "primary_phone": str(instance.primary_phone),
            "origin_company": instance.get_origin_company_display(),
            "status": instance.get_status_display(),
            "str_address": self.street_locations.get(instance.street_location," "),
            "chat": instance.chat
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":1500,
            "download": True,
        }

        data_struct["fields"] = {
            "ID": "int",
            "created": "datetime",
            "creator": "str",
            "name": "str",
            "primary_email": "str",
            "primary_phone": "str",
            "origin_company": "str",
            "status": "str",
            "str_address":"str",
        }

        columns = {
            'creator':{
                "sortable": True,
                "name":"Creador",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Fecha de Creación",
                "width":100,
                "fixed":None
            },
            'updater':{
                "sortable": True,
                "name":"Actualizado",
                "width":100,
                "fixed":None
            },
            'updated':{
                "sortable": True,
                "name":"Última actualización",
                "width":100,
                "fixed":None
            },
            'str_address': {
                "sortable": False,
                "name":"Dirección",
                "width":300,
                "fixed":None
            },
            "name":{
                "sortable": True,
                "name":"Nombre",
                "width":150,
                "fixed":None
            },
            "primary_phone":{
                "sortable": True,
                "name":"Teléfono",
                "width":100,
                "fixed":None
            },
            "primary_email":{
                "sortable": True,
                "name":"Correo",
                "width":100,
                "fixed":None
            },
            "origin_company":{
                "sortable": True,
                "name":"Compañía de Origen",
                "width":100,
                "fixed":None
            },
            "status":{
                "sortable": True,
                "name":"Estado",
                "width":100,
                "fixed":None
            },
            "commentaries":{
                "sortable": True,
                "name":"Comentarios",
                "width":100,
                "fixed":None
            },
        }

        instance_tables_lead: QuerySet = LeadTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_lead.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1

        data_struct_columns["Convertir"] = {
            "field": "custom_to_customer",
            "sortable": False,
            "visible": True,
            "position": position,
            "width":100,
            "fixed":None
        }

        data_struct_columns["Modificar"] = {
            "field": "custom_change",
            "sortable": False,
            "visible": True,
            "position": position + 1,
            "width":100,
            "fixed":'right'
        }

        data_struct["columns"] = data_struct_columns

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Telefono":{
                "type":"str",
                "name":"primary_phone"
            },
            "Correo":{
                "type":"str",
                "name":"primary_email"
            },
            "Compañia de origen":{
                "type":"choices",
                "name":"origin_company",
                "type_choices": "choices",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:"ENTEL",
                        1:"GTD",
                        2:"MOVISTAR",
                        3:"CLARO",
                        4:"VTR",
                        5:"NINGUNA",
                    }
                }
            },
            "Estado":{
                "type":"choices",
                "name":"status",
                "type_choices": "choices",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:"LEAD",
                        1:"LEAFLET",
                    }
                }
            },
            "Dirección":{
                "type":"location",
                "name":"street_location",
            },
        }

        return data_struct

    def get_qs_to_data(self, qs: QuerySet) -> List[Dict[str, Any]]:

        """

            This function, process data

        """

        global_preferences: Dict[str, Any] = global_preferences_registry.manager()

        pulso_token: str = global_preferences['pulso__pulso_check_token']
        pulso_url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility-iris/'

        _random: int = random.randint(5000, 5000000)
        street_locations: List[int] = list( set( [ street['street_location'] for street in qs.values('street_location') ] ) )

        street_locations = requests.post(
            url=f"{pulso_url}?no_cache={_random}",
            json={
                "street_location":street_locations,
            },
            headers={
                'Authorization':pulso_token
            },
            verify=False,
        ).json()

        street_locations: Dict[int, str] = { location['street_location_id']:location['street_location'] for location in street_locations}

        def get_lead_data(instance: LeadCustomerModel) -> Dict[str, Any]:

            return [
                instance.get_status_display(),
                instance.get_origin_company_display(),
                str(instance.primary_phone),
                str(instance.secundary_phone),
                instance.primary_email,
                instance.name,
                street_locations[instance.street_location]
            ]

        return list( map(get_lead_data, qs) )


class LeadletCustomerDatatableBase(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "created", "creator", "name", "primary_email", "primary_phone", "origin_company", "is_customer"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
        "primary_email":"str",
        "primary_phone":"str",
        "origin_company":"choices",
        "plan":"choices",
        "seller":"choices",
        "is_customer":"bool",
        "street_location":"location",
    }
    FIELDS_EXCEL: Dict[str, Dict[str, str]] = {
        "A":"Compañia de origen",
        "B":"Telefono primario",
        "C":"Correo primario",
        "D":"Nombre",
        "E":"Rut",
        "F":"Fecha de conversión",
        "G":"Lead",
        "H":"Dirección",
    }

    model: BaseModel = LeadletCustomerModel

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """
        
        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )
        qs_size = len(qs)

        if qs:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]

            global_preferences: Dict[str, Any] = global_preferences_registry.manager()

            pulso_token: str = global_preferences['pulso__pulso_check_token']
            pulso_url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility-iris/'

            _random: int = random.randint(5000, 5000000)
            street_locations: List[int] = list( set( [ street.street_location for street in qs ] ) )

            street_locations = requests.post(
                url=f"{pulso_url}?no_cache={_random}",
                json={
                    "street_location":street_locations,
                },
                headers={
                    'Authorization':pulso_token
                },
                verify=False,
            ).json()

            self.street_locations: Dict[int, str] = { location['street_location_id']:location['street_location'] for location in street_locations}
            
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )
        
        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

    def get_qs_to_data(self, qs: QuerySet) -> List[Dict[str, Any]]:

        """

            This function, process data

        """

        global_preferences: Dict[str, Any] = global_preferences_registry.manager()

        pulso_token: str = global_preferences['pulso__pulso_check_token']
        pulso_url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility-iris/'

        _random: int = random.randint(5000, 5000000)
        street_locations: List[int] = list( set( [ street['street_location'] for street in qs.values('street_location') ] ) )

        street_locations = requests.post(
            url=f"{pulso_url}?no_cache={_random}",
            json={
                "street_location":street_locations,
            },
            headers={
                'Authorization':pulso_token
            },
            verify=False,
        ).json()

        street_locations: Dict[int, str] = { location['street_location_id']:location['street_location'] for location in street_locations}

        def get_leadlet_data(instance: LeadCustomerModel) -> Dict[str, Any]:

            return [
                instance.get_origin_company_display(),
                str(instance.primary_phone),
                instance.primary_email,
                instance.name,
                instance.rut,
                instance.conversion_date,
                instance.lead.ID if instance.lead else "",
                street_locations[instance.street_location],
            ]

        return list( map(get_leadlet_data, qs) )

    def get_initial_params(self):

        super().get_initial_params()

        self.LOCATION_FILTER : List[List[Union[str, int, bool]]] = list(filter(lambda x: 'street_location' == x[0], self.REQUEST_FILTERS))
        self.REQUEST_FILTERS : List[List[Union[str, int, bool]]] = list(filter(lambda x: 'street_location' != x[0], self.REQUEST_FILTERS))

    def get_filtered_queryset(self) -> QuerySet:

        result : QuerySet = super().get_filtered_queryset()

        global_preferences: Dict[str, Any] = global_preferences_registry.manager()

        pulso_token: str = global_preferences['pulso__pulso_check_token']
        pulso_url: str = global_preferences['pulso__domain_url']

        if self.LOCATION_FILTER:

            street_location_pulso : List[int] = list( set( [ street_location[0] for street_location in result.values_list('street_location') ] ) )

            validated_locations: List[int] = []
            excluded_locations: List[int] = []

            for _filter in self.LOCATION_FILTER:

                location: str = _filter[2]
                _type: str = location.split(':')[0]
                location_search: str = int(location.split(':')[1])

                result_locations : List[int] = []

                if _type != 'streetLocation':
                    
                    _random: int = random.randint(5000, 5000000)
                    
                    response_locations = requests.post(
                        url=f"{pulso_url}/api/v1/locations-iris/?no_cache={_random}",
                        json={
                            "tipo":_type,
                            "location":location_search,
                            "locations":street_location_pulso,
                        },
                        headers={
                            'Authorization':pulso_token
                        },
                        verify=False,
                    )

                    result_locations : List[int] = response_locations.json()

                else:

                    result_locations : List[int] =  result_locations + [location_search]

                if _filter[1] == "equal":

                    validated_locations: List[int] = validated_locations + result_locations

                else:

                    excluded_locations: List[int] = excluded_locations + result_locations
        
            result: QuerySet = result.filter(street_location__in= list( set(validated_locations) - set(excluded_locations) ) )

        return result

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            if search.isdigit():

                search_int: int = int(search)

                qs: QuerySet = qs.filter( Q(ID=int(search)) | Q(lead__ID=search_int) )

            else:

                qs: QuerySet = qs.filter( 
                    Q(name__icontains=search) |
                    Q(primary_email__icontains=search) |
                    Q(primary_phone__icontains=search) |
                    Q(rut__icontains=search) |
                    Q(secundary_phone__icontains=search)
                )


        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username if instance.creator.username != "api@api.com" else "Página Web",
            "name": instance.name,
            "primary_email": instance.primary_email,
            "primary_phone": str(instance.primary_phone),
            "origin_company": instance.get_origin_company_display(),
            "seller":instance.seller,
            "is_customer":instance.is_customer,
            "str_address":self.street_locations[instance.street_location],
            "chat": instance.chat
        }

class LeadletCustomerDatatable(LeadletCustomerDatatableBase):
    
    def get_struct(self)-> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: dict = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "scroll":1500,
            "download": True,
        }

        data_struct["fields"] = {
            "ID": "int",
            "created": "datetime",
            "creator": "str",
            "name": "str",
            "primary_email": "str",
            "primary_phone": "str",
            "origin_company": "str",
            "is_customer": "bool",
            "plan":"int",
            "seller":"int",
            "str_address":"str",
        }

        columns = {
            'ID':{
                "sortable": True,
                "name":"ID",
                "width":100,
                "fixed":None
            },
            'creator':{
                "sortable": True,
                "name":"Creador",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Fecha de Creación",
                "width":100,
                "fixed":None
            },
            'str_address':{
                "sortable": False,
                "name":"Dirección",
                "width":300,
                "fixed":None
            },
            'name':{
                "sortable": True,
                "name":"Nombre",
                "width":150,
                "fixed":None
            },
            'primary_phone':{
                "sortable": True,
                "name":"Telefono",
                "width":100,
                "fixed":None
            },
            'primary_email':{
                "sortable": True,
                "name":"Correo",
                "width":100,
                "fixed":None
            },
            "origin_company":{
                "sortable": True,
                "name":"Compañía de Origen",
                "width":100,
                "fixed":None
            },
            "commentaries":{
                "sortable": True,
                "name":"Comentarios",
                "width":100,
                "fixed":None
            },
        }

        instance_tables_leadlet: QuerySet = LeadletTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_leadlet.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1

        data_struct_columns["¿Cliente?"] = {
            "field": "is_customer",
            "sortable": True,
            "visible": True,
            "position": position,
            "width":100,
            "fixed":None
        }

        data_struct_columns["Contactos"]= {
            "field": "custom_contacts",
            "sortable": True,
            "visible": True,
            "position": position + 1,
            "width":100,
            "fixed":None
        }

        data_struct_columns["Modificar"] = {
            "field": "custom_change",
            "sortable": False,
            "visible": True,
            "position": position + 2,
            "width":100,
            "fixed":'right'
        }

        data_struct["columns"] = data_struct_columns
        
        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Telefono":{
                "type":"str",
                "name":"primary_phone"
            },
            "Correo":{
                "type":"str",
                "name":"primary_email"
            },
            "Compañia de origen":{
                "type":"choices",
                "name":"origin_company",
                "type_choices": "choices",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:"ENTEL",
                        1:"GTD",
                        2:"MOVISTAR",
                        3:"CLARO",
                        4:"VTR",
                        5:"NINGUNA",
                    }
                }
            },
            "Vendedor":{
                "type":"choices",
                "name":"seller",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "matrix/sellers/",
                        "read_data":{
                            "value":"id",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Dirección":{
                "type":"location",
                "name":"street_location",
            },
            "¿Es cliente?":{
                "type": "bool",
                "name": "is_customer"
            }
        }

        return data_struct

    def get_filtered_queryset(self) -> QuerySet:

        instance_tables_leadlet: QuerySet = LeadletTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_leadlet.filters

        result : QuerySet = super().get_filtered_queryset()

        global_preferences: Dict[str, Any] = global_preferences_registry.manager()

        pulso_token: str = global_preferences['pulso__pulso_check_token']
        pulso_url: str = global_preferences['pulso__domain_url']

        #Created to request user
        if instance_tables_leadlet.create_me:

            result: QuerySet = result.filter(creator=self.request.user)

        #Time interval
        if instance_tables_leadlet.last_time_type and instance_tables_leadlet.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            result: QuerySet = result.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_leadlet.last_time_type]  * instance_tables_leadlet.last_time))
        
        if self.LOCATION_FILTER:

            street_location_pulso : List[int] = list( set( [ street_location[0] for street_location in result.values_list('street_location') ] ) )

            validated_locations: List[int] = []
            excluded_locations: List[int] = []

            for _filter in self.LOCATION_FILTER:

                location: str = _filter[2]
                _type: str = location.split(':')[0]
                location_search: str = int(location.split(':')[1])

                result_locations : List[int] = []

                if _type != 'streetLocation':
                    
                    _random: int = random.randint(5000, 5000000)
                    
                    response_locations = requests.post(
                        url=f"{pulso_url}/api/v1/locations-iris/?no_cache={_random}",
                        json={
                            "tipo":_type,
                            "location":location_search,
                            "locations":street_location_pulso,
                        },
                        headers={
                            'Authorization':pulso_token
                        },
                        verify=False,
                    )

                    result_locations : List[int] = response_locations.json()

                else:

                    result_locations : List[int] =  result_locations + [location_search]

                if _filter[1] == "equal":

                    validated_locations: List[int] = validated_locations + result_locations

                else:

                    excluded_locations: List[int] = excluded_locations + result_locations
        
            result: QuerySet = result.filter(street_location__in= list( set(validated_locations) - set(excluded_locations) ) )

        return result
        
class LeadletCustomerOpportunityDatatable(LeadletCustomerDatatableBase):

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        qs: QuerySet = super().get_filtered_search(qs)

        return qs.filter( created__gte= datetime.now() - timedelta(days=15) , is_customer=False )

class CustomerTablesDatatableBase(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]


    model: BaseModel = CustomerTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH
    
        if search:

            if search.isdigit():

                qs: QuerySet = qs.filter( ID=int(search) )

            else:
                
                qs: QuerySet = qs.filter( name__icontains=search )

        return qs
        

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class LeadTablesDatatable(CustomerTablesDatatableBase):

    model: BaseModel = LeadTablesModel

class LeadletTablesDatatable(CustomerTablesDatatableBase):

    model: BaseModel = LeadletTablesModel