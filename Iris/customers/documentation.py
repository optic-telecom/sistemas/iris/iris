from drf_yasg.openapi import Response as Response_Openapi
from drf_yasg.utils import swagger_auto_schema
from typing import Union, List, Dict, Any

from .models import CustomerModel, LeadCustomerModel
from .serializers import LeadCustomerSerializer, LeadletCustomerSerializer
from rest_framework import serializers
from rest_framework.serializers import (BooleanField, CharField, ChoiceField,
                                        DateField, EmailField, IntegerField,
                                        Serializer, ValidationError)


class DocumentationSerializerDatatablesCustomer(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """

        start: int = serializers.IntegerField()
        offset: int = serializers.IntegerField()
        order_field: str = serializers.CharField(default="ID")
        order_type: str = serializers.CharField(default="desc")
        filters: List[list] = serializers.JSONField(default=[['ID','equal','1']])
        
        class Meta:

            fields: Union[list, str] = '__all__'

class DocumentationCustomerView(object):

    """

        Class define documentation of CustomerView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TriggerEvent.
    
        """
        
        class Meta:

            fields: Union[list, str] = []

    class DocumentationCustomerSerializer(serializers.Serializer):

        """

            Class define Serializer of Customer.
    
        """

        class Meta:

            model = CustomerModel
            fields: Union[list, str]  = '__all__'

    class DocumentationPhoneSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TriggerEvent.
    
        """

        phone: str = serializers.CharField(default='+51936569862')
        
        class Meta:

            fields: Union[list, str] = '__all__'

    class DocumentationEmailSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TriggerEvent.
    
        """

        email: str = serializers.EmailField()
        
        class Meta:

            fields: Union[list, str] = '__all__'

    serializer_class_void = DocumentationSerializerVoid
    serializer_class_body = DocumentationCustomerSerializer
    serializer_class_datatables = DocumentationSerializerDatatablesCustomer
    serializer_class_phone = DocumentationPhoneSerializerVoid
    serializer_class_email = DocumentationEmailSerializerVoid

    def documentation_communications_history(self):

        """
            This function search customer communications history

            :param self: Instance of Class DocumentationCustomerView
            :type self: DocumentationCustomerView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='communications history, customer',
            operation_description="""

                This endpoint return history

            """,
            responses = {
                200:Response_Openapi(
                    description="History",
                    examples = [
                        {
                            'message':'Mensaje de prueba',
                            'created':'2020-07-29T09:53:20.894183-05:00',
                            'type':'text',
                        },
                        {
                            'message':'Mensaje de prueba',
                            'created':'2020-07-29T09:53:20.894183-05:00',
                            'type':'whatsapp',
                        },
                        {
                            'message':'Mensaje de prueba',
                            'created':'2020-07-29T09:53:20.894183-05:00',
                            'type':'voice_call',
                        },
                        {
                            'message':'Mensaje de prueba',
                            'created':'2020-07-29T09:53:20.894183-05:00',
                            'type':'email',
                        },
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_remove_phone(self):

        """
            This function remove phone number

            :param self: Instance of Class DocumentationCustomerView
            :type self: DocumentationCustomerView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='remove phone, customer',
            operation_description="""

                This endpoint remove phone .

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Remove phone, customer',
                    examples = {},
                ),
                500:Response_Openapi(
                    description = 'Error remove phone',
                    examples = {
                        "Error pk":"Don`t exist item",
                        "Error phone":"'phone' fields is required",
                    }
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_phone,
        )

    def documentation_remove_email(self):

        """
            This function remove email

            :param self: Instance of Class DocumentationCustomerView
            :type self: DocumentationCustomerView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='remove email, customer',
            operation_description="""

                This endpoint remove email .

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Remove email, customer',
                    examples = {},
                ),
                500:Response_Openapi(
                    description = 'Error remove phone',
                    examples = {
                        "Error pk":"Don`t exist item",
                        "Error email":"'email' fields is required",
                    }
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_phone,
        )

    def documentation_add_phone(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationCustomerView
            :type self: DocumentationCustomerView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='add phone, customer',
            operation_description="""

                This endpoint add phone .

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Add phone, customer',
                    examples = {},
                ),
                500:Response_Openapi(
                    description = 'Error remove phone',
                    examples = {
                        "Error pk":"Don`t exist item",
                        "Error phone":"'phone' fields is required",
                        "Error phone":"Invalid phone format",
                        "Error phone repeat":"Repeated phone"
                    }
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_phone,
        )

    def documentation_add_email(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationCustomerView
            :type self: DocumentationCustomerView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='add email, customer',
            operation_description="""

                This endpoint add email .

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Add email, customer',
                    examples = {},
                ),
                500:Response_Openapi(
                    description = 'Error remove phone',
                    examples = {
                        "Error pk":"Don`t exist item",
                        "Error email":"'email' fields is required",
                        "Error email":"Invalid email format",
                        "Error email repeat":"Repeated email"
                    }
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_email,
        )

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationCustomerView
            :type self: DocumentationCustomerView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, customer',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, customer',
                    examples = [
                        {
                            "ID": "10",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "rut": "12.833.524-2",
                            "phones": ["+51931936596"],
                            "emails": ["cris@optic.cl"],
                            "web": "www.origin.com",
                            "facebook": "www.facebook.com",
                            "instagram": "www.instagram.com",
                            "twitter": "www.twitter.com",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationCustomerView
            :type self: DocumentationCustomerView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, customers',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, customers',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "created": "datetime",
                            "creator": "str",
                            "rut": "str",
                            "phones": "list",
                            "emails": "list",
                            "web": "str",
                            "facebook": "str",
                            "instagram": "str",
                            "twitter": "str",
                        },
                        "columns": {
                            "Rut":{
                                "field": "rut",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Telefonos":{
                                "field": "phones",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Correos":{
                                "field": "emails",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Web":{
                                "field": "web",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                            "Facebook":{
                                "field": "facebook",
                                "sortable": False,
                                "visible": True,
                                "position": 6
                            },
                            "Twitter":{
                                "field": "twitter",
                                "sortable": False,
                                "visible": True,
                                "position": 7
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 8
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 9
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Rut":{
                                "type":"str",
                                "name":"rut"
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationLeadView(object):

    """

        Class define documentation of LeadView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):
        
        class Meta:

            fields: Union[list, str] = []

    serializer_class_void = DocumentationSerializerVoid
    serializer_class_body = LeadCustomerSerializer
    serializer_class_response = LeadCustomerSerializer
    serializer_class_datatables = DocumentationSerializerDatatablesCustomer

    def documentation_change_to_leadlet(self):

        """
            This function change to lead

            :param self: Instance of Class DocumentationLeadView
            :type self: DocumentationLeadView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='change to lead, lead',
            operation_description="""

                This endpoint change to lead

            """,
            responses = {
                500:Response_Openapi(
                    description = 'Error remove phone',
                    examples = {
                        "Item error":"Don`t exist instance",
                        "Intance is leadlef":"Instance has been converted into leadlef",
                        'Error address feasibility': "must have feasibility to change leadlet"
                    }
                )
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationLeadView
            :type self: DocumentationLeadView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Lead',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Lead',
                    examples = [
                        {
                            "ID": "10",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "name": "Antonio",
                            "primary_email": "cristian@optic.cl",
                            "primary_phone": "+51931939080",
                            "origin_company": "MOVISTAR",
                            "status": "LEAD",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationLeadView
            :type self: DocumentationLeadView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Lead',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Lead',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "created": "datetime",
                            "creator": "str",
                            "name": "str",
                            "primary_email": "str",
                            "primary_phone": "str",
                            "origin_company": "str",
                            "status": "str",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Telefono":{
                                "field": "primary_phone",
                                "sortable": True,
                                "visible": True,
                                "position": 3
                            },
                            "Correo":{
                                "field": "primary_email",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Compañia de origen":{
                                "field": "origin_company",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                            "Estado":{
                                "field": "status",
                                "sortable": False,
                                "visible": True,
                                "position": 6
                            },
                            "Convertir":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 8
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 9
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 10
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Telefono":{
                                "type":"str",
                                "name":"primary_phone"
                            },
                            "Correo":{
                                "type":"str",
                                "name":"primary_email"
                            },
                            "Compañia de origen":{
                                "type":"choices",
                                "name":"origin_company",
                                "type_choices": "choices",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0:"ENTEL",
                                        1:"GTD",
                                        2:"MOVISTAR",
                                        3:"CLARO",
                                        4:"VTR",
                                        5:"NINGUNA",
                                    }
                                }
                            },
                            "Estado":{
                                "type":"choices",
                                "name":"status",
                                "type_choices": "choices",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0:"LEAD",
                                        1:"LEAFLET",
                                    }
                                }
                            },
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_create(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationDocumentationLeadView
            :type self: DocumentationDocumentationLeadView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create, lead',
            operation_description="""

                This endpoint create lead record. 

            """,
            responses = {
                200:Response_Openapi(
                    'Creation lead instance',
                    self.serializer_class_response,
                ),
                500:Response_Openapi(
                    description = 'Error create lead instance',
                    examples = {
                        'Error address value': "Must be an existing address",
                        'Error address feasibility': "must be a leadlet"
                    },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_update(self):

        """

            This function return information of documentation view 'update'

            :param self: Instance of Class DocumentationDocumentationLeadView
            :type self: DocumentationDocumentationLeadView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Update, lead',
            operation_description="""

                This endpoint update lead record. 

            """,
            responses = {
                200:Response_Openapi(
                    'Creation lead instance',
                    self.serializer_class_response,
                ),
                500:Response_Openapi(
                    description = 'Error update lead instance',
                    examples = {
                        'Error address value': "Must be an existing address",
                        'Error address feasibility': "must be a leadlet"
                    },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

class DocumentationLeadletView(object):

    """

        Class define documentation of LeadletView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):
        
        class Meta:

            fields: Union[list, str] = []

    class DocumentationCustomerMatrixSerializer(serializers.Serializer):

            class Meta:

                extra_kwargs: Dict[str, Dict[str, Any]] = {
                    'commercial_activity': {'required': False},
                    'notes': {'required': False},
                    'birth_date': {'required': False},
                }

            HOME = 1
            COMPANY = 2
            TYPE_CHOICES = ((HOME, HOME), (COMPANY, COMPANY))

            NETWORK_CABLE = 1
            GPON = 2
            GEPON = 3
            ANTENNA = 4

            TECHNOLOGY_KIND_CHOICES = (
                (NETWORK_CABLE, ("UTP")),
                (GPON, ("GPON")),
                (GEPON, ("GEPON")),
                (ANTENNA, ("antenna")),
            )

            send_email: bool = BooleanField(default=False)
            send_snail_mail: bool = BooleanField(default=False)
            rut: str = CharField(max_length=20)
            name: str = CharField(max_length=255)
            kind: int = ChoiceField(choices=TYPE_CHOICES)
            commercial_activity: int = IntegerField(required = False)
            email: str = EmailField()
            location: int = CharField()
            seller: int = IntegerField()
            #street = CharField(max_length=75)
            #house_number = CharField(max_length=75)
            #apartment_number = CharField(max_length=75)
            #tower = CharField(max_length=75)
            #location = CharField(_("commune"), max_length=5, choices=COMMUNE_CHOICES) Definir esto.
            phone: str = CharField(max_length=75)
            default_due_day: int = CharField(default=5)
            unified_billing: bool = BooleanField(default=False)
            full_process: bool = BooleanField(default=True)
            notes: str = CharField(required = False)
            birth_date: str = DateField(required = False)
            plan: int = IntegerField()
            technology_kind: int = ChoiceField(choices=TECHNOLOGY_KIND_CHOICES)
            operator: int = IntegerField()

            first_category_activity: bool = BooleanField(default=False)
            change_to_service: bool = BooleanField(default=False)

    serializer_class_void = DocumentationSerializerVoid
    serializer_class_body = LeadletCustomerSerializer
    serializer_class_response = LeadletCustomerSerializer
    serializer_class_datatables = DocumentationSerializerDatatablesCustomer
    serializer_class_customer_matrix = DocumentationCustomerMatrixSerializer

    def create_customer(self):

        """
            This function change to leadlet

            :param self: Instance of Class DocumentationLeadletView
            :type self: DocumentationLeadletView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='change to customer, leadlet',
            operation_description="""

                This endpoint change to leadlet

            """,
            responses = {
                500:Response_Openapi(
                    description = 'Create customer',
                    examples = {
                        "Item error":"Don`t exist instance",
                        "Customer error":"Customer has been created",
                    }
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_customer_matrix,
        )

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationLeadletView
            :type self: DocumentationLeadletView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Leadlet',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Leadlet',
                    examples = [
                        {
                            "ID": "10",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "name": "Antonio",
                            "primary_email": "cristian@optic.cl",
                            "primary_phone": "+51931939080",
                            "origin_company": "MOVISTAR",
                            "status": "LEAD",
                            "plan":"0",
                            "seller":"0",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datables_opportunity(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationLeadletView
            :type self: DocumentationLeadletView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Leadlet opportunity',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Leadlet opportunity',
                    examples = [
                        {
                            "ID": "10",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "name": "Antonio",
                            "primary_email": "cristian@optic.cl",
                            "primary_phone": "+51931939080",
                            "origin_company": "MOVISTAR",
                            "status": "LEAD",
                            "plan":"0",
                            "seller":"0",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationLeadView
            :type self: DocumentationLeadView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Leadlet',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Leadlet',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "created": "datetime",
                            "creator": "str",
                            "name": "str",
                            "primary_email": "str",
                            "primary_phone": "str",
                            "origin_company": "str",
                            "status": "str",
                            "plan":"int",
                            "seller":"int",
                        },
                        "columns": {
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Telefono":{
                                "field": "primary_phone",
                                "sortable": True,
                                "visible": True,
                                "position": 3
                            },
                            "Correo":{
                                "field": "primary_email",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Compañia de origen":{
                                "field": "origin_company",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                            "Estado":{
                                "field": "status",
                                "sortable": False,
                                "visible": True,
                                "position": 6
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 8
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 9
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Telefono":{
                                "type":"str",
                                "name":"primary_phone"
                            },
                            "Correo":{
                                "type":"str",
                                "name":"primary_email"
                            },
                            "Compañia de origen":{
                                "type":"choices",
                                "name":"origin_company",
                                "type_choices": "choices",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0:"ENTEL",
                                        1:"GTD",
                                        2:"MOVISTAR",
                                        3:"CLARO",
                                        4:"VTR",
                                        5:"NINGUNA",
                                    }
                                }
                            },
                            "Estado":{
                                "type":"choices",
                                "name":"status",
                                "type_choices": "choices",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0:"LEAD",
                                        1:"LEAFLET",
                                    }
                                }
                            },
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datables_struct_opportunity(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationLeadView
            :type self: DocumentationLeadView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Leadlet opportunity',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Leadlet opportunity',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "created": "datetime",
                            "creator": "str",
                            "name": "str",
                            "primary_email": "str",
                            "primary_phone": "str",
                            "origin_company": "str",
                            "status": "str",
                            "plan":"int",
                            "seller":"int",
                        },
                        "columns": {
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Telefono":{
                                "field": "primary_phone",
                                "sortable": True,
                                "visible": True,
                                "position": 3
                            },
                            "Correo":{
                                "field": "primary_email",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Compañia de origen":{
                                "field": "origin_company",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                            "Estado":{
                                "field": "status",
                                "sortable": False,
                                "visible": True,
                                "position": 6
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 8
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 9
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Telefono":{
                                "type":"str",
                                "name":"primary_phone"
                            },
                            "Correo":{
                                "type":"str",
                                "name":"primary_email"
                            },
                            "Compañia de origen":{
                                "type":"choices",
                                "name":"origin_company",
                                "type_choices": "choices",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0:"ENTEL",
                                        1:"GTD",
                                        2:"MOVISTAR",
                                        3:"CLARO",
                                        4:"VTR",
                                        5:"NINGUNA",
                                    }
                                }
                            },
                            "Estado":{
                                "type":"choices",
                                "name":"status",
                                "type_choices": "choices",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0:"LEAD",
                                        1:"LEAFLET",
                                    }
                                }
                            },
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_create(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationLeadletView
            :type self: DocumentationLeadletView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id="Create, leadlet",
            operation_description="""

                This endpoint create leadlet record. 

            """,
            responses = {
                200:Response_Openapi(
                    "Creation leadlet instance",
                    self.serializer_class_response,
                ),
                500:Response_Openapi(
                    description = "Error create leadlet instance",
                    examples = {
                        "Error address value": "Must be an existing address",
                        "Error address feasibility": "must have feasibility",
                        "Error status": "You cannot change the status, if the client does not exist",
                    },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_update(self):

        """

            This function return information of documentation view 'update'

            :param self: Instance of Class DocumentationLeadLetView
            :type self: DocumentationLeadLetView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id="Update, leadlet",
            operation_description="""

                This endpoint update leadlet record. 

            """,
            responses = {
                200:Response_Openapi(
                    "Creation leadlet instance",
                    self.serializer_class_response,
                ),
                500:Response_Openapi(
                    description = "Error update leadlet instance",
                    examples = {
                        "Error address value": "Must be an existing address",
                        "Error address feasibility": "must have feasibility",
                        "Error status": "You cannot change the status, if the client does not exist",
                    },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )


