from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference, IntegerPreference, ChoicePreference

general = Section('tech_support')


@global_preferences_registry.register
class SlackChannel(StringPreference):

    section = general
    name: str = 'slack_channel'
    default: str = ''
    required: bool = True

@global_preferences_registry.register
class BotToken(StringPreference):

    section = general
    name: str = 'slack_bot_token'
    default: str = 'xoxb-654700021921-2034005833715-t7DoGNBy0sFwMd6JQFIXqjAw'
    required: bool = True