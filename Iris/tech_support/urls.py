from rest_framework.routers import DefaultRouter

from .views import (TicketView, TicketTableView)

router = DefaultRouter()

router.register(r'ticket', TicketView, basename='ticket')
router.register(r'ticket_tables', TicketTableView, basename='ticket_tables')

urlpatterns = router.urls
