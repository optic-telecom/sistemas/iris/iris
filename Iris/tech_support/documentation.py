from typing import Union, List

from drf_yasg.openapi import Response as Response_Openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers

from .models import TicketModel
from .serializers import TicketSerializer

class DocumentationSerializerDatatablesTechSupport(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """

        start: int = serializers.IntegerField()
        offset: int = serializers.IntegerField()
        order_field: str = serializers.CharField(default="ID")
        order_type: str = serializers.CharField(default="desc")
        filters: List[list] = serializers.JSONField(default=[['ID','equal','1']])
        
        class Meta:

            fields: Union[list, str] = '__all__'

class DocumentationSupportTicket(object):

    """

        Class define documentation of TicketView
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of NewContactView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []

    class DocumentationSerializerSupportSlackChannel(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """

        channel: str = serializers.CharField()

        class Meta:

            fields: Union[list, str] = '__all__'

    serializer_class_body = TicketSerializer
    serializer_class_response = TicketSerializer
    serializer_class_slack_channel = DocumentationSerializerSupportSlackChannel
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_datatable_tech_support = DocumentationSerializerDatatablesTechSupport

    def documentation_create_and_update(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create or update, ticket',
            operation_description="""

                This endpoint creates a support ticket

            """,
            responses = {
                200:Response_Openapi(
                    'Create or update ticket',
                    self.serializer_class_response,
                ),
                500:Response_Openapi(
                    description = 'Create or update ticket',
                    examples = {
                        "Status error":"This ticket has been closed and cannot be updated",
                        "Group error":"This user is not part of the system team",
                        "Invalid user": "Only the creator of a ticket or someone authorized can close it"
                    },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_get_tech_support_channel(self):

        """

            This function returns information of documentation view 'get_channel'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Get tech support channel',
            operation_description="""

                This endpoint returns the tech support slack channel

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Tech support Slack channel',
                    examples = {
                        "application/json":{
                            "channel": "CDXXXXX",
                        }
                    },
                )
            }
        )

    def documentation_get_system_groups(self):

        """

            This function returns information of documentation view 'get_channel'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Get tech support system groups',
            operation_description="""

                This endpoint returns the tech support systems groups

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Tech support group systems',
                    examples = {
                        "application/json":
                        [
                            {"name": "Sistema-Iris", "ID": 1},
                            {"name": "Sistema-Matrix", "ID": 2},
                        ]
                        
                    },
                )
            }
        )

    def documentation_update_tech_support_channel(self):

        """

            This function returns information of documentation view 'get_channel'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Update tech support channel',
            operation_description="""

                This endpoint updates the tech support slack channel

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Tech support Slack channel',
                    examples = {
                        "application/json":{}
                    },
                )
            },
            query_serializer = self.serializer_class_slack_channel,
            request_body = self.serializer_class_slack_channel,
        )

    def documentation_datatables(self):

        """

            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationNewContact
            :type self: DocumentationNewContact

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Support tickets datatables',
            operation_description="""

                This endpoint return datatable information. 

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables with ffiltered query',
                    examples = {
                        "application/json":{
                            "size": 2,
                            "result": [
                                {
                                    "ID": 3, 
                                    "created": "2020-10-21T15:36:08.932Z", 
                                    "creator": "ariana.amador@multifiber.cl", 
                                    "SLA": 109.38412199999999,
                                    "agent": "ariana.amador@multifiber.cl",
                                    "assigned_team": "Sistema-Iris",
                                    "status": 0,
                                    "internal_status": 0,
                                    "slack_thread": "1619118381.000500",
                                    "slack_channel": "CJZZXXX"
                                },
                                {
                                    "ID": 4, 
                                    "created": "2020-10-21T15:36:08.932Z", 
                                    "creator": "ariana.amador@multifiber.cl", 
                                    "SLA": 109.38412199999999,
                                    "agent": "ariana.amador@multifiber.cl",
                                    "assigned_team": "Sistema-Matrix",
                                    "status": 0,
                                    "internal_status": 0,
                                    "slack_thread": "1619118381.000500",
                                    "slack_channel": "CJZZXXX"
                                }
                            ], 
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatable_tech_support
        )