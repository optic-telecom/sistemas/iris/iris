# Generated by Django 2.1.7 on 2021-05-04 18:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tech_support', '0011_auto_20210504_1414'),
    ]

    operations = [
        migrations.RenameField(
            model_name='historicalticketmodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RenameField(
            model_name='historicaltickettablesmodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RenameField(
            model_name='ticketmodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RenameField(
            model_name='tickettablesmodel',
            old_name='operator_instance',
            new_name='operator',
        ),
    ]
