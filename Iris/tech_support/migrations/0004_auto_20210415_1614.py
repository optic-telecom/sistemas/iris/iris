# Generated by Django 2.1.7 on 2021-04-15 20:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tech_support', '0003_20210415_1516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticketmodel',
            name='agent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
    ]
