from typing import List

from django.contrib import admin

from .models import (TicketModel, TicketTablesModel)


class TicketAdmin(admin.ModelAdmin):

    fields: List[str] = ["status", "internal_status", "assigned_team", "agent",
    "trello_urls","commits","slack_thread","slack_channel","creator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID","assigned_team", "agent", "status", "internal_status" ]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID"]

    #Define model data list ordening
    ordering: List[str] = ["ID"]

class TicketTableAdmin(admin.ModelAdmin):
    
    fields = ['name', 'columns', 'last_time', "last_time_type", "operator", "create_me", "assigned_to_me", "filters" ]

    #list of fields to display in django admin
    list_display = ['ID','name','operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

admin.site.register(TicketModel, TicketAdmin)
admin.site.register(TicketTablesModel, TicketTableAdmin)