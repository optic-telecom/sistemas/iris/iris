from typing import Any, Dict, List, Union
from slack import WebClient

from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import get_ticket_SLA
from users.models import UserSlackIDModel
from django.contrib.auth.models import User, Group
from django.db.models.query import QuerySet
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import (SerializerMethodField,ValidationError)
from rest_framework import serializers

from .models import (TicketModel, TicketTablesModel)


class TicketSerializer(BaseSerializer):

    """

        Class define serializer of TicketView.
    
    """
    #Method get_SLA
    SLA = serializers.SerializerMethodField()

    assigned_team_name = serializers.SerializerMethodField()

    agent_name = serializers.SerializerMethodField()

    def get_SLA(self, instance_model : TicketModel ) -> float:
        
        closed_status_list = [1,2,3]
        return get_ticket_SLA(instance_model, closed_status_list, 'internal_status')

    def get_assigned_team_name(self, instance_model : TicketModel ) -> str:
        return instance_model.assigned_team.name

    def get_agent_name(self, instance_model : TicketModel ) -> str:
        return instance_model.agent.username if instance_model.agent != None else ''

    def slack_message(self, instance, message):

        global_preferences: Dict[str] = global_preferences_registry.manager()

        workspace: str = 'tech_support__slack_bot_token'

        token: str = global_preferences[workspace]

        client = WebClient(token=token)

        result = client.chat_postMessage(
            token=token,
            channel=instance.slack_channel,
            blocks=message,
            thread_ts=instance.slack_thread
        )

    def create(self, validated_data: Dict[str, Any]) -> TicketModel:

        """
        
            This funtion validate and create instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        def slack_message(instance, data):

            global_preferences: Dict[str] = global_preferences_registry.manager()

            workspace: str = 'tech_support__slack_bot_token'

            token: str = global_preferences[workspace]

            client = WebClient(token=token)

            channel: str = global_preferences['tech_support__slack_channel']

            creator = "<@" + data.get('slack_uid') + ">" if data.get('slack_uid',None) else instance.creator.username
            system =  data.get('team').replace('Sistema-','')

            blocks = [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": creator + " ha creado un ticket de soporte. ID: " + str(instance.ID)
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*Sistema*: " + system
                    }
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*Descripción del problema*: " + data.get('comment')
                    }
                }
            ]

            result = client.chat_postMessage(
                token=token,
                channel=channel,
                blocks=blocks,
                text=creator + " ha creado un ticket de soporte. ID: " + str(instance.ID)
            )

            return result.data['channel'], result.data['ts']

        if validated_data.get("assigned_team",None) == None:
            assigned_team = Group.objects.filter(name=self.context["request"].data["team"]).first()
            validated_data['assigned_team'] = assigned_team

        result = super().create(validated_data)

        slack_channel, slack_thread = slack_message(result, self.context["request"].data)

        if self.context["request"].data.get("slack_uid",None) != None:
            creator_query = UserSlackIDModel.objects.filter(uid=self.context["request"].data["slack_uid"])

            if len(creator_query) > 0:
                result.creator = creator_query.first().user
                result.updater = creator_query.first().user

        result.slack_channel = slack_channel
        result.slack_thread = slack_thread
        result.save()

        return result

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        if instance.status == 4 and 'status' not in validated_data:
            raise ValidationError({'Error': 'This ticket has been closed and cannot be updated'})

        if instance.assigned_team not in self.context['request'].user.groups.all():
            if validated_data.get('status') != 1 and validated_data.get('status') != 4:
                raise ValidationError({'Invalid user': 'This user is not part of the system team'})

        if 'assigned_team' in validated_data and instance.slack_thread != None:

            group_name = validated_data["assigned_team"].name
            agents = User.objects.filter(groups=validated_data["assigned_team"])
            agents_slack_uid = []

            for agent in agents:
                slack_info_query = UserSlackIDModel.objects.filter(user=agent)
                if len(slack_info_query) > 0: agents_slack_uid.append(slack_info_query.first().uid)

            agents = ', '.join(map(lambda x: "<@" + str(x) + ">",agents_slack_uid))
            username = self.context['request'].user.username

            slack_message = [
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"*" + username + "* ha transferido este ticket al sistema " + group_name.replace('Sistema-','')
                        }
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"*" + username + "* ha transferido este ticket al sistema " + group_name.replace('Sistema-','') + ". Agentes asignados: " + agents
                        }
                    }
                ]

            self.slack_message(instance, slack_message)

        if 'status' in validated_data and instance.slack_thread != None:

            if validated_data["status"] == 4 and "slack_uid" in self.context["request"].data:
                user_query = UserSlackIDModel.objects.filter(uid=self.context["request"].data["slack_uid"])
                creator_query = UserSlackIDModel.objects.filter(user=instance.creator)

                if len(creator_query) > 0 and user_query.first().user != instance.creator and not user_query.first().user.groups.filter(name="Supervisor Soporte").exists():
                    raise ValidationError({'Invalid User': 'Only the creator of a ticket can close it'})

            if validated_data["status"] == 1 and instance.internal_status == 3:

                user_query = UserSlackIDModel.objects.filter(uid=self.context["request"].data.get("slack_uid","_"))
                user_uid = '<@' + user_query.first().uid + ">" if len(user_query) > 0 else self.context['request'].user.username
                agent_uid_query = UserSlackIDModel.objects.filter(user=instance.agent)
                agent_slack_uid = '<@' + agent_uid_query.first().uid + ">" if len(agent_uid_query) > 0 else ''

                slack_message = [
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"*" + user_uid + "* ha indicado que el problema no se ha solucionado. " + agent_slack_uid
                        }
                    }
                ]

                self.slack_message(instance, slack_message)
            
            else:

                username = self.context['request'].user.username
                if dict(TicketModel.STATUS_CHOICES)[validated_data.get('status')] == 'Cerrado':
                    message_str: str = "Se ha cerrado el ticket"
                else: 
                    message_str: str = f"*" + username + "* ha cambiado el status del ticket a " + dict(TicketModel.STATUS_CHOICES)[validated_data.get('status')]

                slack_message = [
                        {
                            "type": "divider"
                        },
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": message_str
                            }
                        }
                    ]
                
                self.slack_message(instance, slack_message)

        if 'agent' in validated_data and instance.slack_thread != None and validated_data["agent"] != None:
            
            agent_name = validated_data["agent"].username
            agent_uid_query = UserSlackIDModel.objects.filter(user=validated_data["agent"])
            agent_slack_uid = '<@' + agent_uid_query.first().uid + ">" if len(agent_uid_query) > 0 else ''
            username = self.context['request'].user.username

            slack_message = [
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"*" + username + "* ha transferido este ticket a " + agent_name + ". " + agent_slack_uid
                        }
                    }
                ]

            self.slack_message(instance, slack_message)

        if validated_data.get("internal_status",None) == 3:

            creator_uid_query = UserSlackIDModel.objects.filter(user=instance.creator)
            creator_uid = '<@' + creator_uid_query.first().uid + ">" if len(creator_uid_query) > 0 else ''

            slack_message = [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "El ticket se ha marcado como completado ¿El problema se ha solucionado? " + creator_uid
                    }
                },
                {
                    "type": "actions",
                    "elements": [
                        {
                            "type": "button",
                            "text": {
                                "type": "plain_text",
                                "text": "Sí",
                                "emoji": True
                            },
                            "style": "primary",
                            "value": str(instance.ID),
                            "action_id": "accept_ticket"
                        },
                        {
                            "type": "button",
                            "text": {
                                "type": "plain_text",
                                "text": "No",
                                "emoji": True
                            },
                            "style": "danger",
                            "value": str(instance.ID),
                            "action_id": "reject_ticket"
                        }
                    ]
                }
            ]

            self.slack_message(instance, slack_message)

        return super().update(instance, validated_data)

    class Meta:

        #Class model of serializer
        model: BaseModel = TicketModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'team': {
                'required': False
            },
            'comment':{
                'required': False
            },
            'slack_uid':{
                'required': False
            }
        }

class TicketTablesSerializer(BaseSerializer):

    """

        Class define serializer of TicketTableView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = TicketTablesModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'agent': {
                'required': False
            },
        }