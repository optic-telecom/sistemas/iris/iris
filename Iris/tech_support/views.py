import threading
from typing import Any, Dict, List

from django.contrib.auth.models import User, Group
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.viewsets import BaseViewSet
from django.http import JsonResponse, HttpResponseForbidden
from rest_framework.decorators import action
from rest_framework.serializers import ValidationError
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from dynamic_preferences.registries import global_preferences_registry

from .datatables import (TicketDatatable, TicketTablesDatatable)
from .models import (TicketModel, TicketTablesModel)
from .serializers import (TicketSerializer, TicketTablesSerializer)
from .documentation import (DocumentationSupportTicket)

class TicketView(BaseViewSet):

    """

        Class define ViewSet of TicketModel.
    
    """

    queryset: BaseModel = TicketModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TicketSerializer
    documentation_class = DocumentationSupportTicket()

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('tech_support.view_ticketmodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()
    
    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('tech_support.add_ticketmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)
    
    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('tech_support.change_ticketmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tech_support.delete_ticketmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('tech_support.view_ticketmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """
        ticket_datatables = TicketDatatable(request)
        ticket_datatables.ID_TABLE = pk

        return JsonResponse(ticket_datatables.get_struct(), safe=True)

    @documentation_class.documentation_datatables()
    @method_decorator(permission_required('tech_support.view_ticketmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        ticket_datatables = TicketDatatable(request)
        ticket_datatables.ID_TABLE = pk

        return ticket_datatables.get_data()

    @documentation_class.documentation_get_system_groups()
    @method_decorator(permission_required('tech_support.view_ticketmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def system_groups(self, request) -> JsonResponse:

        system_groups = Group.objects.filter(name__startswith="Sistema")
        data = []

        for group in system_groups:
            data.append({'name': group.name, 'ID': group.pk})

        return JsonResponse(data, safe=False)
    
    @documentation_class.documentation_update_tech_support_channel()
    @method_decorator(permission_required('tech_support.view_ticketmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def update_channel(self, request) -> JsonResponse:
        
        global_preferences = global_preferences_registry.manager()

        channel: str = request.POST['channel']

        global_preferences['tech_support__slack_channel'] = channel
      
        return JsonResponse( data={} )

    @documentation_class.documentation_get_tech_support_channel()
    @method_decorator(permission_required('tech_support.view_ticketmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def get_channel(self, request) -> JsonResponse:
        
        global_preferences = global_preferences_registry.manager()

        channel = global_preferences['tech_support__slack_channel'] 

        return JsonResponse( data={"channel": channel} )

    @method_decorator(permission_required('tech_support.view_ticketmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request, pk):

            ticket_datatables = TicketDatatable(request)
            ticket_datatables.ID_TABLE = pk
            ticket_datatables.download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request, pk]
        ).start()

        return JsonResponse(data={})

class TicketTableView(BaseViewSet):

    """

        Class define ViewSet of TicketTablesModel.
    
    """

    queryset: BaseModel = TicketTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TicketTablesSerializer
    
    @method_decorator(permission_required('tech_support.add_tickettablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('tech_support.change_tickettablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tech_support.delete_tickettablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('tech_support.add_tickettablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Creador":{
                    "type":"choices",
                    "name":"creator__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Estado":{
                    "type":"choices",
                    "name":"status",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "Iniciado",
                            1: "Aceptado",
                            2: "Rechazado",
                            3: "En Proceso",
                            4: "Cerrado"
                        }
                    }
                },
                "Agente Asignado":{
                    "type":"choices",
                    "name":"agent__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Sistema Asignado":{
                    "type":"choices",
                    "name":"asigned_team__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "tech_support/ticket/system_groups/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'creator': "Creador",
                'ID': "ID",
                'updated': "Última Actualización",
                'agent': "Agente Asignado",
                'assigned_team': "Sistema Asignado", 
                'trello_urls': "Tarjetas Trello",
                'commits': "Commits",
                "SLA": "SLA",
                "status": "Estado"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('tech_support.add_tickettablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketTableView
            :type self: TicketTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TicketTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('tech_support.add_tickettablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketTableView
            :type self: TicketTableView
            :param request: data of request
            :type request: request
        
        """

        return TicketTablesDatatable(request).get_data()