import requests
import openpyxl
from typing import IO, List, Union, Dict, Any
from openpyxl.workbook import Workbook
from openpyxl.styles import PatternFill

from django.core.management.base import BaseCommand
from dynamic_preferences.registries import global_preferences_registry
from formerCustomers.models import RemoveEquipamentModel

class Command(BaseCommand):

    help = 'Register remove equipment previous status'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('file')

    def handle(self, *args, **options):

        def iter_rows(ws):
            for row in ws.iter_rows():
                yield [cell.value for cell in row]

        failed_items = []
        wb = openpyxl.load_workbook(filename=options['file'], data_only=True)
        o_sheet = wb["Sheet1"]

        for index,element in enumerate(iter_rows(o_sheet)):

            remove_equip_query = RemoveEquipamentModel.objects.filter(service=int(element[1]))

            for remove_equip in remove_equip_query:
                
                try:
                    remove_equip.previous_state = int(element[3])
                    remove_equip.save()
                except:
                    failed_items.append(element)

        
        if len(failed_items) > 0:
            failed_wb = Workbook()

            ws1 = failed_wb.active

            for item in failed_items:
                ws1.append(item)
                
            failed_wb.save(filename="Registos Fallidos.xlsx")