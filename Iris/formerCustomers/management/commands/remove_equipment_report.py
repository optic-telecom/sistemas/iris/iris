import openpyxl
from typing import IO, List, Union, Dict, Any
from openpyxl.workbook import Workbook

from django.core.management.base import BaseCommand
from dynamic_preferences.registries import global_preferences_registry
from formerCustomers.models import RemoveEquipamentModel

class Command(BaseCommand):

    help = 'Creates an excel with the data of all the instances of Remove Equipment'

    def handle(self, *args, **options):

        remove_equip_list = []

        for remove_equip_instance in RemoveEquipamentModel.objects.filter(deleted=False):

            remove_equip_list.append([remove_equip_instance.ID, remove_equip_instance.service, remove_equip_instance.created])

        failed_wb = Workbook()

        ws1 = failed_wb.active

        for item in remove_equip_list:
            ws1.append(item)
            
        failed_wb.save(filename="Reporte Retiros.xlsx")