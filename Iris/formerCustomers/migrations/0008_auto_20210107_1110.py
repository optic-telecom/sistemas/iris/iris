from django.db import migrations, models

def delete_remove_equip(apps, scheme_editor):
    RemoveEquipInstances = apps.get_model('formerCustomers', 'RemoveEquipamentModel')
    RemoveEquipInstances.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [
        ('formerCustomers', '0007_auto_20201230_1110'),
    ]

    operations = [
        migrations.RunPython(delete_remove_equip),
    ]