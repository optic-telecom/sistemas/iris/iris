# Generated by Django 2.1.7 on 2021-01-20 15:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('formerCustomers', '0014_auto_20210120_0856'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalremoveequipamentmodel',
            name='agent',
        ),
        migrations.RemoveField(
            model_name='removeequipamentmodel',
            name='agent',
        ),
    ]
