from django.db import migrations, models

def delete_remove_equip(apps, scheme_editor):
    RemoveEquipInstances = apps.get_model('formerCustomers', 'RemoveEquipamentModel')
    RemoveEquipInstances.objects.all().delete()
    RemoveEquipTableInstances = apps.get_model('formerCustomers', 'RemoveEquipamentTablesModel')
    RemoveEquipTableInstances.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [
        ('formerCustomers', '0012_historicalremoveequipamentchannelmodel_removeequipamentchannelmodel'),
    ]

    operations = [
        migrations.RunPython(delete_remove_equip),
    ]