# Generated by Django 2.1.7 on 2021-05-04 18:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('formerCustomers', '0028_auto_20210504_1414'),
    ]

    operations = [
        migrations.RenameField(
            model_name='historicalremoveequipamentmodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RenameField(
            model_name='historicalremoveequipamentphotomodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RenameField(
            model_name='historicalremoveequipamenttablesmodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RenameField(
            model_name='removeequipamentmodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RenameField(
            model_name='removeequipamentphotomodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RenameField(
            model_name='removeequipamenttablesmodel',
            old_name='operator_instance',
            new_name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalremoveequipamentchannelmodel',
            name='operator_instance',
        ),
        migrations.RemoveField(
            model_name='historicalremoveequipamentemailmodel',
            name='operator_instance',
        ),
        migrations.RemoveField(
            model_name='removeequipamentchannelmodel',
            name='operator_instance',
        ),
        migrations.RemoveField(
            model_name='removeequipamentemailmodel',
            name='operator_instance',
        ),
    ]
