# Generated by Django 2.1.7 on 2021-01-07 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formerCustomers', '0009_auto_20210107_1040'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalremoveequipamentmodel',
            name='serial',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='historicalremoveequipamentmodel',
            name='service',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='removeequipamentmodel',
            name='serial',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='removeequipamentmodel',
            name='service',
            field=models.PositiveIntegerField(null=True),
        ),
    ]
