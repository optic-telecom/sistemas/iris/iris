# Generated by Django 2.1.7 on 2021-03-29 15:26

from django.db import migrations, models

def restore_tables(apps, schema_editor):
    RemoveEquipTables = apps.get_model('formerCustomers', 'RemoveEquipamentTablesModel')

    for table in RemoveEquipTables.objects.filter(deleted=False):

        if 'status_ti' not in table.columns:
            new_columns = table.columns
            new_columns.append('status_ti')
            table.columns = new_columns
            table.save()

def update_tables(apps, schema_editor):
    RemoveEquipTables = apps.get_model("formerCustomers", "RemoveEquipamentTablesModel")

    for table in RemoveEquipTables.objects.filter(deleted=False):

        if 'status_ti' in table.columns:
            new_columns = table.columns
            new_columns.remove('status_ti')
            table.columns = new_columns
            table.save()

class Migration(migrations.Migration):

    dependencies = [
        ('formerCustomers', '0024_auto_20210326_1239'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalremoveequipamentmodel',
            name='status_ti',
        ),
        migrations.RemoveField(
            model_name='removeequipamentmodel',
            name='status_ti',
        ),
        migrations.AddField(
            model_name='historicalremoveequipamentmodel',
            name='email_sended',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='historicalremoveequipamentmodel',
            name='removed_from_matrix',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='historicalremoveequipamentmodel',
            name='removed_from_net',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='removeequipamentmodel',
            name='email_sended',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='removeequipamentmodel',
            name='removed_from_matrix',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='removeequipamentmodel',
            name='removed_from_net',
            field=models.BooleanField(default=False),
        ),
        migrations.RunPython(update_tables, restore_tables)
    ]
