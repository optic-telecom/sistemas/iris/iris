from typing import List

from django.contrib import admin

from .models import (RemoveEquipamentModel, RemoveEquipamentTablesModel, RemoveEquipamentEmailModel, RemoveEquipamentChannelModel)


class RemoveEquipamentAdmin(admin.ModelAdmin):

    fields: List[str] = ["agent_ti", "agent_sac", "agent_storage", "rut", "service", "serial", "iclass_id", "status", 
    "status_agendamiento", "status_storage", "removed_from_net", "email_sended", "removed_from_matrix", "slack_thread"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "rut", "service", "serial", "status","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "rut", "service"]

    #Define model data list ordening
    ordering: List[str] = ["rut"]

class RemoveEquipamentTablesAdmin(admin.ModelAdmin):
    
    fields = ['name', 'columns', 'last_time', "last_time_type", "operator", "create_me", "assigned_to_me", "filters" ]

    #list of fields to display in django admin
    list_display = ['ID','name','operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

class RemoveEquipamentEmailAdmin(admin.ModelAdmin):

    fields: List[str] = ["sender", "template_email"]

    #List of fields to display in django admin
    list_display: List[str] = ["sender", "template_email","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["sender"]

    #Define model data list ordening
    ordering: List[str] = ["template_email"]

class RemoveEquipamentChannelAdmin(admin.ModelAdmin):

    fields: List[str] = ["channel","operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["channel","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["channel"]

    #Define model data list ordening
    ordering: List[str] = ["channel"]

admin.site.register(RemoveEquipamentModel, RemoveEquipamentAdmin)
admin.site.register(RemoveEquipamentTablesModel, RemoveEquipamentTablesAdmin)
admin.site.register(RemoveEquipamentEmailModel, RemoveEquipamentEmailAdmin)
admin.site.register(RemoveEquipamentChannelModel, RemoveEquipamentChannelAdmin)
