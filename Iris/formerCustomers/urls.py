from rest_framework.routers import DefaultRouter

from .views import RemoveEquipamentView, RemoveEquipamentTableView, RemoveEquipamentEmailView, RemoveEquipamentChannelView, RemoveEquipamentPhotoView
router = DefaultRouter()

router.register(r'remove_equipament', RemoveEquipamentView, basename='remove_equipament')
router.register(r'remove_equipament_tables', RemoveEquipamentTableView, basename='remove_equipament_tables')
router.register(r'remove_equipament_email', RemoveEquipamentEmailView, basename='remove_equipament_email')
router.register(r'remove_equipament_channel', RemoveEquipamentChannelView, basename='remove_equipament_channel')
router.register(r'remove_equipament_photo', RemoveEquipamentPhotoView, basename='remove_equipament_photo')

urlpatterns = router.urls