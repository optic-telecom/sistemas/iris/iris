from common.dataTables import BaseDatatables
from common.models import BaseModel
from django.db.models.query import QuerySet, EmptyQuerySet
from django.db.models import Q
from django.contrib.auth.models import User
from django.http import JsonResponse
from dateutil.relativedelta import relativedelta
from datetime import timedelta, datetime
from functools import reduce

from .models import RemoveEquipamentModel, RemoveEquipamentTablesModel
from dynamic_preferences.registries import global_preferences_registry

from typing import List, Dict, Any
import requests
import random


class RemoveEquipamentTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]


    model: BaseModel = RemoveEquipamentTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs
        

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class RemoveEquipamentDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "rut", "created", "updater", "updated", "service", "serial", "status",
    "agent_ti", "agent_sac", "agent_storage"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "rut":"str",
        "status":"int",
        "removed_from_net": "bool",
        "email_sended": "bool",
        "removed_from_matrix": "bool",
        "status_storage": "int",
        "status_agendamiento": "int",
    }
    FIELDS_EXCEL: Dict[str, Dict[str, str]] = {
        'A': 'Número de retiro',
        'B': 'Fecha de creación',
        'C': 'Agente creador',
        'D': 'Ultima fecha de actualización',
        'E': 'Ultimo agente en actualizar',
        'F': 'RUT',
        'G': 'Nombre del cliente',
        'H': 'Comuna',
        'I': 'Dirección',
        'J': 'Servicio',
        'K': 'Serial',
        'L': 'Estado',
        'M': 'Estado SAC',
        'N': 'Estado Terreno',
        'O': 'Agente SAC',
        'P': 'Agente Terreno',
        'Q': 'Agente TI',
        'R': 'Correo Enviado',
        'S': 'Retirado en Matrix',
        'T': 'Retirado de Red',
        'U': 'Estado actual en Matrix',
        'V': 'Estado previo en Matrix'
    }

    model: BaseModel = RemoveEquipamentModel

    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_remove_equipament: QuerySet = RemoveEquipamentTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_remove_equipament.filters

        data_remove_equipament: QuerySet = super().get_filtered_queryset()

        #Tickets assigned to request user
        if instance_tables_remove_equipament.assigned_to_me:

            user: User = self.request.user
            original_query: QuerySet = data_remove_equipament
            resultQuery = data_remove_equipament.none()

            if user.groups.filter(name = "Agente SAC").exists():
                data_remove_equipament_sac: QuerySet = original_query.filter(agent_sac=user)
                resultQuery = data_remove_equipament_sac.union(resultQuery)

            if user.groups.filter(name = "Agente Terreno").exists():
                data_remove_equipament_storage: QuerySet = original_query.filter(agent_storage=user)
                resultQuery = data_remove_equipament_storage.union(resultQuery)

            if user.groups.filter(name = "Agente TI").exists():
                data_remove_equipament_ti: QuerySet = original_query.filter(agent_ti=user)
                resultQuery = data_remove_equipament_ti.union(resultQuery)

            if resultQuery != None:
                return resultQuery
            else: 
                return EmptyQuerySet

        #Tickets created to request user
        if instance_tables_remove_equipament.create_me:

            data_remove_equipament: QuerySet = data_remove_equipament.filter(creator=self.request.user)

        #Tickets time interval
        if instance_tables_remove_equipament.last_time_type and instance_tables_remove_equipament.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_remove_equipament: QuerySet = data_remove_equipament.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_remove_equipament.last_time_type] * instance_tables_remove_equipament.last_time) )

        return data_remove_equipament

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = str(self.SEARCH)

        if search:

            if search.isdigit():

                int_value: int = int(search)
                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: int_value == x.service, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(ID=int_value) |
                    Q(ID__in=list_typify_id)
                )

            else:

                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: search == x.serial, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(rut__icontains=search) |
                    Q(ID__in=list_typify_id)
                )

        return qs

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """
        
        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )
        qs_size = len(qs)
        
        if qs:

            #Esto es para obtener solo los registros que estoy buscando

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]

            global_preferences: Dict[str, Any] = global_preferences_registry.manager()

            customer = {}
            address = {}
            matrix_status = {}
            commune = {}

            matrix_url = global_preferences['general__matrix_domain'] + 'services/'
            matrix_headers: Dict[str, str] = {
            'AUTHORIZATION': global_preferences['matrix__matrix_token']
            }

            for removeEquip in qs:

                response = requests.get(
                    url=matrix_url + f"?number={removeEquip.service}",
                    headers=matrix_headers,
                    verify=False
                ).json()['results'][0]

                address[removeEquip.service] = response['composite_address']
                matrix_status[removeEquip.service] = response['get_status_display']
                commune[removeEquip.service] = response['commune']

                customer_name = requests.get(
                    url=response['customer'],
                    headers=matrix_headers,
                    verify=False
                ).json()['name']

                customer[removeEquip.service] = customer_name

            self.address= address
            self.matrix_status = matrix_status
            self.customer = customer
            self.commune = commune
            
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )
        
        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "rut": instance.rut,
            "ID": instance.ID,
            "created": instance.created,
            "service": instance.service,
            "serial": instance.serial,
            "updater": instance.updater.username,
            "updated": instance.updated,
            "status": instance.status,
            "status_agendamiento": [instance.status_agendamiento, instance.serial, instance.status],
            "status_storage": [instance.status_storage, instance.status_agendamiento, instance.status],
            "agent_ti": instance.agent_ti.username if instance.agent_ti != None else None,
            "agent_sac": instance.agent_sac.username if instance.agent_sac != None else None,
            "agent_storage": instance.agent_storage.username if instance.agent_storage != None else None,
            "customer": self.customer[instance.service],
            "commune": self.commune[instance.service],
            "address": self.address[instance.service],
            "matrix_status": self.matrix_status[instance.service],
            "history": instance.ID,
            "email_sended": instance.email_sended,
            "removed_from_matrix": instance.removed_from_matrix,
            "removed_from_net": instance.removed_from_net,
            "previous_state": instance.get_previous_state_display()
        }

    def get_qs_to_data(self, qs: QuerySet) -> List[Dict[str, Any]]:

        def get_retirement_data(instance):

            result = [] 

            global_preferences: Dict[str, Any] = global_preferences_registry.manager()

            matrix_url = global_preferences['general__matrix_domain'] + 'services/'
            matrix_headers: Dict[str, str] = {
            'AUTHORIZATION': global_preferences['matrix__matrix_token']
            }

            response = requests.get(
                url=matrix_url + "?number=" + str(instance.service),
                headers=matrix_headers,
                verify=False
            ).json()

            instance.address = response['results'][0]['composite_address']
            instance.matrix_status = response['results'][0]['get_status_display']
            instance.commune = response['results'][0]['commune']

            customer_name = requests.get(
                url=response['results'][0]['customer'],
                headers=matrix_headers,
                verify=False
            ).json()['name']

            instance.customer = customer_name

            result.append([
                instance.ID,
                instance.created - timedelta(hours=5),
                instance.creator.username if instance.creator else '',
                instance.updated - timedelta(hours=5),
                instance.updater.username if instance.creator else '',
                instance.rut,
                instance.customer,
                instance.commune,
                instance.address,
                instance.service,
                instance.serial,
                instance.get_status_display(),
                instance.get_status_agendamiento_display(),
                instance.get_status_storage_display(),
                instance.agent_sac.username if instance.agent_sac else '',
                instance.agent_storage.username if instance.agent_storage else '',
                instance.agent_ti.username if instance.agent_ti else '',
                instance.email_sended,
                instance.removed_from_matrix,
                instance.removed_from_net,
                instance.matrix_status,
                instance.get_previous_state_display()
            ])

            return result

        return reduce( lambda x, y: x + y, map(get_retirement_data, qs) )

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900,
            "download":True
        }

        data_struct["fields"] = {
            "ID": "int",
            "rut": "str",
            "created": "datetime",
            "updated": "datetime",
            "updater": "str",
            "service": "int",
            "status": "int",
            "serial": "str",
        }

        columns = {

            'ID':{
                "sortable": True,
                "name":"ID",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Fecha de Creación",
                "width":100,
                "fixed":None
            },
            'updater':{
                "sortable": True,
                "name":"Actualizado",
                "width":100,
                "fixed":None
            },
            'updated':{
                "sortable": True,
                "name":"Última actualización",
                "width":100,
                "fixed":None
            },
            'rut':{
                "sortable": True,
                "name":"Cliente",
                "width":100,
                "fixed":None
            },
            'service':{
                "sortable": True,
                "name":"Servicio",
                "width":100,
                "fixed":None
            },
            'serial':{
                "sortable": False,
                "name":"Serial",
                "width":100,
                "fixed":None
            },
            'status':{
                "sortable": True,
                "name":"Estado",
                "width":220,
                "fixed":None
            },
             'status_agendamiento':{
                "sortable": False,
                "name":"Estado SAC",
                "width":220,
                "fixed":None
            },
             'status_storage':{
                "sortable": False,
                "name":"Estado Almacén",
                "width":220,
                "fixed":None
            },
            'history':{
                "sortable": False,
                "name":"Historial",
                "width":220,
                "fixed":None
            },
            'customer':{
                "sortable": False,
                "name":"Nombre del Cliente",
                "width":200,
                "fixed":None
            },
            'agent_ti':{
                "sortable": True,
                "name":"Agente TI",
                "width":100,
                "fixed":None
            },
            'agent_sac':{
                "sortable": True,
                "name":"Agente SAC",
                "width":100,
                "fixed":None
            },
            'agent_storage':{
                "sortable": True,
                "name":"Agente Terreno",
                "width":100,
                "fixed":None
            },
            'commune':{
                "sortable": False,
                "name":"Comuna",
                "width":100,
                "fixed":None
            },
            'address':{
                "sortable": False,
                "name":"Dirección",
                "width":300,
                "fixed":None
            },
            'matrix_status':{
                "sortable": False,
                "name":"Estado en Matrix",
                "width":100,
                "fixed":None
            },
            'email_sended':{
                "sortable": False,
                "name":"Correo Enviado",
                "width":100,
                "fixed":None
            },
            'removed_from_matrix':{
                "sortable": False,
                "name":"Retirado en Matrix",
                "width":100,
                "fixed":None
            },
            'removed_from_net':{
                "sortable": False,
                "name":"Retirado de Red",
                "width":100,
                "fixed":None
            },
            'previous_state':{
                "sortable": False,
                "name": "Estado previo en Matrix",
                "width":100,
                "fixed":None
            },
        }

        instance_tables_remove: QuerySet = RemoveEquipamentTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_remove.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1

        data_struct_columns["Modificar"] = {
            "field": "custom_change",
            "sortable": False,
            "visible": True,
            "position": position,
            "width":100,
            "fixed":"right",
            "download": True,
        }

        data_struct["columns"] = data_struct_columns

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Rut del Cliente":{
                "type":"str",
                "name":"rut"
            },
            "Estado":{
                "type":"choices",
                "name":"status",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'ABIERTO',
                        1:'CERRADO',
                        2:'CANCELADO',
                    }
                }
            },
            "Estado SAC":{
                "type":"choices",
                "name":"status_agendamiento",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'REGISTRADO EN ICLASS',
                        1:'ORDEN CREADA',
                        2:'RETIRAR ORDEN',
                    }
                }
            },
            "Estado ALMACEN":{
                "type":"choices",
                "name":"status_storage",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'EN ALMACEN',
                        1:'EN BODEGA',
                        2:'PROCESAR EN ICLASS',
                    }
                }
            },
            "Correo Enviado":{
                    "type":"bool",
                    "name":"email_sended",
                },
            "Retirado en Matrix":{
                "type":"bool",
                "name":"removed_from_matrix",
            },
            "Retirado de Red":{
                "type":"bool",
                "name":"removed_from_net",
            },
        }

        return data_struct
