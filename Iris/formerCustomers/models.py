from typing import List, Tuple, Union, Any, Set, Dict, Callable

import requests
from datetime import datetime
from common.models import BaseModel
from common.utilitarian import get_token, matrix_url
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_q.models import Schedule
from rest_framework.serializers import ValidationError
from communications.models import TemplateEmailModel

class RemoveEquipamentModel(BaseModel):

    def clean(self):

        def validate_services( service: int, operator: int, rut: str ):

            url_services: str = matrix_url('services')
            url_services: str = f'{url_services}?fields=number&customer__rut={rut}&operator={operator}'
            
            services_check: List[int] = requests.get(
                url=url_services,
                verify=False,
                headers={
                    "Authorization": get_token()
                }
            ).json()

            if len(services_check )== 0 or service not in services_check[0].values():

                raise ValidationError({
                    'Error services value': f"Invalid services: {service}"
                })

        if self.service:

            if self.rut == None:
                raise ValidationError({ 
                'rut': ['This field is required.']})

            # validate_services(self.service, self.operator, self.rut)
            # RemoveEquipamentModel.validate_rut(self.rut)
        

    def validate_rut(value: str):

        """
        
            This function, validate customer rut
        
        """

        url: str = matrix_url('customers') + f'?fields=id&rut={value}'
        token: str = get_token()
        
        response = requests.get(
            url=url,
            headers={
                'Authorization': f'JWT {token}'
            },
            verify=False,
        ).json()

        if not response:

            raise ValidationError({
                "Don't exist": "There is no customer with that rut"
            })

    def validate_seriales(value: str):

        if type(value) != str:

            raise ValidationError({
                f"Invalid {value} value": "must be str type"
            })


    agent_ti = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name='agent_user_ti_remove_equipament',
        on_delete=models.SET_NULL,
    )

    agent_sac = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name='agent_user_sac_remove_equipament',
        on_delete=models.SET_NULL,
    )

    agent_storage = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name='agent_user_storage_remove_equipament',
        on_delete=models.SET_NULL,
    )

    rut: str = models.CharField(max_length=20, null=False, default="")
    service: List[int] = models.PositiveIntegerField(null=False, default=0)
    serial: List[str] = models.CharField(max_length=20, null=True)
    iclass_id: int = models.PositiveIntegerField(blank=True, null=True)

    STATUS_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'ABIERTO'),
        (1, 'CERRADO'), 
        (2, 'CANCELADO')
    )

    STATUS_AGENDAMIENTO_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'REGISTRADO EN ICLASS'),
        (1, 'ORDEN CREADA'),
        (2, 'ORDEN CONFIRMADA'),
    )

    STATUS_STORAGE_CHOICES : Tuple[Tuple[int, str]] =(
        (0, 'EN ALMACEN'),
        (1, 'EN BODEGA'),
        (2, 'PROCESAR EN ICLASS'),
        (3, 'INGRESAR SERIAL'),
    )

    status: int = models.PositiveIntegerField(choices=STATUS_CHOICES,default=0)
    status_agendamiento: int = models.PositiveIntegerField(choices=STATUS_AGENDAMIENTO_CHOICES, null=True)
    status_storage: int = models.PositiveIntegerField(choices=STATUS_STORAGE_CHOICES, null=True)
    removed_from_net: bool = models.BooleanField(default=False)
    email_sended: bool = models.BooleanField(default=False)
    removed_from_matrix: bool = models.BooleanField(default=False)
    slack_thread: List[int] = JSONField(default=dict, null=True, blank=True)
    
    PREVIOUS_STATUS_CHOICES: Tuple[Tuple[int, str]] = (
        (1, 'ACTIVO'),
        (2, 'POR RETIRAR'), 
        (3, 'RETIRADO'),
        (4, 'NO INSTALADO'),
        (5, 'CANJE'),
        (6, 'INSTALACIÓN RECHAZADA'),
        (7, 'MOROSO'),
        (8, 'CAMBIO DE TITULAR'),
        (9, 'DESCARTADO'),
        (10, 'BAJA TEMPORAL'),
        (11, 'PERDIDO, NO SE PUDO RETIRAR')
    )
    previous_state: int = models.PositiveIntegerField(choices=PREVIOUS_STATUS_CHOICES,default=1,blank=False,null=False)
    order_start = models.DateTimeField(null=True)
    order_end = models.DateTimeField(null=True)

class RemoveEquipamentTablesModel(BaseModel):

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "agent__pk":"choices",
                    "status":"choices",
                    "status_storage":"choices",
                    "status_agendamiento":"choices",
                    "email_sended": "bool",
                    "removed_from_matrix": "bool",
                    "removed_from_net": "bool"
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'updater', 'created', 'updated', 'service', 'email_sended', 'removed_from_matrix', 'removed_from_net',
                                            'status', 'serial', 'rut', 'customer', 'matrix_status', 'address', 'history', 'previous_state',
                                            'commune', 'status_agendamiento', 'status_storage', 'agent_ti', 'agent_sac', 'agent_storage'
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class RemoveEquipamentEmailModel(BaseModel):
    
    """

        Class define model of RemoveEquipment Email.
    
    """

    
    #Sender of email message
    sender: str = models.TextField(blank=False, null=False, default='')
    #Template HTML of email message
    template_email: int = models.ForeignKey( TemplateEmailModel, on_delete=models.PROTECT, null=False)

class RemoveEquipamentChannelModel(BaseModel):
    
    """

        Class define model of RemoveEquipment Channel.
    
    """

    
    #Sender of email message
    channel: str = models.TextField(blank=False, null=False, default='')
    
class RemoveEquipamentPhotoModel(BaseModel):
    
    """

        Class define model of RemoveEquipamentPhoto
    
    """

    image = models.ImageField(upload_to='remove_equipaments')
    remove_equipament = models.ForeignKey(
        RemoveEquipamentModel,
        null=False,
        blank=False,
        on_delete=models.PROTECT
    )