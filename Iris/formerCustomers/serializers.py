import requests
import base64
import json
import string
import random
from jinja2 import Template
from datetime import datetime, date, timedelta

from magic import from_buffer
from mimetypes import guess_extension
from typing import Dict, Any, Union, List
from slack import WebClient

from rest_framework.serializers import ValidationError

from .models import RemoveEquipamentTablesModel, RemoveEquipamentModel, RemoveEquipamentEmailModel, RemoveEquipamentChannelModel, RemoveEquipamentPhotoModel
from .api import send_remove_equip_email
from common.models import BaseModel
from django_q.models import Schedule
from dynamic_preferences.registries import global_preferences_registry
from communications.models import TemplateEmailModel
from customers.models import CustomerModel
from common.utilitarian import get_token, matrix_url
from django.core.files.base import ContentFile
from django.contrib.auth.models import User
from common.serializers import BaseSerializer
from communications.api import create_email

class RemoveEquipamentTablesSerializer(BaseSerializer):

    """

        Class define serializer of RemoveEquipamentView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = RemoveEquipamentTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: RemoveEquipamentTablesModel, validated_data: Dict[str, Any]) -> RemoveEquipamentTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)


class RemoveEquipamentSerializer(BaseSerializer):

    """

        Class define serializer of RemoveEquipamentView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = RemoveEquipamentModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def slack_message(self, instance_model : RemoveEquipamentModel):

        def send_message(slack_message: RemoveEquipamentChannelModel, instance_model : RemoveEquipamentModel):

            def search_value(field: str, instance_model : RemoveEquipamentModel) -> Any:

                _callable: Dict[str, callable()] = {
                    'agent_sac': lambda instance: instance.agent_sac.username if instance.agent_sac != None else " ",
                    'service': lambda instance: instance.service, 
                    'rut': lambda instance: instance.rut, 
                    'status': lambda instance: instance.get_status_display(),
                }

                return _callable[field](instance_model)

            data: Dict[str, Any] = {}
            fields: List = ['agent_sac', 'service', 'rut', 'status']
            for field in fields:
                data[field] = search_value(field, instance_model)

            global_preferences = global_preferences_registry.manager()

            client = WebClient(token=instance_model.operator.slack_token)

            labels: Dict[str,str] = {
                "agent_sac": f"Agente SAC: {data.get('agent_agendamiento')} \n", 
                "service": f"Servicio: {data.get('service')}\n", 
                "rut": f"Rut: {data.get('rut')}\n",               
                "status": f"Estado: {data.get('status')}\n", 
            }           

            items: str = ""
            for item in data:
                items = items + labels[item]
           
            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Retiro de equipos"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":pencil:Se ha creado un nuevo retiro de equipo. ID: *{instance_model.pk}*"
                    }
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": items
                    }
                },
                {
                    "type": "divider"
                }
            ]

            response = client.chat_postMessage(
                channel=slack_message.channel,
                blocks=blocks,
                text=f"Se ha creado un nuevo retiro de equipo. ID: {instance_model.pk}"
            )

            return response.data['ts']

        threads = {}

        try:
            slack_channel = RemoveEquipamentChannelModel.objects.get(operator=instance_model.operator)
        except:
            raise ValidationError({"slack channel":"Theres no remove equipment slack channel assigned to that operator"})

        thread = send_message(slack_channel, instance_model)
        threads[thread] = slack_channel.channel
            
        instance_model.slack_thread = threads
        instance_model.save()

    def create(self, validated_data: Dict[str, Any]) -> RemoveEquipamentModel:

        """
        
            This funtion validate and create instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """
        result = super().create(validated_data)
        self.slack_message(result)
        return result

    def update(self, instance: RemoveEquipamentModel, validated_data: Dict[str, Any]) -> RemoveEquipamentModel:

        def slack_message(instance: RemoveEquipamentModel, validated_data: Dict[str,Any]):

                def send_message(thread: str, channel: str, validated_data: Dict[str,Any], instance: RemoveEquipamentModel):

                    def search_value(field: str, instance_model : RemoveEquipamentModel) -> Any:

                        _callable: Dict[str, callable()] = {
                            'agent_sac': lambda instance: validated_data.get("agent_sac").username, 
                            'agent_storage': lambda instance: validated_data.get("agent_storage").username, 
                            'agent_ti': lambda instance: validated_data.get("agent_ti").username, 
                            'rut': lambda instance: validated_data.get('rut'),
                            'service': lambda instance: validated_data.get('service'),  
                            'liveagent': lambda instance: validated_data.get('liveagent'),
                            'serial': lambda instance: validated_data.get('serial'),
                            'iclass_id': lambda instance: validated_data.get('iclass_id'),
                            'status': lambda instance: RemoveEquipamentModel.STATUS_CHOICES[validated_data.get('status')][1],
                            'status_agendamiento': lambda instance: RemoveEquipamentModel.STATUS_AGENDAMIENTO_CHOICES[validated_data.get('status_agendamiento')][1],
                            'status_storage': lambda instance: RemoveEquipamentModel.STATUS_STORAGE_CHOICES[validated_data.get('status_storage')][1],
                            'order_start': lambda instance: validated_data.get('order_start'),
                            'order_end': lambda instance: validated_data.get('order_end'),
                            'removed_from_net': lambda instance: validated_data.get('removed_from_net'),
                            'email_sended': lambda instance: validated_data.get('email_sended'),
                            'removed_from_matrix': lambda instance: validated_data.get('removed_from_matrix'),
                        }

                        return _callable[field](validated_data)

                    data: Dict[str, Any] = {}
                    for field in validated_data:
                        if field != 'operator':
                            data[field] = search_value(field, validated_data)
                    
                    labels: Dict[str,str] = {
                        "agent_sac": f"Agente SAC: {data.get('agent_sac')} \n", 
                        "agent_storage": f"Agente Terreno: {data.get('agent_storage')}\n", 
                        "agent_ti": f"Agente TI: {data.get('agent_ti')}\n", 
                        "service": f"Servicio: {data.get('service')}\n", 
                        "rut": f"Rut: {data.get('rut')}\n", 
                        "serial": f"Serial: {data.get('serial')}\n", 
                        "iclass_id": f"ID de iclass: {data.get('iclass_id')}\n",                 
                        "status": f"Estado: {data.get('status')}\n", 
                        "status_agendamiento": f"Estado SAC: {data.get('status_agendamiento')}\n", 
                        "status_storage": f"Estado Almacén: {data.get('status_storage')}\n", 
                        "order_start": f"Inicio agendamiento: {data.get('order_start')}\n", 
                        "order_end": f"Inicio agendamiento: {data.get('order_end')}\n", 
                        "removed_from_net": f"Se ha retirado de Red \n", 
                        "email_sended": f"Se ha enviado el correo \n", 
                        "removed_from_matrix": f"Se ha actualizado a retirado en Matrix \n", 
                    }    

                    global_preferences = global_preferences_registry.manager()

                    client = WebClient(token=instance.operator.slack_token)
                    items = " "
                    for item in data:
                        items = items + labels[item]
                    
                    blocks: List[Dict[str, Any]] =  [
                        {
                            "type": "divider"
                        },
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": ":iris: *Iris* Retiro de Equipo"
                            }
                        },
                        {
                            "type": "divider"
                        },
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": f":writing_hand:Se ha actualizado el retiro de equipo"
                            }
                        },
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": items
                            }
                        },
                        {
                            "type": "divider"
                        }
                    ]

                    client.chat_postMessage(
                        blocks=blocks,
                        thread_ts=thread,
                        channel=channel,
                        text=f"Se ha actualizado el retiro de equipo"
                    )

                if instance.slack_thread != None:
                    for thread in instance.slack_thread:
                        send_message(thread, instance.slack_thread[thread], validated_data, instance)

        if 'service' in validated_data:

            if validated_data.get("rut", None) == None:
                raise ValidationError({ 
                'rut': ['This field is required.']})

        update_data = validated_data
        if 'operator' in update_data: del update_data['operator']
        
        if len(update_data) == 0 and instance.agent_sac == None:

            #The first person updating becomes the agent_sac if they have the right permissions
            user: User = self.context['request'].user

            try:
                validated_data["agent_sac"] = User.objects.get(groups__name="Agente SAC", pk=user.pk)
            except:

                raise ValidationError({
                    "Invalid user":"Don´t exist user or invalid group"
                })

        if 'status' in validated_data:

            if validated_data['status'] == 0 or validated_data['status'] == 2:
                
                if instance.iclass_id != None:
                    global_preferences: Dict[str, any] = global_preferences_registry.manager()
                    url: str = global_preferences['iclass__iclass_url'] + 'service_orders/' + str(instance.iclass_id) + '/delete_order/'

                    result: List[Dict[str, Any]] = []
                    status_code: int = 200

                    headers: Dict[str, str] = {
                        'AUTHORIZATION': global_preferences['iclass__iclass_token']
                    }

                    response = requests.delete(
                    url=url + '?motive=Cierre Matrix - Prueba Interna',
                    headers=headers,
                    verify=False,
                    )
            
            if validated_data['status'] == 1:
                if instance.status_agendamiento != 2 or (instance.status_storage != 3 and 'status_storage' not in validated_data):
                    raise ValidationError({
                    "Incorrect value": "Must have ended all previous steps"
                })

        if 'status_agendamiento' in validated_data:

            if instance.status_agendamiento != None and abs(int(instance.status_agendamiento) - int(validated_data['status_agendamiento'])) != 1:
                
                raise ValidationError({
                    "Incorrect value": "Must be consecutive"
                })
    
        if (validated_data.get('removed_from_net', False) and instance.status_storage != None and instance.status_storage > 0) or \
        (validated_data.get('status_storage',0) > 0 and instance.removed_from_net == True):
            
            #Update service status in Matrix
            global_preferences: Dict[str, any] = global_preferences_registry.manager()

            url: str = global_preferences['general__matrix_domain'] + "services/"
            headers: Dict[str, str] = {
                'AUTHORIZATION': global_preferences['matrix__matrix_token']
            }
            params: Dict[str, str] = {'number': instance.service}
            try: 
                response = requests.get(
                    url=url,
                    headers=headers,
                    verify=False,
                    params=params,
                ).json()

                url = url + str(response['results'][0]['id']) + "/"

                data={ 'status': '3'  }

                response2 = requests.patch(
                    url=url,
                    headers=headers,
                    verify=False,
                    data= data
                ).json()

                validated_data['removed_from_matrix'] = True

            except:
                pass
        
        if 'status_storage' in validated_data:

            if validated_data['status_storage'] == 0:

                user: User = self.context['request'].user

                try:
                    validated_data["agent_storage"] = User.objects.get(groups__name="Agente Terreno", pk=user.pk)
                except:

                    raise ValidationError({
                        "Invalid user":"Don´t exist user or invalid group"
                    })
            
            if validated_data['status_storage'] == 3:
                
                #Send email
                try:

                    validated_data['email_sended'] = send_remove_equip_email(instance_pk=instance.ID, current_status=self.context.get("request").POST.get("current_status"), user=self.context['request'].user)
                
                except:

                    schedule_object: Schedule = Schedule.objects.create(
                        func='formerCustomers.api.send_remove_equip_email',
                        name=''.join(random.choice(
                            string.ascii_uppercase + string.digits) for _ in range(20)),
                        schedule_type='O',
                        repeats=1,
                        next_run=datetime.now() + timedelta(minutes=5),
                        kwargs={
                            'instance_pk': instance.ID,
                            'current_status': self.context.get("request").POST.get("current_status"),
                            'user': self.context['request'].user
                        },
                        minutes=5,
                    )

                    schedule_object_two: Schedule = Schedule.objects.create(
                        func='formerCustomers.api.send_remove_equip_email',
                        name=''.join(random.choice(
                            string.ascii_uppercase + string.digits) for _ in range(20)),
                        schedule_type='O',
                        repeats=1,
                        next_run=datetime.now() + timedelta(minutes=15),
                        kwargs={
                            'instance_pk': instance.ID,
                            'current_status': self.context.get("request").POST.get("current_status"),
                            'user': self.context['request'].user
                        },
                        minutes=5,
                    )

                    schedule_object_three: Schedule = Schedule.objects.create(
                        func='formerCustomers.api.send_remove_equip_email',
                        name=''.join(random.choice(
                            string.ascii_uppercase + string.digits) for _ in range(20)),
                        schedule_type='O',
                        repeats=1,
                        next_run=datetime.now() + timedelta(minutes=30),
                        kwargs={
                            'instance_pk': instance.ID,
                            'current_status': self.context.get("request").POST.get("current_status"),
                            'user': self.context['request'].user
                        },
                        minutes=5,
                    )

            if validated_data['status_storage'] == 3 and instance.removed_from_net == True:
                validated_data['status'] = 1

            if instance.status_storage != None and abs(int(instance.status_storage) - int(validated_data['status_storage'])) != 1:

                raise ValidationError({
                    "Incorrect value": "Must be consecutive"
                })
        
        if validated_data.get('removed_from_net', False) and instance.status_storage == 3:
            validated_data['status'] = 1

        slack_message(instance , validated_data)
        return super().update(instance, validated_data)

class RemoveEquipamentEmailSerializer(BaseSerializer):

    
    """

        Class define serializer of RemoveEquipamentEmailView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = RemoveEquipamentEmailModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def create(self, validated_data: Dict[str, Any]) -> RemoveEquipamentEmailModel:

        same_op_email = RemoveEquipamentEmailModel.objects.filter(operator = validated_data['operator'])

        if len(same_op_email) > 0:

            raise ValidationError({
                    'Error same operator': str(same_op_email[0].ID)
                })

        return super().create(validated_data)

    def update(self, instance: RemoveEquipamentEmailModel, validated_data: Dict[str, Any]) -> RemoveEquipamentEmailModel:

        return super().update(instance, validated_data)

class RemoveEquipamentChannelSerializer(BaseSerializer):

    
    """

        Class define serializer of RemoveEquipamentChannelView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = RemoveEquipamentChannelModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def create(self, validated_data: Dict[str, Any]) -> RemoveEquipamentChannelModel:

        same_op_channel = RemoveEquipamentChannelModel.objects.filter(operator = validated_data['operator'])

        if len(same_op_channel) > 0:

            raise ValidationError({
                    'Error same operator': str(same_op_channel[0].ID)
                })

        return super().create(validated_data)

    def update(self, instance: RemoveEquipamentChannelModel, validated_data: Dict[str, Any]) -> RemoveEquipamentChannelModel:

        return super().update(instance, validated_data)

class RemoveEquipamentPhotoSerializer(BaseSerializer):

    
    """

        Class define serializer of RemoveEquipamentChannelView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = RemoveEquipamentPhotoModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }