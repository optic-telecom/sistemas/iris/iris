import requests
from typing import Dict, Any, Union, List
from magic import from_buffer
import base64
import json
from jinja2 import Template
from datetime import datetime, date
from mimetypes import guess_extension
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.viewsets import BaseViewSet
from common.utilitarian import get_token, matrix_url
from django.http import JsonResponse
from django_q.models import Schedule
from django.core.files.base import ContentFile
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.decorators import action
from rest_framework.serializers import (DateTimeField, Serializer,
                                        ValidationError)
from .api import send_remove_equip_email
from .datatables import RemoveEquipamentDatatable, RemoveEquipamentTablesDatatable
from .models import RemoveEquipamentModel, RemoveEquipamentTablesModel, RemoveEquipamentEmailModel, RemoveEquipamentChannelModel, RemoveEquipamentPhotoModel
from .serializers import RemoveEquipamentSerializer, RemoveEquipamentTablesSerializer, RemoveEquipamentEmailSerializer, RemoveEquipamentChannelSerializer, RemoveEquipamentPhotoSerializer
from datetime import datetime
import threading
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse, HttpResponseForbidden


# @method_decorator(permission_required('formerCustomers.view_removeequipamentmodel',raise_exception=True), name='dispatch')
class RemoveEquipamentView(BaseViewSet):

    """

        Class define ViewSet of CategoryModel.
    
    """

    queryset: BaseModel = RemoveEquipamentModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RemoveEquipamentSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('formerCustomers.view_removeequipamentmodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()

    @method_decorator(permission_required('formerCustomers.add_removeequipamentmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.change_removeequipamentmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.change_removeequipamentmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.delete_removeequipamentmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('formerCustomers.view_removeequipamentmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class RemoveEquipamentView
            :type self: RemoveEquipamentView
            :param request: data of request
            :type request: request
        
        """
        typify_datatables = RemoveEquipamentDatatable(request)
        typify_datatables.ID_TABLE = pk

        return JsonResponse(typify_datatables.get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('formerCustomers.view_removeequipamentmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class RemoveEquipamentView
            :type self: RemoveEquipamentView
            :param request: data of request
            :type request: request
        
        """

        typify_datatables = RemoveEquipamentDatatable(request)
        typify_datatables.ID_TABLE = pk

        return typify_datatables.get_data()

    @method_decorator(permission_required('tickets.view_typifymodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifyView
            :type self: TypifyView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request, pk):

            removeEquip_datatables = RemoveEquipamentDatatable(request)
            removeEquip_datatables.ID_TABLE = pk
            removeEquip_datatables.download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request, pk]
        ).start()

        return JsonResponse(data={})

    @method_decorator(permission_required('tickets.view_typifymodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def retry_email(self, request, pk) -> JsonResponse:

        """
        
            This function retries to send the remove equip email

            :param self: Instance of Class RemoveEquipamentView
            :type self: RemoveEquipamentView
            :param request: data of request
            :type request: request
        
        """

        try:
            instance = RemoveEquipamentModel.objects.get(ID=pk)
        except:
            raise ValidationError({'pk error': 'Unable to find remove equipment instance'})

        send_remove_equip_email(instance.ID, self.request.POST.get("current_status"), self.request.user)

        return JsonResponse(data={})

    @method_decorator(permission_required('tickets.view_typifymodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_from_matrix(self, request, pk) -> JsonResponse:

        """
        
            This function retries to update matrix status

            :param self: Instance of Class RemoveEquipamentView
            :type self: RemoveEquipamentView
            :param request: data of request
            :type request: request
        
        """

        try:
            instance = RemoveEquipamentModel.objects.get(ID=pk)
        except:
            raise ValidationError({'pk error': 'Unable to find remove equipment instance'})

        global_preferences: Dict[str, any] = global_preferences_registry.manager()

        url: str = global_preferences['general__matrix_domain'] + "services/"
        headers: Dict[str, str] = {
            'AUTHORIZATION': global_preferences['matrix__matrix_token']
        }
        params: Dict[str, str] = {'number': instance.service}

        response = requests.get(
            url=url,
            headers=headers,
            verify=False,
            params=params,
        ).json()

        url = url + str(response['results'][0]['id']) + "/"

        data={ 'status': '3'  }
        try:
            response2 = requests.patch(
                url=url,
                headers=headers,
                verify=False,
                data= data
            ).json()

            instance.removed_from_matrix = True
            instance.save()
        except:
            raise ValidationError({'Error': 'Unable to update matrix status'})

        return JsonResponse(data={})

# @method_decorator(permission_required('formerCustomers.view_removeequipamenttablesmodel',raise_exception=True), name='dispatch')
class RemoveEquipamentTableView(BaseViewSet):

    """

        Class define ViewSet of CategoryModel.
    
    """

    queryset: BaseModel = RemoveEquipamentTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RemoveEquipamentTablesSerializer

    # @method_decorator(permission_required('formerCustomers.view_removeequipamentmodel',raise_exception=False))
    # def get_queryset(self, *args, **kwargs):
    #     if self.request.user.has_perm('formerCustomers.view_removeequipamenttablesmodel'):
    #         return self.queryset
    #     else:
    #         return HttpResponseForbidden()
    
    
    @method_decorator(permission_required('formerCustomers.add_removeequipamenttablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.change_removeequipamenttablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.change_removeequipamenttablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.delete_removeequipamenttablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('formerCustomers.view_removeequipamenttablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {

            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Estado":{
                    "type":"choices",
                    "name":"status",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: 'ABIERTO',
                            1: 'CERRADO',
                            2: 'CANCELADO',
                        }
                    }
                },
                "Estado SAC":{
                    "type":"choices",
                    "name":"status_agendamiento",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: 'REGISTRADO EN ICLASS',
                            1: 'ORDEN CREADA',
                            2: 'RETIRAR ORDEN',
                        }
                    }
                },
                "Estado Terreno":{
                    "type":"choices",
                    "name":"status_storage",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: 'EN ALMACEN',
                            1: 'EN BODEGA',
                            2: 'PROCESAR EN ICLASS',
                        }
                    },
                },
                "Correo Enviado":{
                    "type":"bool",
                    "name":"email_sended",
                },
                "Retirado en Matrix":{
                    "type":"bool",
                    "name":"removed_from_matrix",
                },
                "Retirado de Red":{
                    "type":"bool",
                    "name":"removed_from_net",
                },
            },
            "columns":  {
                'updater': "Último en actualizar",
                'created': "Fecha de creación",
                'updated': "Fecha de actualización",
                'agent_ti': "Agente TI",
                'agent_sac': "Agente SAC",
                'agent_storage': "Agente Terreno",
                'service': "Servicios", 
                'status': "Estado",
                'status_storage': "Estado Almacén",
                'status_agendamiento': "Estado SAC",
                'email_sended': "Correo Enviado",
                'removed_from_matrix': "Retirado en Matrix",
                'removed_from_net': "Retirado de Red",
                'rut': "RUT",
                'serial':"Seriales",
                "history": "Historial",
                "matrix_status": "Estado actual en Matrix",
                "address": "Dirección",
                "commune": "Comuna",
                "customer": "Nombre del cliente",
                "previous_state": "Estado previo en Matrix"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('formerCustomers.view_removeequipamenttablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class RemoveEquipamentTableView
            :type self: RemoveEquipamentTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(RemoveEquipamentTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('formerCustomers.view_removeequipamenttablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class RemoveEquipamentTableView
            :type self: RemoveEquipamentTableView
            :param request: data of request
            :type request: request
        
        """

        return RemoveEquipamentTablesDatatable(request).get_data()


class RemoveEquipamentEmailView(BaseViewSet):

    """

        Class define ViewSet of RemoveEquipamentEmailModel.
    
    """

    queryset: BaseModel = RemoveEquipamentEmailModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RemoveEquipamentEmailSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('formerCustomers.view_removeequipamentemailmodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()
    
    
    @method_decorator(permission_required('formerCustomers.add_removeequipamentemailmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.change_removeequipamentemailmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

class RemoveEquipamentChannelView(BaseViewSet):

    """

        Class define ViewSet of RemoveEquipamentChannelModel.
    
    """

    queryset: BaseModel = RemoveEquipamentChannelModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RemoveEquipamentChannelSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('formerCustomers.view_removeequipamentchannelmodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()
    
    
    @method_decorator(permission_required('formerCustomers.add_removeequipamentchannelmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.change_removeequipamentchannelmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

class RemoveEquipamentPhotoView(BaseViewSet):

    """

        Class define ViewSet of RemoveEquipamentPhotoModel.
    
    """

    queryset: BaseModel = RemoveEquipamentPhotoModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RemoveEquipamentPhotoSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('formerCustomers.aview_removeequipamentmodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()
    
    
    @method_decorator(permission_required('formerCustomers.add_removeequipamentmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.change_removeequipamentmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('formerCustomers.delete_removeequipamentmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)