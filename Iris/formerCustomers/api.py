import requests
from magic import from_buffer
import base64
from mimetypes import guess_extension
from django.core.files.base import ContentFile

from communications.api import create_email
from django_q.models import Schedule
from dynamic_preferences.registries import global_preferences_registry
from communications.models import TemplateEmailModel
from .models import RemoveEquipamentModel, RemoveEquipamentEmailModel
from jinja2 import Template
from datetime import date, datetime,timedelta
from rest_framework.serializers import ValidationError
from typing import Dict, Any, Union, List

def send_remove_equip_email(instance_pk: int, current_status: str, user):
    
    try:
        instance = RemoveEquipamentModel.objects.get(ID=instance_pk)
    except:
        raise ValidationError({'pk error': 'Unable to find remove equipment instance'})

    if instance.email_sended == False:

        global_preferences: Dict[str, any] = global_preferences_registry.manager()

        def get_matrix_document(service: int, rut: str, status: str):

            current_date = str(date.today())
            current_time = str(datetime.now().time())

            matrix_url: str = global_preferences['general__matrix_domain'] + 'services/?number=' + str(service)

            service_id = requests.get(
                url=matrix_url,
                headers={
                "Authorization": global_preferences['matrix__matrix_token']
                },
                verify=False,
            ).json()['results'][0]['id']

            matrix_url: str = global_preferences['general__matrix_domain'] + 'customers/?rut=' + str(rut)

            customer = requests.get(
                url=matrix_url,
                headers={
                "Authorization": global_preferences['matrix__matrix_token']
                },
                verify=False,
            ).json()['results'][0]['name']

            matrix_url: str = global_preferences['general__matrix_domain'] + 'retire_equiment/'

            data: Dict[str, Any] = {
                "service_id": service_id,
                "person": customer,
                "status": status,
                "pick_up_date": current_date,
                "pick_up_time": current_time
            }

            response = requests.post(
                url=matrix_url,
                headers={
                    "Authorization": global_preferences['matrix__matrix_token']
                },
                json=data,
                verify=False,
            )
            return response.json()['files'][0]

        try:
            email_data = RemoveEquipamentEmailModel.objects.get(operator=instance.operator)
            email_template = email_data.template_email
        except:
            raise ValidationError({
                "Incorrect value": "Invalid Email Template"
            })

        try:
            
            url: str = global_preferences['general__matrix_domain'] + 'customers/' + f'?rut={instance.rut}'

            response = requests.get(
                url=url,
                headers={
                "Authorization": global_preferences['matrix__matrix_token']
                },
                verify=False,
            )
            
            receiver= response.json()['results'][0]['email']

        except:
            raise ValidationError({
                "Incorrect value": "Customer doesnt exists or doesnt have an email assigned"
            })

        def get_matrix_data(service):

            filters = [['number', 'equal', service]]

            data: Dict[str, Any] = {
                "filters": filters,
                "includes": [],
                "excludes": [],
                "unpaid_ballot": False,
                "criterion": 'D'
            }

            matrix_url: str = global_preferences['general__matrix_domain'] + 'services-filters/'

            response = requests.get(
                url=matrix_url,
                headers={
                    "Authorization": global_preferences['matrix__matrix_token']
                },
                json=data,
                verify=False,
            )

            if response.status_code != 200:

                raise ValidationError({
                    "Error matrix": "Error matrix filter"
                })

            result = response.json()[0]

            if 'service__number' in result:
                result['service__number_64'] = str(base64.b64encode(bytes(str(result['service__number']), 'utf-8')), 'utf-8')

            if 'customer__id' in result:
                result['customer__id_64'] = str(base64.b64encode(bytes(str(result['customer__id']), 'utf-8')), 'utf-8')

            return result

        def current_time_tags(data: Dict[str, Any]) -> Dict[str, Any]:

            months: Dict[int, str] = {
                1: "Enero",
                2: "Febrero",
                3: "Marzo",
                4: "Abril",
                5: "Mayo",
                6: "Junio",
                7: "Julio",
                8: "Agosto",
                9: "Septiembre",
                10: "Octubre",
                11: "Noviembre",
                12: "Diciembre",
            }

            weekdays: Dict[int, str] = {
                0: "Lunes",
                1: "Martes",
                2: "Miercoles",
                3: "Jueves",
                4: "Viernes",
                5: "Sabado",
                6: "Domingo",
            }

            current_datetime: datetime = datetime.now()

            data['current__month'] = months[int(current_datetime.month)]
            data['current__year'] = current_datetime.year
            data['current__day'] = current_datetime.day
            data['current__week_day'] = weekdays[int(current_datetime.weekday())]
            data['current__datetime'] = current_datetime.strftime("%d/%m/%Y")

            return data

        def base64_to_file(str_file: str) -> ContentFile:

                """
        
                    This function return file object

                    :param str_file: Str base 64
                    :type str_file: Str

                    :returns: File of base64
                    :rtype: File

                """

                file_64: str = base64.b64decode(str_file, validate=True)

                mime_type = from_buffer(file_64, mime=True)
                file_ext = guess_extension(mime_type)

                file_object: ContentFile = ContentFile(file_64, name='Documento_Retiro' + file_ext )

                return file_64 , 'Documento_Retiro' + file_ext, file_object

        data: Dict[str, Any] = get_matrix_data(instance.service)
        data: Dict[str, Any] = current_time_tags(data)
        file_str = get_matrix_document(instance.service, instance.rut, current_status)

        file_email, file_name, file_object = base64_to_file(file_str)

        enviroment: str = global_preferences['general__Enviroment']

        if enviroment != "production": 
            receiver = global_preferences['communications__test_email']

        json_data = {
            'subject': email_template.subject,
            'sender': email_data.sender,
            'receiver': receiver,
            'operator': instance.operator.ID,
            'message': Template(email_template.template_html).render(data),
        }

        create_email(json_data, user, [file_object])

        instance.email_sended = True
        instance.save()
    
        return True