

import pytest
import requests
from django.contrib.auth.models import User
from django.urls import reverse
from typing import Any, Dict, List
import json
from dynamic_preferences.registries import global_preferences_registry


"""
_dj_autoclear_mailbox, _django_clear_site_cache, _django_db_marker, _django_set_urlconf, _django_setup_unittest, _fail_for_invalid_template_variable,
_live_server_helper, _template_string_if_invalid_marker, admin_client, admin_user, async_client, async_rf, cache, capfd, capfdbinary, caplog, capsys,
capsysbinary, client, db, django_assert_max_num_queries, django_assert_num_queries, django_db_blocker, django_db_createdb, django_db_keepdb,
django_db_modify_db_settings, django_db_modify_db_settings_parallel_suffix, django_db_modify_db_settings_tox_suffix, django_db_modify_db_settings_xdist_suffix,
django_db_reset_sequences, django_db_setup, django_db_use_migrations, django_mail_dnsname, django_mail_patch_dns, django_test_environment,
django_user_model, django_username_field, doctest_namespace, live_server, mailoutbox, monkeypatch, pytestconfig, record_property, record_testsuite_property,
record_xml_attribute, recwarn, remove_equipament_model, rf, settings, tmp_path, tmp_path_factory, tmpdir, tmpdir_factory, transactional_db
"""

@pytest.fixture
def user(django_user_model):

    user: User = django_user_model(
        username="user",
        password="password",
    )

    user.save()

    return user

class BaseTest():

    def reverse_url(self, url: str, args: List[Any]):

        try:

            url: str = reverse(url, args=args)

        except:

            assert False

        return url

@pytest.mark.django_db
@pytest.mark.parametrize(
    'url_base,validate_data,url_base_detail', [
        (
            'formerCustomers:remove_equipament-list',
            'remove_equipament',
            'formerCustomers:remove_equipament-detail'
        ),
        (
            'formerCustomers:remove_equipament_tables-list',
            'remove_equipament_tables',
            'formerCustomers:remove_equipament_tables-detail'
        )
    ]
)
class TestPermissionWriteOrUpdate(BaseTest):

    """

        Check user permissions
    
    """

    CREATE_DICT: Dict[str, Any] = {
        "remove_equipament":{
            "sucesfull":{
                "operator":"1",
                "rut":"14.586.738-K", #Agregar rut valido
                "services":[5000], #Agregar servicios del rut y operador
                "seriales":["Gh5j2k","ol9d6"],
                "status":0,
            },
            "errors":[
                {
                    "data":{},
                    "error_message":{
                        'operator': [
                            'This field is required.'
                        ],
                        'status': [
                            'This field is required.'
                        ]
                    }
                },
                {
                    "data":{
                        "operator":"100",
                        "status":25,
                    },
                    "error_message":{
                        "operator": [
                            "\"100\" is not a valid choice."
                        ],
                        "status": [
                            "\"25\" is not a valid choice."
                        ]
                    }
                },
                {
                    "data":{
                        "rut":"99999999999999", #Agregar rut invalido
                        "status":1,
                        "operator":"1",
                    },
                    "error_message":{
                        "rut": {
                            "Don't exist": "There is no customer with that rut"
                        }
                    }
                },
                {
                    "data":{
                        "rut":"14.586.738-K", #Agregar rut valido
                        "status":1,
                        "services":[9999999999,888888888],
                        "operator":"1",
                    },
                    "error_message":{
                        'Error services value':"Invalid services: 888888888, 9999999999"
                    }
                },
                {
                    "data":{
                        "seriales":[1],
                        "status":1,
                        "operator":"1",
                    },
                    "error_message":{
                        'seriales': {
                            'Invalid 1 value': 'must be str type'
                        }
                    }
                },
                {
                    "data":{
                        "services":[5000],
                        "status":1,
                        "operator":"2",
                    },
                    "error_message":{
                        'rut': [
                            'This field is required.'
                        ],
                    }
                }

            ]
        },
        "remove_equipament_tables":{
            "sucesfull":{
                "operator":"1",
                "name":"General",
            },
            "errors":[
                {
                    "data":{},
                    "error_message":{
                        'operator': [
                            'This field is required.'
                        ],
                        'name': [
                            'This field is required.'
                        ]
                    }
                },
                {
                    "data":{
                        "operator":"1",
                        "name":"General",
                        "last_time_type":0
                    },
                    "error_message":{
                        'last_time': [
                            'This field is required.'
                        ],
                    }
                },
                {
                    "data":{
                        "operator":"1",
                        "name":"General",
                        "last_time":0
                    },
                    "error_message":{
                        'last_time_type': [
                            'This field is required.'
                        ],
                    }
                },
                {
                    "data":{
                        "operator":"1",
                        "name":"General",
                        "columns":["category"]
                    },
                    "error_message":{
                        'Invalid columns': 'category'
                    }
                },
            ]
        },
    }

    def test_user_with_permissions_write_instance(self, url_base: str, validate_data: str, admin_client, url_base_detail: str):

        """
            This function check user has permission

            :param url_base: URL base to validate
            :type url_base: str
            :param admin_client: client with admin user
            :type admin_client: Client
            :param validate_data: Name validation dict
            :type validate_data: str
        
        """

        errors: List[Dict[str, Any]] = self.CREATE_DICT[validate_data]["errors"]

        url_base: str = self.reverse_url(url_base, [])

        for error in errors:

            response = admin_client.post( 
                path=url_base,
                data=json.dumps(error["data"]),
                content_type='application/json',
            )

            assert response.status_code == 400 and error["error_message"] == response.json()

    def test_user_with_permissions_update_instance(self, url_base: str, validate_data: str, admin_client, url_base_detail: str):

        """
            This function check user has permission

            :param url_base: URL base to validate
            :type url_base: str
            :param admin_client: client with admin user
            :type admin_client: Client
            :param validate_data: Name validation dict
            :type validate_data: str
        
        """

        errors: List[Dict[str, Any]] = self.CREATE_DICT[validate_data]["errors"]
        sucesfull: Dict[str, Any] = self.CREATE_DICT[validate_data]["sucesfull"]

        url_base: str = self.reverse_url(url_base, [])

        response = admin_client.post( 
            path=url_base,
            data=json.dumps(sucesfull),
            content_type='application/json',
        )

        assert response.status_code == 201

        url_base_detail: str = self.reverse_url(url_base_detail, [ response.json()["ID"] ])

        for error in errors:

            response = admin_client.put( 
                path=url_base_detail,
                data=json.dumps(error["data"]),
                content_type='application/json',
            )

            assert response.status_code == 400 and error["error_message"] == response.json()

@pytest.mark.django_db
@pytest.mark.parametrize(
    'url_base', [
        ('formerCustomers:remove_equipament-list'),
        ('formerCustomers:remove_equipament_tables-list'),
    ]
)
class TestPermissionRead(BaseTest):

    """

        Check user permissions
    
    """

    def test_user_with_permissions_read_list(self, url_base: str, admin_client):

        """
            This function check user has permission

            :param url_base: URL base to validate
            :type url_base: str
            :param admin_client: client with admin user
            :type admin_client: Client
        
        """

        response = admin_client.get( 
            path=self.reverse_url(url_base, [])
        )
        assert response.status_code == 200

    def test_user_without_permissions_read_list(self, url_base: str,  client, user):

        """
            This function check user does has permission

            :param url_base: URL base to validate
            :type url_base: str
            :param admin_client: client
            :type admin_client: Client
          
        """

        client.force_login(user)
        response = client.get( 
            path=self.reverse_url(url_base, [])
        )

        assert response.status_code == 403

class TestRemoveEquipament(BaseTest):

    def test_change_status(self, admin_client, client):

        def change_status(admin_client, url, range):

            for i in range:

                response = admin_client.put( 
                    path=url,
                    data=json.dumps({
                        "status":i
                    }),
                    content_type='application/json',
                )

                assert response.status_code == 200



        url_base_request: str = "formerCustomers:remove_equipament-list"
        url_base_detail_request: str = "formerCustomers:remove_equipament_tables-detail"

        url_base: str = self.reverse_url(url_base_request, [])

        response = admin_client.post( 
            path=url_base,
            data=json.dumps({
                "operator":"1",
                "rut":"14.586.738-K", #Agregar rut valido
                "services":[5000], #Agregar servicios del rut y operador
                "seriales":["Gh5j2k","ol9d6"],
                "status":0,
            }),
            content_type='application/json',
        )

        assert response.status_code == 201

        url_base_detail: str = self.reverse_url(url_base_detail_request, [ response.json()["ID"] ])

        change_status(admin_client, url_base_detail, range(0, 5))
        change_status(admin_client, url_base_detail, range(0, 1))
        change_status(admin_client, url_base_detail, range(0, 8))

        url_emails_list: str = "communications:email-list"
        url_emails_list: str = self.reverse_url(url_emails_list, []) + "?fields=ID"

        response = admin_client.get( 
            path=url_emails_list
        )

        before_length: int = len( response.json() )

        change_status(admin_client, url_base_detail, range(8, 9))

        response = admin_client.get( 
            path=url_emails_list
        )

        assert before_length + 1 == len( response.json() )

        change_status(admin_client, url_base_detail, range(9, 10))

        global_preferences = global_preferences_registry.manager()
        domain: str = global_preferences['general__iris_backend_domain'] + "api/v1/matrix/services/?fields=status&number=5000"

        client.get(
            path=domain,
            headers={
                "Authorization": global_preferences['matrix__matrix_token']
            },
            verify=False,
        )

        assert 3 == client.json()[1]['status']

        change_status(admin_client, url_base_detail, range(10, 12))









