import re
from datetime import datetime, timezone, timedelta
from typing import Any, List, Union, Dict
import requests
import random
import string
from slack import WebClient
from rest_framework import serializers
from schema import And, Schema, Use
from dynamic_preferences.registries import global_preferences_registry
from django_q.models import Schedule
from .models import  TypifyModel, TypifySlackMessageModel

def send_reminder(*args, **kwargs):

    def slack_reminder(thread: str, channel: str, typify_instance: TypifyModel, closed: bool, message: str = None):

        global_preferences = global_preferences_registry.manager()

        client = WebClient(token=typify_instance.operator.slack_token)

        reminder = "El tiempo de duración de este ticket se ha agotado, por favor proceder a cerrarlo" if closed else "Atención. La mitad del tiempo de duración de este ticket ha transcurrido"

        blocks: List[Dict[str, Any]] =  [
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": ":iris: *Iris* Tickets"
                }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": reminder if message == None else message
                }
            },
            {
                "type": "divider"
            }
        ]

        client.chat_postMessage(
            blocks=blocks,
            thread_ts=thread,
            channel=channel,
            text="Recordatorio de la tipificación"
        )

    if 'kwargs' in kwargs.keys():

        pk_typify: int = kwargs['kwargs']['pk_typify']
        closed: bool = kwargs['kwargs']['closed']
        custom: bool = kwargs['kwargs']['custom']

        message = kwargs['kwargs']['message'] if custom else None

    else:   

        pk_typify: int = kwargs['pk_typify']
        closed: bool = kwargs['closed']
        custom: bool = kwargs['custom']

        message = kwargs['message'] if custom else None


    typify_instance = TypifyModel.objects.get(ID=pk_typify)

    if typify_instance.slack_thread != None and typify_instance.status != 1:
            for thread in typify_instance.slack_thread:
                slack_reminder(thread, typify_instance.slack_thread[thread], typify_instance, closed, message)