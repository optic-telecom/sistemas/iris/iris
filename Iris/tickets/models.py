from itertools import cycle
from typing import Any, Dict, List, Mapping, Tuple, Union, Set
import calendar
from datetime import datetime
from slack import WebClient

import requests
from slack import WebClient
from common.models import BaseModel
from communications.models import ChatModel, ConversationModel
from common.utilitarian import communications_url, get_token, matrix_url
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from dynamic_preferences.registries import global_preferences_registry
from phone_field import PhoneField
from rest_framework.serializers import ValidationError


class CategoryModel(BaseModel):

    """

        Class define model category.
    
    """

    def clean(self):

        if self.classification == 1:

            self.parent = None

        elif not self.parent:

            raise ValidationError({
                "error parent":"Required parent"
            })

        elif self.parent.classification + 1 != self.classification:

            raise ValidationError({
                "error parent":"Incorrect parent"
            })

    name: str = models.TextField(blank=False, null=False)

    ORIGIN_CHOICES = (
        (1, 'Tipo'),
        (2, 'Categoria'),
        (3, 'Subcategoria'),
        (4, 'Segunda subcategoria'),
    )
    classification: int = models.PositiveSmallIntegerField(choices=ORIGIN_CHOICES)

    parent = models.ForeignKey(
        'self',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    sla: int = models.PositiveIntegerField(default=1)

class ProcessModel(BaseModel):

    name : str = models.CharField(max_length=100, blank=False)
    description: Dict[str, any] = JSONField(default=dict)

class TypifyModel(BaseModel):

    """

        Class define model typify.
    
    """

    def clean(self):

        def validate_services( services: List[int], operator: int, rut: str ):
            
            global_preferences: Dict[str, any] = global_preferences_registry.manager()
            url_services: str = global_preferences['general__matrix_domain'] + 'services/'
            url_services: str = f'{url_services}?fields=number&customer__rut={rut[0]}&operator={operator}'
            
            services_check: List[int] = requests.get(
                url=url_services,
                verify=False,
                headers={
                    "Authorization": global_preferences['matrix__matrix_token']
                }
            ).json()

            no_valid_services: List[int] = list( set(services) - set( [ service['number'] for service in services_check] ) )

            if no_valid_services:

                raise ValidationError({
                    'Error services value': f"Invalid services: {', '.join(no_valid_services)}"
                })


        if self.customer_type == 0:

            self.services = []
            self.rut = []

        else:

            self.services = list ( set(self.services) )

            if not bool(self.rut) or not all(isinstance(elem, str) for elem in self.rut):
                raise ValidationError({'Value Error': 'Invalid RUT data, must be a list of str'})

            # validate_services( self.services, self.operator, self.rut )    

        sesion = requests.Session()
        sesion.headers.update({"Authorization": get_token()})

        chatInstance = ChatModel.objects.create(operator=self.operator)
        self.commentaries = chatInstance.ID

    category = models.ManyToManyField(CategoryModel, related_name='categories')

    agent = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name='agent_user',
        on_delete=models.SET_NULL,
    )

    #Services of typify
    services: List[int] = JSONField(default=list)
    commentaries: str = models.IntegerField(null=True)
    rut: List[str] = JSONField(default=list, null=True, blank=True)
    slack_thread: List[int] = JSONField(default=dict, null=True, blank=True)

    CUSTOMER_TYPE_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'No cliente'),
        (3, 'Cliente'),
        (4, 'Múltiples Clientes')
    )
    customer_type: int = models.PositiveIntegerField(default=0, choices=CUSTOMER_TYPE_CHOICES)

    ORIGIN_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'CORREO'),
        (1, 'LLAMADA'),
        (2, 'CHAT'),
        (3, 'REDES'),
        (4, 'WHATSAPP'),
        (5,  'FACEBOOK'),
        (6, 'INSTAGRAM')
    )
    channel: int = models.PositiveIntegerField(default=0, choices=ORIGIN_CHOICES)

    TYPE_TK_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'IN'),
        (1, 'OUT'),
    )
    type: int  = models.PositiveIntegerField(default=0, choices=TYPE_TK_CHOICES)

    STATE_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'ABIERTO'),
        (1, 'CERRADO'),
        (2, 'ESPERANDO RESPUESTA'),
        (3, 'DAR SEGUIMIENTO'),
        (4, 'LLAMAR LUEGO'),
        (5, 'CANCELADO')
    )
    status: int  = models.PositiveIntegerField(default=0, choices=STATE_CHOICES)

    CITY_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'ARICA'),
        (1, 'SANTIAGO'),
        (2, 'OTRA'),
    )
    city: int  = models.PositiveIntegerField(default=2, choices=CITY_CHOICES)

    escalation_tk = models.PositiveIntegerField(
        null=True, 
        default=None
    )

    process = models.ForeignKey(
        ProcessModel,
        on_delete=models.PROTECT,
        null=True,
    )

    slack_message: int = JSONField(default=list)
    cancelation_motive: str = models.TextField(null=True, blank=True)
    
    conversation_whatsapp = models.ForeignKey(
        ConversationModel,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="conversation_whatsapp"
    )

    conversation_email = models.ForeignKey(
        ConversationModel,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="conversation_email"
    )

class NewContactModel(BaseModel):

    """

        Class define model new contact.
    
    """

    typify = models.ForeignKey(
        TypifyModel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    commentary: str = models.TextField(null=True, blank=True)
    channel: str = models.CharField(max_length=25, choices=TypifyModel.ORIGIN_CHOICES)

class TypifySlackMessageModel(BaseModel):

    def clean(self):
        
        TypifySlackMessageModel.validate_slack_channel(self.channel, self.operator)


    def validation_fields(value: List[str]):

        fields: List[str, str] = [
            'subcategory', 'second_subcategory', 'agent', 'services', 'rut', 'customer_type', 'channel', 'type', 'status',
            'city'
        ]

        for item in value:

            if not item in fields:

                raise ValidationError({
                    f'field {item}':'No valid item'
                })

    name: str = models.CharField(max_length=50)

    category = models.ForeignKey(
        CategoryModel,
        on_delete=models.PROTECT,
    )

    def validate_slack_channel(value: str, operator):

        """

            This function, validate slack_channel field of MassiveCommunicationsModel

        """

        global_preferences: Dict[str] = global_preferences_registry.manager()

        token: str = operator.slack_token

        client = WebClient(token=token)
        exists: bool = False
        search: bool = True
        cursor: str = ''

        while search:
            response = client.conversations_list(token=token, cursor=cursor)
            if response["ok"]:
                for channel in response["channels"]:
                        if channel["id"] == value:
                            exists = True
                            pass
                        pass
                cursor = response.get("response_metadata",{}).get("next_cursor", '')
                search = True if cursor != '' else False
            else:
                raise ValidationError({
                    'Slack connection': "Unable to connect to Slack"
                })

        if not exists:
            raise ValidationError({
                'Error channel ID': "Channel not found"
            })

    fields: List[str] = JSONField(default=list, validators=[validation_fields])

    channel: str = models.CharField(max_length=11, null=True)

class TypifyTablesModel(BaseModel):

    def clean(self):

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "creator__pk":"choices",
                    "agent__pk":"choices",
                    "customer_type":"choices",
                    "channel":"choices",
                    "type":"choices",
                    "status":"choices",
                    "city":"choices",
                    "category__pk":"int",
                    "rut": "str"
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results

        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'creator', 'updater', 'created', 'updated', 'category', 'agent', 'services', 
                                            'rut', 'customer_type', 'channel', 'type', 'status', 'city', 
                                           
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class TicketCommunicationModel(BaseModel):

    """

        Class define model new contact.
    
    """

    typify = models.ForeignKey(
        TypifyModel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    commentary: str = models.TextField(null=True, blank=True)
    channel: str = models.CharField(max_length=25, choices=TypifyModel.ORIGIN_CHOICES)
    TYPE_TK_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'IN'),
        (1, 'OUT'),
    )
    type: int  = models.PositiveIntegerField(default=0, choices=TYPE_TK_CHOICES)