from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import IntegerPreference, StringPreference

general = Section('general')

@global_preferences_registry.register
class SiteTitle(StringPreference):

    section = general
    name: str = 'Enviroment'
    default: str = 'local'
    required: bool = False

@global_preferences_registry.register
class SiteTitle(StringPreference):

    section = general
    name: str = 'matrix_domain'
    default: str = 'https://190.113.247.198/api/v1/'
    required: bool = True

@global_preferences_registry.register
class SiteTitle(StringPreference):

    section = general
    name: str = 'iris_backend_domain'
    default: str = 'http://127.0.0.1:8000/'
    required: bool = True

@global_preferences_registry.register
class SiteTitle(StringPreference):

    section = general
    name: str = 'iris_front_domain'
    default: str = 'http://127.0.0.1:8001/'
    required: bool = True

@global_preferences_registry.register
class SiteTitle(StringPreference):
    
    section = general
    name: str = 'Slack_error_channel'
    default: str = 'https://hooks.slack.com/services/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'
    required: bool = True

@global_preferences_registry.register
class SiteTitle(StringPreference):
    
    section = general
    name: str = 'Email_send_iris_data'
    default: str = 'angela.almonacid@bandaancha.cl gregory.mejia@bandaancha.cl'
    required: bool = True

@global_preferences_registry.register
class SiteTitle(StringPreference):
    
    section = general
    name: str = 'category_token'
    default: str = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InN1cGVydXNlckBvcHRpYy5jbCIsImV4cCI6MTU5NjczMjU1MSwiZW1haWwiOiJzdXBlcnVzZXJAb3B0aWMuY2wifQ.Yxtpa23dyH0T_u9NIra9BKFDEi3eteB9yNVSl9OpBIM'
    required: bool = True