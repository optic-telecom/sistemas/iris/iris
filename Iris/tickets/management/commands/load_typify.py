import openpyxl
import datetime
import requests
from openpyxl.workbook import Workbook
from openpyxl.styles import PatternFill

from ...models import TypifyModel, CategoryModel
from communications.models import InternalChatModel, ChatModel
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from dynamic_preferences.registries import global_preferences_registry


class Command(BaseCommand):

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('file')

    def handle(self, *args, **options):
        failed_items = []
        
        def iter_rows(ws):
            for row in ws.iter_rows():
                yield [cell.value for cell in row]

        def add_to_excel(typify, motive, index):
            typify.append(motive)
            if typify[0] != index: typify.insert(0,index)
            failed_items.append(typify)

        global_preferences: Dict[str, any] = global_preferences_registry.manager()
        next_matrix_url: str = global_preferences['general__matrix_domain'] + 'services/?operator=2'

        matrix_services = []

        while next_matrix_url != None:

            matrix_response = requests.get(
                    url=next_matrix_url,
                    headers={
                    "Authorization": global_preferences['matrix__matrix_token']
                    },
                    verify=False,
                ).json()

            for service in matrix_response['results']:
                matrix_services.append(service['number'])

            next_matrix_url = matrix_response['next']


        wb = openpyxl.load_workbook(filename=options['file'], data_only=True)

        o_sheet = wb["Tipificaciones"]
        channels = {
            'Correo Electrónico': 0,'Llamadas': 1, 'Chat': 2, 'Redes': 3, 'Whatsapp': 4
        }
        subcategories = {
            "1.1.1": 13, "1.1.2": 14, "1.1.3": 16, "1.1.4": 15, "1.1.5": 17, "1.1.6":18, "1.1.7": 19, "1.1.8": 21, "1.1.9": 22,
            "1.1.10": 20, "1.1.11": 145, "1.1.12": 148, "1.1.13": 149, "1.2.1": 23, "1.2.2": 24, "1.2.3": 25, "1.3.1": 28, "1.3.2": 26, "1.3.3": 27, "1.3.4": 29, "1.4.1": 30,
            "1.4.2": 31, "2.1.1": 33, "2.1.2": 32, "2.1.3": 34, "2.1.4": 35, "2.1.5": 36, "2.1.6": 37, "2.1.7": 38, "2.1.8": 41, "2.1.9": 39,
            "2.1.10": 40, "2.1.11": 42, "2.1.12": 146, "2.1.13": 147, "2.2.1": 43, "2.2.2": 44, "2.2.3": 47, "2.2.4": 46, "2.2.5": 45, "2.2.6": 48, "2.2.7": 49,
            "2.2.8": 51, "2.2.9": 50, "2.2.10": 53, "2.2.11": 52, "2.2.12": 54, "2.2.13": 55, "2.3.1": 56, "2.3.2": 57, "2.3.3": 58,
            "2.3.4": 59, "2.3.5": 60,  "2.3.6": 61, "2.3.7": 62, "2.3.8": 63, "2.3.9": 64, "2.4.4": 68, "3.1.1": 69, "3.1.2": 71, "3.1.3": 70,
            "2.4.1": 65, "2.4.1.1": 150, "2.4.1.2": 151, "2.4.2.1": 152, "2.4.2": 67, "2.4.2.2": 155, "2.4.2.3": 153,"2.4.2.4": 157,"2.4.2.5": 154,"2.4.2.6": 156,
            "2.4.3": 66, "2.4.3.1": 159, "2.4.3.2": 161, "2.4.3.3": 158, "2.4.3.4": 160
        }
        locations= {"Santiago": 1,"Arica": 0}

        for index,element in enumerate(iter_rows(o_sheet)):
            if index == 0 or element[0] == None or element[0] < datetime.datetime(2020, 12, 1):
                continue
            motive = None
            customer_type = 3
            created_time = element[0]
            creator = element[1]
            channel = element[2]
            number = element[3]
            subcategory1 = element[6].split(" ")[0] if element[6] != None else None
            subcategory2 = element[7].split(" ")[0] if element[7] != None else None
            type_typify = element[8]
            city = element[9]
            commentary = element[10]
            status = element[11]

            if type_typify == None:
                motive = "No se ha seleccionado In o Out"
                add_to_excel(element, motive, index)
            if subcategory1 == None or subcategory1 not in subcategories:
                if subcategory2 == None or subcategory2 not in subcategories:
                    motive = "Subcategoría inválida"
                    add_to_excel(element, motive, index)
            if number == 1111 or number == 111:
                if subcategory1 == "1.1.1" or subcategory1 == "1.1.13" or subcategory1 == "1.1.9" or subcategory1 == "2.1.1" or subcategory1 == "2.2.10":
                    customer_type = 0
                else:
                    motive = "Código inválido"
                    add_to_excel(element, motive, index)
            if status == None:
                motive = "Estado inválido"
                add_to_excel(element, motive, index)
            if number != None and type(number) == int and  number > 40000:
                try: 
                    typify = TypifyModel.objects.get(ID=number, operator=2)
                    continue
                except:
                    print("No registrado en Iris")

            if (number == None or number not in matrix_services) and customer_type == 3:
                motive ="Servicio no registrado en Matrix"
                add_to_excel(element, motive, index)
            else:
                if customer_type == 3:
                    numberList = []
                    numberList.append(number)

            try: 
                if creator == "amarilis.marrero@bandaancha.cl": creator = "amarilis@optic.cl"
                if creator == "yennerbi@bandaancha.cl": creator = "yennerbi.gonzalez@bandaancha.cl"
                if creator == "nayoly@bandaancha.cl": creator = "nayoly.navas@bandaancha.cl"
                User.objects.get(username=creator)
            except:
                motive ="Usuario no encontrado"
                add_to_excel(element, motive, index)

            try: 
                CategoryModel.objects.get(pk=subcategories[subcategory2] if subcategory2 != None else subcategories[subcategory1])
            except:
                motive ="Categoría no encontrada"
                add_to_excel(element, motive, index)

            if motive == None: 
                try:
                    typify_instance = TypifyModel.objects.create(operator=2,services=numberList,channel=channels[channel],customer_type = customer_type,status= 1 if status == "Resuelto" else 0,type=1 if type_typify == "OUT" else 0,city=locations[city] if city in locations else 2,agent= User.objects.get(username=creator),category=CategoryModel.objects.get(pk=subcategories[subcategory2] if subcategory2 != None else subcategories[subcategory1]))
                    typify_instance.created = created_time
                    typify_instance.creator = User.objects.get(username=creator)
                    typify_instance.updater = User.objects.get(username=creator)
                    InternalChatModel.objects.create(
                        operator=2,
                        message=commentary,
                        chat=ChatModel.objects.get(pk=typify_instance.commentaries)
                    )
                    typify_instance.save()
                    print("Guardado")
                except:
                    motive ="Error inesperado"
                    add_to_excel(element, motive, index)

        if len(failed_items) > 0:
            failed_wb = Workbook()
            FIELDS_EXCEL = {
                'A': "ID - Nro. Registro",
                'B': 'Fecha de creación',
                'C': 'Agente creador',
                'D': 'Canal',
                'E': 'Código',
                'F': 'Tipo',
                'G': 'Categoria',
                'H': 'Sub-Categoria',
                'I': 'Segunda Sub-Categoría',
                'J': 'Tipo',
                'K': 'Ciudad',
                'L': 'Comentario',
                'M': 'Estado',
                'T': 'Motivo',
            }
            redFill = PatternFill(start_color='3bd464', end_color='3bd464', fill_type='solid' )
            ws1 = failed_wb.active
            for key, value in FIELDS_EXCEL.items():

                ws1[f'{key}1'].fill: PatternFill = redFill
                ws1[f'{key}1']: str = value
            
            for item in failed_items:
                ws1.append(item)
                
            failed_wb.save(filename="Registos Fallidos.xlsx")


        

                
                
