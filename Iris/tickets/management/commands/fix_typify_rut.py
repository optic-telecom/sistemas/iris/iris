import openpyxl
import datetime
import requests
from openpyxl.workbook import Workbook
from openpyxl.styles import PatternFill

from ...models import TypifyModel
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from dynamic_preferences.registries import global_preferences_registry


class Command(BaseCommand):

    def handle(self, *args, **options):

        for typify in TypifyModel.objects.filter(deleted=False):

            if typify.rut != None:

                if not all(isinstance(elem, str) for elem in typify.rut):
                    
                    for index,rut_dict in enumerate(typify.rut):
                        typify.rut[index] = rut_dict.get("value","")
                        typify.save()

                    

                    
                        
                        