from typing import Union

from common.models import BaseModel
from drf_yasg.openapi import Response as Response_Openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers
from rest_framework.parsers import MultiPartParser

from .models import *
from .serializers import *


class DocumentationTicket(object):

    """

        Class define documentation to ViewSet TypifyView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of TypifyView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []

    class DocumentationSerializerMultipleChanges(serializers.Serializer):

        """

            Class define void serializer of TypifyView Body.
    
        """

        new_status = serializers.ChoiceField(choices=TypifyModel.STATE_CHOICES, required=False)
        new_agent = serializers.IntegerField(required=False)
        new_queue = serializers.IntegerField(required=False)

        city = serializers.ChoiceField(choices=TypifyModel.CITY_CHOICES)
        customer_type = serializers.ChoiceField(choices=TypifyModel.CUSTOMER_TYPE_CHOICES)

        category = serializers.CharField()
        operator = serializers.CharField()
        queue = serializers.CharField()

        endtime = serializers.CharField()
        inittime = serializers.CharField()

        
        
        class Meta:

            model: BaseModel = TypifyModel
            fields: Union[list,str] = '__all__'

    class DocumentationSerializerGetID(serializers.Serializer):

        """

            Class define void serializer of TypifyView view called get_ID.
    
        """

        search = serializers.IntegerField(required=False)
        operator = serializers.IntegerField(required=False)

        class Meta:

            fields: Union[list,str] = '__all__'

    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_massive_changes = DocumentationSerializerMultipleChanges
    serializer_class_get_ID = DocumentationSerializerGetID
    serializer_class_body = TypifySerializer
    serializer_class_response = TypifySerializer

    def documentation_create_and_update(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create or update, ticket',
            operation_description="""

                This endpoint createt ypify record. 

            """,
            responses = {
                200:Response_Openapi(
                    'Create or update ticket',
                    self.serializer_class_response,
                ),
                500:Response_Openapi(
                    description = 'Create or update ticket',
                    examples = {
                        "Category error":"Typify requeried type 'Subcategoria' or 'Subcategoria dos'",
                        "Invalid update":"Ticket is close"
                    },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_new_contact(self):

        """

            This function return information of documentation view 'new_contact'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='GET, New contacts customers',
            operation_description="""

                This endpoint return customers with open typify.

                param : 
                    rut
                type:
                    String
                Example:
                    /typify/new_contact/?rut=10.256.23-k

            """,
            responses = {
                200:Response_Openapi(
                    description = 'List, typify',
                    examples = [
                        [6954, '55625,58326', '1.1.1 Factibilidad'],
                        [3659, '55625,36', '1.1.1 Factibilidad']
                ]   ,
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_multiple_changes(self):

        """

            This function return information of documentation view 'multiple_changes'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Changes multiples values',
            operation_description="""

                This endpoint change value multiple instances.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Multiples values changes',
                    examples = {
                        
                    },
                ),
            },
            query_serializer = self.serializer_class_massive_changes,
        )

    def documentation_datables(self):

        """

            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, massive communications',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables with filters',
                    examples = {
                        "result":{
                            "draw": 1,
                            "recordsTotal": 1,
                            "recordsFiltered": 1,
                            "data": [
                                [ '2020-02-10T22:43:47.981Z', '2020-02-10T22:43:47.981Z', '24795', '1.2.1 Estatus de caída de red', 'No cliente', '', 'Prueba', 'LLAMADA', 'IN', 'CERRADO', 'NORMAL', 'SANTIAGO', 'NINGÚNA', '94', 'superuser@optic.cl'],
                            ], 
                            "result": "ok"
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationNewContact(object):

    """

        Class define documentation to ViewSet NewContactView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of NewContactView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []

    
    serializer_class_void = DocumentationSerializerVoid

    def documentation_datables(self):

        """

            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationNewContact
            :type self: DocumentationNewContact

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, new contacts',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables with filters',
                    examples = {
                        "result":{
                            "draw": 1,
                            "recordsTotal": 135,
                            "recordsFiltered": 128,
                            "data": [
                                ["cristian@optic.cl", "05-05-2019 10:59", "CORREO", "Aqui un comentario", "10"],
                                ["cristian.cantero@multifiber.cl", "05-09-2019 10:58", "CORREO", "Aqui un comentario", "12"],
                            ], 
                            "result": "ok"
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationOperator(object):

    """

        Class define documentation to ViewSet OperatorView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of OperatorView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []
    
    serializer_class_void = DocumentationSerializerVoid

    def documentation_datables(self):

        """

            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationOperator
            :type self: DocumentationOperator

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables with filters',
                    examples = {
                        "result":{
                            "draw": 1,
                            "recordsTotal": 1,
                            "recordsFiltered": 1,
                            "data": [
                                ["1", "Banda Ancha", "1", "1"],
                            ], 
                            "result": "ok"
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationSerializerDatatablesTickets(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """

        start: int = serializers.IntegerField()
        offset: int = serializers.IntegerField()
        order_field: str = serializers.CharField(default="ID")
        order_type: str = serializers.CharField(default="desc")
        filters: List[list] = serializers.JSONField(default=[['ID','equal','1']])
        
        class Meta:

            fields: Union[list, str] = '__all__'

class DocumentationCategory(object):

    """

        Class define documentation to ViewSet CategoryView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of CategoryView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_body = CategorySerializer
    serializer_class_response = CategorySerializer
    serializer_class_datatables = DocumentationSerializerDatatablesTickets

    def documentation_create_and_update(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationCategory
            :type self: DocumentationCategory

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create or update, category',
            operation_description="""

                This endpoint create typify record

            """,
            responses = {
                200:Response_Openapi(
                    'Create or update category',
                    self.serializer_class_response,
                ),
                500:Response_Openapi(
                    description = 'Create or update category',
                    examples = {
                        "SLA error":"SLA <= 0",
                        "cycle error":"Recursive cycle",
                        "error parent":"Incorrect parent",
                        "error parent":"Required parent"
                    },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_tree(self):

        """

            This function return information of documentation view 'tree'

            :param self: Instance of Class DocumentationCategory
            :type self: DocumentationCategory

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Tree typify',
            operation_description="""

                This endpoint return tree.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Tree',
                    examples = {
                        'name': '3. Queja', 
                        'title': 'Tipo',
                        'id': 1,
                        'children': [
                            {
                                'name': '3. Queja', 
                                'title': 'Tipo',
                                'id': 1,
                            },
                        ]
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationCategory
            :type self: DocumentationCategory

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, category',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, category',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationCategory
            :type self: DocumentationCategory

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, category',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, category',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "classification": "int",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "int",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Tipo":{
                                "type":"choices",
                                "name":"classification",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        item[0]:item[1] for item in CategoryModel.ORIGIN_CHOICES
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationTypifySlackMessage(object):

    """

        Class define documentation of TypifySlackMessageView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TypifySlackMessageView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []

    class DocumentationSerializerTestCallVoid(serializers.Serializer):

        """

            Class define void Serializer of TypifySlackMessageView.
    
        """

        template: str = serializers.CharField(write_only=True)
        telephone: str = serializers.CharField(write_only=True)
        
        class Meta:

            fields: Union[list, str] = '__all__'
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatablesTickets

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTypifySlackMessage
            :type self: DocumentationTypifySlackMessage

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Typify slack message',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Typify slack message',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "category": 1,
                            "fields": ['agent', 'channel', 'subcategory', 'second_subcategory', 'service', 'location', 'rut', 'plan']
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "category": 1,
                            "fields": ['agent', 'channel', 'subcategory', 'second_subcategory', 'service', 'location', 'rut', 'plan']
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationTypifySlackMessage
            :type self: DocumentationTypifySlackMessage

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Typify slack message',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Typify slack message',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "category_name": "str",
                            "fields":"list",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Categoria":{
                                "field": "category_name",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Tipificación":{
                                "type":"choices",
                                "name":"category__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "tickets/category/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_fields_options(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTypifySlackMessage
            :type self: DocumentationTypifySlackMessage

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Typify message options',
            operation_description="""             """,
            responses = {
                200:Response_Openapi(
                    description = 'Typify message options',
                    examples = [
                        {"value":"category", "name":"Categoría"},
                        {"value":"agent", "name":"Agente"},
                        {"value":"services", "name":"Servicios"},
                        {"value":"liveagent", "name":"Liveagent"},
                        {"value":"rut", "name":"RUT"},
                        {"value":"customer_type", "name":"Tipo de cliente"},
                        {"value":"channel", "name":"Canal"},
                        {"value":"type", "name":"Tipo"},
                        {"value":"status", "name":"Estado"},
                        {"value":"city", "name":"Ciudad"},
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationTypifyTables(object):

    """

        Class define documentation of TypifyTablesView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TypifyTablesView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatablesTickets

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTypifyTables
            :type self: DocumentationTypifyTables

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Typify tables',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, tables',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "category": 1,
                            "fields": ['agent', 'channel', 'subcategory', 'second_subcategory', 'service', 'location', 'rut', 'plan']
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "category": 1,
                            "fields": ['agent', 'channel', 'subcategory', 'second_subcategory', 'service', 'location', 'rut', 'plan']
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_definition(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTypifyTables
            :type self: DocumentationTypifyTables

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Definition, Typify tables',
            operation_description="""            """,
            responses = {
                200:Response_Openapi(
                    description = 'Definition, Typify tables',
                    examples = {

                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": ["datetime", "str", "int", "bool", "choices", "location"]
                            },
                            "Año": {
                                "name": "year",
                                "type": ["int"]
                            },
                            "Mes": {
                                "name": "month",
                                "type": ["int"]
                            },
                            "Día": {
                                "name": "day",
                                "type": ["int"]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": ["int"]
                            },
                            "Semana": {
                                "name": "week",
                                "type": ["int"]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": ["int"]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": ["datetime", "str", "int", "bool"]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": ["datetime", "str", "int", "bool"]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": ["datetime", "str", "int", "bool"]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type":"datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": ["datetime", "str", "int", "bool", "choices", "location"]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type":"str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type":"str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type":"str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type":"str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                            "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                            "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                            "bool": [ "Igual", "Distinto" ],
                            "choices": [ "Igual", "Distinto" ],
                            "location": [ "Igual", "Distinto" ]
                        },
                        "filters": {
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "int",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Agente":{
                                "type":"choices",
                                "name":"agent__pk",
                                "type_choices": "int",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Fecha de creación":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Tipo de cliente":{
                                "type":"choices",
                                "name":"customer_type",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0: "No cliente",
                                        1: "Nuevo cliente",
                                        2: "Prospecto",
                                        3: "Cliente",
                                    }
                                }
                            },
                            "Canal de comunicación":{
                                "type":"choices",
                                "name":"channel",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0: "CORREO",
                                        1: "LLAMADA",
                                        2: "CHAT",
                                        3: "REDES",
                                        4: "WHATSAPP",
                                    }
                                }
                            },
                            "Tipo":{
                                "type":"choices",
                                "name":"type",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0: "IN",
                                        1: "OUT",
                                    }
                                }
                            },
                            "Estado":{
                                "type":"choices",
                                "name":"status",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0: "ABIERTO",
                                        1: "CERRADO",
                                        2: "ESPERANDO RESPUESTA",
                                        3: "DAR SEGUIMIENTO",
                                        4: "LLAMAR LUEGO",
                                    }
                                }
                            },
                            "Ciudad":{
                                "type":"choices",
                                "name":"city",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0: "ARICA",
                                        1: "SANTIAGO",
                                        2: "OTRA",
                                    }
                                }
                            },
                        },
                        "columns":  {
                            'creator': "Creador",
                            'updater': "Último en actualizar",
                            'created': "Fecha de creación",
                            'updated': "Fecha de actualización",
                            'category': "Categoria",
                            'agent': "Agente asignado",
                            'services': "Servicios", 
                            'liveagent': "Liveagent",
                            'rut': "RUT",
                            'customer_type': "Tipo de cliente",
                            'channel': "Canal",
                            'type': "Tipo",
                            'status': "Estado",
                            'city': "Ciudad", 
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationTypifyTables
            :type self: DocumentationTypifyTables

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, tables',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, tables',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                        },
                        "filters": {},
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )