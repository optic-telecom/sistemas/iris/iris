from .models import TypifyModel

def get_typify(typify_id: int):

    try:
        typify_instance = TypifyModel.objects.get(ID=int(typify_id))
        return typify_instance
    except:
        return False