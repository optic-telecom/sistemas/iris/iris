from django.apps import AppConfig


class TicketsConfig(AppConfig):

    """

        Class define documentation app Tickets.
    
    """

    name: str = 'tickets'
