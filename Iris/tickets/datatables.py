from datetime import date, datetime, timezone, timedelta
from dateutil.relativedelta import relativedelta
from typing import Any, List
from functools import reduce

import pytz
from django.db.models import Count, Q
from django.db.models.query import QuerySet
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.html import escape
from requests.api import delete
from django_datatables_view.base_datatable_view import BaseDatatableView

from common.dataTables import BaseDatatables
from common.models import BaseModel
from .serializers import TypifySerializer

from .models import *

class ProcessDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID","name"]
    FIELDS_FILTERS: Dict[str, str] = {

    }

    model: BaseModel = ProcessModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        return qs.filter( name__icontains=self.SEARCH)

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "description": instance.description
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":200
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "description": "json"

        }

        data_struct["columns"] = {

            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },

        }

        data_struct["filters"] = {

        }

        return data_struct

class CategoryDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
        "classification":"choices",
    }

    model: BaseModel = CategoryModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search)

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "classification": instance.classification,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":500
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "classification": "int",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Tipo":{
                "type":"choices",
                "name":"classification",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        item[0]:item[1] for item in self.model.ORIGIN_CHOICES
                    }
                }
            }

        }

        return data_struct

class TypifySlackMessageDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = TypifySlackMessageModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search) | Q(category__name__icontains=search))

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "category_name": instance.category.name,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":700
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "category_name": "str",
            "fields":"list",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Categoria":{
                "field": "category_name",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Tipificación":{
                "type":"choices",
                "name":"category__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "tickets/category/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class TypifyTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]


    model: BaseModel = TypifyTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class TypifyDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = [
        "ID", "created", "creator", "updated", "updater", "category", "agent", "rut", "customer_type",
        "channel", "type", "status","city", "services"
    ]

    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "creator__pk":"choices",
        "agent__pk":"choices",
        "customer_type":"choices",
        "channel":"choices",
        "type":"choices",
        "status":"choices",
        "category__pk":"choices", 
    }

    FIELDS_EXCEL: Dict[str, Dict[str, str]] = {
        'A': 'Número de ticket',
        'B': 'Fecha de creación',
        'C': 'Agente creador',
        'D': 'Ultima fecha de actualización',
        'E': 'Ultimo agente en actualizar',
        'F': 'Tipo',
        'G': 'Categoria',
        'H': 'Sub-Categoria',
        'I': 'Segunda Sub-Categoría',
        'J': 'Servicio',
        'K': 'Comentarios',
        'L': 'Recontactos',
        'M': 'Comunicaciones',
        'N': 'Canal',
        'O': 'Tipo',
        'P': 'Ciudad',
        'Q': 'Estatus',
        'R': 'SLA',
        'S': 'Rut',
    }

    model: BaseModel = TypifyModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = str(self.SEARCH)

        if search:

            if search.isdigit():

                int_value: int = int(search)
                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: int_value in x.services, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(ID=int_value) |
                    Q(ID__in=list_typify_id)
                )

            else:

                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: search in x.rut, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(category__name__icontains=search) | 
                    Q(agent__username__icontains=search) |
                    Q(ID__in=list_typify_id) 
                )

        return qs

    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_typify: QuerySet = TypifyTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_typify.filters

        data_typify: QuerySet = super().get_filtered_queryset()

        #Tickets assigned to request user
        if instance_tables_typify.assigned_to_me:

            data_typify: QuerySet = data_typify.filter(agent=self.request.user)

        #Tickets created to request user
        if instance_tables_typify.create_me:

            data_typify: QuerySet = data_typify.filter(creator=self.request.user)

        #Tickets time interval
        if instance_tables_typify.last_time_type and instance_tables_typify.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_typify: QuerySet = data_typify.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_typify.last_time_type] * instance_tables_typify.last_time) )

        return data_typify

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        result: Dict[str, Union[str, int, float]] = TypifySerializer(instance).data

        result["creator"]: str = instance.creator.username
        result["updater"]: str = instance.updater.username if instance.updater != None else None
        result["category"]: str = [ category.name for category in instance.category.all() ]
        result["agent"]: str = instance.agent.username
        result["customer_type"]: str = instance.get_customer_type_display()
        result["channel"]: str = instance.get_channel_display()
        result["type"]: str = instance.get_type_display()
        result["status"]: str = instance.get_status_display()
        result["city"]: str = instance.get_city_display()

        return result

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_tables_typify: QuerySet = TypifyTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "download":True
        }

        data_struct["fields"] = {
            "ID": "int",
            "created": "datetime",
            "updated": "datetime",
            "SLA": "float",
            "status_SLA": "str",
            "operator": "int",
            "services": "list",
            "commentaries": "int",
            "rut": "list",
            "customer_type": "int",
            "channel": "int",
            "type": "int",
            "city": "int",
            "status": "int",
            "creator": "str",
            "updater": "str",
            "category": "str",
            "agent": "str"
        }

        #Init column definition#
        ########################

        columns: Dict[str, Dict[str, Any]] = {
            'creator':{
                "sortable": True,
                "name":"Creador",
                "width":100,
                "fixed":None
            },
            'updater':{
                "sortable": True,
                "name":"Actualizado",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Creado",
                "width":100,
                "fixed":None
            },
            'updated':{
                "sortable": True,
                "name":"Última actualización",
                "width":100,
                "fixed":None
            },
            'category':{
                "sortable": True,
                "name":"Categoría",
                "width":100,
                "fixed":None
            },
            'agent':{
                "sortable": True,
                "name":"Agente",
                "width":100,
                "fixed":None
            },
            'services':{
                "sortable": True,
                "name":"Servicios",
                "width":200,
                "fixed":None
            }, 
            'city':{
                "sortable": True,
                "name":"Ciudad",
                "width":100,
                "fixed":None
            },
            'rut':{
                "sortable": True,
                "name":"Rut",
                "width":100,
                "fixed":None
            },
            'customer_type':{
                "sortable": True,
                "name":"Tipo de cliente",
                "width":100,
                "fixed":None
            },
            'channel':{
                "sortable": True,
                "name":"Canal",
                "width":100,
                "fixed":None
            },
            'type':{
                "sortable": True,
                "name":"Tipo",
                "width":100,
                "fixed":None
            },
            'status':{
                "sortable": True,
                "name":"Estado",
                "width":100,
                "fixed":None
            },
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_typify.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1

        data_struct_columns["Modificar"] = {
            "field": "custom_change",
            "sortable": False,
            "visible": True,
            "position": position,
            "width":100,
            "fixed":"right",
            "download": True,
        }

        data_struct["columns"] = data_struct_columns

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll": reduce(lambda x, y: x + y, [ column["width"] for column in  data_struct_columns.values()] ) + 100,
            "download":True
        }

        #End  column definition#
        ########################

        #Init filters definition#
        #########################

        filters: Dict[str, Dict[str, Any]] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Tipo de cliente":{
                "type":"choices",
                "name":"customer_type",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'No cliente',
                        3:'Cliente',
                    }
                }
            },
            "Canal":{
                "type":"choices",
                "name":"channel",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'CORREO',
                        1:'LLAMADA',
                        2:'CHAT',
                        3:'REDES',
                        4:'WHATSAPP',
                    }
                }
            },
            "Tipo":{
                "type":"choices",
                "name":"type",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'IN',
                        1:'OUT',
                    }
                }
            },
            "Ciudad":{
                "type":"choices",
                "name":"type",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'Arica',
                        1:'Santiago',
                        2:'Otra',
                    }
                }
            },
            "Estado":{
                "type":"choices",
                "name":"status",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'ABIERTO',
                        1:'CERRADO',
                        2:'ESPERANDO RESPUESTA',
                        3:'DAR SEGUIMIENTO',
                        4:'LLAMAR LUEGO',
                        5: 'CANCELADO'
                    }
                }
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Agente":{
                "type":"choices",
                "name":"agent__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Categoría":{
                "type":"choices",
                "name":"category__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "tickets/category/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        filters_names : Dict[str, str] = {
            'created': ["Fecha de inicio", "Fecha de fin"],
            'city':["Ciudad"],
            'customer_type':["Tipo de cliente"],
            'channel':["Canal"],
            'type':["Tipo"],
            'status':["Estado"],
            'creator__pk':["Creador"],
            'agent__pk':["Agente"],
            'category':["Categoría"],
        }

        for _filter in instance_tables_typify.filters:

            for _field in filters_names[_filter[0]]:

                del filters[_field]

        data_struct["filters"] = filters

        #End filters definition#
        ########################

        return data_struct

    def get_qs_to_data(self, qs: QuerySet) -> List[Dict[str, Any]]:

        """

            This function, process data

        """

        #Count new contacts
        new_contact: BaseModel = NewContactModel.objects.all().values('typify__ID').annotate(total=Count('typify__ID'))
        new_contact_count: Dict[int, int] = { contact['typify__ID']:contact['total'] for contact in new_contact }

        #Get technicians
        technicians = requests.get(
            url=matrix_url('technicians'),
            params={
                'fields':'name,id',
            },
            headers={
                "Authorization": get_token()
            }
        ).json()
        dict_technicians: Dict[int, str] = { item['id']: item['name'] for item in technicians}

        def get_typify_data(instance: TypifyModel) -> List[List[Any]]:

            def get_category( category ) -> List[str]:

                """
                
                    This function return all parents of category

                    :param category: Instance of CategoryModel
                    :type category: CategoryModel

                    :return: Lisf of all parent of category
                    :rtype: list
                
                """

                result: List[str] = []

                if category:

                    result.append( category.name )

                    if category.parent:

                        result.append( category.parent.name )

                        if category.parent.parent:

                            result.append( category.parent.parent.name )

                            if category.parent.parent.parent:

                                result.append( category.parent.parent.parent.name )

                result.reverse()

                if len( result ) == 3:

                    result.append( '' )
            
                return result

            def open_time( instance_model ) -> int:

                """
        
                    This funtion calc open time of ticket

                    :param self: Instance of Class TypifySerializer
                    :type self: TypifySerializer
                    :param instance_model: instance of model TypifyModel
                    :type instance_model: TypifyModel

                    :returns: Returns float of open time
                    :rtype: float
                
                """

                def next_opened(changes) -> Dict[str, Any]:

                    """
        
                        This funtion search next closed change

                        :param self: List of changes
                        :type self: list

                        :returns: Returns list of next closed change
                        :rtype: list
                
                    """

                    while ( len( changes ) and changes[0]['value'] == 'CERRADO' ):

                        changes = changes[1:]

                    return changes

                def next_closed(changes) -> Dict[str, Any]:

                    """
        
                        This funtion search next opened change

                        :param self: List of changes
                        :type self: list

                        :returns: Returns list of next opened change
                        :rtype: list
                
                    """

                    while ( len( changes ) and changes[0]['value'] != 'CERRADO' ):

                        changes = changes[1:]

                    return changes

                def all_changes(instance_model_history: QuerySet) -> List[Dict[str, Any]]:

                    """
        
                            This funtion get all changes

                            :param instance_model_history: List of changes
                            :type instance_model_history: QuerySet

                            :returns: Returns list of changes
                            :rtype: list
                    
                        """

                    result: List[Dict[str, Any]] = []
                    next_value: Any = None
                    next_datetime: datetime = None

                    for index, change in enumerate(instance_model_history[0: len(instance_model_history) - 1], start=0):

                        #Get changes
                        fields_changes = change.diff_against(instance_model_history[index + 1])

                        for change in fields_changes.changes:
                            
                            #Save changes of 'status' field
                            if change.field == 'status' and change.new != change.old:
                                
                                #Save old value
                                result.append(
                                    {
                                        'value': change.old,
                                        'datetime': instance_model_history[index].history_date,
                                    }
                                )

                                next_value: Any = change.new
                                next_datetime: datetime = instance_model_history[index + 1].history_date

                    if ( next_value ):

                        #Save latest value
                        result.append(
                            {
                                'value': next_value,
                                'datetime': next_datetime,
                            }
                        )

                    return result
                
                total: int = 0
                instance_model_history: QuerySet = instance_model.history.all().reverse()
                changes: List[Dict[str, Any]] = all_changes(instance_model_history)

                #Check if have been changes 'status' field
                if len( changes ) :
                    
                    #While there are time interval to count
                    while len( changes ):

                        #Next time open
                        changes: Dict[str, Any] = next_opened(changes)

                        #While there are time opened interval to count
                        if changes:

                            #Time it was open
                            datetime_opened: datetime = changes[0]['datetime']

                            #Next time close
                            changes: Dict[str, Any] = next_closed(changes)

                            #While there are time closed interval to count
                            if changes:

                                #Time it was close
                                datetime_closed: datetime = changes[0]['datetime']
                                #Time difference
                                total: int = total + (datetime_closed - datetime_opened).seconds

                            else:

                                #Time difference
                                total: int = total + (datetime.now(timezone.utc) - datetime_opened).seconds

                else:

                    #Instance was not created closed
                    if instance_model.status != 'CERRADO':

                        #Time difference
                        total:int = (instance_model.created - datetime.now(timezone.utc)).seconds


                return total//60

            def get_new_contacts (instance_model ) -> list:

                result = []
                new_contacts = NewContactModel.objects.filter(typify=instance_model, deleted=False)

                for contact in new_contacts:
                    instance_list = [contact.ID, contact.created.strftime('%d/%m/%Y %H:%M')] + ([' '] * 9) + [contact.commentary] + [' '] + [dict(TypifyModel.ORIGIN_CHOICES)[int(contact.get_channel_display())]]
                    result.append(instance_list)

                return result

            def get_communications (instance_model ) -> list:

                result = []
                communications = TicketCommunicationModel.objects.filter(typify=instance_model, deleted=False)

                for communication in communications:
                    instance_list = [communication.ID, communication.created.strftime('%d/%m/%Y %H:%M')] + ([' '] * 10) + [communication.commentary] + [dict(TypifyModel.ORIGIN_CHOICES)[int(communication.get_channel_display())], communication.get_type_display()]
                    result.append(instance_list)

                return result

            result : List[List[Any]] = []

            category: List[str] = get_category(instance.category.first())
            contact: int = new_contact_count[instance.ID] if instance.ID in new_contact_count else 0

            result.append([
                instance.ID,
                (instance.created - timedelta(hours=5)).strftime('%d/%m/%Y %H:%M'),
                instance.creator.username if instance.creator else '',
                (instance.updated - timedelta(hours=5)).strftime('%d/%m/%Y %H:%M'),
                instance.updater.username if instance.updater else '',
                category[0],
                category[1] if len(category) > 1 else " ",
                category[2] if len(category) > 2 else " ",
                category[3] if len(category) > 3 else " ",
                ','.join( list( map( lambda x: str(x), instance.services) ) ),
                instance.commentaries,
                get_new_contacts(instance),
                get_communications(instance),
                " ",
                " ",
                instance.get_city_display(),
                instance.get_status_display(),
                open_time(instance),
                ','.join( list( map( lambda x: str(x), instance.rut) ) ),
            ])

            return result

        return reduce( lambda x, y: x + y, map(get_typify_data, qs) )