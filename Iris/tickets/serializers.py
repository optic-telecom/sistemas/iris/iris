import re
from datetime import datetime, timezone, timedelta
from typing import Any, List, Union, Dict
import requests
import random
import string
import json
import base64
from jinja2 import Template
from slack import WebClient
from common.models import BaseModel
from communications.models import TemplateEmailModel
from common.serializers import BaseSerializer
from common.utilitarian import communications_url, get_token, matrix_url, get_ticket_SLA, get_ticket_status_SLA
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from rest_framework import serializers
from rest_framework.serializers import ValidationError
from schema import And, Schema, Use
from dynamic_preferences.registries import global_preferences_registry
from escalation_ti.models import EscalationModel, EvaluationTestModel, OptionsTestModel, TestModel
from escalation_ti.api import create_escalation
from users.models import UserSlackIDModel
from django_q.models import Schedule
from .models import CategoryModel, NewContactModel, TicketCommunicationModel, TypifyModel, TypifySlackMessageModel, TypifyTablesModel, ProcessModel
from .queue import send_reminder
from operators.models import OperatorModel
from retentions.models import RetentionsTipifyCategoryModel, RetentionModel
from escalation_finance.models import FinanceTipifyCategoryModel, FinanceEscalationModel

class TypifySlackMessageSerializer(BaseSerializer):

    """

        Class define serializer of TypifySlackMessageView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = TypifySlackMessageModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class ProcessSerializer(BaseSerializer):

    """

        Class define serializer of ProcessView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = ProcessModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }


class CategorySerializer(BaseSerializer):

    """

        Class define serializer of CategoryView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = CategoryModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
            'operator': {'required': False},
        }

    def update(self, instance, validated_data):

        def delete_item(item: str, value: Dict [str, Any]) -> Dict [str, Any]:

            if item in value:

                del value[item]

            return value

        return super().update(instance, delete_item('classification', delete_item('parent', validated_data) ))

class TypifySerializer(BaseSerializer):

    """

        Class define serializer of TypifyView.
    
    """
 
    #Method get_SLA
    SLA = serializers.SerializerMethodField()
    #Method get_status_SLA
    status_SLA = serializers.SerializerMethodField()
    process_display = serializers.SerializerMethodField()

    def get_process_display(self, instance_model : TypifyModel ) -> str:

        return instance_model.process.name if instance_model.process else ""
            
    def get_SLA(self, instance_model : TypifyModel ) -> float:

        closed_status_list = [1,5]

        return get_ticket_SLA(instance_model, closed_status_list, 'status')

    def get_status_SLA(self, obj: TypifyModel) -> str:

       return get_ticket_status_SLA(obj, self.get_SLA(obj))

    def slack_message(self, instance_model : TypifyModel):

        def send_message(typify_slack_message: TypifySlackMessageModel, instance_model : TypifyModel):

            def search_value(field: str, instance_model : TypifyModel) -> Any:

                def get_technician(instance_model : TypifyModel):

                    result: str = ''

                    if instance_model.technician:

                        url_technician: str = matrix_url('technicians')
                        url_technician: str = f'{url_technician}{instance_model.technician}/'
                        
                        technician_data: List[int] = requests.get(
                            url=url_technician,
                            verify=False,
                            headers={
                                "Authorization": get_token()
                            }
                        ).json()

                        result: str = technician_data['name']

                    return result

                def get_plan(instance_model : TypifyModel):

                    result: str = ''

                    if instance_model.services:

                        str_services: str = ','.join( list( map( lambda service: str(service), instance_model.services ) ) )

                        url_service: str = matrix_url('services')
                        
                        services_data: List[int] = requests.get(
                            url=url_service,
                            verify=False,
                            headers={
                                "Authorization": get_token()
                            },
                            params={
                                "number__in":instance_model.services,
                                "fields":"plan"
                            }
                        ).json()

                        plans : List[int] = list(map( lambda service: service['plan'].split('plans/')[1].replace('/','') , services_data ))
                        url_plans: str = matrix_url('plans')

                        plans_data: List[int] = requests.get(
                            url=url_plans,
                            verify=False,
                            headers={
                                "Authorization": get_token()
                            },
                            params={
                                "id__in":plans,
                                "fields":"name"
                            }
                        ).json()

                    return ', '.join([ plan['name'] for plan in plans_data ])

                _callable: Dict[str, callable()] = {
                    'agent': lambda instance: instance.agent.username , 
                    'channel': lambda instance: instance.get_channel_display(), 
                    'subcategory': lambda instance:  ', '.join( list( map( lambda category: category.name, instance.category.all() ) ) ),
                    'second_subcategory': lambda instance: ', '.join( list( map( lambda category: category.name if category.classification == 4 else ' ', instance.category.all() ) ) ),
                    'services': lambda instance: ', '.join( list( map( lambda service: str(service), instance.services ) ) ), 
                    'rut': lambda instance: ', '.join( list( map( lambda rut: str(rut), instance.rut ) ) ),
                    'plan': get_plan,
                    'customer_type': lambda instance: instance.get_customer_type_display(),
                    'type': lambda instance: instance.get_type_display(),
                    'city': lambda instance: instance.get_city_display(),
                    'status': lambda instance: instance.get_status_display(),
                }

                return _callable[field](instance_model)

            data: Dict[str, Any] = {}
            for field in typify_slack_message.fields:
                data[field] = search_value(field, instance_model)

            global_preferences = global_preferences_registry.manager()

            #Get the right access token based on the operator
            user_uid_query: str = UserSlackIDModel.objects.filter(user=instance_model.creator,operator=instance_model.operator)
            user_uid = "<@" + str(user_uid_query[0].uid) + ">" if len(user_uid_query) > 0 else ''
            client = WebClient(token=instance_model.operator.slack_token)

            labels: Dict[str,str] = {
                "customer_type": f"Tipo de Cliente: {data.get('customer_type')} \n", 
                "agent": f"Agente: {data.get('agent')}\n", 
                "channel": f"Canal: {data.get('channel')}\n",
                "subcategory": f"Subcategoría: {data.get('subcategory')}\n",
                "second_subcategory": f"Segunda Subcategoría: {data.get('second_subcategory')}\n",
                "services": f"Servicios: {data.get('services')}\n", 
                "rut": f"Rut: {data.get('rut')}\n", 
                "plan": f"Plan: {data.get('plan')}\n", 
                "type": f"Tipo: {data.get('type')}\n", 
                "city": f"Ciudad: {data.get('city')}\n", 
                "status": f"Estado: {data.get('status')}\n", 
            }           

            #Get labels and assemble slack message depending on the fields specified
            items: str = ""
            for item in data:
                if (item == "second_subcategory" and data.get('second_subcategory') == ''):
                    continue
                items = items + labels[item]
           
            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Tickets"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":pencil:*{instance_model.creator}* {user_uid} ha creado un nuevo ticket. ID: *{instance_model.pk}*"
                    }
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": items
                    }
                },
                {
                    "type": "divider"
                }
            ]

            response = client.chat_postMessage(
                channel=typify_slack_message.channel,
                blocks=blocks,
                text=f"{instance_model.creator} {user_uid} ha creado un nuevo ticket. ID: {instance_model.pk}"
            )

            return response.data['ts']

        threads = {}
        #Check if there is a slack mesage that matches the category
        for typify_slack in TypifySlackMessageModel.objects.filter(category__in=instance_model.category.all(), deleted=False):
            thread = send_message(typify_slack, instance_model)
            threads[thread] = typify_slack.channel
        
        #Save the instance message thread so it can send the updates
        instance_model.slack_thread = threads
        instance_model.save()

    def add_reminder(self, instance_model : TypifyModel):

        #Get instance sla
        sla = instance_model.category.first().sla 

        #Send a message when half of the sla has passed
        schedule_object: Schedule = Schedule.objects.create(
            func='tickets.queue.send_reminder',
            name=''.join(random.choice(
                string.ascii_uppercase + string.digits) for _ in range(20)),
            schedule_type='O',
            repeats=1,
            next_run=datetime.now() + timedelta(hours=sla/2),
            kwargs={
                'pk_typify': int(instance_model.ID),
                'closed': False,
                'custom': False
            },
            minutes=5,
        )

        #Send a message when the ticket la has ended
        schedule_object: Schedule = Schedule.objects.create(
            func='tickets.queue.send_reminder',
            name=''.join(random.choice(
                string.ascii_uppercase + string.digits) for _ in range(20)),
            schedule_type='O',
            repeats=1,
            next_run=datetime.now() + timedelta(hours=sla),
            kwargs={
                'pk_typify': int(instance_model.ID),
                'closed': True,
                'custom': False
            },
            minutes=5,
        )

    def send_email(self, instance_model: TypifyModel):

        global_preferences: Dict[str, any] = global_preferences_registry.manager()

        #Check that the template exists
        try:
            email_template = TemplateEmailModel.objects.get(ID=instance_model.process.description['template'])
        except:
            raise ValidationError({
                "Incorrect value": "Invalid Email Template"
            })

        #Verify that the description has a valid sender
        if instance_model.process.description.get('sender', None) == None:
            raise ValidationError({
                "Incorrect value": "Invalid sender email"
            })

        #Validate customer data
        if instance_model.rut == None or instance_model.services == None\
            or len(instance_model.services) == 0  or len(instance_model.rut) == 0:
            raise ValidationError({
                "Invalid Customer": "There was no rut or services specified"
            })

        for rut in instance_model.rut:
            #Get customer email
            try:

                url: str = matrix_url('customers') + f'?rut={rut}'
                token: str = get_token()
            
                receiver = requests.get(
                    url=url,
                    headers={
                        'Authorization': f'JWT {token}'
                    },
                    verify=False,
                ).json()[0]['email']

            except:
                raise ValidationError({
                    "Incorrect value": "Customer doesnt exists or doesnt have an email assigned"
                })

            def get_matrix_data(service):

                filters = [['number', 'equal', service]]

                data: Dict[str, Any] = {
                    "filters": filters,
                    "includes": [],
                    "excludes": [],
                    "unpaid_ballot": False,
                    "criterion": 'D'
                }

                matrix_url: str = global_preferences['general__matrix_domain'] + 'services-filters/'

                response = requests.get(
                    url=matrix_url,
                    headers={
                        "Authorization": global_preferences['matrix__matrix_token']
                    },
                    json=data,
                    verify=False,
                )

                if response.status_code != 200:

                    raise ValidationError({
                        "Error matrix": "Error matrix filter"
                    })

                result = response.json()[0]

                if 'service__number' in result:
                   result['service__number_64'] = str(base64.b64encode(bytes(str(result['service__number']), 'utf-8')), 'utf-8')

                if 'customer__id' in result:
                    result['customer__id_64'] = str(base64.b64encode(bytes(str(result['customer__id']), 'utf-8')), 'utf-8')

                return result

            def current_time_tags(data: Dict[str, Any]) -> Dict[str, Any]:

                months: Dict[int, str] = {
                    1: "Enero",
                    2: "Febrero",
                    3: "Marzo",
                    4: "Abril",
                    5: "Mayo",
                    6: "Junio",
                    7: "Julio",
                    8: "Agosto",
                    9: "Septiembre",
                    10: "Octubre",
                    11: "Noviembre",
                    12: "Diciembre",
                }

                weekdays: Dict[int, str] = {
                    0: "Lunes",
                    1: "Martes",
                    2: "Miercoles",
                    3: "Jueves",
                    4: "Viernes",
                    5: "Sabado",
                    6: "Domingo",
                }

                current_datetime: datetime = datetime.now()

                data['current__month'] = months[int(current_datetime.month)]
                data['current__year'] = current_datetime.year
                data['current__day'] = current_datetime.day
                data['current__week_day'] = weekdays[int(current_datetime.weekday())]
                data['current__datetime'] = current_datetime.strftime("%d/%m/%Y")

                return data

            data: Dict[str, Any] = get_matrix_data(instance_model.services[0])
            data: Dict[str, Any] = current_time_tags(data)

            enviroment: str = global_preferences['general__Enviroment']

            if enviroment != "production": 
                receiver = "arianaamador@hotmail.com"

            json_data = {
                'subject': email_template.subject,
                'sender': instance_model.process.description['sender'],
                'receiver': receiver,
                'operator': instance_model.operator,
                'message': Template(email_template.template_html).render(data),
            }
            #TODO update with email api function
            url: str = global_preferences['general__iris_backend_domain'] + "api/v1/communications/email/"

            headers: Dict[str, str] = {
                'AUTHORIZATION': self.context.get("request").META.get("HTTP_AUTHORIZATION")
            }


            #Send email data
            response = requests.post(
                url=url,
                headers=headers,
                verify=False,
                data=json_data,
            )

    def create(self, validated_data: Dict[str, Any]) -> TypifyModel:

        """
        
            This funtion validate and create instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """
        validated_data['agent'] = self.context['request'].user
        escalate = self.context['request'].data.get('escalate', False)
        escalation_data = self.context['request'].data.get('escalation_data', None)
        result = super().create(validated_data)
        for category in json.loads(self.context['request'].data.get("category")):
            category = CategoryModel.objects.get(pk=int(category))
            result.category.add(category)
        communication_instance = TicketCommunicationModel.objects.create(
            creator=result.creator,
            operator=result.operator,
            typify=result,
            commentary= "Inicial",
            channel = result.channel,
            type=result.type
        )
        if escalate:
            if escalation_data == None or len(escalation_data) == 0:
                raise ValidationError({'tests': 'No test were provided'})
            else:
                escalation_data = json.loads(escalation_data)
                evaluation_list = []
                for test in escalation_data["tests"]:
                    try: 
                        test_instance = TestModel.objects.get(ID=test['test'])
                        option_instance = OptionsTestModel.objects.get(ID=test['result'])
                        evaluation_instance = EvaluationTestModel.objects.create(
                            operator=result.operator, 
                            creator=self.context['request'].user,
                            updater=self.context['request'].user,
                            test=test_instance,
                            result=option_instance
                        )
                        evaluation_list.append(evaluation_instance)
                    except:
                        raise ValidationError({'escalation_data': 'Invalid test or result id'})

                escalation_id = create_escalation(self.context['request'], evaluation_list, result, escalation_data["comment"])
                result.escalation_tk = escalation_id
                result.save()

        self.add_reminder(result)
        self.slack_message(result)

        #If the process has a template associated, send an email to the customer
        if 'process' in validated_data and 'template' in validated_data['process'].description:
            self.send_email(result)

        if len(RetentionsTipifyCategoryModel.objects.filter(category__in=result.category.all())) > 0 and result.services != []:
            for service in result.services:
                RetentionModel.objects.create(
                    creator=result.creator,
                    operator=result.operator,
                    service=service
                )

        if len(FinanceTipifyCategoryModel.objects.filter(category__in=result.category.all())) > 0 and result.services != []:
            finance_ticket_instance = FinanceEscalationModel.objects.create(
                creator=result.creator,
                operator=result.operator,
                typify=result.ID,
                services=result.services,
                rut=result.rut[0],
                whatsapp_conversation=result.conversation_whatsapp,
                email_conversation=result.conversation_email,
            )

        return result

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        #Check that the typify is not cancelled nor closed

        if instance.status == 5:
            raise ValidationError(
                {
                    "Invalid update":"Ticket has been cancelled"
                }
            )

        if  instance.status != 1 or \
            (instance.status == 1 and validated_data.get('status', 1) != 1):

            def slack_message(instance: TypifyModel, validated_data: Dict[str,Any], username: str):

                def send_message(thread: str, channel: str, validated_data: Dict[str,Any], instance: TypifyModel, username: str):

                    def search_value(field: str, validated_data : Dict[str,Any], typify_instance: TypifyModel) -> Any:

                        def get_technician(validated_data : Dict[str,Any]):

                            result: str = ''

                            if validated_data.get('technician'):

                                technician=validated_data.get('technician')

                                url_technician: str = matrix_url('technicians')
                                url_technician: str = f'{url_technician}{technician}/'
                                
                                technician_data: List[int] = requests.get(
                                    url=url_technician,
                                    verify=False,
                                    headers={
                                        "Authorization": get_token()
                                    }
                                ).json()

                                result: str = technician_data['name']

                            return result

                        def get_plan(validated_data : Dict[str,Any]):

                            result: str = ''

                            if validated_data.get('services'):

                                str_services: str = ','.join( list( map( lambda service: str(service), validated_data.get('services') ) ) )

                                url_service: str = matrix_url('services')
                                
                                services_data: List[int] = requests.get(
                                    url=url_service,
                                    verify=False,
                                    headers={
                                        "Authorization": get_token()
                                    },
                                    params={
                                        "number__in":validated_data.get('services'),
                                        "fields":"plan"
                                    }
                                ).json()

                                plans : List[int] = list(map( lambda service: service['plan'].split('plans/')[1].replace('/','') , services_data ))
                                url_plans: str = matrix_url('plans')

                                plans_data: List[int] = requests.get(
                                    url=url_plans,
                                    verify=False,
                                    headers={
                                        "Authorization": get_token()
                                    },
                                    params={
                                        "id__in":plans,
                                        "fields":"name"
                                    }
                                ).json()

                            return ', '.join([ plan['name'] for plan in plans_data ])

                        _callable: Dict[str, callable()] = {
                            'agent': lambda instance: validated_data.get("agent").username, 
                            'channel': lambda instance: dict(TypifyModel.ORIGIN_CHOICES)[validated_data.get('channel')], 
                            'subcategory': lambda instance:  ', '.join( list( map( lambda category: category.name, instance.category.all() ) ) ),
                            'second_subcategory': lambda instance: ', '.join( list( map( lambda category: category.name if category.classification == 4 else ' ', instance.category.all() ) ) ),
                            'services':lambda instance: ','.join( list( map( lambda service: str(service), validated_data.get("services",[]) ) ) ), 
                            'rut': lambda instance: ','.join( list( map( lambda rut: str(rut), validated_data.get("rut",[]) ) ) ), 
                            'plan': get_plan,
                            'customer_type': lambda instance: dict(TypifyModel.CUSTOMER_TYPE_CHOICES)[validated_data.get('customer_type')],
                            'type': lambda instance: dict(TypifyModel.TYPE_TK_CHOICES)[validated_data.get('type')],
                            'city': lambda instance: dict(TypifyModel.CITY_CHOICES).get(validated_data.get('city')),
                            'status': lambda instance: dict(TypifyModel.STATE_CHOICES)[validated_data.get('status')],
                        }

                        if field != 'escalation_tk': 
                            return _callable[field](validated_data)
                        else: 
                            return typify_instance.escalation_tk

                    history = instance.history.all()

                    last_change = history[0]

                    fields_changed = last_change.diff_against(history[1]).changes

                    fields = []
                    for change in fields_changed:
                        fields.append(change.field)          
                    
                    data: Dict[str, Any] = {}
                    skip_fields = ['operator', 'updated', 'updater', 'commentaries', 'created', 'creator']

                    for field in fields:
                        if field in skip_fields:
                            continue
                        data[field] = search_value(field, validated_data, instance)
                    
                    labels: Dict[str,str] = {
                    "customer_type": f"Tipo de Cliente: {data.get('customer_type')} \n", 
                    "agent": f"Agente: {data.get('agent')}\n", 
                    "channel": f"Canal: {data.get('channel')}\n",
                    "subcategory": f"Subcategoría: {data.get('subcategory')}\n",
                    "second_subcategory": f"Segunda Subcategoría: {data.get('second_subcategory')}\n",
                    "services": f"Servicios: {data.get('services')}\n", 
                    "rut": f"Rut: {data.get('rut')}\n", 
                    "plan": f"Plan: {data.get('plan')}\n", 
                    "type": f"Tipo: {data.get('type')}\n", 
                    "city": f"Ciudad: {data.get('city')}\n", 
                    "status": f"Estado: {data.get('status')}\n", 
                    "escalation_tk": f"Ticket de escalamiento ID: {data.get('escalation_tk')}\n", 
                    }

                    global_preferences = global_preferences_registry.manager()

                    #Get the right access token based on the operator
                    client = WebClient(token=instance.operator.slack_token)
                    items = " "
                    #Get labels and assemble slack message depending on the changed fields
                    for item in data:
                        items = items + labels[item]
                    
                    blocks: List[Dict[str, Any]] =  [
                        {
                            "type": "divider"
                        },
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": ":iris: *Iris* Tickets"
                            }
                        },
                        {
                            "type": "divider"
                        },
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": f":writing_hand:*" + username + "* ha actualizado el ticket"
                            }
                        },
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": items
                            }
                        },
                        {
                            "type": "divider"
                        }
                    ]

                    client.chat_postMessage(
                        blocks=blocks,
                        thread_ts=thread,
                        channel=channel,
                        text=f" " + username + " ha actualizado el ticket"
                    )

                #Check if the typify has an active slack thread, if it has one, send the updates as a response
                if instance.slack_thread != None:
                    for thread in instance.slack_thread:
                        send_message(thread, instance.slack_thread[thread], validated_data, instance, username)

            if instance.escalation_tk and 'status' in validated_data:

                instance_escalation: EscalationModel = EscalationModel.objects.get(typify=instance.ID)

                if validated_data['status'] == 5:
                        
                        instance_escalation.status = 3
                        instance_escalation.save()
                
                #If the instance is being closed, close the escalation related too
                if validated_data['status'] == 1:

                    #For closing an escalation, it has to be reviewed by SAC first
                    if instance_escalation.status != 1 and validated_data['status'] == 1:
                        raise ValidationError(
                            {
                                "escalation status":"the escalation ticket hasn't been closed"
                            }
                        )

                    else:

                        instance_escalation.status = 2
                        instance_escalation.save()

                else:
                    
                    #If the typify is still open, make sure that the escalation related is also open
                    try:

                        instance_escalation: EscalationModel = EscalationModel.objects.get(typify=instance.ID)

                        if instance_escalation.status == 2:

                            instance_escalation.status = 0
                            instance_escalation.save()
                            
                    except:

                        pass

        else:

            raise ValidationError(
                {
                    "Invalid update":"Ticket is closed"
                }
            )

        

        result = super().update(instance, validated_data)
        
        #Escalate ticket
        escalate = self.context['request'].data.get('escalate', False)
        escalation_data = self.context['request'].data.get('escalation_data', None)
        if escalate:
            if escalation_data == None or len(escalation_data) == 0:
                raise ValidationError({'tests': 'No test were provided'})
            else:
                escalation_data = json.loads(escalation_data)
                evaluation_list = []
                for test in escalation_data['tests']:
                    try: 
                        test_instance = TestModel.objects.get(ID=test['test'])
                        option_instance = OptionsTestModel.objects.get(ID=test['result'])
                        evaluation_instance = EvaluationTestModel.objects.create(
                            operator=result.operator, 
                            creator=self.context['request'].user,
                            updater=self.context['request'].user,
                            test=test_instance,
                            result=option_instance
                        )
                        evaluation_list.append(evaluation_instance)
                    except:
                        raise ValidationError({'escalation_data': 'Invalid test or result id'})

                escalation_id = create_escalation(self.context['request'], evaluation_list, result)
                result.escalation_tk = escalation_id
                result.save()
        
        #Send a slack message with the updates of the ticket
        slack_message(instance , validated_data, self.context['request'].user.username)
        return result

    class Meta:

        #Class model of serializer
        model: BaseModel = TypifyModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        read_only_fields: Union[List[str], str] = ['category']
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'customer': {'required': False},
            'creator': {'required': False},
            'updater': {'required': False},
            'parent': {'required': False},
            'agent': {'required': False},
            'technician': {'required': False},
            'created': {'read_only': True},
            'discount_reason': {'required': False},
            'discount_responsable_area': {'required': False},
            'discount_porcentage': {'required': False},
        }

class NewContactSerializer(BaseSerializer):

    """

        Class define serializer of NewContactView.
    
    """

    typify = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='tickets:typify-detail',
    )
    typify_id = serializers.IntegerField(write_only=True, source='typify', required=False)

    def typify_get(self, validated_data: Dict[str, Any]) -> Dict[str, Any]:

        """
        
            This funtion get typify instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict

            :returns: Returns validated dict
            :rtype: dict
        
        """
        
        validated_data['typify']: TypifyModel = TypifyModel.objects.get(ID=int(validated_data['typify']))

        return validated_data

    def send_message(self, thread: str, channel: str, data: Dict[Any, Any], username: str, created: bool):

            global_preferences = global_preferences_registry.manager()

            #Get the right access token based on the operator
            operator_instance = OperatorModel.objects.get(pk=int(data.get('operator','2')))
            client = WebClient(token=operator_instance.slack_token)

            channel_labels= dict(TypifyModel.ORIGIN_CHOICES)

            action: str = "hecho" if created else "actualizado"

            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Tickets"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":writing_hand:*" + username + "* ha " + action + " un recontacto a través del canal " + 
                        str(channel_labels[data.get('channel')]) +  ": *" + str(data.get('commentary')) + "*"
                    }
                }
            ]

            client.chat_postMessage(
                blocks=blocks,
                thread_ts=thread,
                channel=channel,
                text=f" " + username + " ha " + action + " un recontacto a través del canal " + 
                        str(channel_labels[data.get('channel')]) +  ": " + str(data.get('commentary')) + " "
            )

    def create(self, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and create instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        typify_instance = TypifyModel.objects.filter(ID=validated_data['typify']).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                self.send_message(thread, typify_instance.slack_thread[thread], validated_data, self.context['request'].user.username, True)

        return super().create( self.typify_get(validated_data) )

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        typify_instance = TypifyModel.objects.filter(ID=validated_data['typify']).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                self.send_message(thread, typify_instance.slack_thread[thread], validated_data, self.context['request'].user.username, False)
                
        return super().update(instance,  self.typify_get(validated_data) )

    class Meta:

        #Class model of serializer
        model: BaseModel = NewContactModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class TypifyTablesSerializer(BaseSerializer):

    class Meta:

        #Class model of serializer
        model: BaseModel = TypifyTablesModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'customer': {'required': False},
            'creator': {'required': False},
            'updater': {'required': False},
        }

class TicketCommunicationSerializer(BaseSerializer):

    """

        Class define serializer of NewContactView.
    
    """

    typify = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='tickets:typify-detail',
    )
    typify_id = serializers.IntegerField(write_only=True, source='typify', required=False)

    def typify_get(self, validated_data: Dict[str, Any]) -> Dict[str, Any]:

        """
        
            This funtion get typify instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict

            :returns: Returns validated dict
            :rtype: dict
        
        """
        
        validated_data['typify']: TypifyModel = TypifyModel.objects.get(ID=int(validated_data['typify']))

        return validated_data

    def send_message(self, thread: str, channel: str, data: Dict[Any, Any], username: str, created: bool):

            global_preferences = global_preferences_registry.manager()

            #Get the right access token based on the operator
            operator_instance = OperatorModel.objects.get(pk=int(data.get('operator','2')))
            client = WebClient(token=operator_instance.slack_token)

            channel_labels= dict(TypifyModel.ORIGIN_CHOICES)

            action: str = "hecho" if created else "actualizado"

            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Tickets"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":writing_hand:*" + username + "* ha " + action + " una comunicación a través del canal " + 
                        str(channel_labels[data.get('channel')]) +  ": *" + str(data.get('commentary')) + "*"
                    }
                }
            ]

            client.chat_postMessage(
                blocks=blocks,
                thread_ts=thread,
                channel=channel,
                text=f" " + username + " ha " + action + " un recontacto a través del canal " + 
                        str(channel_labels[data.get('channel')]) +  ": " + str(data.get('commentary')) + " "
            )

    def create(self, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and create instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        typify_instance = TypifyModel.objects.filter(ID=validated_data['typify']).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                self.send_message(thread, typify_instance.slack_thread[thread], validated_data, self.context['request'].user.username, True)

        return super().create( self.typify_get(validated_data) )

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        typify_instance = TypifyModel.objects.filter(ID=validated_data['typify']).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                self.send_message(thread, typify_instance.slack_thread[thread], validated_data, self.context['request'].user.username, False)
                
        return super().update(instance,  self.typify_get(validated_data) )

    class Meta:

        #Class model of serializer
        model: BaseModel = TicketCommunicationModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }