from typing import List

from django.contrib import admin

from .models import CategoryModel , TypifyModel, NewContactModel, TypifySlackMessageModel, ProcessModel, TypifyTablesModel


class CategoryAdmin(admin.ModelAdmin):

    fields: List[str] = ["name", "classification", "parent", "sla"]
    list_display: List[str] = ["ID","name", "classification", "parent", "sla", "operator"]
    search_fields: List[str] = ["ID","name"]
    ordering: List[str] = ["name", "classification", "parent", "sla"]

"""class TypifyAdmin(admin.ModelAdmin):

    fields: List[str] = [
                            "category", "parent", "agent", "services", "liveagent", "customer_type", "channel",
                            "type", "status", "urgency", "city", "management", "discount_reason", "discount_responsable_area",
                            "discount_porcentage"
                        ]
    list_display: List[str] = ["category", "services", "liveagent", "type", "status", "urgency", "city", "management"]
    search_fields: List[str] = ["services", "liveagent", "ID"]
    ordering: List[str] = ["category", "services", "liveagent", "type", "status", "urgency", "city", "management"]"""

class NewContactAdmin(admin.ModelAdmin):

    fields: List[str] = ["commentary","typify", "channel"]
    list_display: List[str] = ["typify", "channel", "commentary"]
    search_fields: List[str] = ["typify","commentary"]
    ordering: List[str] = ["typify", "channel"]

class TypifySlackMessageAdmin(admin.ModelAdmin):

    fields: List[str] = ["name", "category", "fields", "channel"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID","name", "category", "fields","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID","name"]

    #Define model data list ordening
    ordering: List[str] = ["name"]

class ProcessAdmin(admin.ModelAdmin):

    fields: List[str] = ["name", "description"]

    #List of fields to display in django admin
    list_display: List[str] = ["name", "description"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["name"]

    #Define model data list ordening
    ordering: List[str] = ["name"]

class TypifyTablesAdmin(admin.ModelAdmin):
    
    fields = ['name', 'columns', 'last_time', "last_time_type", "operator", "create_me", "assigned_to_me", "filters" ]

    #list of fields to display in django admin
    list_display = ['ID','name','operator']


    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

admin.site.register(CategoryModel, CategoryAdmin)
#admin.site.register(TypifyModel, TypifyAdmin)
admin.site.register(NewContactModel, NewContactAdmin)
admin.site.register(TypifySlackMessageModel, TypifySlackMessageAdmin)
admin.site.register(ProcessModel, ProcessAdmin)
admin.site.register(TypifyTablesModel, TypifyTablesAdmin)
