import ast
import os
import threading
import random
import string
import json
from datetime import datetime, timedelta, timezone
from functools import reduce
from random import randint
from typing import Any, Dict, List
from slack import WebClient

import requests
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import communications_url, get_token, matrix_url
from common.viewsets import BaseViewSet
from django.conf import settings
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from django.db.models import Count, Q
from django.db.models.query import QuerySet
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django_datatables_view.base_datatable_view import BaseDatatableView
from fuzzywuzzy import fuzz, process
from jinja2 import Template
from openpyxl.styles import PatternFill
from openpyxl.workbook import Workbook
from openpyxl.writer.excel import save_virtual_workbook
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from escalation_ti.models import EscalationModel
from rest_framework.serializers import ValidationError
from django_q.models import Schedule
from dynamic_preferences.registries import global_preferences_registry

from .datatables import (CategoryDatatable, TypifyDatatable,
                         TypifySlackMessageDatatable, TypifyTablesDatatable, ProcessDatatable)
from .documentation import (DocumentationCategory, DocumentationTicket,
                            DocumentationTypifySlackMessage,
                            DocumentationTypifyTables)
from .models import (CategoryModel, NewContactModel, TicketCommunicationModel, TypifyModel,
                     TypifySlackMessageModel, TypifyTablesModel, ProcessModel)
from .serializers import (CategorySerializer, NewContactSerializer, TicketCommunicationSerializer,
                          TypifySerializer, TypifySlackMessageSerializer,
                          TypifyTablesSerializer, ProcessSerializer)


class ProcessView(BaseViewSet):

    queryset: BaseModel = ProcessModel.objects.filter()
    serializer_class: BaseSerializer = ProcessSerializer

    @method_decorator(permission_required('tickets.view_processmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(ProcessDatatable(
            request).get_struct(), safe=True)

    @method_decorator(permission_required('tickets.view_processmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return ProcessDatatable(request).get_data()

class CategoryView(BaseViewSet):

    """

        Class define ViewSet of CategoryModel.
    
    """

    queryset: BaseModel = CategoryModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = CategorySerializer
    documentation_class = DocumentationCategory()

    @method_decorator(permission_required('tickets.delete_categorymodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        
        instance = self.get_object()

        def get_children(parent: CategoryModel) -> List[int]:

            id_children: List[int] = []

            for category in CategoryModel.objects.filter(parent=parent):

                id_children = id_children + get_children(category)

            return [parent.ID] + id_children

        def delete_category(parent: CategoryModel):

            for category in CategoryModel.objects.filter(parent=parent):

                delete_category(category)

            parent.delete()

        if TypifyModel.objects.filter(category__ID__in=get_children(instance)):

            raise ValidationError({
                "Error delete":"typify related category"
            })

        delete_category(instance)

        return JsonResponse({}, safe=False)

    @method_decorator(permission_required('tickets.add_categorymodel',raise_exception=True))
    @documentation_class.documentation_create_and_update()
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_categorymodel',raise_exception=True))
    @documentation_class.documentation_create_and_update()
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_categorymodel',raise_exception=True))
    @documentation_class.documentation_create_and_update()
    def partial_update(self, request, *args, **kwargs):
        
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.view_categorymodel',raise_exception=True))
    @documentation_class.documentation_datatables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(CategoryDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('tickets.view_categorymodel',raise_exception=True))
    @documentation_class.documentation_datatables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return CategoryDatatable(request).get_data()

    @method_decorator(permission_required('tickets.view_categorymodel',raise_exception=True))
    @documentation_class.documentation_tree()
    @action(detail=True, methods=['get'])
    def tree(self, request, pk: int):

        """
        
            This funtion return tree Category

            :param self: Instance of Class CategoryView
            :type self: CategoryView
            :param request: data of request
            :type request: request
        
        """

        def tree_data_json(node_parent: CategoryModel) -> Dict[str, Any]:

            """
        
                This funtion is recursive. Return tree of nodes

                :param node_parent: Instance of Class CategoryModel
                :type node_parent: CategoryModel
            
                :return: Dict of nodes
                :rtype: dict

            """

            result: Dict[str, str] = {

                "id": node_parent.ID,
                "name": node_parent.name,
                "title": node_parent.get_classification_display(),

            }

            children : QuerySet = CategoryModel.objects.filter(parent=node_parent)

            if children:

                result["children"] = [tree_data_json(node_children) for node_children in CategoryModel.objects.filter(parent=node_parent, deleted=False)]

            return result

        return JsonResponse(tree_data_json(CategoryModel.objects.get(ID=pk, deleted=False)), safe=False)

class TypifyView(BaseViewSet):

    queryset: BaseModel = TypifyModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TypifySerializer
    history_ignore_fields: List[str] = ['first_data_customer', 'commentaries', 'updater','updated']

    documentation_class = DocumentationTicket()

    @method_decorator(permission_required('tickets.view_typifymodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        typify_datatables = TypifyDatatable(request)
        typify_datatables.ID_TABLE = pk

        return JsonResponse(typify_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('tickets.view_typifymodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifyView
            :type self: TypifyView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request, pk):

            typify_datatables = TypifyDatatable(request)
            typify_datatables.ID_TABLE = pk
            typify_datatables.download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request, pk]
        ).start()

        return JsonResponse(data={})

    @method_decorator(permission_required('tickets.view_typifymodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        typify_datatables = TypifyDatatable(request)
        typify_datatables.ID_TABLE = pk

        return typify_datatables.get_data()

    @method_decorator(permission_required('tickets.add_typifymodel',raise_exception=True))
    @documentation_class.documentation_create_and_update()
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_typifymodel',raise_exception=True))
    @documentation_class.documentation_create_and_update()
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_typifymodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def give_back(self, request, pk) -> JsonResponse:

        try:

            typify_instance: TypifyModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error instance":"Don´t exist"
            })

        if not typify_instance.escalation_tk:

            raise ValidationError({
                "Error escalation":"Don´t exist escalation"
            })

        escalation_instance: EscalationModel = EscalationModel.objects.get(ID=typify_instance.escalation_tk)

        escalation_instance.status = 0
        escalation_instance.save()

        return JsonResponse(data={})
    
    @method_decorator(permission_required('tickets.change_typifymodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def create_reminder(self, request, pk) -> JsonResponse:

        schedule_object: Schedule = Schedule.objects.create(
            func='tickets.queue.send_reminder',
            name=''.join(random.choice(
                string.ascii_uppercase + string.digits) for _ in range(20)),
            schedule_type='O',
            repeats=1,
            next_run=request.POST.get('start', datetime.now()),
            kwargs={
                'pk_typify': pk,
                'custom': True,
                'closed': False,
                'message': request.POST.get('message', None)
            },
            minutes=5,
        )

        return JsonResponse(data={})

    @method_decorator(permission_required('tickets.view_typifymodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def categories_names(self, request) -> JsonResponse:

        """
        
            This funtion return the names of specific categories

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        result = []
        for category_id in json.loads(request.data.get("categories")):
            category_instance = CategoryModel.objects.get(ID=int(category_id))
            result.append({"ID": category_id, "name": category_instance.name})
        
        return JsonResponse(result, safe=False)

class NewContactView(BaseViewSet):

    queryset: BaseModel  = NewContactModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = NewContactSerializer

    @method_decorator(permission_required('tickets.add_newcontactmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_newcontactmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_newcontactmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.delete_newcontactmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):

        def send_message(thread: str, channel: str, instance: NewContactModel, username: str):

            global_preferences = global_preferences_registry.manager()

            #Get the right access token based on the operator
            client = WebClient(token=instance.operator.slack_token)

            channel_labels= dict(TypifyModel.ORIGIN_CHOICES)

            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Tickets"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":writing_hand:*" + username + "* ha eliminado un recontacto a través del canal " + 
                        str(channel_labels[int(instance.channel)]) +  ": *" + instance.commentary + "*"
                    }
                }
            ]

            client.chat_postMessage(
                blocks=blocks,
                thread_ts=thread,
                channel=channel,
                text=f" " + username + " ha eliminado un recontacto a través del canal " + 
                        str(channel_labels[int(instance.channel)]) +  ": " + instance.commentary + " "
            )

        typify_instance = TypifyModel.objects.filter(ID=self.get_object().typify.ID).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                send_message(thread, typify_instance.slack_thread[thread], self.get_object(), self.request.user.username)

        return super().destroy(request, *args, **kwargs)

class TypifySlackMessageView(BaseViewSet):

    queryset: BaseModel  = TypifySlackMessageModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TypifySlackMessageSerializer
    documentation_class = DocumentationTypifySlackMessage()

    @method_decorator(permission_required('tickets.add_typifyslackmessagemodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_typifyslackmessagemodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_typifyslackmessagemodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.delete_typifyslackmessagemodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.view_typifyslackmessagemodel',raise_exception=True))
    @documentation_class.documentation_fields_options()
    @action(detail=False, methods=['get'])
    def fields_options(self, request) -> JsonResponse:

        """
        
            This funtion return fields option data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return  JsonResponse([
            {"value":"subcategory", "name":"Subcategoría"},
            {"value":"second_subcategory", "name":"segunda subcategoría"},
            {"value":"agent", "name":"Agente"},
            {"value":"services", "name":"Servicios"},
            {"value":"rut", "name":"RUT"},
            {"value":"customer_type", "name":"Tipo de cliente"},
            {"value":"channel", "name":"Canal"},
            {"value":"type", "name":"Tipo"},
            {"value":"city", "name":"Ciudad"},
            {"value":"status", "name":"Estado"},
        ], safe=False)

    @method_decorator(permission_required('tickets.view_typifyslackmessagemodel',raise_exception=True))
    @documentation_class.documentation_datatables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TypifySlackMessageDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('tickets.view_typifyslackmessagemodel',raise_exception=True))
    @documentation_class.documentation_datatables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return TypifySlackMessageDatatable(request).get_data()

class TypifyTablesView(BaseViewSet):

    queryset: BaseModel  = TypifyTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TypifyTablesSerializer
    documentation_class = DocumentationTypifyTables()

    @method_decorator(permission_required('tickets.add_typifytablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_typifytablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_typifytablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.delete_typifytablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.view_typifytablesmodel',raise_exception=True))
    @documentation_class.documentation_definition()
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:

        data_struct : Dict[str, Dict[str, str]] = {

            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Creador":{
                    "type":"choices",
                    "name":"creator__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Agente":{
                    "type":"choices",
                    "name":"agent__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Tipo de cliente":{
                    "type":"choices",
                    "name":"customer_type",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "No cliente",
                            3: "Cliente",
                        }
                    }
                },
                "Canal de comunicación":{
                    "type":"choices",
                    "name":"channel",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "CORREO",
                            1: "LLAMADA",
                            2: "CHAT",
                            3: "REDES",
                            4: "WHATSAPP",
                        }
                    }
                },
                "Tipo":{
                    "type":"choices",
                    "name":"type",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "IN",
                            1: "OUT",
                        }
                    }
                },
                "Ciudad":{
                    "type":"choices",
                    "name":"city",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "Arica",
                            1: "Santiago",
                            2: "Otros"
                        }
                    }
                },
                "Estado":{
                    "type":"choices",
                    "name":"status",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "ABIERTO",
                            1: "CERRADO",
                            2: "ESPERANDO RESPUESTA",
                            3: "DAR SEGUIMIENTO",
                            4: "LLAMAR LUEGO",
                            5: "CANCELADO"
                        }
                    }
                },
            },
            "columns":  {
                'creator': "Creador",
                'updater': "Último en actualizar",
                'created': "Fecha de creación",
                'updated': "Fecha de actualización",
                'category': "Categoria",
                'agent': "Agente asignado",
                'services': "Servicios", 
                'rut': "RUT",
                'customer_type': "Tipo de cliente",
                'channel': "Canal",
                'type': "Tipo",
                'city': "Ciudad",
                'status': "Estado",
            }
        }

        return JsonResponse(data_struct, safe=True)

    @method_decorator(permission_required('tickets.view_typifytablesmodel',raise_exception=True))
    @documentation_class.documentation_datatables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TypifyTablesDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('tickets.view_typifytablesmodel',raise_exception=True))
    @documentation_class.documentation_datatables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TypifySlackMessageView
            :type self: TypifySlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return TypifyTablesDatatable(request).get_data()

class TicketCommunicationView(BaseViewSet):

    queryset: BaseModel = TicketCommunicationModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TicketCommunicationSerializer

    @method_decorator(permission_required('tickets.add_ticketcommunicationmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_ticketcommunicationmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.change_ticketcommunicationmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('tickets.delete_ticketcommunicationmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):

        def send_message(thread: str, channel: str, instance: NewContactModel, username: str):

            global_preferences = global_preferences_registry.manager()

            #Get the right access token based on the operator
            client = WebClient(token=instance.operator.slack_token)

            channel_labels= dict(TypifyModel.ORIGIN_CHOICES)

            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Tickets"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":writing_hand:*" + username + "* ha eliminado una comunicación a través del canal " + 
                        str(channel_labels[int(instance.channel)]) +  ": *" + instance.commentary + "*"
                    }
                }
            ]

            client.chat_postMessage(
                blocks=blocks,
                thread_ts=thread,
                channel=channel,
                text=f" " + username + " ha eliminado un recontacto a través del canal " + 
                        str(channel_labels[int(instance.channel)]) +  ": " + instance.commentary + " "
            )

        typify_instance = TypifyModel.objects.filter(ID=self.get_object().typify.ID).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                send_message(thread, typify_instance.slack_thread[thread], self.get_object(), self.request.user.username)

        return super().destroy(request, *args, **kwargs)
