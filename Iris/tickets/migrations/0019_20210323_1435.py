
from django.db import migrations

def delete_typify_tables(apps, schema_editor):
    TypifyTableInstances = apps.get_model('tickets', 'TypifyTablesModel')
    TypifyTableInstances.objects.all().delete()

def update_typify_table(apps, schema_editor):
    TypifyTable = apps.get_model("tickets", "TypifyTablesModel")

    for typify_table in TypifyTable.objects.filter(deleted=False):

        if 'liveagent' in typify_table.columns:
            new_columns = typify_table.columns
            new_columns.remove('liveagent')
            typify_table.columns = new_columns
            typify_table.save()

class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0018_auto_20210317_1435'),
    ]

    operations = [
        migrations.RunPython(update_typify_table, delete_typify_tables)
    ]
