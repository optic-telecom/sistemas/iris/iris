# Generated by Django 2.1.7 on 2021-05-03 20:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0024_auto_20210430_0845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicaltypifymodel',
            name='customer_type',
            field=models.PositiveIntegerField(choices=[(0, 'No cliente'), (3, 'Cliente'), (4, 'Múltiples Clientes')], default=0),
        ),
        migrations.AlterField(
            model_name='typifymodel',
            name='customer_type',
            field=models.PositiveIntegerField(choices=[(0, 'No cliente'), (3, 'Cliente'), (4, 'Múltiples Clientes')], default=0),
        ),
    ]
