# Generated by Django 2.1.7 on 2021-02-02 21:39

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0006_historicalprocess_process'),
    ]

    operations = [
        migrations.AlterField(
            model_name='processmodel',
            name='creator',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tickets_processmodel_creator', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='processmodel',
            name='updater',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tickets_processmodel_updater', to=settings.AUTH_USER_MODEL),
        ),
    ]
