
from django.db import migrations

def update_group(apps, scheme_editor):
    
    Group = apps.get_model('auth', 'Group')
    Permission = apps.get_model("auth", "Permission")

    agent_group, p = Group.objects.get_or_create(name="Agente SAC")

    permission_list = ['view_escalationmodel','view_testmodel']

    for permission in permission_list:

        permission_instance = Permission.objects.get(codename=permission, content_type__app_label='escalation_ti')
        agent_group.permissions.add(permission_instance)

def delete_group(apps, scheme_editor):
    Group = apps.get_model('auth', 'Group')
    group_names = ["Agente SAC"]
    
    for name in group_names:
        try:
            Group.objects.filter(name=name).delete()
        except:
            pass

class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0019_20210323_1435'),
    ]

    operations = [
        migrations.RunPython(update_group, delete_group)
    ]
