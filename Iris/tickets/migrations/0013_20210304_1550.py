from django.db import migrations, models

def delete_typify_tables(apps, schema_editor):
    TypifyTableInstances = apps.get_model('tickets', 'TypifyTablesModel')
    TypifyTableInstances.objects.all().delete()

def create_typify_table(apps, schema_editor):
    TypifyTable = apps.get_model("tickets", "TypifyTablesModel")
    User = apps.get_model("auth", "User")
    db_alias = schema_editor.connection.alias
    TypifyTable.objects.using(db_alias).bulk_create([
        TypifyTable(
            name="General",
            columns=["creator","updater","created","updated","category","agent", "city","liveagent","status","channel","customer_type","rut","type","services"],
            operator=2,
            filters=[], 
            creator=User.objects.get(pk=1)
        ),
        TypifyTable(
            name="General",
            columns=["creator","updater","created","updated","category","agent", "city","liveagent","status","channel","customer_type","rut","type","services"],
            operator=1,
            filters=[], 
            creator=User.objects.get(pk=1)
        ),
    ])


class Migration(migrations.Migration):
    dependencies = [
        ('tickets', '0012_auto_20210303_1550'),
    ]

    operations = [
        migrations.RunPython(delete_typify_tables),
        migrations.RunPython(create_typify_table),

    ]