# Generated by Django 2.2 on 2021-04-30 12:36

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0020_20210420_1435'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaltypifymodel',
            name='rut_list',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=list, null=True),
        ),
        migrations.AddField(
            model_name='typifymodel',
            name='rut_list',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=list, null=True),
        ),
    ]
