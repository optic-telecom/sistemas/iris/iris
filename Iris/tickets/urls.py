from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import CategoryView, TicketCommunicationView, TypifyView, NewContactView, TypifySlackMessageView, TypifyTablesView, ProcessView

router = DefaultRouter()

router.register(r'category', CategoryView, basename='category')
router.register(r'typify', TypifyView, basename='typify')
router.register(r'new_contact', NewContactView, basename='new_contact')
router.register(r'typify_message', TypifySlackMessageView, basename='typify_message')
router.register(r'typify_tables', TypifyTablesView, basename='typify_tables')
router.register(r'process', ProcessView, basename='process')
router.register(r'communications', TicketCommunicationView, basename='communications')
urlpatterns = router.urls
