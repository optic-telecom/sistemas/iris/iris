from typing import List

from django.contrib import admin

from .models import (ProblemsModel, EscalationModel,
                     EscalationSlackMessageModel, EscalationTablesModel,
                    SolutionModel, TestModel, EvaluationTestModel)


class ProblemsAdmin(admin.ModelAdmin):

    fields: List[str] = ["name", "classification", "sla", "parent"]

    #List of fields to display in django admin
    list_display: List[str] = ["name", "classification", "parent"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["name"]

    #Define model data list ordening
    ordering: List[str] = ["name"]

class EscalationAdmin(admin.ModelAdmin):

    fields: List[str] = ["solutions", "problems", "tests", "escalation_level", "status", "services", "rut", "typify", "agent_level_tree", "agent_level_two", "agent_level_one"]

    #List of fields to display in django admin
    list_display: List[str] = ["escalation_level", "status", "services", "rut", "typify", "ID"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID","rut","services"]

    #Define model data list ordening
    ordering: List[str] = ["ID"]

class TestAdmin(admin.ModelAdmin):

    fields: List[str] = ["name", "description", "documentation","operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["name", "description"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["name", "description"]

    #Define model data list ordening
    ordering: List[str] = ["name"]

class EvaluationTestAdmin(admin.ModelAdmin):

    fields: List[str] = ["test", "result"]

    #List of fields to display in django admin
    list_display: List[str] = ["test", "result"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["test"]

    #Define model data list ordening
    ordering: List[str] = ["test"]

class SolutionAdmin(admin.ModelAdmin):

    fields: List[str] = ["name", "description", "documentation","operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["name", "description"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["name"]

    #Define model data list ordening
    ordering: List[str] = ["name"]

class EscalationSlackMessageAdmin(admin.ModelAdmin):

    fields: List[str] = ["name", "category", "fields", "channel"]

    #List of fields to display in django admin
    list_display: List[str] = ["name", "category", "fields"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["name"]

    #Define model data list ordening
    ordering: List[str] = ["name"]

class EscalationTablesAdmin(admin.ModelAdmin):
    
    fields = ['name', 'columns', 'last_time', "last_time_type", "operator", "create_me", "assigned_to_me", "filters" ]

    #list of fields to display in django admin
    list_display = ['ID','name','operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

admin.site.register(EscalationModel, EscalationAdmin)
admin.site.register(TestModel, TestAdmin)
admin.site.register(EvaluationTestModel, EvaluationTestAdmin)
admin.site.register(ProblemsModel, ProblemsAdmin)
admin.site.register(SolutionModel, SolutionAdmin)
admin.site.register(EscalationSlackMessageModel, EscalationSlackMessageAdmin)
admin.site.register(EscalationTablesModel, EscalationTablesAdmin)
