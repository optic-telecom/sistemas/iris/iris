import threading
import string
import random
from datetime import datetime, timedelta
from typing import Any, Dict, List

from django_q.models import Schedule
from django.contrib.auth.models import User
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.viewsets import BaseViewSet
from django.http import JsonResponse, HttpResponseForbidden
from rest_framework.decorators import action
from rest_framework.serializers import ValidationError
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from dynamic_preferences.registries import global_preferences_registry


from .datatables import (ProblemsDatatable, EscalationDatatable,
                         EscalationSlackMessageDatatable,
                         EscalationTablesDatatable,
                         SolutionDatatable, TestDatatable, TestGroupDatatable)
from .documentation import (DocumentationProblems,
                            DocumentationEscalationSlackMessage,
                            DocumentationEscalationTables,
                            DocumentationProblem, DocumentationSolution,
                            DocumentationTest)
from .models import (ProblemsModel, EscalationModel,
                     EscalationSlackMessageModel, EscalationTablesModel,
                     SolutionModel, TestModel, TestGroupModel, OptionsTestModel, EscalationPhotoModel)
from .serializers import (EscalationProblemsSerializer, EscalationSerializer,
                          EscalationSlackMessageSerializer,
                          EscalationTablesSerializer,
                          SolutionSerializer, TestGroupSerializer,
                          TestSerializer, UrlDocumentationSerializer, OptionsTestSerializer, 
                          EvaluationTestSerializer, EscalationPhotoSerializer)


class ProblemsView(BaseViewSet):

    """

        Class define ViewSet of ProblemsModel.
    
    """

    queryset: BaseModel = ProblemsModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EscalationProblemsSerializer
    documentation_class = DocumentationProblems()

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('escalation_ti.view_problemsmodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()

    @method_decorator(permission_required('escalation_ti.delete_problemsmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        
        instance = self.get_object()

        def get_children(parent: ProblemsModel) -> List[int]:

            id_children: List[int] = []

            for problem in ProblemsModel.objects.filter(parent=parent):

                id_children = id_children + get_children(problem)

            return [parent.ID] + id_children

        def delete_problem(parent: ProblemsModel):

            for problem in ProblemsModel.objects.filter(parent=parent):

                delete_problem(problem)

            parent.delete()

        if EscalationModel.objects.filter(problems__ID__in=get_children(instance)):

            raise ValidationError({
                "Error delete":"typify related problem"
            })

        delete_problem(instance)

        return JsonResponse({}, safe=False)

    @method_decorator(permission_required('escalation_ti.add_problemsmodel',raise_exception=True))
    @documentation_class.documentation_create_and_update()
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_problemsmodel',raise_exception=True))
    @documentation_class.documentation_create_and_update()
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_problemsmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.view_problemsmodel',raise_exception=True))
    @documentation_class.documentation_datatables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(ProblemsDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('escalation_ti.view_problemsmodel',raise_exception=True))
    @documentation_class.documentation_datatables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return ProblemsDatatable(request).get_data()

    @method_decorator(permission_required('escalation_ti.view_problemsmodel',raise_exception=True))
    @documentation_class.documentation_tree()
    @action(detail=True, methods=['get'])
    def tree(self, request, pk: int):

        """
        
            This funtion return tree Problems

            :param self: Instance of Class ProblemsView
            :type self: ProblemsView
            :param request: data of request
            :type request: request
        
        """

        def tree_data_json(node_parent: ProblemsModel) -> Dict[str, Any]:

            """
        
                This funtion is recursive. Return tree of nodes

                :param node_parent: Instance of Class ProblemsModel
                :type node_parent: ProblemsModel
            
                :return: Dict of nodes
                :rtype: dict

            """

            result: Dict[str, str] = {

                "id": node_parent.ID,
                "name": node_parent.name,
                "title": node_parent.get_classification_display(),

            }

            children : QuerySet = ProblemsModel.objects.filter(parent=node_parent)

            if children:

                result["children"] = [tree_data_json(node_children) for node_children in ProblemsModel.objects.filter(parent=node_parent, deleted=False)]

            return result

        return JsonResponse(tree_data_json(ProblemsModel.objects.get(ID=pk, deleted=False)), safe=False)

class EscalationSlackMessageView(BaseViewSet):

    queryset: BaseModel  = EscalationSlackMessageModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EscalationSlackMessageSerializer
    documentation_class = DocumentationEscalationSlackMessage()

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('escalation_ti.view_escalationslackmessagemodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()

    @method_decorator(permission_required('escalation_ti.add_escalationslackmessagemodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_escalationslackmessagemodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_escalationslackmessagemodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.delete_escalationslackmessagemodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.view_escalationslackmessagemodel',raise_exception=True))
    @documentation_class.documentation_fields_options()
    @action(detail=False, methods=['get'])
    def fields_options(self, request) -> JsonResponse:

        """
        
            This funtion return fields option data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return  JsonResponse([
            {"value":"problem", "name":"Problema"},
            {"value":"agent", "name":"Agente"},
            {"value":"services", "name":"Servicios"},
            {"value":"rut", "name":"RUT"},
            {"value":"typify", "name":"Tipificación"},
            {"value":"status", "name":"Estado"},
            {"value":"escalation_level", "name":"Nivel"},
        ], safe=False)

    @method_decorator(permission_required('escalation_ti.view_escalationslackmessagemodel',raise_exception=True))
    @documentation_class.documentation_datatables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(EscalationSlackMessageDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('escalation_ti.view_escalationslackmessagemodel',raise_exception=True))
    @documentation_class.documentation_datatables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return EscalationSlackMessageDatatable(request).get_data()

class EscalationTablesView(BaseViewSet):

    queryset: BaseModel  = EscalationTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EscalationTablesSerializer
    documentation_class = DocumentationEscalationTables()

    # def get_queryset(self, *args, **kwargs):
    #     if self.request.user.has_perm('escalation_ti.view_escalationtablesmodel'):
    #         return self.queryset
    #     else:
    #         return HttpResponseForbidden()

    @method_decorator(permission_required('escalation_ti.add_escalationtablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_escalationtablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_escalationtablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.delete_escalationtablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.view_escalationtablesmodel',raise_exception=True))
    @documentation_class.documentation_definition()
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:

        data_struct : Dict[str, Dict[str, str]] = {

            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Creador":{
                    "type":"choices",
                    "name":"creator__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Estado":{
                    "type":"choices",
                    "name":"status",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: 'ABIERTO',
                            1: 'EN SAC',
                            2: 'CERRADO',
                        }
                    }
                },
                "Nivel":{
                    "type":"choices",
                    "name":"escalation_level",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: 'PRIMER',
                            1: 'SEGUNDO',
                            2: 'TERCER',
                        }
                    }
                },
            },
            "columns":  {
                'creator': "Creador",
                'updater': "Último en actualizar",
                'created': "Fecha de creación",
                'updated': "Fecha de actualización",
                'agent_level_one': "Agente nivel uno",
                'agent_level_two': "Agente nivel dos",
                'agent_level_tree': "Agente nivel tres",
                'services': "Servicios", 
                'status': "Estado",
                'rut': "RUT",
                'escalation_level': "Nivel",
                'typify':"Tipificación"
            }
        }

        return JsonResponse(data_struct, safe=True)

    @method_decorator(permission_required('escalation_ti.view_escalationtablesmodel',raise_exception=True))
    @documentation_class.documentation_datatables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(EscalationTablesDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('escalation_ti.view_escalationtablesmodel',raise_exception=True))
    @documentation_class.documentation_datatables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return EscalationTablesDatatable(request).get_data()

class EscalationView(BaseViewSet):

    queryset: BaseModel = EscalationModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EscalationSerializer
    history_ignore_fields: List[str] = ['commentaries', 'updater','updated']

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('escalation_ti.view_escalationmodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()

    #documentation_class = DocumentationTicket()

    @method_decorator(permission_required('escalation_ti.view_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        typify_datatables = EscalationDatatable(request)
        typify_datatables.ID_TABLE = pk

        return JsonResponse(typify_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('escalation_ti.view_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct_view(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        data_struct: Dict[str, Any] = {
            "id_table": None,
            "fields":{},
            "columns":{},
            "filters":{},
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons":{
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            }
        }

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {
            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
        }

        data_struct["columns"] = {
            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Número de Escalamiento":{
                "field": "ID",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Número de Tipificación":{
                "field": "typify",
                "sortable": True,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Estado":{
                "field": "status",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Agente":{
                "field": "agent",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },
        }

        data_struct["filters"] = {}

        return JsonResponse(data_struct, safe=False)

    @method_decorator(permission_required('escalation_ti.view_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request, pk):

            typify_datatables = EscalationDatatable(request)
            typify_datatables.ID_TABLE = pk
            typify_datatables.download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request, pk]
        ).start()

        return JsonResponse(data={})

    @method_decorator(permission_required('escalation_ti.view_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        typify_datatables = EscalationDatatable(request)
        typify_datatables.ID_TABLE = pk

        return typify_datatables.get_data()

    #@documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('escalation_ti.add_escalationmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    #@documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('escalation_ti.change_escalationmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):

        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_escalationmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.delete_escalationmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_test(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds test to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        serialized_data = EvaluationTestSerializer( data=request.data )
        serialized_data.context['request'] = request

        if serialized_data.is_valid():

            instance_result_test: BaseModel = serialized_data.create( serialized_data.validated_data )            
            escalation.tests.add(instance_result_test)

        else:

            raise ValidationError( serialized_data.errors )

        return JsonResponse( data=EvaluationTestSerializer(instance_result_test).data )

    @method_decorator(permission_required('escalation_ti.change_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_test(self, request, pk) -> JsonResponse:

        """
        
            This funtion removes test from escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        try:
            escalation.tests.get(ID=request.POST['test_id']).delete()
            escalation.tests.remove(request.POST['test_id'])
        except:

            raise ValidationError({
                "Error escalation test": "Item Don't exist"         
            })

        return JsonResponse( data={} )

    @method_decorator(permission_required('escalation_ti.change_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_problem(self,request,pk) -> JsonResponse:
        
        """
        
            This funtion adds problem to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)
            problem_escalation: BaseModel = ProblemsModel.objects.get(ID=request.POST['problem_id'])

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        escalation.problems.add( problem_escalation )
        escalation.save()

        sla: int = problem_escalation.sla

        schedule_object: Schedule = Schedule.objects.create(
            func='escalation_ti.queue.send_reminder',
            name=''.join(random.choice(
                string.ascii_uppercase + string.digits) for _ in range(20)),
            schedule_type='O',
            repeats=1,
            next_run=datetime.now() + timedelta(minutes=sla),
            kwargs={
                'pk_escalation': int(escalation.ID),
                'initial': False
            },
            minutes=5,
        )

        schedule_dict = escalation.schedules
        schedule_dict[problem_escalation.ID] = schedule_object.id
        escalation.schedules = schedule_dict
        escalation.save()

        return JsonResponse( data=EscalationSerializer(escalation).data )

    @method_decorator(permission_required('escalation_ti.change_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_problem(self, request, pk) -> JsonResponse:

        """
        
            This funtion removes problem from escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        try:
            instance_problem: BaseModel = escalation.problems.get(ID=request.POST['problem_id'])
            escalation.problems.remove(instance_problem)
        except:

            raise ValidationError({
                "Error escalation test": "Item Don't exist"         
            })

        schedule_id: int = escalation.schedules.get(str(instance_problem.ID))
        Schedule.objects.get(id=schedule_id).delete()
        escalation.schedules.pop(str(instance_problem.ID), None)
        escalation.save()

        return JsonResponse( data=EscalationSerializer(escalation).data )

    @method_decorator(permission_required('escalation_ti.change_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_solution(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds solution to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)
            solution_escalation: BaseModel = SolutionModel.objects.get(ID=request.POST['solution_id'])

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        escalation.solutions.add( solution_escalation )
        escalation.save()

        return JsonResponse( data=EscalationSerializer(escalation).data )

    @method_decorator(permission_required('escalation_ti.change_escalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_solution(self, request, pk) -> JsonResponse:

        """
        
            This funtion removes solution from escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        try:
            instance_solution: BaseModel = escalation.solutions.get(ID=request.POST['solution_id'])
            escalation.solutions.remove(instance_solution)
        except:

            raise ValidationError({
                "Error escalation test": "Item Don't exist"         
            })

        return JsonResponse( data=EscalationSerializer(escalation).data )
 
class TestView(BaseViewSet):

    """

        Class define ViewSet of TestModel.
    
    """

    queryset: BaseModel  = TestModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TestSerializer
    #documentation_class = DocumentationTest()

    @method_decorator(permission_required('escalation_ti.add_testmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_testmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_testmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.delete_testmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('escalation_ti.view_testmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TestView
            :type self: TestView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TestDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('escalation_ti.view_testmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TestView
            :type self: TestView
            :param request: data of request
            :type request: request
        
        """

        return TestDatatable(request).get_data()
    
    @method_decorator(permission_required('escalation_ti.view_testmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_documentation(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds documentation to test

            :param self: Instance of Class TestView
            :type self: TestView
            :param request: data of request
            :type request: request
            :param pk: Test id
            :type pk: int
        
        """

        try:
            test: BaseModel = self.queryset.get(ID=pk)
        except:
            raise ValidationError({
                "Error test": "Item Don't exist"         
            })
        serialized_data = UrlDocumentationSerializer( data=request.POST )
        serialized_data.context['request'] = request

        if serialized_data.is_valid():
            instance_documentation: BaseModel = serialized_data.create( serialized_data.validated_data )            
            test.documentation.add(instance_documentation)
        else:
            raise ValidationError( serialized_data.errors )

        return JsonResponse( data=UrlDocumentationSerializer(instance_documentation).data )

    @method_decorator(permission_required('escalation_ti.view_testmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_documentation(self, request, pk) -> JsonResponse:

        """
        
            This funtion removes documentation from test

            :param self: Instance of Class TestView
            :type self: TestView
            :param request: data of request
            :type request: request
            :param pk: Test id
            :type pk: int
        
        """

        try:
            test: BaseModel = self.queryset.get(ID=pk)
        except:
            raise ValidationError({
                "Error test": "Item Don't exist"         
            })
        try:
            test.documentation.get(ID=request.POST['documentation_id']).delete()
            test.documentation.remove(request.POST['documentation_id'])
        except:

            raise ValidationError({
                "Error problem documentation": "Item Don't exist"         
            })

        return JsonResponse( data={} )

    
class SolutionView(BaseViewSet):

    """

        Class define ViewSet of SolutionModel.
    
    """

    queryset: BaseModel  = SolutionModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = SolutionSerializer
    #documentation_class = DocumentationSolution()

    @method_decorator(permission_required('escalation_ti.add_solutionmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_solutionmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_solutionmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.delete_solutionmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('escalation_ti.view_solutionmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class SolutionView
            :type self: SolutionView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(SolutionDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('escalation_ti.view_solutionmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class SolutionView
            :type self: SolutionView
            :param request: data of request
            :type request: request
        
        """

        return SolutionDatatable(request).get_data()

    @method_decorator(permission_required('escalation_ti.change_solutionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_documentation(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds documentation to solution

            :param self: Instance of Class SolutionView
            :type self: SolutionView
            :param request: data of request
            :type request: request
            :param pk: solution id
            :type pk: int
        
        """

        try:
            solution: BaseModel = self.queryset.get(ID=pk)
        except:
            raise ValidationError({
                "Error solution": "Item Don't exist"         
            })
        serialized_data = UrlDocumentationSerializer( data=request.POST )
        serialized_data.context['request'] = request

        if serialized_data.is_valid():
            instance_documentation: BaseModel = serialized_data.create( serialized_data.validated_data )            
            solution.documentation.add(instance_documentation)
        else:
            raise ValidationError( serialized_data.errors )

        return JsonResponse( data=UrlDocumentationSerializer(instance_documentation).data )

    @method_decorator(permission_required('escalation_ti.change_solutionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_documentation(self, request, pk) -> JsonResponse:

        """
        
            This funtion removes documentation from solution

            :param self: Instance of Class SolutionView
            :type self: SolutionView
            :param request: data of request
            :type request: request
            :param pk: solution id
            :type pk: int
        
        """

        try:
            solution: BaseModel = self.queryset.get(ID=pk)
        except:
            raise ValidationError({
                "Error solution": "Item Don't exist"         
            })
        try:
            solution.documentation.get(ID=request.POST['documentation_id']).delete()
            solution.documentation.remove(request.POST['documentation_id'])
        except:

            raise ValidationError({
                "Error problem documentation": "Item Don't exist"         
            })

        return JsonResponse( data={} )

class OptionsTestView(BaseViewSet):

    """

        Class define ViewSet of OptionsTestModel.
    
    """

    queryset: BaseModel  = OptionsTestModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = OptionsTestSerializer

    def destroy(self, request, *args, **kwargs):
        escalations = EscalationModel.objects.filter(tests__result=self.get_object().ID)
        if len(escalations) > 0:
            raise ValidationError({'option error': 'A escalation was created with this test option'})
        return super().destroy(request, *args, **kwargs)

class EscalationPhotoView(BaseViewSet):

    """

        Class define ViewSet of EscalationPhotoModel.
    
    """

    queryset: BaseModel = EscalationPhotoModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EscalationPhotoSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('escalation_ti.view_escalationphotomodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()
    
    
    @method_decorator(permission_required('escalation_ti.add_escalationphotomodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_escalationphotomodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.delete_escalationphotomodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

class TestGroupView(BaseViewSet):

    """

        Class define ViewSet of TestGroupModel.
    
    """

    queryset: BaseModel = TestGroupModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TestGroupSerializer
    
    @method_decorator(permission_required('escalation_ti.add_testgroupmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.change_testgroupmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.delete_testgroupmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_ti.view_testgroupmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class SolutionView
            :type self: SolutionView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TestGroupDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('escalation_ti.view_testgroupmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class SolutionView
            :type self: SolutionView
            :param request: data of request
            :type request: request
        
        """

        return TestGroupDatatable(request).get_data()