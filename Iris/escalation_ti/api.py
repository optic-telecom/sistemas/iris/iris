from .models import EscalationModel, EvaluationTestModel
from .serializers import EscalationSerializer
from communications.models import InternalChatModel, ChatModel
from tickets.models import TypifyModel
from typing import List
import json

def create_escalation(request, evaluation_list: List[EvaluationTestModel], typify_instance: TypifyModel, comment: str = None) -> int:

    """
    
        This funtion allows to create a escalation ticket 

        :param request: request with the escalation data
        :type request: Request
        :param evaluation_list: EvaluationTestModel instances to add to the escalation tests
        :type evaluation_list: List
        :param typify_instance: TypifyModel instance asociated
        :type typify_instance: TypifyModel
        :param comment: escalation comment
        :type comment: str
        
    """

    escalation_instance: EscalationModel = EscalationModel.objects.create(
        operator=typify_instance.operator,
        creator=request.user,
        updater=request.user,
        services=typify_instance.services,
        status=0,
        escalation_level=0,
        rut=typify_instance.rut,
        typify=typify_instance.ID,
    )

    escalation_instance.tests.set(evaluation_list)
    escalation_instance.save()
    EscalationSerializer(escalation_instance).slack_message(escalation_instance)
    EscalationSerializer(escalation_instance).add_reminder(escalation_instance)
    if comment != None:
        try:
            chat_instance = InternalChatModel.objects.create(
                chat=ChatModel.objects.get(ID=escalation_instance.commentaries), 
                creator=request.user,
                updater=request.user,
                message=comment, 
                operator=escalation_instance.operator
            )
            chat_instance.save()

        except: 
            pass


    return escalation_instance.ID