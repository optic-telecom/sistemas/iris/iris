from rest_framework.routers import DefaultRouter

from .views import (ProblemsView, EscalationSlackMessageView,
                    EscalationTablesView, EscalationView, TestGroupView,
                    SolutionView, TestView, OptionsTestView, EscalationPhotoView)


router = DefaultRouter()

router.register(r'problems', ProblemsView, basename='problems')
router.register(r'escalation_message', EscalationSlackMessageView, basename='escalation_message')
router.register(r'escalation_tables', EscalationTablesView, basename='escalation_tables')
router.register(r'escalation', EscalationView, basename='escalation')
router.register(r'test', TestView, basename='test')
router.register(r'test_group', TestGroupView, basename='test_group')
router.register(r'test_option', OptionsTestView, basename='test_option')
router.register(r'solution', SolutionView, basename='solution')
router.register(r'escalation_photo', EscalationPhotoView, basename='escalation_photo')

urlpatterns = router.urls
