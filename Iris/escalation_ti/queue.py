import re
from datetime import datetime, timezone, timedelta
from typing import Any, List, Union, Dict
import requests
import random
import string
from slack import WebClient
from rest_framework import serializers
from schema import And, Schema, Use
from dynamic_preferences.registries import global_preferences_registry
from django_q.models import Schedule
from .models import  EscalationModel

def send_reminder(*args, **kwargs):

    def slack_reminder(thread: str, channel: str, escalation_instance: EscalationModel, message: str = None):

        global_preferences = global_preferences_registry.manager()

        client = WebClient(token=escalation_instance.operator.slack_token)

        blocks: List[Dict[str, Any]] =  [
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": ":iris: *Iris* Tickets de Escalamiento"
                }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": message
                }
            },
            {
                "type": "divider"
            }
        ]

        client.chat_postMessage(
            blocks=blocks,
            thread_ts=thread,
            channel=channel,
            text="Recordatorio ticket de escalamiento"
        )

    if 'kwargs' in kwargs.keys():

        pk_escalation: int = kwargs['kwargs']['pk_escalation']
        initial: bool = kwargs['kwargs']['initial']

    else:   

        pk_escalation: int = kwargs['pk_escalation']
        initial: bool = kwargs['initial']

    escalation_instance = EscalationModel.objects.get(ID=pk_escalation)
    valid: bool = False

    if initial:
        message: str = "No se ha asignado un agente a este ticket"
        valid: bool = escalation_instance.agent_level_one != None and escalation_instance.agent_level_two != None and escalation_instance.agent_level_three != None
    else:
        message: str = "El tiempo de duración de este ticket se ha agotado, por favor proceder a cerrarlo"
        valid: bool = escalation_instance.status != 2 and escalation_instance.status != 3
        
    if escalation_instance.slack_thread != None and valid:
            for thread in escalation_instance.slack_thread:
                slack_reminder(thread, escalation_instance.slack_thread[thread], escalation_instance, message)