from typing import Any, Dict, List, Union

from common.dataTables import BaseDatatables
from common.models import BaseModel
from django.db.models import Q
from django.db.models.query import QuerySet, EmptyQuerySet
from datetime import timedelta, datetime, timezone
from dateutil.relativedelta import relativedelta

from .models import (ProblemsModel, EscalationModel, TestGroupModel,
                     EscalationSlackMessageModel, EscalationTablesModel, TestModel, SolutionModel)
from .serializers import EscalationSerializer
from functools import reduce


class ProblemsDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
        "classification":"choices",
    }

    model: BaseModel = ProblemsModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "classification": instance.classification,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":500
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "classification": "int",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Tipo":{
                "type":"choices",
                "name":"classification",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        item[0]:item[1] for item in self.model.ORIGIN_CHOICES
                    }
                }
            }

        }

        return data_struct

class EscalationSlackMessageDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = EscalationSlackMessageModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":700
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },

        }

        return data_struct

class EscalationTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]


    model: BaseModel = EscalationTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class EscalationDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = [
        "ID", "created", "creator", "updated", "updater", "agent",
        "rut", "services", "status", "escalation_level", "agent_level_one", "agent_level_two", "agent_level_tree"
    ]

    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "creator__pk":"choices",
        "agent_level_one__pk":"choices",
        "agent_level_two__pk":"choices",
        "agent_level_tree__pk":"choices",
        "status":"choices",
        "escalation_level":"choices",
        "services": "str"
    }

    FIELDS_EXCEL: Dict[str, Dict[str, str]] = {
        'A': 'Número de ticket',
        'B': 'Fecha de creación',
        'C': 'Agente creador',
        'D': 'Ultima fecha de actualización',
        'E': 'Ultimo agente en actualizar',
        'F': 'Ticket de SAC',
        'G': 'Estado',
        'H': 'Nivel de escalamiento',
        'I': 'Cliente',
        'J': 'Servicios',
    }

    model: BaseModel = EscalationModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH


        if search:
            if search.isdigit():

                int_value: int = int(search)
                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: int_value in x.services, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(ID=int_value) | 
                    Q(id__in=list_typify_id)
                )
            
            else: 

                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: search in x.rut, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(agent_level_one__username__icontains=search) |
                    Q(agent_level_two__username__icontains=search) |
                    Q(agent_level_tree__username__icontains=search) |
                    Q(ID__in=list_typify_id)
                )

        return qs

    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_escalation: QuerySet = EscalationTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_escalation.filters

        data_escalation: QuerySet = super().get_filtered_queryset()

        #Tickets assigned to request user
        if instance_tables_escalation.assigned_to_me:

            user: User = self.request.user
            original_query: QuerySet = data_escalation
            resultQuery = data_escalation.none()

            if user.groups.filter(name = "Agente TI nivel uno").exists():
                data_escalation_one: QuerySet = original_query.filter(agent_level_one=user)
                resultQuery = data_escalation_one.union(resultQuery)

            if user.groups.filter(name = "Agente TI nivel dos").exists():
                data_escalation_two: QuerySet = original_query.filter(agent_level_two=user)
                resultQuery = data_escalation_two.union(resultQuery)

            if user.groups.filter(name = "Agente TI nivel tres").exists():
                data_escalation_three: QuerySet = original_query.filter(agent_level_tree=user)
                resultQuery = data_escalation_three.union(resultQuery)

            if resultQuery != None:
                data_escalation = resultQuery
            else: 
                return EmptyQuerySet

        #Tickets created to request user
        if instance_tables_escalation.create_me:

            data_escalation: QuerySet = data_escalation.filter(creator=self.request.user)

        #Tickets time interval
        if instance_tables_escalation.last_time_type and instance_tables_escalation.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_escalation: QuerySet = data_escalation.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_escalation.last_time_type] * instance_tables_escalation.last_time) )

        return data_escalation

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        result: Dict[str, Union[str, int, float]] = EscalationSerializer(instance).data

        result["creator"]: str = instance.creator.username
        result["updater"]: str = instance.updater.username if instance.updater else ''
        result["agent_level_one"]: str = instance.agent_level_one.username if instance.agent_level_one else ''
        result["agent_level_two"]: str = instance.agent_level_two.username if instance.agent_level_two else ''
        result["agent_level_tree"]: str = instance.agent_level_tree.username if instance.agent_level_tree else ''

        result["escalation_level"]: str = instance.get_escalation_level_display()
        result["status"]: str = instance.get_status_display()
        result["agent"] = result["agent_level_tree"] if result["agent_level_tree"] else result["agent_level_two"] if result["agent_level_two"] else result["agent_level_one"]

        return result

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_tables_escalation: QuerySet = EscalationTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "download":True
        }

        data_struct["fields"] = {
            "ID": "int",
            "created": "datetime",
            "updated": "datetime",
            "status_SLA": "str",
            "operator": "int",
            "services": "list",
            "rut": "list",
            "creator": "str",
            "updater": "str",
            "agent_level_one": "str",
            "agent_level_two": "str",
            "agent_level_tree": "str"
        }

        #Init column definition#
        ########################

        columns: Dict[str, Dict[str, Any]] = {
            'creator':{
                "sortable": True,
                "name":"Creador",
                "width":100,
                "fixed":None
            },
            'updater':{
                "sortable": True,
                "name":"Actualizado",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Creado",
                "width":100,
                "fixed":None
            },
            'updated':{
                "sortable": True,
                "name":"Última actualización",
                "width":100,
                "fixed":None
            },
            'agent_level_one':{
                "sortable": True,
                "name":"Agente nivel uno",
                "width":100,
                "fixed":None
            },
            'agent_level_two':{
                "sortable": True,
                "name":"Agente nivel dos",
                "width":100,
                "fixed":None
            },
            'agent_level_tree':{
                "sortable": True,
                "name":"Agente nivel tres",
                "width":100,
                "fixed":None
            },
            'services':{
                "sortable": True,
                "name":"Servicios",
                "width":200,
                "fixed":None
            }, 
            'escalation_level':{
                "sortable": True,
                "name":"Nivel de escalamiento",
                "width":100,
                "fixed":None
            },
            'status':{
                "sortable": True,
                "name":"Estado",
                "width":100,
                "fixed":None
            },
            'typify':{
                "sortable": True,
                "name":"Tipificación",
                "width":100,
                "fixed":None
            },
            'rut':{
                "sortable": True,
                "name":"Cliente",
                "width":100,
                "fixed":None
            },     
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_escalation.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1

        data_struct_columns["Modificar"] = {
            "field": "custom_change",
            "sortable": False,
            "visible": True,
            "position": position,
            "width":100,
            "fixed":"right",
            "download": True,
        }

        data_struct["columns"] = data_struct_columns

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "download": True,
            "scroll": reduce(lambda x, y: x + y, [ column["width"] for column in  data_struct_columns.values()] ) + 100
        }

        #End  column definition#
        ########################

        #Init filters definition#
        #########################

        filters: Dict[str, Dict[str, Any]] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Estado":{
                "type":"choices",
                "name":"status",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'ABIERTO',
                        1:'EN SAC',
                        2:'CERRADO',
                    }
                }
            },
            "Nivel de escalamiento":{
                "type":"choices",
                "name":"escalation_level",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:'PRIMER',
                        1:'SEGUNDO',
                        2:'TERCER',
                    }
                }
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Agente nivel uno":{
                "type":"choices",
                "name":"agent_level_one__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Agente nivel dos":{
                "type":"choices",
                "name":"agent_level_two__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Agente nivel tres":{
                "type":"choices",
                "name":"agent_level_tree__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },

        }

        filters_names : Dict[str, str] = {
            'created': ["Fecha de inicio", "Fecha de fin"],
            'status':["Estado"],
            'escalation_level':["Nivel de escalamiento"],
            'creator__pk':["Creador"],
            'agent_level_one__pk':["Agente nivel uno"],
            'agent_level_two__pk':["Agente nivel dos"],
            'agent_level_tree__pk':["Agente nivel tres"],
            'services': ["Servicio"]
        }

        for _filter in instance_tables_escalation.filters:

            for _field in filters_names[_filter[0]]:

                del filters[_field]

        data_struct["filters"] = filters

        #End filters definition#
        ########################

        return data_struct

    def get_qs_to_data(self, qs: QuerySet) -> List[Dict[str, Any]]:

        """

            This function, process data

        """

        def get_escalation_data(instance: EscalationModel) -> List[Any]:

            def open_time( instance_model ) -> int:

                """
        
                    This funtion calc open time of ticket

                    :param self: Instance of Class EscalationSerializer
                    :type self: EscalationSerializer
                    :param instance_model: instance of model EscalationModel
                    :type instance_model: EscalationModel

                    :returns: Returns float of open time
                    :rtype: float
                
                """

                def next_opened(changes) -> Dict[str, Any]:

                    """
        
                        This funtion search next closed change

                        :param self: List of changes
                        :type self: list

                        :returns: Returns list of next closed change
                        :rtype: list
                
                    """

                    while ( len( changes ) and changes[0]['value'] == 'CERRADO' ):

                        changes = changes[1:]

                    return changes

                def next_closed(changes) -> Dict[str, Any]:

                    """
        
                        This funtion search next opened change

                        :param self: List of changes
                        :type self: list

                        :returns: Returns list of next opened change
                        :rtype: list
                
                    """

                    while ( len( changes ) and changes[0]['value'] != 'CERRADO' ):

                        changes = changes[1:]

                    return changes

                def all_changes(instance_model_history: QuerySet) -> List[Dict[str, Any]]:

                    """
        
                            This funtion get all changes

                            :param instance_model_history: List of changes
                            :type instance_model_history: QuerySet

                            :returns: Returns list of changes
                            :rtype: list
                    
                        """

                    result: List[Dict[str, Any]] = []
                    next_value: Any = None
                    next_datetime: datetime = None

                    for index, change in enumerate(instance_model_history[0: len(instance_model_history) - 1], start=0):

                        #Get changes
                        fields_changes = change.diff_against(instance_model_history[index + 1])

                        for change in fields_changes.changes:
                            
                            #Save changes of 'status' field
                            if change.field == 'status' and change.new != change.old:
                                
                                #Save old value
                                result.append(
                                    {
                                        'value': change.old,
                                        'datetime': instance_model_history[index].history_date,
                                    }
                                )

                                next_value: Any = change.new
                                next_datetime: datetime = instance_model_history[index + 1].history_date

                    if ( next_value ):

                        #Save latest value
                        result.append(
                            {
                                'value': next_value,
                                'datetime': next_datetime,
                            }
                        )

                    return result
                
                total: int = 0
                instance_model_history: QuerySet = instance_model.history.all().reverse()
                changes: List[Dict[str, Any]] = all_changes(instance_model_history)

                #Check if have been changes 'status' field
                if len( changes ) :
                    
                    #While there are time interval to count
                    while len( changes ):

                        #Next time open
                        changes: Dict[str, Any] = next_opened(changes)

                        #While there are time opened interval to count
                        if changes:

                            #Time it was open
                            datetime_opened: datetime = changes[0]['datetime']

                            #Next time close
                            changes: Dict[str, Any] = next_closed(changes)

                            #While there are time closed interval to count
                            if changes:

                                #Time it was close
                                datetime_closed: datetime = changes[0]['datetime']
                                #Time difference
                                total: int = total + (datetime_closed - datetime_opened).seconds

                            else:

                                #Time difference
                                total: int = total + (datetime.now(timezone.utc) - datetime_opened).seconds

                else:

                    #Instance was not created closed
                    if instance_model.status != 'CERRADO':

                        #Time difference
                        total:int = (instance_model.created - datetime.now(timezone.utc)).seconds


                return total//60

            return [
                instance.ID,
                instance.created - timedelta(hours=5),
                instance.creator.username if instance.creator else '',
                instance.updated - timedelta(hours=5),
                instance.updater.username if instance.updater else '',
                instance.typify,
                instance.get_status_display(),
                instance.get_escalation_level_display(),
                ','.join( list( map( lambda x: str(x), instance.rut) ) ),
                ','.join( list( map( lambda x: str(x), instance.services) ) ),
            ]

        return list( map( lambda x: x , map(get_escalation_data, qs) ) )

class TestDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = TestModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "description": instance.description,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":500
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "description": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },

        }

        return data_struct

class SolutionDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = SolutionModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "description": instance.description,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":500
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "description": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },

        }

        return data_struct

class TestGroupDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = TestGroupModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "tests": list(test.name for test in instance.tests.all())
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":500
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "tests": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Tests":{
                "field": "tests",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },

        }

        return data_struct
