# Generated by Django 2.1.7 on 2021-05-04 18:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('escalation_ti', '0015_auto_20210504_1201'),
        ('operators', '0003_update_operators')
    ]

    operations = [
        migrations.RemoveField(
            model_name='escalationmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='escalationphotomodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='escalationslackmessagemodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='escalationtablesmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='evaluationtestmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalescalationmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalescalationphotomodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalescalationslackmessagemodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalescalationtablesmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalevaluationtestmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicaloptionstestmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalproblemsmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalsolutionmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicaltestgroupmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicaltestmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='historicalurldocumentation',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='optionstestmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='problemsmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='solutionmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='testgroupmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='testmodel',
            name='operator',
        ),
        migrations.RemoveField(
            model_name='urldocumentation',
            name='operator',
        ),
    ]
