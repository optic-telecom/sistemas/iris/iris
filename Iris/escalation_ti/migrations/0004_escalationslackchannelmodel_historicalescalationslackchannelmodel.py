# Generated by Django 2.1.7 on 2021-03-16 20:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models
from django.db.models import Q

def create_groups(apps, scheme_editor):
    
    Group = apps.get_model('auth', 'Group')
    Permission = apps.get_model("auth", "Permission")

    group_names = ["Agente TI nivel uno","Agente TI nivel dos","Agente TI nivel tres"]

    for name in group_names:
        group_instance, p = Group.objects.get_or_create(name=name)

    admin_group, p = Group.objects.get_or_create(name = "Administrador TI")

    for permission in Permission.objects.filter(content_type__app_label='escalation_ti').filter(Q(content_type__model='escalationslackchannelmodel') | Q(content_type__model='escalationtablesmodel') |
    Q(content_type__model='solutionmodel') | Q(content_type__model='optionstestmodel') | Q(content_type__model='testmodel') | Q(content_type__model='problemsmodel')):

        admin_group.permissions.add(permission)

def delete_groups(apps, scheme_editor):
    Group = apps.get_model('auth', 'Group')
    group_names = ["Agente TI nivel uno","Agente TI nivel dos","Agente TI nivel tres","Administrador TI"]
    
    for name in group_names:
        try:
            Group.objects.filter(name=name).delete()
        except:
            pass
    

class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('escalation_ti', '0003_auto_20210311_1226'),
    ]

    operations = [
        migrations.RunPython(create_groups, delete_groups)
    ]
