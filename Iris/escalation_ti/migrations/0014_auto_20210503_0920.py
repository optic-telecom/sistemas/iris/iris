# Generated by Django 2.1.7 on 2021-05-03 13:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('escalation_ti', '0013_auto_20210503_0918'),
    ]

    operations = [
        migrations.RenameField(
            model_name='escalationmodel',
            old_name='rut_list',
            new_name='rut',
        ),
        migrations.RenameField(
            model_name='historicalescalationmodel',
            old_name='rut_list',
            new_name='rut',
        ),
    ]
