from datetime import datetime, timezone, timedelta
from typing import Any, Dict, List, Union
import random 
import string 

from django_q.models import Schedule
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import get_ticket_SLA
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import (BooleanField, SerializerMethodField,
                                        ValidationError)
from slack import WebClient
from rest_framework import serializers
from tickets.models import TypifyModel
from users.models import UserSlackIDModel
from .models import (ProblemsModel, EscalationSlackMessageModel, EscalationTablesModel,
                     SolutionModel, EscalationModel,
                     EscalationModel, TestModel, TestGroupModel,
                     UrlDocumentation, OptionsTestModel, EvaluationTestModel, EscalationPhotoModel)


class EscalationProblemsSerializer(BaseSerializer):

    """

        Class define serializer of ProblemsView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = ProblemsModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'tests': {
                'required': False
            },
            'problems': {
                'required': False
            },
            'solutions': {
                'required': False
            },
        }

    def update(self, instance, validated_data):

        def delete_item(item: str, value: Dict [str, Any]) -> Dict [str, Any]:

            if item in value:

                del value[item]

            return value

        return super().update(instance, validated_data)

class EscalationSlackMessageSerializer(BaseSerializer):

    """

        Class define serializer of EscalationSlackMessageView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = EscalationSlackMessageModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class EscalationTablesSerializer(BaseSerializer):

    class Meta:

        #Class model of serializer
        model: BaseModel = EscalationTablesModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class TestSerializer(BaseSerializer):

    documentation = SerializerMethodField()
    results = SerializerMethodField()
    
    def get_documentation(self,instance_model: TestModel) -> str:

        docs_url: List[Dict[str,str]] = []
        for doc in instance_model.documentation.all():
            docs_url.append({"id": doc.ID, "url": doc.url})
        return docs_url

    def get_results(self, instance_model: TestModel):
        options = OptionsTestModel.objects.filter(test=instance_model, deleted=False)
        options_list: List[Dict[str,Any]] = []
        for option in options:
            options_list.append({"id": option.ID, "result": option.result})
        return options_list


    class Meta:

        #Class model of serializer
        model: BaseModel = TestModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'tests': {
                'required': False
            },
        }

class SolutionSerializer(BaseSerializer):

    documentation = SerializerMethodField()
    
    def get_documentation(self,instance_model: TestModel) -> str:

        docs_url: List[Dict[str,str]] = []
        for doc in instance_model.documentation.all():
            docs_url.append({"id": doc.ID, "url": doc.url})
        return docs_url


    class Meta:

        #Class model of serializer
        model: BaseModel = SolutionModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'tests': {
                'required': False
            },
        }

class UrlDocumentationSerializer(BaseSerializer):

    class Meta:

        #Class model of serializer
        model: BaseModel = UrlDocumentation
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class EscalationSerializer(BaseSerializer):

    """

        Class define serializer of EscalationView.
    
    """
    #Method get_SLA
    SLA = serializers.SerializerMethodField()
    #Method get_status_SLA
    is_agent = BooleanField(default=True)

    agent = serializers.SerializerMethodField()

    def get_agent(self, instance_model: EscalationModel) -> str:
        result = instance_model.agent_level_tree if instance_model.agent_level_tree else instance_model.agent_level_two if instance_model.agent_level_two else instance_model.agent_level_one
        return result.username if result else " "

    def get_SLA(self, instance_model : EscalationModel ) -> float:
        
        closed_status_list = [2,3]
        return get_ticket_SLA(instance_model, closed_status_list, 'status')

    def slack_message(self, instance_model: EscalationModel):

        def send_message(channel: str, instance_model: EscalationModel):

            def search_value(field: str, instance_model : EscalationModel) -> Any:

                _callable: Dict[str, callable()] = {
                    'services': lambda instance: ','.join( list( map( lambda service: str(service), instance.services ) ) ), 
                    'rut': lambda instance: instance.rut, 
                    'escalation_level': lambda instance: instance.get_escalation_level_display(), 
                    'typify': lambda instance: instance.typify,
                }

                return _callable[field](instance_model)

            fields = ["rut","services","escalation_level","typify"]
            data: Dict[str, Any] = {}
            for field in fields:
                if field == "typify" and instance_model.typify == None:
                    continue
                data[field] = search_value(field, instance_model)

            global_preferences = global_preferences_registry.manager()
            user_uid_query: str = UserSlackIDModel.objects.filter(user=instance_model.creator,operator=instance_model.operator)
            user_uid = "<@" + str(user_uid_query[0].uid) + ">" if len(user_uid_query) > 0 else ''
            client = WebClient(token=instance_model.operator.slack_token)

            labels: Dict[str,str] = {
                "typify": f"ID tipificación: {data.get('typify')} \n", 
                "services": f"Servicios: {data.get('services')}\n", 
                "rut": f"Rut: {data.get('rut')}\n", 
                "escalation_level": f"Nivel de escalamiento: {data.get('escalation_level')}\n", 
            }           

            items: str = ""
            for item in data:
                items = items + labels[item]
           
            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Ticket Escalamiento"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":pencil:*{instance_model.creator.username}* {user_uid} ha creado un nuevo ticket de escalamiento. ID: *{instance_model.pk}*"
                    }
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": items
                    }
                },
                {
                    "type": "divider"
                }
            ]

            response = client.chat_postMessage(
                channel=channel,
                blocks=blocks,
                text=f"{instance_model.creator.username} {user_uid} ha creado un nuevo ticket de escalamiento. ID: {instance_model.pk}"
            )

            return response.data['ts']

        threads: Dict[str, str] = {}

        try:

            global_preferences = global_preferences_registry.manager()
            if instance_model.operator.ID == 2:
                channel: str = global_preferences['escalation_ti__slack_channel_bandaancha'] 
            else: 
                channel: str = global_preferences['escalation_ti__slack_channel_multifiber']
            
            if channel != '' and channel != None:
                thread = send_message(channel, instance_model)
                threads[thread] = channel
        except:
            pass
        instance_model.slack_thread = threads
        instance_model.save()

    def add_reminder(self, instance_model : EscalationModel):

        #Send a message 5 minutes after creation if nowone has taken it
        schedule_object: Schedule = Schedule.objects.create(
            func='escalation_ti.queue.send_reminder',
            name=''.join(random.choice(
                string.ascii_uppercase + string.digits) for _ in range(20)),
            schedule_type='O',
            repeats=1,
            next_run=datetime.now() + timedelta(minutes=5),
            kwargs={
                'pk_escalation': int(instance_model.ID),
                'initial': True
            },
            minutes=5,
        )

    def create(self, validated_data: Dict[str, Any]) -> EscalationModel:

        """
        
            This funtion validate and create instance

            :param self: EscalationSerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        if validated_data.pop('is_agent'):

            user: User = self.context['request'].user

            if User.objects.filter(groups__name="Agente TI nivel tres", pk=user.pk):

                validated_data['agent_level_tree'] = user
                validated_data["escalation_level"] = 2

            elif User.objects.filter(groups__name="Agente TI nivel dos", pk=user.pk):

                validated_data['agent_level_two'] = user
                validated_data["escalation_level"] = 1

            elif User.objects.filter(groups__name="Agente TI nivel uno", pk=user.pk):

                validated_data['agent_level_one'] = user
                validated_data["escalation_level"] = 0

            else:

                if "agent_level_one" in validated_data:

                    del validated_data["agent_level_one"]

                if "agent_level_two" in validated_data:

                    del validated_data["agent_level_two"]

                if "agent_level_tree" in validated_data:

                    del validated_data["agent_level_tree"]

                validated_data["escalation_level"] = 0

        result = super().create(validated_data)
        self.slack_message(result)
        self.add_reminder(result)
        return result

    def update(self, instance, validated_data: Dict[str, Any]) -> EscalationModel:

        """
        
            This funtion validate and update instance

            :param self: EscalationSerializer instance
            :type self: EscalationSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        def slack_message(instance: EscalationModel, validated_data: Dict[str,Any]):


            def send_message(thread: str, channel: str, validated_data: Dict[str,Any], instance: EscalationModel):

                def search_value(field: str, validated_data : Dict[str,Any]) -> Any:

                    _callable: Dict[str, callable()] = {
                        'agent': lambda instance: validated_data.get("agent").username, 
                        'services':lambda instance: ','.join( list( map( lambda service: str(service), validated_data.get("services",[]) ) ) ), 
                        'rut': lambda instance: validated_data.get('rut'), 
                        'status': lambda instance: EscalationModel.STATUS_CHOICES[validated_data.get('status')][1],
                        'typify': lambda instance: validated_data.get('typify'), 
                        'escalation_level': lambda instance: EscalationModel.ESCALATION_CHOICES[validated_data.get('escalation_level')][1],
                        'tests': lambda instance: ','.join( list( map( lambda test: str(test), validated_data.get("tests",[]) ) ) ), 
                        'problems': lambda instance: ','.join( list( map( lambda problem: str(problem), validated_data.get("problems",[]) ) ) ),
                        'solutions': lambda instance: ','.join( list( map( lambda solution: str(solution), validated_data.get("solutions",[]) ) ) ),
                        'agent_level_one': lambda instance: validated_data.get('agent_level_one').username, 
                        'agent_level_two': lambda instance: validated_data.get('agent_level_two').username, 
                        'agent_level_three': lambda instance: validated_data.get('agent_level_three').username, 
                    }

                    return _callable[field](validated_data)

                data: Dict[str, Any] = {}
                for field in validated_data:
                    if field == "operator" or field == "updater":
                        continue
                    data[field] = search_value(field, validated_data)
                
                labels: Dict[str,str] = {
                "agent": f"Agente: {data.get('agent')}\n", 
                "subcategory": f"Subcategoría: {data.get('subcategory')}\n",
                "second_subcategory": f"Segunda Subcategoría: {data.get('second_subcategory')}\n",
                "services": f"Servicios: {data.get('services')}\n", 
                "rut": f"Rut: {data.get('rut')}\n", 
                "status": f"Estado: {data.get('status')}\n", 
                "typify": f"Tipificación: {data.get('typify')}\n", 
                "commentaries": f"Comentarios: {data.get('commentaries')}\n", 
                "escalation_level": f"Nivel de escalamiento: {data.get('escalation_level')}\n",
                "tests": f"Pruebas: {data.get('tests')}\n",
                "problems": f"Problemas: {data.get('problems')}\n",
                "solutions": f"Soluciones: {data.get('solutions')}\n",
                "agent_level_one": f"Agente nivel uno: {data.get('agent_level_one')}\n",
                "agent_level_two": f"Agente nivel dos: {data.get('agent_level_two')}\n",
                "agent_level_three": f"Agente nivel tres: {data.get('agent_level_three')}\n"
                }

                global_preferences = global_preferences_registry.manager()
                client = WebClient(token=instance.operator.slack_token)
                items = ""
                for item in data:
                    items = items + labels[item]
                
                blocks: List[Dict[str, Any]] =  [
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":iris: *Iris* Ticket de Escalamiento"
                        }
                    },
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f":writing_hand:Se ha actualizado el ticket de escalamiento"
                        }
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": items
                        }
                    },
                    {
                        "type": "divider"
                    }
                ]

                client.chat_postMessage(
                    blocks=blocks,
                    thread_ts=thread,
                    channel=channel,
                    text= "Se ha actualizado el ticket de escalamiento"
                )
                
            if instance.slack_thread != None:
                for thread in instance.slack_thread:
                    send_message(thread, instance.slack_thread[thread], validated_data, instance)

            if "status" in validated_data:
                if (instance.typify != None and validated_data['status'] == 1) or (instance.typify == None and validated_data['status'] == 2):
                    global_preferences = global_preferences_registry.manager()
                    client = WebClient(token=instance.operator.slack_token)
                    solved_block: List[Dict[str, Any]] =  [
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":iris: *Iris* Ticket de Escalamiento"
                        }
                    },
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f":writing_hand:Se ha resuelto el ticket de escalamiento"
                        }
                    }
                    ]

                    if instance.slack_thread != None:    
                        for thread in instance.slack_thread:
                            client.chat_postMessage(
                                blocks=solved_block,
                                thread_ts=thread,
                                channel=instance.slack_thread[thread],
                                text="Se ha resuelto el ticket de escalamiento"
                            )

                    if instance.typify != None:
                        typify: TypifyModel = TypifyModel.objects.get(ID=instance.typify)
                        if typify.slack_thread != None:    
                            for thread in typify.slack_thread:
                                client.chat_postMessage(
                                    blocks=solved_block,
                                    thread_ts=thread,
                                    channel=typify.slack_thread[thread],
                                    text="Se ha resuelto el ticket de escalamiento"
                                )

        if "status" in validated_data:
            if instance.typify != None:
                if (validated_data['status'] == 2 or validated_data['status'] == 3) and TypifyModel.objects.get(ID=instance.typify).status != 1:
                    raise ValidationError({"open typify":"The asociated typification is still open"})

        if "agent_level_one" in validated_data and ( instance.escalation_level == 0 or validated_data["escalation_level"] == 0):

            try:

                validated_data["agent_level_one"] = User.objects.get(groups__name="Agente TI nivel uno", pk=validated_data["agent_level_one"].pk)

            except:

                raise ValidationError({
                    "Invalid user":"Don´t exist user or invalid group"
                })

        if "agent_level_two" in validated_data and ( instance.escalation_level == 1 or validated_data["escalation_level"] == 1):

            try:

                validated_data["agent_level_two"] = User.objects.get(groups__name="Agente TI nivel dos", pk=validated_data["agent_level_two"].pk)

            except:

                raise ValidationError({
                    "Invalid user":"Don´t exist user or invalid group"
                })

        if "agent_level_tree" in validated_data and ( instance.escalation_level == 2 or validated_data["escalation_level"] == 2):

            try:

                validated_data["agent_level_tree"] = User.objects.get(groups__name="Agente TI nivel tres", pk=validated_data["agent_level_tree"].pk)

            except:

                raise ValidationError({
                    "Invalid user":"Don´t exist user or invalid group"
                })

        slack_message(instance , validated_data)
        
        return super().update(instance, validated_data)

    def to_representation(self, instance):

        representation: Dict[str, Any] = super().to_representation(instance)
        representation["tests"] = list(
            map( 
                lambda x: EvaluationTestSerializer(x).data,
                EvaluationTestModel.objects.filter(ID__in=representation["tests"])
            )
        )

        return representation


    class Meta:

        #Class model of serializer
        model: BaseModel = EscalationModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'agent': {
                'required': False
            },
        }

class OptionsTestSerializer(BaseSerializer):

    """

        Class define serializer of OptionTestView.
    
    """


    def update(self, instance, validated_data: Dict[str, Any]) -> OptionsTestModel:

        """
        
            This funtion validate and update instance

            :param self: OptionsTestSerializer instance
            :type self: OptionsTestSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        if 'test' in validated_data:
            raise ValidationError({'test': 'changing the test reference is not allowed'})

        return super().update(instance, validated_data)

    class Meta:

        #Class model of serializer
        model: BaseModel = OptionsTestModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class EvaluationTestSerializer(BaseSerializer):

    """

        Class define serializer of EvaluationTestView.
    
    """


    class Meta:

        #Class model of serializer
        model: BaseModel = EvaluationTestModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class EscalationPhotoSerializer(BaseSerializer):

    
    """

        Class define serializer of EscalationPhotoView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = EscalationPhotoModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class TestGroupSerializer(BaseSerializer):

    
    """

        Class define serializer of TestGroupView.

    """

    tests = SerializerMethodField()
    
    def get_tests(self,instance_model: TestGroupModel) -> str:

        test_list: List[Dict[str,str]] = []
        for test in instance_model.tests.all():
            options = OptionsTestModel.objects.filter(test=test, deleted=False)
            options_list: List[Dict[str,Any]] = []
            for option in options:
                options_list.append({"id": option.ID, "result": option.result})
            test_list.append({"id": test.ID, "name": test.name, "options": options_list})
        return test_list

    def update(self, instance, validated_data):

        """
        
            This funtion updates instance

            :param self: TestGroupSerializer instance
            :type self: TestGroupSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        tests = self.context["request"].data.get("tests",[])

        result = super().update(instance, validated_data)

        if tests != []:
            result.tests.clear()
            for test in tests:
                test_instance = TestModel.objects.get(ID=test)
                result.tests.add(test_instance)

        return result

    def create(self, validated_data: Dict[str, Any]) -> TestGroupModel:

        """
        
            This funtion validate and create instance

            :param self: TestGroupSerializer instance
            :type self: TestGroupSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        tests = self.context["request"].data.get("tests",[])

        result = super().create(validated_data)
        for test in tests:
            test_instance = TestModel.objects.get(ID=test)
            result.tests.add(test_instance)

        return result

    class Meta:

        # Class model of serializer
        model: BaseModel = TestGroupModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }