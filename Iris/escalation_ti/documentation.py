
from typing import Union, List
from .serializers import EscalationProblemsSerializer
from rest_framework import serializers
from drf_yasg.openapi import Response as Response_Openapi
from drf_yasg.utils import swagger_auto_schema
from .models import ProblemsModel

class DocumentationSerializerDatatablesEscalation(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """

        start: int = serializers.IntegerField()
        offset: int = serializers.IntegerField()
        order_field: str = serializers.CharField(default="ID")
        order_type: str = serializers.CharField(default="desc")
        filters: List[list] = serializers.JSONField(default=[['ID','equal','1']])
        
        class Meta:

            fields: Union[list, str] = '__all__'

class DocumentationProblems(object):

    """

        Class define documentation to ViewSet ProblemsView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of ProblemsView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_body = EscalationProblemsSerializer
    serializer_class_response = EscalationProblemsSerializer
    serializer_class_datatables = DocumentationSerializerDatatablesEscalation

    def documentation_create_and_update(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationProblems
            :type self: DocumentationProblems

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create or update, Problems',
            operation_description="""

                This endpoint create lead record. 

            """,
            responses = {
                200:Response_Openapi(
                    'Create or update Problems',
                    self.serializer_class_response,
                ),
                500:Response_Openapi(
                    description = 'Create or update Problems',
                    examples = {
                        "SLA error":"SLA <= 0",
                        "cycle error":"Recursive cycle",
                        "error parent":"Incorrect parent o Required parent",
                    },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_tree(self):

        """

            This function return information of documentation view 'tree'

            :param self: Instance of Class DocumentationProblems
            :type self: DocumentationProblems

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Tree typify',
            operation_description="""

                This endpoint return tree.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Tree',
                    examples = {
                        'name': '3. Queja', 
                        'title': 'Tipo',
                        'id': 1,
                        'children': [
                            {
                                'name': '3. Queja', 
                                'title': 'Tipo',
                                'id': 1,
                            },
                        ]
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationProblems
            :type self: DocumentationProblems

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Problems',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Problems',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationProblems
            :type self: DocumentationProblems

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Problems',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Problems',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "classification": "int",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "int",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Tipo":{
                                "type":"choices",
                                "name":"classification",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        item[0]:item[1] for item in ProblemsModel.ORIGIN_CHOICES
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationEscalationSlackMessage(object):

    """

        Class define documentation of EscalationSlackMessageView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of EscalationSlackMessageView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatablesEscalation

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationEscalationSlackMessage
            :type self: DocumentationEscalationSlackMessage

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Escalation slack message',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Escalation slack message',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationEscalationSlackMessage
            :type self: DocumentationEscalationSlackMessage

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Escalation slack message',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Escalation slack message',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "Problems_name": "str",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Categoria":{
                                "field": "Problems_name",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Categoría":{
                                "type":"choices",
                                "name":"Problems__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "escalation_ti/Problems/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_fields_options(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationEscalationSlackMessage
            :type self: DocumentationEscalationSlackMessage

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Escalation message options',
            operation_description="""             """,
            responses = {
                200:Response_Openapi(
                    description = 'Escalation message options',
                    examples = [
                        {"value":"Problems", "name":"Categoría"},
                        {"value":"agent", "name":"Agente"},
                        {"value":"services", "name":"Servicios"},
                        {"value":"rut", "name":"RUT"},
                        {"value":"typify", "name":"Tipificación"},
                        {"value":"status", "name":"Estado"},
                        {"value":"escalation_level", "name":"Nivel"},
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationEscalationTables(object):

    """

        Class define documentation of EscalationTablesView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of EscalationTablesView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatablesEscalation

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationEscalationTables
            :type self: DocumentationEscalationTables

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Escalation tables',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, tables',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                            "fields": ['agent', 'channel', 'subProblems', 'second_subProblems', 'service', 'location', 'rut', 'plan']
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                            "fields": ['agent', 'channel', 'subProblems', 'second_subProblems', 'service', 'location', 'rut', 'plan']
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_definition(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationEscalationTables
            :type self: DocumentationEscalationTables

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Definition, Escalation tables',
            operation_description="""            """,
            responses = {
                200:Response_Openapi(
                    description = 'Definition, Escalation tables',
                    examples = {
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": ["datetime", "str", "int", "bool", "choices", "location"]
                            },
                            "Año": {
                                "name": "year",
                                "type": ["int"]
                            },
                            "Mes": {
                                "name": "month",
                                "type": ["int"]
                            },
                            "Día": {
                                "name": "day",
                                "type": ["int"]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": ["int"]
                            },
                            "Semana": {
                                "name": "week",
                                "type": ["int"]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": ["int"]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": ["datetime", "str", "int", "bool"]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": ["datetime", "str", "int", "bool"]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": ["datetime", "str", "int", "bool"]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type":"datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": ["datetime", "str", "int", "bool", "choices", "location"]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type":"str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type":"str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type":"str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type":"str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                            "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                            "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                            "bool": [ "Igual", "Distinto" ],
                            "choices": [ "Igual", "Distinto" ],
                            "location": [ "Igual", "Distinto" ]
                        },
                        "filters": {
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "int",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Agente":{
                                "type":"choices",
                                "name":"agent__pk",
                                "type_choices": "int",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Fecha de creación":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Estado":{
                                "type":"choices",
                                "name":"status",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0: 'ABIERTO',
                                        1: 'EN SAC',
                                        2: 'CERRADO',
                                    }
                                }
                            },
                            "Nivel":{
                                "type":"choices",
                                "name":"escalation_level",
                                "type_choices": "int",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        0: 'PRIMER',
                                        1: 'SEGUNDO',
                                        2: 'TERCER',
                                    }
                                }
                            },
                        },
                        "columns":  {
                            'creator': "Creador",
                            'updater': "Último en actualizar",
                            'created': "Fecha de creación",
                            'updated': "Fecha de actualización",
                            'Problems': "Categoría",
                            'agent': "Agente asignado",
                            'services': "Servicios", 
                            'status': "Estado",
                            'rut': "RUT",
                            'escalation_level': "Nivel",
                            'typify':"Tipificación"
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationEscalationTables
            :type self: DocumentationEscalationTables

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, tables',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, tables',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                        },
                        "filters": {},
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationTest(object):

    """

        Class define documentation of TestView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TestView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatablesEscalation

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTest
            :type self: DocumentationTest

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Escalation slack message',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Escalation slack message',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationTest
            :type self: DocumentationTest

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Escalation slack message',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Escalation slack message',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "Problems_name": "str",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Categoria":{
                                "field": "Problems_name",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Categoría":{
                                "type":"choices",
                                "name":"Problems__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "escalation_ti/Problems/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationProblem(object):

    """

        Class define documentation of ProblemView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of ProblemView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatablesEscalation

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationProblem
            :type self: DocumentationProblem

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Escalation slack message',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Escalation slack message',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationProblem
            :type self: DocumentationProblem

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Escalation slack message',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Escalation slack message',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "Problems_name": "str",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Categoria":{
                                "field": "Problems_name",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Categoría":{
                                "type":"choices",
                                "name":"Problems__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "escalation_ti/Problems/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationSolution(object):

    """

        Class define documentation of SolutionView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of SolutionView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatablesEscalation

    def documentation_datatables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationSolution
            :type self: DocumentationSolution

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, Escalation slack message',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, Escalation slack message',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Mensaje",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                        },
                        {
                            "ID": "10",
                            "name": "Notificación",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "Problems": 1,
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_datatables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationSolution
            :type self: DocumentationSolution

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, Escalation slack message',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, Escalation slack message',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "Problems_name": "str",
                        },
                        "columns": {
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Categoria":{
                                "field": "Problems_name",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            },
                            "Categoría":{
                                "type":"choices",
                                "name":"Problems__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "escalation_ti/Problems/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )