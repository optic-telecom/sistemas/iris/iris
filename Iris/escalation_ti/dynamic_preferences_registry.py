from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference, IntegerPreference, ChoicePreference

general = Section('escalation_ti')


@global_preferences_registry.register
class EscalationChannelBandaancha(StringPreference):

    section = general
    name: str = 'slack_channel_bandaancha'
    default: str = ''
    required: bool = True

@global_preferences_registry.register
class EscalationChannelMultifiber(StringPreference):

    section = general
    name: str = 'slack_channel_multifiber'
    default: str = ''
    required: bool = True
