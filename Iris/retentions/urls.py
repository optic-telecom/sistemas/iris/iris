from rest_framework.routers import DefaultRouter

from .views import (RetentionAgreementView, RetentionView,RetentionTableView,RetentionChannelView,RetentionsTipifyCategoryView)

router = DefaultRouter()

router.register(r'customer_retention', RetentionView, basename='customer_retention')
router.register(r'retention_table', RetentionTableView, basename='retention_table')
router.register(r'retention_channel', RetentionChannelView, basename='retention_channel')
router.register(r'retention_category', RetentionsTipifyCategoryView, basename='retention_category')
router.register(r'agreement', RetentionAgreementView, basename='agreement')

urlpatterns = router.urls
