from typing import Any, Dict, List, Union
from slack import WebClient
import json
import requests
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import get_ticket_SLA
from users.models import UserSlackIDModel
from django.contrib.auth.models import User, Group
from django.db.models.query import QuerySet
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import (SerializerMethodField,ValidationError)
from rest_framework import serializers
from django_q.models import Schedule

from .models import (RetentionAgreementModel, RetentionModel, RetentionsTablesModel, RetentionsChannelModel,RetentionsTipifyCategoryModel)
from formerCustomers.models import RemoveEquipamentModel
from formerCustomers.serializers import RemoveEquipamentSerializer
from tickets.models import TypifyModel, CategoryModel

class RetentionSerializer(BaseSerializer):

    """

        Class define serializer of RetentionView.
    
    """
    reminder = SerializerMethodField()

    channel = SerializerMethodField()
    
    def get_reminder(self,instance_model: RetentionModel) -> str:

        reminder_data: Dict[str,str] = {}
        if instance_model.reminder:
            if type(instance_model.reminder.kwargs) != dict:
                message = json.loads(instance_model.reminder.kwargs.replace("'",'"'))["message"]
            else:
                message = instance_model.reminder.kwargs.get("message")
            reminder_data = {"date": instance_model.reminder.next_run, "message": message}
            return reminder_data
        else:
            return None

    def get_channel(self,instance_model: RetentionModel) -> str:
        if instance_model.typify:
            return instance_model.typify.channel
        else:
            return None

    def slack_message(self, instance, message):

        token: str = instance.operator.slack_token

        client = WebClient(token=token)

        result = client.chat_postMessage(
            token=token,
            channel=instance.slack_channel,
            blocks=message,
            thread_ts=instance.slack_thread
        )

    def create_typify(self, instance, channel, categories):

        self.global_preferences = global_preferences_registry.manager()
        matrix_url: str = self.global_preferences['general__matrix_domain'] + 'services/'

        response = requests.get(
            url=matrix_url,
            headers={
                "Authorization": self.global_preferences['matrix__matrix_token']
            },
            params={"number": instance.service},
            verify=False,
        ).json()

        rut = requests.get(
            url=response["results"][0]["customer"],
            headers={
                "Authorization": self.global_preferences['matrix__matrix_token']
            },
            verify=False,
        ).json()["rut"]

        typify_instance = TypifyModel.objects.create(
            creator=instance.creator,
            operator=instance.operator,
            agent=instance.creator,
            services=[instance.service],
            customer_type=3,
            rut=[rut],
            channel=int(channel)
        )
        
        for category in categories:
            category_instance = CategoryModel.objects.get(pk=category)
            typify_instance.category.add(category_instance)

        typify_instance.save()
        instance.typify = typify_instance

    def create(self, validated_data: Dict[str, Any]) -> RetentionModel:

        """
        
            This funtion validate and create instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        def slack_message(instance, data):

            token: str = instance.operator.slack_token

            client = WebClient(token=token)

            channel_instance: str = RetentionsChannelModel.objects.filter(operator=instance.operator).first()
            creator = instance.creator.username

            blocks = [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": creator + " ha creado una retención. ID: " + str(instance.ID)
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*Número de servicio*: " + str(instance.service) + "\n"
                    }
                }
            ]

            if channel_instance: 
                result = client.chat_postMessage(
                    token=token,
                    channel=channel_instance.channel,
                    blocks=blocks,
                    text=creator + " ha creado una retención. ID: " + str(instance.ID)
                )

                return result.data['channel'], result.data['ts']

            else:
                return None,None

        categories = json.loads(self.context['request'].data.get("categories"))
        result = super().create(validated_data)
        if len(RetentionsTipifyCategoryModel.objects.filter(deleted=False).all()) > 0:
            self.create_typify(result, self.context["request"].data.get("channel"), categories)
        slack_channel, slack_thread = slack_message(result, self.context["request"].data)

        result.slack_channel = slack_channel
        result.slack_thread = slack_thread
        result.save()

        return result

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        if instance.status == 1 and 'status' not in validated_data:
            raise ValidationError({'Error': 'This retention has been closed and cannot be updated'})

        if 'succesfull' in validated_data:

            if validated_data["succesfull"] == False:

                global_preferences = global_preferences_registry.manager()
                
                matrix_url: str = global_preferences['general__matrix_domain'] + 'services/'

                response = requests.get(
                    url=matrix_url,
                    headers={
                        "Authorization": global_preferences['matrix__matrix_token']
                    },
                    params={'number': instance.service},
                    verify=False,
                ).json()

                # Update service status in Matrix
                url = matrix_url + str(response['results'][0]['id']) + "/"

                data={ 'status': '2'  }

                response2 = requests.patch(
                    url=url,
                    headers={
                        "Authorization": global_preferences['matrix__matrix_token']
                    },
                    verify=False,
                    data= data
                ).json()

                # Create remove equip instance
                customer_url: str = response["results"][0]["customer"]

                response = requests.get(
                    url=customer_url,
                    headers={
                        "Authorization": global_preferences['matrix__matrix_token']
                    },
                    verify=False,
                ).json()

                remove_equip = RemoveEquipamentModel.objects.create(
                    operator=instance.operator,
                    service=instance.service,
                    rut=response["rut"]
                )

                RemoveEquipamentSerializer(remove_equip).slack_message(remove_equip)
            
            if instance.slack_thread:

                message = [{
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "La retención ha sido exitosa" if validated_data.get("succesfull") else "La retención no ha sido exitosa"
                    }
                }]

                self.slack_message(instance, message)

        return super().update(instance, validated_data)
   
    class Meta:

        #Class model of serializer
        model: BaseModel = RetentionModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'agent': {
                'required': False
            },
        }

class RetentionTableSerializer(BaseSerializer):

    """

        Class define serializer of RetentionTableView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = RetentionsTablesModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'agent': {
                'required': False
            },
        }

class RetentionChannelSerializer(BaseSerializer):

    
    """

        Class define serializer of LeadChannelView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = RetentionsChannelModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def create(self, validated_data: Dict[str, Any]) -> RetentionsChannelModel:

        same_op_channel = RetentionsChannelModel.objects.filter(operator = validated_data['operator'])

        if len(same_op_channel) > 0:

            raise ValidationError({
                    'Error same operator': str(same_op_channel[0].ID)
                })

        return super().create(validated_data)

class RetentionsTipifyCategorySerializer(BaseSerializer):

    
    """

        Class define serializer of RetentionsTipifyCategoryView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = RetentionsTipifyCategoryModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class RetentionAgreementSerializer(BaseSerializer):

    """

        Class define serializer of QAchannelView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = RetentionAgreementModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }