import threading
import random
import string
import json
from typing import Any, Dict, List

from django.contrib.auth.models import User, Group
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.viewsets import BaseViewSet
from django.http import JsonResponse, HttpResponseForbidden
from rest_framework.decorators import action
from rest_framework.serializers import ValidationError
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from dynamic_preferences.registries import global_preferences_registry
from django_q.models import Schedule

from .models import (RetentionAgreementModel, RetentionModel, RetentionsTablesModel,RetentionsChannelModel, RetentionsTipifyCategoryModel)
from .serializers import (RetentionAgreementSerializer, RetentionChannelSerializer, RetentionSerializer, RetentionTableSerializer,
RetentionsTipifyCategorySerializer)
from .datatables import (RetentionAgreementDatatable, RetentionDatatable,RetentionTablesDatatable)
from tickets.models import CategoryModel
from operators.models import OperatorModel

class RetentionView(BaseViewSet):

    """

        Class define ViewSet of TicketModel.
    
    """

    queryset: BaseModel = RetentionModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RetentionSerializer

    def get_queryset(self, *args, **kwargs):
        if self.request.user.has_perm('retentions.view_retentionmodel'):
            return self.queryset
        else:
            return HttpResponseForbidden()
    
    @method_decorator(permission_required('retentions.add_retentionmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)
    
    @method_decorator(permission_required('retentions.change_retentionmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.delete_retentionmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.delete_retentionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_agreement(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds solution to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            retention: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error retention": "Item doesnt exists"         
            })

        retention.agreements.append(request.POST['agreement'])
        retention.save()

        message = [{
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": "Se ha registrado un acuerdo *" + str(request.POST['agreement']) + "*"
            }
        }]

        RetentionSerializer(retention).slack_message(retention, message)
        return JsonResponse( data=RetentionSerializer(retention).data, safe=False )

    @method_decorator(permission_required('retentions.delete_retentionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_followup(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds solution to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            retention: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error retention": "Item doesnt exists"         
            })

        retention.followup.append(request.POST['followup'])
        retention.save()

        message = [{
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": "Se ha registrado un seguimiento *" + str(request.POST['followup']) + "*"
            }
        }]

        RetentionSerializer(retention).slack_message(retention, message)

        return JsonResponse( data=RetentionSerializer(retention).data, safe=False )

    @method_decorator(permission_required('retentions.delete_retentionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_agreement(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds solution to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            retention: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error retention": "Item doesnt exists"         
            })

        try:    
            retention.agreements.remove(request.POST['agreement'])
            retention.save()
        except:
            pass
        
        return JsonResponse( data=RetentionSerializer(retention).data, safe=False )

    @method_decorator(permission_required('retentions.delete_retentionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_followup(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds solution to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            retention: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error retention": "Item doesnt exists"         
            })

        try:    
            retention.followup.remove(request.POST['followup'])
            retention.save()
        except:
            pass

        return JsonResponse( data=RetentionSerializer(retention).data, safe=False )

    @method_decorator(permission_required('retentions.delete_retentionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_reminder(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds solution to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            retention: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error retention": "Item doesnt exists"         
            })

        schedule_object: Schedule = Schedule.objects.create(
            func='retentions.api.send_retention_reminder',
            name=''.join(random.choice(
                string.ascii_uppercase + string.digits) for _ in range(20)),
            schedule_type='D',
            repeats=1,
            next_run=request.POST['date'],
            kwargs={
                'instance_id': int(retention.ID),
                'message': request.POST['message']
            },
            minutes=5,
        )
        # Add schedule to cycle instance
        retention.reminder = schedule_object
        retention.save()
        
        return JsonResponse( data=RetentionSerializer(retention).data, safe=False )

    @method_decorator(permission_required('retentions.view_retentionmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """
        retention_datatables = RetentionDatatable(request)
        retention_datatables.ID_TABLE = pk

        return JsonResponse(retention_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('retentions.view_retentionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class RetentionView
            :type self: RetentionView
            :param request: data of request
            :type request: request
        
        """

        retention_datatables = RetentionDatatable(request)
        retention_datatables.ID_TABLE = pk

        return retention_datatables.get_data()

    @method_decorator(permission_required('retentions.view_retentionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class RetentionView
            :type self: RetentionView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request, pk):

            retention_datatables = RetentionDatatable(request)
            retention_datatables.ID_TABLE = pk
            retention_datatables.download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request, pk]
        ).start()

        return JsonResponse(data={})

    @method_decorator(permission_required('retentions.view_retentionmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def add_retention_categories(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        categories = json.loads(request.data.get('categories'))
        retentions_categories = RetentionsTipifyCategoryModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.data.get('operator'))).all()
        for category in retentions_categories:
            if category.category.pk not in categories:
                category.delete()
        
        for new_category in categories:
            if len(retentions_categories.filter(category__pk=new_category, deleted=False)) == 0:
                RetentionsTipifyCategoryModel.objects.create(
                    creator=request.user, 
                    category=CategoryModel.objects.get(ID=new_category),
                    operator=OperatorModel.objects.get(pk=request.data.get('operator'))
                    )
        result = [instance.category.ID for instance in RetentionsTipifyCategoryModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.data.get('operator')))]
        return JsonResponse(result, safe=False)

    @method_decorator(permission_required('retentions.view_retentionmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def get_retention_categories(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        result = [instance.category.ID for instance in RetentionsTipifyCategoryModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.GET.get('operator')))]
        return JsonResponse(result, safe=False)

class RetentionTableView(BaseViewSet):

    """

        Class define ViewSet of TicketTablesModel.
    
    """

    queryset: BaseModel = RetentionsTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RetentionTableSerializer
    
    @method_decorator(permission_required('retentions.add_retentionstablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.change_retentionstablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.delete_retentionstablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.add_retentionstablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Creador":{
                    "type":"choices",
                    "name":"creator__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Estado":{
                    "type":"choices",
                    "name":"status",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "Abierto",
                            1: "Cerrado",
                        }
                    }
                },
                "Servicio":{
                    "type":"int",
                    "name":"service"
                },
                "Exitosa":{
                    "type":"bool",
                    "name":"succesfull",
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'creator': "Creador",
                'ID': "ID",
                'updated': "Última Actualización",
                'service': "Servicio",
                'slack_thread': "Hilo de Slack", 
                'succesfull': "Éxito",
                'agreements': "Acuerdos",
                "followup": "Seguimiento",
                "status": "Estado",
                "contact_date": "Hora de Contacto"
            }
        }

        return JsonResponse(data_struct, safe=True)

    @method_decorator(permission_required('retentions.add_retentionstablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketTableView
            :type self: TicketTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(RetentionTablesDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('retentions.add_retentionstablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketTableView
            :type self: TicketTableView
            :param request: data of request
            :type request: request
        
        """

        return RetentionTablesDatatable(request).get_data()

class RetentionChannelView(BaseViewSet):

    """

        Class define ViewSet of RetentionsChannelModel.
    
    """

    queryset: BaseModel = RetentionsChannelModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RetentionChannelSerializer
    
    @method_decorator(permission_required('retentions.add_retentionschannelmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.change_retentionschannelmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

class RetentionsTipifyCategoryView(BaseViewSet):

    """

        Class define ViewSet of RetentionsTipifyCategoryModel.
    
    """

    queryset: BaseModel = RetentionsTipifyCategoryModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RetentionsTipifyCategorySerializer
    
    @method_decorator(permission_required('retentions.add_retentionstipifycategorymodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.change_retentionstipifycategorymodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

class RetentionAgreementView(BaseViewSet):

    """

        Class define ViewSet of QAIndicatorModel.
    
    """

    queryset: BaseModel = RetentionAgreementModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = RetentionAgreementSerializer

    @method_decorator(permission_required('retentions.view_retentionagreementmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.view_retentionagreementmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.view_retentionagreementmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.view_retentionagreementmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('retentions.view_retentionagreementmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAIndicatorView
            :type self: QAIndicatorView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(RetentionAgreementDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('retentions.view_retentionagreementmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAIndicatorView
            :type self: QAIndicatorView
            :param request: data of request
            :type request: request
        
        """

        return RetentionAgreementDatatable(request).get_data()