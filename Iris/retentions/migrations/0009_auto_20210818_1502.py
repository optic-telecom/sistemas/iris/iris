# Generated by Django 3.2.2 on 2021-08-18 19:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('retentions', '0008_agent_permissions'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalretentionmodel',
            name='contact_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='retentionmodel',
            name='contact_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
