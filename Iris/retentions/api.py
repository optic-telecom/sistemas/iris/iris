from slack import WebClient
from common.models import BaseModel
from rest_framework.serializers import ValidationError
from .models import RetentionModel

def send_retention_reminder(*args, **kwargs):

    if 'kwargs' in kwargs.keys():

        instance_id: int = kwargs['kwargs']['instance_id']
        message: int = kwargs['kwargs']['message']

    else:

        instance_id: int = kwargs['instance_id']
        message: int = kwargs['message']

    try:

        retention: BaseModel = RetentionModel.objects.get(ID=instance_id)

    except:

        raise ValidationError({
            "Error retention": "Item doesnt exists"         
        })

    token: str = retention.operator.slack_token

    client = WebClient(token=token)

    message_block = {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": message
        }
    }

    result = client.chat_postMessage(
        token=token,
        channel=retention.slack_channel,
        blocks=message_block,
        thread_ts=retention.slack_thread,
        text=message
    )