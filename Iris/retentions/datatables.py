from typing import List, Dict, Any, Union
from functools import reduce
from datetime import datetime, timedelta
from django.contrib.auth.models import Group
from django.db.models import Q
from django.db.models.query import QuerySet
from dateutil.relativedelta import relativedelta
from common.dataTables import BaseDatatables
from common.utilitarian import get_ticket_SLA
from django.http import JsonResponse
from common.models import BaseModel
from .models import (RetentionAgreementModel, RetentionsTablesModel, RetentionModel)
from .serializers import RetentionSerializer

class RetentionTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]

    model: BaseModel = RetentionsTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class RetentionDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = [
        "ID", "created", "creator", "service", "contact_date"
    ]

    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "creator__pk":"choices",
        "service": "int",
        "status": "choices",
        "succesfull": "bool"
    }

    FIELDS_EXCEL: Dict[str, Dict[str, str]] = {
        'A': 'Número de ticket',
        'B': 'Fecha de creación',
        'C': 'Agente creador',
        'D': 'Ultima fecha de actualización',
        'E': 'Ultimo agente en actualizar',
        'F': 'Servicio',
        'H': 'Estado',
        'I': 'Exitosa',
        'J': 'Seguimientos',
        'K': 'Acuerdos',
        'L': 'Hora de Contacto',
    }

    model: BaseModel = RetentionModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            if search.isdigit():

                int_value: int = int(search)

                qs: QuerySet = qs.filter( Q(ID=int_value))

            else:

                qs: QuerySet = qs.filter( Q(agent__username__icontains=search) | Q(assigned_team__name__icontains=search))


        return qs

    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_retention: QuerySet = RetentionsTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_retention.filters

        data_retention: QuerySet = super().get_filtered_queryset()

        #Tickets created to request user
        if instance_tables_retention.create_me:

            data_retention: QuerySet = data_retention.filter(creator=self.request.user)

        #Tickets time interval
        if instance_tables_retention.last_time_type and instance_tables_retention.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_retention: QuerySet = data_retention.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_retention.last_time_type] * instance_tables_retention.last_time) )

        return data_retention

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        result: Dict[str, Union[str, int, float]] = RetentionSerializer(instance).data

        result["creator"]: str = instance.creator.username
        result["updater"]: str = instance.updater.username if instance.updater else ''
        result["status"]: str = instance.get_status_display()
        result["followup"]: str = ', '.join( list( map( lambda x: str(x), instance.followup) ) )
        result["agreements"]: str = ', '.join( list( map( lambda x: str(x), instance.agreements) ) )
        return result
        
    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_tables_retention: QuerySet = RetentionsTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "download":True
        }

        data_struct["fields"] = {
            "ID": "int",
            "created": "datetime",
            "updated": "datetime",
            "operator": "int",
            "creator": "str",
            "updater": "str",
            "service": "int",
            "status": "str",
            "succesfull": "bool",
            "agreements": "list",
            "followup": "list"
        }

        #Init column definition#
        ########################

        columns: Dict[str, Dict[str, Any]] = {
            'creator':{
                "sortable": True,
                "name":"Creador",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Creado",
                "width":100,
                "fixed":None
            },
            'ID':{
                "sortable": True,
                "name":"ID",
                "width":100,
                "fixed":None
            },   
            'updated':{
                "sortable": True,
                "name":"Última actualización",
                "width":100,
                "fixed":None
            },
            'service':{
                "sortable": True,
                "name":"Servicio",
                "width":100,
                "fixed":None
            },
            'contact_date':{
                "sortable": True,
                "name":"Hora de Contacto",
                "width":100,
                "fixed":None
            },
            'succesfull':{
                "sortable": False,
                "name":"Éxito",
                "width":100,
                "fixed":None
            },
            'agreements':{
                "sortable": False,
                "name":"Acuerdos",
                "width":100,
                "fixed":None
            },
            'followup':{
                "sortable": False,
                "name":"Seguimiento",
                "width":100,
                "fixed":None
            },
            'status':{
                "sortable": False,
                "name":"Estado",
                "width":100,
                "fixed":None
            }, 
            'slack_thread':{
                "sortable": False,
                "name":"Hilo de Slack",
                "width":100,
                "fixed":None
            },    
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}
        
        position: int = 0

        for _column in instance_tables_retention.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1

        data_struct_columns["Modificar"] = {
            "field": "custom_change",
            "sortable": False,
            "visible": True,
            "position": position,
            "width":100,
            "fixed":"right",
            "download": True,
        }

        data_struct_columns["Eliminar"] = {
            "field": "custom_delete",
            "sortable": False,
            "visible": True,
            "position": position+1,
            "width":100,
            "fixed":"right",
            "download": True,
        }

        data_struct["columns"] = data_struct_columns

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll": reduce(lambda x, y: x + y, [ column["width"] for column in  data_struct_columns.values()] ) + 100,
            "download": True
        }

        #End  column definition#
        ########################

        #Init filters definition#
        #########################

        filters: Dict[str, Dict[str, Any]] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Estado":{
                "type":"choices",
                "name":"status",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0: "Iniciado",
                        1: "Aceptado",
                        2: "Rechazado",
                        3: "En Proceso",
                        4: "Cerrado"
                    }
                }
            },
            "Servicio":{
                "type":"int",
                "name":"service"
            },
            "Exitosa":{
                "type":"bool",
                "name":"succesfull",
            },
        }

        filters_names : Dict[str, str] = {
            'created': ["Fecha de inicio", "Fecha de fin"],
            'customer_rut':["RUT"],
            'service':["Servicio"],
            'creator__pk':["Creador"],
            'status': ["Estado"],
            'succesfull': ["Éxito"]
        }

        for _filter in instance_tables_retention.filters:

            for _field in filters_names[_filter[0]]:

                del filters[_field]

        data_struct["filters"] = filters

        #End filters definition#
        ########################

        return data_struct

    def get_qs_to_data(self, qs: QuerySet) -> List[Dict[str, Any]]:

        """

            This function, process data

        """

        def get_retention_data(instance: RetentionModel) -> List[Any]:

            return [
                instance.ID,
                instance.created.strftime("%m/%d/%Y, %H:%M:%S"),
                instance.creator.username if instance.creator else '',
                instance.updated.strftime("%m/%d/%Y, %H:%M:%S"),
                instance.updater.username if instance.updater else '',
                instance.service,
                instance.get_status_display(),
                instance.succesfull,
                ', '.join( list( map( lambda x: str(x), instance.followup) ) ),
                ', '.join( list( map( lambda x: str(x), instance.agreements) ) ),
                instance.contact_date.strftime("%m/%d/%Y, %H:%M:%S") if instance.contact_date else '',
            ]

        return list( map( lambda x: x , map(get_retention_data, qs) ) )

class RetentionAgreementDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = RetentionAgreementModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search) )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "template": "str",

        }

        data_struct["columns"] = {

            "ID":{
                "field": "ID",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct
