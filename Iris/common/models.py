from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _
from simple_history.models import HistoricalRecords
from operators.models import OperatorModel

class BaseModel(models.Model):

    """

        Class define models or tables on DB.
    
    """

    operator = models.ForeignKey(
        OperatorModel,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
    )

    #Indicates unique ID of instance
    ID: int = models.AutoField(primary_key=True)
    #Indicates if instance is deleted
    deleted: bool = models.BooleanField(default=False)
    #Save historical changes of instance
    history = HistoricalRecords(inherit=True)
    #Indicates datetime was created instance
    created: datetime = models.DateTimeField(auto_now_add=True)
    #Indicates last datetime was update instance
    updated:datetime = models.DateTimeField(auto_now=True)
    #Indicates user who created instance
    creator: int = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
        related_name='%(app_label)s_%(class)s_creator'
    )
    #Indicates user who updated instance
    updater: int = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
        related_name='%(app_label)s_%(class)s_updater'
    )


    def delete(self, *args, **kwargs):

        """

            This funtion, overwrites delete method. Prevent delete instance and only set 'deleted' field to 'True'

        """

        self.deleted: bool = True
        self.save()

    def save(self, *args, **kwargs):

        self.clean()
        super().save( *args, **kwargs)

    class Meta:

        abstract: bool = True