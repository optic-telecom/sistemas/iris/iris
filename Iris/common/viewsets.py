from datetime import datetime
from typing import Any, Dict, List, Union

from requests import request

from django.contrib.auth.models import User
from django.core.cache import cache
from django.db.models import Model
from django.http import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response
from url_filter.integrations.drf import DjangoFilterBackend

from .models import BaseModel


class BaseViewSet(viewsets.ModelViewSet):

    """

        Class define base of all viewset.
    
    """

    filter_fields: Union[list, str]  = '__all__'
    history_ignore_fields: List[str] = []
    filter_backends: List[Any] = [DjangoFilterBackend,OrderingFilter]
    ordering_fields: Union[list, str] = '__all__'

    @swagger_auto_schema(

        operation_id=f'History changes',
        operation_description="""

            This endpoint return history changes. 

            \n This endpoint returns a dictionary. The key 'first' return the data with which the object was created.
            \n The key 'changes' returns a list of dictionaries, this return the fields that have changed, the user  who made them and the date
            \n **Example:**

                {
                    "first": 
                        {
                            "ID": 22321,
                            "deleted": false,
                            "created": "2020-03-05T02:39:10.431Z",
                            "services": "",
                            "liveagent": "121451",
                            "customer_type": "Cliente",
                            "channel": "CORREO",
                            "type": "IN",
                            "status": "ABIERTO",
                            "urgency": "CRITICO",
                            "city": "SANTIAGO",
                            "management": "INSTALACI\u00d3N",
                            "technician": 1, 
                            "discount_reason": "RETENCION",
                            "discount_responsable_area": "TECNICA",
                            "discount_porcentage": 0,
                            "creator": 1,
                            "updater": 1,
                            "operator": 1,
                            "category": 86,
                            "second_category": null,
                            "parent": null,
                            "agent": 1,
                        }, 
                    "changes": 
                        [
                            {
                                "urgency": 
                                {
                                    "old_value": "CRITICO",
                                    "new_value": "RECLAMO"
                                }, 
                                "updater": 
                                {
                                    "old_value": 1,
                                    "new_value": 1
                                },
                                "updated": 
                                {
                                    "old_value": "2020-03-05T15:16:56.868Z",
                                    "new_value": "2020-03-05T15:16:56.868Z"
                                }
                            }
                        ]
                }

        """,
    )
    @action(detail=True, methods=['get'])
    def history(self, request, pk: int = None) -> JsonResponse:

        """
            
            This function returns Historical changes of instance model

            :param self: Instance of ViewSet
            :type self: ViewSet Class extend of BaseViewSet

            :returns: List of str, this contains fields of Model defined in Viewset
            :rtype: list
        
        """

        def get_fields(self: BaseViewSet) -> List[str]:

            """
            
                This function returns fields of defined Model in Viewset

                :param self: Instance of ViewSet
                :type self: ViewSet Class extend of BaseViewSet

                :returns: List of str, this contains fields of Model defined in Viewset
                :rtype: list
            
            """

            return [ field.name for field in type(self.queryset[0])._meta.fields]

        def get_first(self: BaseViewSet, instance ) -> Dict[str, Any]:

            """
            
                This function returns fields of defined Model in Viewset

                :param self: Instance of ViewSet
                :type self: ViewSet Class extend of BaseViewSet

                :returns: Dict, this contains first value of instance
                :rtype: dict
            
            """

            def replace_id_pk(value: str) -> str:

                """
            
                    This function returns the 'value' field without '_id' and '_pk' substring

                    :param self: Instance of ViewSet
                    :type self: ViewSet Class extend of BaseViewSet

                    :returns: List of str, this contains fields of Model defined in Viewset
                    :rtype: list
            
                """

                return value.replace('_id','').replace('_pk','')

            #Get fileds of Model defined in ViewSet
            fields: List[str] = get_fields(self)

            #Return dict, contains initial values of instance
            return { replace_id_pk(key):value for key, value in instance.__dict__.items() if (replace_id_pk(key) in fields) and (not (key in self.history_ignore_fields)) }

        def is_related(self: BaseViewSet, field) -> int:

            """
            
                This function returns id of related object to instance of Model

                :param self: Instance of ViewSet
                :type self: ViewSet Class extend of BaseViewSet
                :param field: Field in Model
                :type field: All posible fields in Django Model

                :returns: Int, pk or ID of related object
                :rtype: int
            
            """

            #Get list related models
            related_models: Model = [ related.model for related in type(self.queryset[0])._meta.related_objects] + [User]
            result: int = field

            #If 'field' is 'related_models' search primary key
            if field.__class__ in related_models:

                if hasattr(field, 'ID'):

                    result = field.ID

                elif hasattr(field, 'pk'):

                    result = field.pk

            return result

        #Get instance of model to get history
        instance_model: BaseModel = self.queryset.get(ID=pk)
        #Get history of instance
        instance_model_history: BaseModel = instance_model.history.all()
        #Get first value of instance
        first: Dict[str, Any] = get_first(self, instance_model_history[len(instance_model_history) - 1])

        changes: List[Dict[str, Any]] = []

        for index, change in enumerate(instance_model_history[0: len(instance_model_history) - 1], start=0):

            #Get instance of change
            change_aux: BaseModel = change
            #Get fields that have changed between 'change' and next change
            fields_changes: Dict[str, Any] = change.diff_against(instance_model_history[index + 1])
            dict_changes: Dict[str, Dict[str, Any]] = {}
            
            for change in fields_changes.changes:

                #Get field that have changed
                field = change.field

                #Exclude fields changes
                if not (field in self.history_ignore_fields):

                    #Save Old and New value in fields
                    dict_changes[field]: Dict[str, Any] =   {
                                                'old_value': is_related(self, change.old),
                                                'new_value': is_related(self, change.new),
                                            }

            #Save user thah have changed instance
            dict_changes['updater']: Dict[str, User] =   {
                                        'old_value': is_related(self, change_aux.updater),
                                        'new_value': is_related(self, change_aux.updater),
                                    }

            #Save datetime that have changed instance
            dict_changes['updated']: Dict[str, datetime] =   {
                                        'old_value': is_related(self, change_aux.history_date),
                                        'new_value': is_related(self, change_aux.history_date),
                                    }
            #Save changes to list of changes
            changes.append(dict_changes)

        result: Dict[str, Any] = {
            'first': first,
            'changes': changes,
        }

        return JsonResponse( result, safe=False)
