from datetime import datetime
from typing import Any, Dict

from django.contrib.auth.models import User
from rest_framework import serializers

from .mixins import QueryFieldsMixin
from .models import BaseModel


class BaseSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    """

        Class define base of all serializers.
    
    """

    #Define fields (deleted, created, updated) as read-only for all serializers
    deleted: bool = serializers.BooleanField(read_only=True)
    created: datetime = serializers.DateTimeField(read_only=True)
    updated: datetime = serializers.DateTimeField(read_only=True)


    def destroy(self, request, *args, **kwargs):

        """

            This funtion, overwrites destroy method. Prevent delete instance and only set 'deleted' field to 'True'

        """

        instance = self.get_object()
        instance.deleted: bool = True
        instance.save()

    def create(self, validated_data: Dict[str, Any]) -> BaseModel:

        """

            This funtion, overwrites create method. Set fields 'creator' and 'updater' with user of request

        """
        
        validated_data['creator']: User = self.context['request'].user
        validated_data['updater']: User = self.context['request'].user

        return super().create(validated_data)

    def update(self, instance, validated_data: Dict[str, Any]) -> BaseModel:

        """

            This funtion, overwrites update method. Set field 'updater' with user of request and field 'updated' with current datetime

        """

        validated_data['updater']: User = self.context['request'].user
        validated_data['updated']: datetime = datetime.now()
        
        return super().update(instance, validated_data)
