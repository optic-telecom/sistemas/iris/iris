from datetime import datetime, timezone
from pdb import set_trace
from typing import Callable, Dict, List, Any

import requests
from dateutil.relativedelta import relativedelta
from django.db.models.query import QuerySet
from django.conf import settings
from django.urls import reverse
from dynamic_preferences.registries import global_preferences_registry


def communications_url(viewname: str) -> str:

    """
    
        This function return the full link of 'viewname' view of the comunnications app

        :param viewname: This param is the name of view
  
        :returns: Full link of view
    
    """

    global_preferences = global_preferences_registry.manager()
    return global_preferences['general__iris_backend_domain'] + reverse(viewname=f'communications:{viewname}')[1:]

def matrix_url(viewname: str) -> str:

    """
    
        This function return the full link of 'viewname' view of the matrix app

        :param viewname: This param is the name of view

        :returns: Full link of view
    
    """

    global_preferences = global_preferences_registry.manager()
    return global_preferences['general__iris_backend_domain'] + f'api/v1/matrix/{viewname}/'

def users_url(viewname: str) -> str:

    """
    
        This function return the full link of 'viewname' view of the users app

        :param viewname: This param is the name of view

        :returns: Full link of view
    
    """

    global_preferences = global_preferences_registry.manager()
    return global_preferences['general__iris_backend_domain'] + reverse(viewname=f'users:{viewname}')[1:]

def iris_url(app: str, viewname: str) -> str:

    global_preferences = global_preferences_registry.manager()
    return global_preferences['general__iris_backend_domain'] + reverse(viewname=f'{app}:{viewname}')[1:]

def get_token() -> str:

    """
    
        This function return the session token of user

        :returns: Session Token
        :rtype: str
    
    """

    global_preferences = global_preferences_registry.manager()

    token: str = global_preferences['users__API_token']

    return f'{token}'

def next_datetime(type_next: str, _datetime: datetime) -> datetime:

    """
    
        This function return next datetime

        :param type_next: This param indicate type of next datetime
        :param _datetime: This param is indicate datetime
    
    """

    def next_minute(_datetime: datetime) -> datetime:

        """
    
            This function return next datetime

            :param datetime: This param indicate datetime to change
            :returns: _datetime more one minute
    
        """
        
        return _datetime + relativedelta(minutes=+1)

    def next_hour(_datetime: datetime) -> datetime:

        """
    
            This function return next datetime

            :param datetime: This param indicate datetime to change
            :returns: _datetime more one hour
    
        """
        
        return _datetime + relativedelta(hours=+1)

    def next_day(_datetime: datetime) -> datetime:

        """
    
            This function return next datetime

            :param datetime: This param indicate datetime to change
            :returns: _datetime more one day
    
        """
        
        return _datetime + relativedelta(days=+1)

    def next_weekly(_datetime: datetime) -> datetime:

        """
    
            This function return next datetime

            :param datetime: This param indicate datetime to change
            :returns: _datetime more one week
    
        """
        
        return _datetime + relativedelta(weeks=+1)

    def next_monthly(_datetime: datetime) -> datetime:

        """
    
            This function return next datetime

            :param datetime: This param indicate datetime to change
            :returns: _datetime more one month
    
        """
        
        return _datetime + relativedelta(months=+1)

    def next_quarterly(_datetime: datetime) -> datetime:

        """
    
            This function return next datetime

            :param datetime: This param indicate datetime to change
            :returns: _datetime more one quarter
    
        """
        
        return _datetime + relativedelta(months=+3)

    def next_yearly(_datetime: datetime) -> datetime:

        """
    
            This function return next datetime

            :param datetime: This param indicate datetime to change
            :returns: _datetime more one year
    
        """
        
        return _datetime + relativedelta(years=+1)

    def current_datetime(_datetime: datetime) -> datetime:

        """
    
            This function return next datetime

            :param datetime: This param indicate datetime to change
            :returns: _datetime
    
        """

        return _datetime

    types_repeats: Dict[str, Callable] = {
        'O':current_datetime,
        'I':next_minute,
        'H':next_hour,
        'D':next_day,
        'W':next_weekly,
        'M':next_monthly,
        'Q':next_quarterly,
        'Y':next_yearly,
    }

    return types_repeats[ type_next ]( _datetime )

def get_ticket_SLA(instance_model, closed_status: List[int], status_field: str) -> float:

        """
        
            This funtion calc open time of ticket

            :param self: Instance of Class TypifySerializer
            :type self: TypifySerializer
            :param instance_model: instance of model TypifyModel
            :type instance_model: TypifyModel

            :returns: Returns float of open time
            :rtype: float
        
        """

        def next_opened(changes : List[dict], closed_status: List[int]) -> List[dict]:

            """
        
                This funtion search next closed change

                :param self: List of changes
                :type self: list

                :returns: Returns list of next closed change
                :rtype: list
        
            """

            while ( len( changes ) and changes[0]['value'] in closed_status ):

                changes = changes[1:]

            return changes

        def next_closed(changes: List[dict], closed_status: List[int]) -> List[dict]:

            """
        
                This funtion search next opened change

                :param self: List of changes
                :type self: list

                :returns: Returns list of next opened change
                :rtype: list
        
            """

            while ( len( changes ) and changes[0]['value'] not in closed_status ):

                changes = changes[1:]

            return changes

        def all_changes(instance_model_history: QuerySet, closed_status: List[int], status_field: str) -> List[dict]:

            """
        
                This funtion get all changes

                :param instance_model_history: List of changes
                :type instance_model_history: QuerySet

                :returns: Returns list of changes
                :rtype: list
        
            """

            result: List[dict] = []
            next_value: Any = None
            next_datetime: datetime = None

            for index, change in enumerate(instance_model_history[0: len(instance_model_history) - 1], start=0):

                #Get changes
                fields_changes = change.diff_against(instance_model_history[index + 1])

                for change in fields_changes.changes:

                    #Save changes of 'status' field
                    if change.field == status_field and change.new != change.old:
                        
                        #Save old value
                        result.append(
                            {
                                'value': change.old,
                                'datetime': instance_model_history[index].history_date,
                            }
                        )

                        next_value = change.new
                        next_datetime = instance_model_history[index + 1].history_date

            if ( next_value ):

                #Save latest value
                result.append(
                    {
                        'value': next_value,
                        'datetime': next_datetime,
                    }
                )

            return result
                
        total: float = 0
        instance_model_history: QuerySet = instance_model.history.all().reverse()
        changes: List[dict] = all_changes(instance_model_history, closed_status, status_field)


        #Check if have been changes 'status' field
        if len( changes ) :
            
            #While there are time interval to count
            while len( changes ):

                #Next time open
                changes = next_opened(changes, closed_status)

                #While there are time opened interval to count
                if changes:

                    #Time it was open
                    datetime_opened = changes[0]['datetime']

                    #Next time close
                    changes = next_closed(changes, closed_status)

                    #While there are time closed interval to count
                    if changes:

                        #Time it was close
                        datetime_closed = changes[0]['datetime']
                        #Time difference
                        total += (datetime_closed - datetime_opened).total_seconds()

                    else:

                        #Time difference
                        total += (datetime.now(timezone.utc) - datetime_opened).total_seconds()
                       
        else:

            #Instance was not created closed
            if getattr(instance_model, status_field) not in closed_status:

                #Time difference
                total = (datetime.now(timezone.utc) - instance_model.created).total_seconds()

        return total

def get_ticket_status_SLA(obj, ticket_sla) -> str:

    """
    
        This funtion get sla status

        :param self: TypifySerializer instance
        :type self: TypifySerializer
        :param obj: TypifyModel instance
        :type obj: TypifyModel

        :returns: Returns float, opened time
        :rtype: float
    
    """
    
    #SLA time to seconds
    try:
        sla: int = obj.category.sla * 3600
    except:
        try:
            sla: int = obj.category.first().sla * 3600
        except:
            pass
    #Get opend time in seconds
    seconds: int = ticket_sla

    if sla < seconds:

        result: str = 'danger'

    else:

        if  seconds < sla / 2 :

            result: str = 'good'

        else:

            result: str = 'alert'

    return result
