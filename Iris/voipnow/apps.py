from django.apps import AppConfig


class VoipnowConfig(AppConfig):
    name = 'voipnow'
