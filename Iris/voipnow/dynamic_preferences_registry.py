from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference

general = Section('voipnow')


@global_preferences_registry.register
class Token(StringPreference):

    section = general
    name: str = 'token'
    default: int = ""
    required: bool = True

@global_preferences_registry.register
class IDClient(StringPreference):

    section = general
    name: str = 'id_client'
    default: int = ""
    required: bool = True

@global_preferences_registry.register
class Secret(StringPreference):

    section = general
    name: str = 'secret'
    default: int = ""
    required: bool = True

@global_preferences_registry.register
class Domain(StringPreference):

    section = general
    name: str = 'domain'
    default: int = "https://190.113.247.12/"
    required: bool = True