import json

import requests
from django.http import JsonResponse
from dynamic_preferences.registries import global_preferences_registry


def proxy(request) -> JsonResponse:

    """

        This function return information of voipnow request

        :param request: Values of user request
        :type request: request

        :returns: Response data user
        :rtype: JsonResponse
    
    """

    global_preferences: Dict[str, any] = global_preferences_registry.manager()

    origin_url: str = request.build_absolute_uri().split('/api/v1/voipnow/')
    url: str = global_preferences['voipnow__domain'] + origin_url[1]

    headers: Dict[str, str] = {
        'AUTHORIZATION': global_preferences['voipnow__token'],
        'content-type': 'application/json'
    }

    error_access: Dict[str, Dict[str, str]] = {
        "error": {
            "code": "access_denied",
            "message": "No está autorizado a utilizar esta solicitud."
        }
    }

    if request.method == 'GET':

        response = requests.get(
            url=url,
            headers=headers,
            verify=False
        )

        if response.json() == error_access:

            url_get_token: str = global_preferences['voipnow__domain'] + "oauth/token.php"
            header_get_token: Dict[str, str] = {
                'content-type': 'application/x-www-form-urlencoded'
            }

            data: Dict[str, str] = {
                'grant_type':'client_credentials',
                'client_id':global_preferences['voipnow__id_client'],
                'client_secret':global_preferences['voipnow__secret']
            }

            response = requests.post(
                url=url_get_token,
                headers=header_get_token,
                verify=False,
                data=data
            )

            token: str = response.json()["access_token"]
            global_preferences['voipnow__token'] = f"Bearer {token}"

            headers['AUTHORIZATION']: str = global_preferences['voipnow__token']

            response = requests.get(
                url=url,
                headers=headers,
                verify=False
            )

        status_code: int = response.status_code
        result: Dict[str, Any] = response.json()
        result: JsonResponse = JsonResponse(data=result, safe=False, status=status_code)

    else:

        result: JsonResponse = JsonResponse(safe=False, status=500, data={'Invalid method':'Only get method'})

    return result
