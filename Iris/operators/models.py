from typing import Any, Callable, Dict, List, Set, Tuple, Union
from django.db import models
from rest_framework.serializers import ValidationError
from django.contrib.postgres.fields import JSONField
from phonenumber_field.modelfields import PhoneNumberField
# from common.models import BaseModel

class CompanyModel(models.Model):

    """

        Class define model Company.
    
    """
    
    name : str = models.CharField(max_length=100, blank=False)
    ID: int = models.AutoField(primary_key=True)

class CommunicationsSettingsModel(models.Model):

    """

        Class define model Company.
    
    """

    def clean(self):

        if self.kind == 0 and self.extra_data:

            try:
                namespace = self.extra_data.get("namespace")
                whatsapp_channel = self.extra_data.get("whatsapp_channel")

                if namespace == None and whatsapp_channel == None:
                    raise ValidationError({
                        'Error extra data': f"Must provide a namespace and a whatsapp channel"
                    })

            except:
                raise ValidationError({
                    'Error extra data': f"Must be a valid dict"
                })

    company = models.ForeignKey(
        CompanyModel,
        on_delete=models.PROTECT,
        blank=False
    )
    token: str = models.TextField(null=False, default='')
    PLATFORM_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'MessageBird'),
        (1, 'Twilio'),
    )
    kind: int = models.PositiveIntegerField(default=0, choices=PLATFORM_CHOICES)

    extra_data: Dict[Any,Any] = JSONField(default=dict)

    phone_sender: str = PhoneNumberField(null=True, blank=True)
    email_sender: str = models.CharField(max_length=100, blank=False)

class OperatorModel(models.Model):

    """

        Class define model Operator.
    
    """
    
    name : str = models.CharField(max_length=100, blank=False)
    ID: int = models.AutoField(primary_key=True)
    company = models.ForeignKey(
        CompanyModel,
        on_delete=models.PROTECT,
        blank=False
    )
    slack_token: str = models.TextField(null=True, default='')
    domain: str = models.CharField(max_length=100, null=True)