from typing import Any, Dict, List

from django.contrib.auth.models import User, Group
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.viewsets import BaseViewSet
from django.http import JsonResponse, HttpResponseForbidden
from rest_framework.decorators import action
from rest_framework.serializers import ValidationError
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from dynamic_preferences.registries import global_preferences_registry

from .models import ( CommunicationsSettingsModel, OperatorModel, CompanyModel)
from .serializers import ( CommunicationsSettingsSerializer, OperatorSerializer, CompanySerializer)
from .documentation import DocumentationOperator, DocumentationCompany

class OperatorView(BaseViewSet):

    """

        Class define ViewSet of OperatorModel.
    
    """

    queryset: BaseModel = OperatorModel.objects.all()
    serializer_class: BaseSerializer = OperatorSerializer
    documentation_class = DocumentationOperator()

    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('operators.add_operatormodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('operators.change_operatormodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('operators.delete_operatormodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @documentation_class.documentation_get_companies()
    @action(detail=False, methods=['get'])
    def company_list(self, request) -> JsonResponse:

        company_list = CompanyModel.objects.all()
        data = []

        for company in company_list:
            operator_data = []
            operator_list = OperatorModel.objects.filter(company=company)
            for operator in operator_list:
                operator_data.append({'ID': operator.ID, 'name': operator.name})
            
            data.append({'name': company.name, 'ID': company.ID, 'operators': operator_data})

        return JsonResponse(data, safe=False)

class CompanyView(BaseViewSet):

    """

        Class define ViewSet of OperatorModel.
    
    """

    queryset: BaseModel = CompanyModel.objects.all()
    serializer_class: BaseSerializer = CompanySerializer
    documentation_class = DocumentationCompany()

    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('operators.add_companymodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('operators.change_companymodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('operators.delete_companymodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

class CommunicationsSettingsView(BaseViewSet):

    """

        Class define ViewSet of CommunicationsSettingsModel.
    
    """

    queryset: BaseModel = CommunicationsSettingsModel.objects.all()
    serializer_class: BaseSerializer = CommunicationsSettingsSerializer

    @method_decorator(permission_required('operators.add_communicationssettingsmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('operators.change_communicationssettingsmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('operators.delete_communicationssettingsmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)