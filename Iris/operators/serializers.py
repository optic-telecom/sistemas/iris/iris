from typing import Any, Dict, List, Union

from common.models import BaseModel
from common.serializers import BaseSerializer
from django.db.models.query import QuerySet
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import (SerializerMethodField,ValidationError)
from rest_framework import serializers

from .models import (CommunicationsSettingsModel, OperatorModel, CompanyModel)

class CompanySerializer(BaseSerializer):

    """

        Class define serializer of CompanyView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = CompanyModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            }
        }

class CommunicationsSettingsSerializer(BaseSerializer):

    """

        Class define serializer of CommunicationsSettingsView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = CommunicationsSettingsModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            }
        }

class OperatorSerializer(BaseSerializer):

    """

        Class define serializer of OperatorView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = OperatorModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {}