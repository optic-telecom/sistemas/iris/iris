# Generated by Django 3.2.2 on 2021-05-28 13:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('operators', '0004_operatormodel_slack_token'),
    ]

    operations = [
        migrations.AddField(
            model_name='operatormodel',
            name='domain',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
