# Generated by Django 3.2.2 on 2021-05-28 13:04

from django.db import migrations, models

def add_domains(apps, scheme_editor):

    OperatorModel = apps.get_model('operators', 'OperatorModel')

    operator_list = {"Optic": "optic.cl", "Bandaancha": "bandaancha.cl", "Multifiber": "multifiber.cl"}

    for operator_name in operator_list.keys():
        try:
            operator = OperatorModel.objects.filter(name=operator_name).first()
            operator.domain = operator_list[operator_name]
            operator.save()
        except: 
            pass

def remove_domains(apps, scheme_editor):

    OperatorModel = apps.get_model('operators', 'OperatorModel')

    operator_list = {"Optic": "optic.cl", "Bandaancha": "bandaancha.cl", "Multifiber": "multifiber.cl"}

    for operator_name in operator_list.keys():
        try:
            operator = OperatorModel.objects.filter(name=operator_name).first()
            operator.domain = None
            operator.save()
        except:
            pass

class Migration(migrations.Migration):

    dependencies = [
        ('operators', '0005_operatormodel_domain'),
    ]

    operations = [
        migrations.RunPython(add_domains, remove_domains)
    ]
