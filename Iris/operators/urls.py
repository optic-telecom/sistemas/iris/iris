from rest_framework.routers import DefaultRouter

from .views import (CommunicationsSettingsView, OperatorView, CompanyView)

router = DefaultRouter()

router.register(r'operators', OperatorView, basename='operators')
router.register(r'company', CompanyView, basename='company')
router.register(r'communication_settings', CommunicationsSettingsView, basename='communication_settings')

urlpatterns = router.urls
