from typing import List

from django.contrib import admin

from .models import OperatorModel, CompanyModel


class OperatorAdmin(admin.ModelAdmin):

    fields: List[str] = ["name","company","slack_token"]
    list_display: List[str] = ["ID", "name"]
    search_fields: List[str] = ["name"]
    ordering: List[str] = ["name"]

class CompanyAdmin(admin.ModelAdmin):

    fields: List[str] = ["name"]
    list_display: List[str] = ["ID", "name"]
    search_fields: List[str] = ["name"]
    ordering: List[str] = ["name"]


admin.site.register(OperatorModel,OperatorAdmin)
admin.site.register(CompanyModel,CompanyAdmin)