from rest_framework.routers import DefaultRouter

from .views import (FiannceEmailView, FinanceProblemsView, FinanceEscalationSlackMessageView, FinanceEscalationTablesView,
                    FinanceEscalationPhotoView, FinanceSolutionView, FinanceEscalationView,
                    FinanceTipifyCategoryView)

router = DefaultRouter()

router.register(r'problems', FinanceProblemsView, basename='problems')
router.register(r'escalation_message', FinanceEscalationSlackMessageView, basename='escalation_message')
router.register(r'escalation_tables', FinanceEscalationTablesView, basename='escalation_tables')
router.register(r'escalation', FinanceEscalationView, basename='escalation')
router.register(r'solution', FinanceSolutionView, basename='solution')
router.register(r'escalation_photo', FinanceEscalationPhotoView, basename='escalation_photo')
router.register(r'escalation_category', FinanceTipifyCategoryView, basename='escalation_category')
router.register(r'email', FiannceEmailView, basename='email')

urlpatterns = router.urls
