from datetime import datetime
from typing import Any, Callable, Dict, List, Set, Tuple, Union
import requests
from communications.models import ConversationModel
from communications.models import ChatModel
from tickets.api import get_typify
from common.models import BaseModel
from common.utilitarian import (communications_url, get_token, iris_url,
                                matrix_url)
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import gettext_lazy as _
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import ValidationError
from slack import WebClient
from tickets.models import CategoryModel

class FinanceProblemsModel(BaseModel):

    """

        Class define model Problems.
    
    """

    def clean(self):

        if self.classification == 1:

            self.parent = None

        elif not self.parent:

            raise ValidationError({
                "error parent":"Required parent"
            })

        elif self.parent.classification + 1 != self.classification:

            raise ValidationError({
                "error parent":"Incorrect parent"
            })

    name: str = models.TextField(blank=False, null=False)

    ORIGIN_CHOICES = (
        (1, 'Tipo'),
        (2, 'Categoria'),
        (3, 'Subcategoria'),
    )
    classification: int = models.PositiveSmallIntegerField(choices=ORIGIN_CHOICES)

    parent = models.ForeignKey(
        'self',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    sla: int = models.PositiveIntegerField(default=1)

class FinanceEscalationModel(BaseModel):

    """

        Class define model typify.
    
    """

    def validation_typify(self, value: int):
        
        if isinstance( value, int):

            response = get_typify(value)

            if not response:

                raise ValidationError({
                    "error typify":"Don't exist"
                })

            try:
                escalation: FinanceEscalationModel = FinanceEscalationModel.objects.get(typify=value)
                if escalation != None:
                    raise ValidationError({
                    "error typify":"Already has an escalation ticket assigned"
                    })
            except:
                pass

    agent = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    whatsapp_conversation = models.ForeignKey(
        ConversationModel,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="whatsapp_conversation"
    )

    email_conversation = models.ForeignKey(
        ConversationModel,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="email_conversation"
    )

    typify: str = models.PositiveIntegerField(validators=[validation_typify], null=True)
    autoclose_typify = models.BooleanField(default=False)
    rut: List[int] = JSONField(default=list, null=True)
    services: List[int] = JSONField(default=list)

    STATUS_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'ABIERTO'),
        (1, 'EN SAC'),
        (2, 'CERRADO'),
        (3, 'CANCELADO')
    )

    status: int = models.PositiveIntegerField(choices=STATUS_CHOICES, default=0)

    problems: List[int] = models.ManyToManyField("FinanceProblemsModel", blank=True)
    solutions: List[int] = models.ManyToManyField("FinanceSolutionModel", blank=True)

    slack_thread: Dict[str, str] = JSONField(default=dict)
    media_urls: Dict[Any, Any] = JSONField(default=list)

class FinanceSolutionModel(BaseModel):

    name: str = models.CharField(max_length=50, null=False, blank=False)
    description: str = models.TextField()

class FinanceEscalationSlackMessageModel(BaseModel):

    def clean(self):
        
        FinanceEscalationSlackMessageModel.validate_slack_channel(self.channel, self.operator)

    def validate_slack_channel(value: int, operator):

        """

            This function, validate slack_channel field of EscalationSlackMessageModel

        """

        global_preferences: Dict[str] = global_preferences_registry.manager()

        token: str = operator.slack_token

        client = WebClient(token=token)
        exists: bool = False
        search: bool = True
        cursor: str = ''

        while search:
            response = client.conversations_list(token=token, cursor=cursor)
            if response["ok"]:
                for channel in response["channels"]:
                        if channel["id"] == value:
                            exists = True
                            pass
                        pass
                cursor = response.get("response_metadata",{}).get("next_cursor", '')
                search = True if cursor != '' else False
            else:
                raise ValidationError({
                    'Slack connection': "Unable to connect to Slack"
                })

        if not exists:
            raise ValidationError({
                'Error channel ID': "Channel not found"
            })

    name: str = models.CharField(max_length=50)

    channel: int = models.CharField(max_length=15, null=True)

class FinanceEscalationTablesModel(BaseModel):

    def clean(self):

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "creator__pk":"choices",
                    "agent__pk":"choices",
                    "typify":"int",
                    "customer":"str",
                    "status":"choices",
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results

        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'creator', 'updater', 'created', 'updated', 'agent', 'services', 
                                            'status', 'rut', 'typify',   
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class FinanceEscalationPhotoModel(BaseModel):
    
    """

        Class define model of EscalationPhoto
    
    """

    image = models.ImageField(upload_to='escalation_finance')
    # escalation = models.ForeignKey(
    #     FinanceEscalationModel,
    #     null=False,
    #     blank=False,
    #     on_delete=models.PROTECT
    # )

class FinanceTipifyCategoryModel(BaseModel):
    
    """

        Class define model of Retentions Channel.
    
    """
    category = models.ForeignKey(
        CategoryModel,
        null=False,
        blank=False,
        on_delete=models.CASCADE
    )

class FinanceEmailModel(BaseModel):

    """

        Class define model Company.
    
    """

    email: str = models.TextField(null=False, default='')