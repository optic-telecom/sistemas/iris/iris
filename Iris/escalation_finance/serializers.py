from datetime import datetime, timezone, timedelta
from typing import Any, Dict, List, Union
import random 
import string 

from django_q.models import Schedule
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import get_ticket_SLA
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import (BooleanField, SerializerMethodField,
                                        ValidationError)
from slack import WebClient
from rest_framework import serializers
from tickets.models import TypifyModel
from users.models import UserSlackIDModel
from .models import (FinanceEmailModel, FinanceProblemsModel,FinanceEscalationSlackMessageModel,
                    FinanceEscalationTablesModel,FinanceEscalationPhotoModel,
                    FinanceSolutionModel, FinanceEscalationModel, FinanceTipifyCategoryModel)


class FinanceEscalationProblemsSerializer(BaseSerializer):

    """

        Class define serializer of ProblemsView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = FinanceProblemsModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'tests': {
                'required': False
            },
            'problems': {
                'required': False
            },
            'solutions': {
                'required': False
            },
        }

    def update(self, instance, validated_data):

        def delete_item(item: str, value: Dict [str, Any]) -> Dict [str, Any]:

            if item in value:

                del value[item]

            return value

        return super().update(instance, validated_data)

class FinanceEscalationSlackMessageSerializer(BaseSerializer):

    """

        Class define serializer of EscalationSlackMessageView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = FinanceEscalationSlackMessageModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class FinanceEscalationTablesSerializer(BaseSerializer):

    class Meta:

        #Class model of serializer
        model: BaseModel = FinanceEscalationTablesModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class FinanceSolutionSerializer(BaseSerializer):

    class Meta:

        #Class model of serializer
        model: BaseModel = FinanceSolutionModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'tests': {
                'required': False
            },
        }

class FinanceEscalationSerializer(BaseSerializer):

    """

        Class define serializer of EscalationView.
    
    """
    #Method get_SLA
    SLA = serializers.SerializerMethodField()

    def get_SLA(self, instance_model : FinanceEscalationModel ) -> float:
        
        closed_status_list = [2,3]
        return get_ticket_SLA(instance_model, closed_status_list, 'status')

    def slack_message(self, instance_model: FinanceEscalationModel):

        def send_message(channel: str, instance_model: FinanceEscalationModel):

            def search_value(field: str, instance_model : FinanceEscalationModel) -> Any:

                _callable: Dict[str, callable()] = {
                    'services': lambda instance: ','.join( list( map( lambda service: str(service), instance.services ) ) ), 
                    'rut': lambda instance: instance.rut, 
                    'typify': lambda instance: instance.typify,
                }

                return _callable[field](instance_model)

            fields = ["rut","services","typify"]
            data: Dict[str, Any] = {}
            for field in fields:
                if field == "typify" and instance_model.typify == None:
                    continue
                data[field] = search_value(field, instance_model)

            user_uid_query: str = UserSlackIDModel.objects.filter(user=instance_model.creator,operator=instance_model.operator)
            user_uid = "<@" + str(user_uid_query[0].uid) + ">" if len(user_uid_query) > 0 else ''
            client = WebClient(token=instance_model.operator.slack_token)

            labels: Dict[str,str] = {
                "typify": f"ID tipificación: {data.get('typify')} \n", 
                "services": f"Servicios: {data.get('services')}\n", 
                "rut": f"Rut: {data.get('rut')}\n", 
                "escalation_level": f"Nivel de escalamiento: {data.get('escalation_level')}\n", 
            }           

            items: str = ""
            for item in data:
                items = items + labels[item]
           
            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Ticket Escalamiento"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":pencil:*{instance_model.creator.username}* {user_uid} ha creado un nuevo ticket de escalamiento de finanzas. ID: *{instance_model.pk}*"
                    }
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": items
                    }
                },
                {
                    "type": "divider"
                }
            ]

            response = client.chat_postMessage(
                channel=channel,
                blocks=blocks,
                text=f"{instance_model.creator.username} {user_uid} ha creado un nuevo ticket de escalamiento de finanzas. ID: {instance_model.pk}"
            )

            return response.data['ts']

        threads: Dict[str, str] = {}

        try:

            global_preferences = global_preferences_registry.manager()
            channel = FinanceEscalationSlackMessageModel.get(operator=instance_model.operator).channel
            
            if channel != '' and channel != None:
                thread = send_message(channel, instance_model)
                threads[thread] = channel
        except:
            pass
        instance_model.slack_thread = threads
        instance_model.save()

    def create(self, validated_data: Dict[str, Any]) -> FinanceEscalationModel:

        """
        
            This funtion validate and create instance

            :param self: EscalationSerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        result = super().create(validated_data)
        self.slack_message(result)
        return result

    def update(self, instance, validated_data: Dict[str, Any]) -> FinanceEscalationModel:

        """
        
            This funtion validate and update instance

            :param self: EscalationSerializer instance
            :type self: EscalationSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        def slack_message(instance: FinanceEscalationModel, validated_data: Dict[str,Any]):


            def send_message(thread: str, channel: str, validated_data: Dict[str,Any], instance: FinanceEscalationModel):

                def search_value(field: str, validated_data : Dict[str,Any]) -> Any:

                    _callable: Dict[str, callable()] = {
                        'agent': lambda instance: validated_data.get("agent").username, 
                        'services':lambda instance: ','.join( list( map( lambda service: str(service), validated_data.get("services",[]) ) ) ), 
                        'rut': lambda instance: validated_data.get('rut'), 
                        'status': lambda instance: EscalationModel.STATUS_CHOICES[validated_data.get('status')][1],
                        'typify': lambda instance: validated_data.get('typify'), 
                        'escalation_level': lambda instance: EscalationModel.ESCALATION_CHOICES[validated_data.get('escalation_level')][1],
                        'tests': lambda instance: ','.join( list( map( lambda test: str(test), validated_data.get("tests",[]) ) ) ), 
                        'problems': lambda instance: ','.join( list( map( lambda problem: str(problem), validated_data.get("problems",[]) ) ) ),
                        'solutions': lambda instance: ','.join( list( map( lambda solution: str(solution), validated_data.get("solutions",[]) ) ) ),
                        'agent_level_one': lambda instance: validated_data.get('agent_level_one').username, 
                        'agent_level_two': lambda instance: validated_data.get('agent_level_two').username, 
                        'agent_level_three': lambda instance: validated_data.get('agent_level_three').username, 
                    }

                    return _callable[field](validated_data)

                data: Dict[str, Any] = {}
                for field in validated_data:
                    if field == "operator" or field == "updater":
                        continue
                    data[field] = search_value(field, validated_data)
                
                labels: Dict[str,str] = {
                "agent": f"Agente: {data.get('agent')}\n", 
                "subcategory": f"Subcategoría: {data.get('subcategory')}\n",
                "second_subcategory": f"Segunda Subcategoría: {data.get('second_subcategory')}\n",
                "services": f"Servicios: {data.get('services')}\n", 
                "rut": f"Rut: {data.get('rut')}\n", 
                "status": f"Estado: {data.get('status')}\n", 
                "typify": f"Tipificación: {data.get('typify')}\n", 
                "commentaries": f"Comentarios: {data.get('commentaries')}\n", 
                "escalation_level": f"Nivel de escalamiento: {data.get('escalation_level')}\n",
                "tests": f"Pruebas: {data.get('tests')}\n",
                "problems": f"Problemas: {data.get('problems')}\n",
                "solutions": f"Soluciones: {data.get('solutions')}\n",
                "agent_level_one": f"Agente nivel uno: {data.get('agent_level_one')}\n",
                "agent_level_two": f"Agente nivel dos: {data.get('agent_level_two')}\n",
                "agent_level_three": f"Agente nivel tres: {data.get('agent_level_three')}\n"
                }

                global_preferences = global_preferences_registry.manager()
                client = WebClient(token=instance.operator.slack_token)
                items = ""
                for item in data:
                    items = items + labels[item]
                
                blocks: List[Dict[str, Any]] =  [
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":iris: *Iris* Ticket de Escalamiento"
                        }
                    },
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f":writing_hand:Se ha actualizado el ticket de escalamiento"
                        }
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": items
                        }
                    },
                    {
                        "type": "divider"
                    }
                ]

                client.chat_postMessage(
                    blocks=blocks,
                    thread_ts=thread,
                    channel=channel,
                    text= "Se ha actualizado el ticket de escalamiento"
                )
                
            if instance.slack_thread != None:
                for thread in instance.slack_thread:
                    send_message(thread, instance.slack_thread[thread], validated_data, instance)

            if "status" in validated_data:
                if (instance.typify != None and validated_data['status'] == 1) or (instance.typify == None and validated_data['status'] == 2):
                    global_preferences = global_preferences_registry.manager()
                    client = WebClient(token=instance.operator.slack_token)
                    solved_block: List[Dict[str, Any]] =  [
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":iris: *Iris* Ticket de Escalamiento"
                        }
                    },
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f":writing_hand:Se ha resuelto el ticket de escalamiento"
                        }
                    }
                    ]

                    if instance.slack_thread != None:    
                        for thread in instance.slack_thread:
                            client.chat_postMessage(
                                blocks=solved_block,
                                thread_ts=thread,
                                channel=instance.slack_thread[thread],
                                text="Se ha resuelto el ticket de escalamiento"
                            )

                    if instance.typify != None:
                        typify: TypifyModel = TypifyModel.objects.get(ID=instance.typify)
                        if typify.slack_thread != None:    
                            for thread in typify.slack_thread:
                                client.chat_postMessage(
                                    blocks=solved_block,
                                    thread_ts=thread,
                                    channel=typify.slack_thread[thread],
                                    text="Se ha resuelto el ticket de escalamiento"
                                )

        if "status" in validated_data:

            if validated_data["status"] == 2:
                if instance.typify != None:
                    if instance.autoclose_typify:
                        typify = TypifyModel.objects.get(pk=instance.typify)
                        typify.status = 1
                        typify.save()
                    else:
                        raise ValidationError({"open typify":"The asociated typification is still open"})

        slack_message(instance , validated_data)
        
        return super().update(instance, validated_data)

    def to_representation(self, instance):

        representation: Dict[str, Any] = super().to_representation(instance)
        return representation

    class Meta:

        #Class model of serializer
        model: BaseModel = FinanceEscalationModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
            'agent': {
                'required': False
            },
        }

class FinanceEscalationPhotoSerializer(BaseSerializer):

    
    """

        Class define serializer of EscalationPhotoView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = FinanceEscalationPhotoModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class FinanceTipifyCategorySerializer(BaseSerializer):

    
    """

        Class define serializer of FinanceTipifyCategoryView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = FinanceTipifyCategoryModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

class FinanceEmailSerializer(BaseSerializer):

    """

        Class define serializer of ConversationTagView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = FinanceEmailModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

