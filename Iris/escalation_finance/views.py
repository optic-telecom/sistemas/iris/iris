import threading
import string
import random
import json
from datetime import datetime, timedelta
from typing import Any, Dict, List

from django_q.models import Schedule
from django.contrib.auth.models import User
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.viewsets import BaseViewSet
from django.http import JsonResponse, HttpResponseForbidden
from rest_framework.decorators import action
from rest_framework.serializers import ValidationError
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from dynamic_preferences.registries import global_preferences_registry
from operators.models import OperatorModel
from tickets.models import CategoryModel

from .models import (FinanceEmailModel, FinanceProblemsModel, FinanceEscalationSlackMessageModel, FinanceEscalationTablesModel,
                    FinanceEscalationPhotoModel, FinanceSolutionModel, FinanceEscalationModel, FinanceTipifyCategoryModel)
from .serializers import (FinanceEmailSerializer, FinanceEscalationProblemsSerializer, FinanceEscalationSlackMessageSerializer,
                        FinanceEscalationTablesSerializer, FinanceEscalationPhotoSerializer, FinanceSolutionSerializer,
                        FinanceEscalationSerializer, FinanceTipifyCategorySerializer)
from .datatables import (FinanceEscalationDatatable, FinanceEscalationSlackMessageDatatable, FinanceEscalationTablesDatatable, FinanceProblemsDatatable, FinanceSolutionDatatable)

class FinanceProblemsView(BaseViewSet):

    """

        Class define ViewSet of ProblemsModel.
    
    """

    queryset: BaseModel = FinanceProblemsModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FinanceEscalationProblemsSerializer

    @method_decorator(permission_required('escalation_finance.delete_financeproblemsmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        
        instance = self.get_object()

        def get_children(parent: ProblemsModel) -> List[int]:

            id_children: List[int] = []

            for problem in ProblemsModel.objects.filter(parent=parent):

                id_children = id_children + get_children(problem)

            return [parent.ID] + id_children

        def delete_problem(parent: ProblemsModel):

            for problem in ProblemsModel.objects.filter(parent=parent):

                delete_problem(problem)

            parent.delete()

        if EscalationModel.objects.filter(problems__ID__in=get_children(instance)):

            raise ValidationError({
                "Error delete":"typify related problem"
            })

        delete_problem(instance)

        return JsonResponse({}, safe=False)

    @method_decorator(permission_required('escalation_finance.add_financeproblemsmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.change_financeproblemsmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.change_financeproblemsmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.view_financeproblemsmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(FinanceProblemsDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('escalation_finance.view_financeproblemsmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return FinanceProblemsDatatable(request).get_data()

    @method_decorator(permission_required('escalation_finance.view_financeproblemsmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def tree(self, request, pk: int):

        """
        
            This funtion return tree Problems

            :param self: Instance of Class ProblemsView
            :type self: ProblemsView
            :param request: data of request
            :type request: request
        
        """

        def tree_data_json(node_parent: FinanceProblemsModel) -> Dict[str, Any]:

            """
        
                This funtion is recursive. Return tree of nodes

                :param node_parent: Instance of Class ProblemsModel
                :type node_parent: ProblemsModel
            
                :return: Dict of nodes
                :rtype: dict

            """

            result: Dict[str, str] = {

                "id": node_parent.ID,
                "name": node_parent.name,
                "title": node_parent.get_classification_display(),

            }

            children : QuerySet = FinanceProblemsModel.objects.filter(parent=node_parent)

            if children:

                result["children"] = [tree_data_json(node_children) for node_children in FinanceProblemsModel.objects.filter(parent=node_parent, deleted=False)]

            return result

        return JsonResponse(tree_data_json(FinanceProblemsModel.objects.get(ID=pk, deleted=False)), safe=False)

class FinanceEscalationSlackMessageView(BaseViewSet):

    queryset: BaseModel  = FinanceEscalationSlackMessageModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FinanceEscalationSlackMessageSerializer

    @method_decorator(permission_required('escalation_finance.add_efinancescalationslackmessagemodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.changfinancee_escalationslackmessagemodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.changfinancee_escalationslackmessagemodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.deletfinancee_escalationslackmessagemodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.view_financeescalationslackmessagemodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(FinanceEscalationSlackMessageDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('escalation_finance.view_financeescalationslackmessagemodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return FinanceEscalationSlackMessageDatatable(request).get_data()

    @method_decorator(permission_required('escalation_finance.change_financeescalationmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def update_channel(self, request) -> JsonResponse:
        
        global_preferences = global_preferences_registry.manager()

        channel: str = request.POST['channel']
        operator: str = request.POST['operator']
        
        if str(operator) == "2":
            global_preferences['escalation_ti__slack_channel_bandaancha'] = channel
        else:
            global_preferences['escalation_ti__slack_channel_multifiber'] = channel

        return JsonResponse( data={} )

    @method_decorator(permission_required('escalation_finance.change_financeescalationmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def get_channel(self, request) -> JsonResponse:
        
        global_preferences = global_preferences_registry.manager()

        operator: str = request.GET['operator']

        if str(operator) == "2":
            channel = global_preferences['escalation_ti__slack_channel_bandaancha'] 
        else:
            channel = global_preferences['escalation_ti__slack_channel_multifiber']

        return JsonResponse( data={"channel": channel} )

        
class FinanceEscalationTablesView(BaseViewSet):

    queryset: BaseModel  = FinanceEscalationTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FinanceEscalationTablesSerializer

    @method_decorator(permission_required('escalation_finance.add_efinancescalationtablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.changfinancee_escalationtablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.changfinancee_escalationtablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.deletfinancee_escalationtablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.view_financeescalationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:

        data_struct : Dict[str, Dict[str, str]] = {

            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Creador":{
                    "type":"choices",
                    "name":"creator__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Estado":{
                    "type":"choices",
                    "name":"status",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: 'ABIERTO',
                            1: 'EN SAC',
                            2: 'CERRADO',
                        }
                    }
                },
                "Agente":{
                    "type":"choices",
                    "name":"agent__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
            },
            "columns":  {
                'creator': "Creador",
                'updater': "Último en actualizar",
                'created': "Fecha de creación",
                'updated': "Fecha de actualización",
                'agent': "Agente",
                'services': "Servicios", 
                'status': "Estado",
                'rut': "RUT",
                'typify':"Tipificación"
            }
        }

        return JsonResponse(data_struct, safe=True)

    @method_decorator(permission_required('escalation_finance.view_financeescalationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(FinanceEscalationTablesDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('escalation_finance.view_financeescalationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        return FinanceEscalationTablesDatatable(request).get_data()

class FinanceEscalationView(BaseViewSet):

    queryset: BaseModel = FinanceEscalationModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FinanceEscalationSerializer
    history_ignore_fields: List[str] = ['commentaries', 'updater','updated']

    @method_decorator(permission_required('escalation_finance.view_esfinancecalationmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        typify_datatables = FinanceEscalationDatatable(request)
        typify_datatables.ID_TABLE = pk

        return JsonResponse(typify_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('escalation_finance.view_esfinancecalationmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct_view(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        data_struct: Dict[str, Any] = {
            "id_table": None,
            "fields":{},
            "columns":{},
            "filters":{},
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons":{
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            }
        }

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {
            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
        }

        data_struct["columns"] = {
            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Número de Escalamiento":{
                "field": "ID",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Número de Tipificación":{
                "field": "typify",
                "sortable": True,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Estado":{
                "field": "status",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Agente":{
                "field": "agent",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },
        }

        data_struct["filters"] = {}

        return JsonResponse(data_struct, safe=False)

    @method_decorator(permission_required('escalation_finance.view_esfinancecalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request, pk):

            typify_datatables = FinanceEscalationDatatable(request)
            typify_datatables.ID_TABLE = pk
            typify_datatables.download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request, pk]
        ).start()

        return JsonResponse(data={})

    @method_decorator(permission_required('escalation_finance.view_esfinancecalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EscalationSlackMessageView
            :type self: EscalationSlackMessageView
            :param request: data of request
            :type request: request
        
        """

        typify_datatables = FinanceEscalationDatatable(request)
        typify_datatables.ID_TABLE = pk

        return typify_datatables.get_data()

    @method_decorator(permission_required('escalation_finance.add_escfinancealationmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.change_financeescalationmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):

        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.change_financeescalationmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.delete_financeescalationmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.change_financeescalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_problem(self,request,pk) -> JsonResponse:
        
        """
        
            This funtion adds problem to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)
            problem_escalation: BaseModel = FinanceProblemsModel.objects.get(ID=request.POST['problem_id'])

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        escalation.problems.add( problem_escalation )
        escalation.save()

        return JsonResponse( data=FinanceEscalationSerializer(escalation).data )

    @method_decorator(permission_required('escalation_finance.change_financeescalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_problem(self, request, pk) -> JsonResponse:

        """
        
            This funtion removes problem from escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        try:
            instance_problem: BaseModel = escalation.problems.get(ID=request.POST['problem_id'])
            escalation.problems.remove(instance_problem)
        except:

            raise ValidationError({
                "Error escalation problem": "Item Don't exist"         
            })

        escalation.save()

        return JsonResponse( data=FinanceEscalationSerializer(escalation).data )

    @method_decorator(permission_required('escalation_finance.change_financeescalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def add_solution(self, request, pk) -> JsonResponse:

        """
        
            This funtion adds solution to escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)
            solution_escalation: BaseModel = FinanceSolutionModel.objects.get(ID=request.POST['solution_id'])

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        escalation.solutions.add( solution_escalation )
        escalation.save()

        return JsonResponse( data=FinanceEscalationSerializer(escalation).data )

    @method_decorator(permission_required('escalation_finance.change_financeescalationmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def remove_solution(self, request, pk) -> JsonResponse:

        """
        
            This funtion removes solution from escalation ticket

            :param self: Instance of Class EscalationView
            :type self: EscalationView
            :param request: data of request
            :type request: request
            :param pk: Escalation id
            :type pk: int
        
        """

        try:

            escalation: BaseModel = self.queryset.get(ID=pk)

        except:

            raise ValidationError({
                "Error escalation": "Item Don't exist"         
            })

        try:
            instance_solution: BaseModel = escalation.solutions.get(ID=request.POST['solution_id'])
            escalation.solutions.remove(instance_solution)
        except:

            raise ValidationError({
                "Error escalation test": "Item Don't exist"         
            })

        return JsonResponse( data=FinanceEscalationSerializer(escalation).data )

class FinanceSolutionView(BaseViewSet):

    """

        Class define ViewSet of SolutionModel.
    
    """

    queryset: BaseModel  = FinanceSolutionModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FinanceSolutionSerializer

    @method_decorator(permission_required('escalation_finance.add_sfinanceolutionmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.changfinancee_solutionmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.changfinancee_solutionmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.deletfinancee_solutionmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.view_financesolutionmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class SolutionView
            :type self: SolutionView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(FinanceSolutionDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('escalation_finance.view_financesolutionmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class SolutionView
            :type self: SolutionView
            :param request: data of request
            :type request: request
        
        """

        return FinanceSolutionDatatable(request).get_data()

class FinanceEscalationPhotoView(BaseViewSet):

    """

        Class define ViewSet of EscalationPhotoModel.
    
    """

    queryset: BaseModel = FinanceEscalationPhotoModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FinanceEscalationPhotoSerializer

    @method_decorator(permission_required('escalation_finance.add_escfinancealationphotomodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.change_financeescalationphotomodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.delete_financeescalationphotomodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

class FinanceTipifyCategoryView(BaseViewSet):

    """

        Class define ViewSet of EscalationPhotoModel.
    
    """

    queryset: BaseModel = FinanceTipifyCategoryModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FinanceTipifyCategorySerializer

    @method_decorator(permission_required('escalation_finance.add_escfinetipifycategorymodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.change_financetipifycategorymodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.delete_financetipifycategorymodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('escalation_finance.view_financetipifycategorymodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def add_escalation_categories(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        categories = json.loads(request.data.get('categories'))
        retentions_categories = FinanceTipifyCategoryModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.data.get('operator'))).all()
        for category in retentions_categories:
            if category.category.pk not in categories:
                category.delete()
        
        for new_category in categories:
            if len(retentions_categories.filter(category__pk=new_category, deleted=False)) == 0:
                FinanceTipifyCategoryModel.objects.create(
                    creator=request.user, 
                    category=CategoryModel.objects.get(ID=new_category),
                    operator=OperatorModel.objects.get(pk=request.data.get('operator'))
                    )
        result = [instance.category.ID for instance in FinanceTipifyCategoryModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.data.get('operator')))]
        return JsonResponse(result, safe=False)

    @method_decorator(permission_required('escalation_finance.view_financetipifycategorymodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def get_escalation_categories(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        result = [instance.category.ID for instance in FinanceTipifyCategoryModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.GET.get('operator')))]
        return JsonResponse(result, safe=False)

class FiannceEmailView(BaseViewSet):

    """

        Class define ViewSet of ConversationTagModel.
    
    """

    @method_decorator(permission_required('communications.view_financeemailmodelmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def update_emails(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        emails = json.loads(request.data.get('emails'))
        existent_emails = FinanceEmailModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.data.get('operator'))).all()
        for email in existent_emails:
            if email.email not in emails:
                email.delete()
        
        for email in emails:
            if len(existent_emails.filter(email=email, deleted=False)) == 0:
                FinanceEmailModel.objects.create(
                    creator=request.user, 
                    email=email,
                    operator=OperatorModel.objects.get(pk=request.data.get('operator'))
                    )
        result = [instance.email for instance in FinanceEmailModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.data.get('operator')))]
        return JsonResponse(result, safe=False)

    queryset: BaseModel = FinanceEmailModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FinanceEmailSerializer
