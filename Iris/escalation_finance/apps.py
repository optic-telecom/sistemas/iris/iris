from django.apps import AppConfig


class EscalationFinanceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'escalation_finance'
