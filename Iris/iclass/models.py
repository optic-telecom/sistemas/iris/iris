from datetime import date, datetime
from typing import Any, Callable, Dict, List, Set, Tuple, Union
import requests
from django.utils.translation import gettext_lazy as _
from common.models import BaseModel
from django.contrib.auth.models import User, Group
from django.contrib.postgres.fields import JSONField
from django.db import models
from rest_framework.serializers import ValidationError
from django_q.models import Schedule
from django.core.validators import MinValueValidator

class ServiceOrderSlackMessage(BaseModel):

    """

        Class define model Ticket.
    
    """

    ONCE = 'O'
    MINUTES = 'I'
    HOURLY = 'H'
    DAILY = 'D'
    WEEKLY = 'W'
    MONTHLY = 'M'
    QUARTERLY = 'Q'
    YEARLY = 'Y'
    TYPE = (
        (MINUTES, _('Minutos')),
        (ONCE, _('Una vez')),
        (HOURLY, _('Cada Hora')),
        (DAILY, _('Diariamente')),
        (WEEKLY, _('Semanalmente')),
        (MONTHLY, _('Mensualmente')),
        (QUARTERLY, _('Trimestralmente')),
        (YEARLY, _('Anualmente')),
    )


    type_repeats = models.CharField(max_length=1, choices=TYPE, default=TYPE[5][0], verbose_name=_('Type of repetitions'), null=False, blank=False)
    number_repeats = models.IntegerField(validators=[MinValueValidator(-1)], verbose_name=_('Number of repetitions'), null=False, blank=False)
    schedule = models.ForeignKey( Schedule, on_delete=models.SET_NULL, null=True)
    start: datetime = models.DateTimeField()