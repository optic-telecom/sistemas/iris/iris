from rest_framework.routers import DefaultRouter
from django.urls import re_path
from .views import proxy
from typing import List, Callable

urlpatterns: List[Callable] = [
    re_path(r'$', proxy)
]