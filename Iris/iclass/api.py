import random
import string
from typing import ChainMap
import requests
import json 

from django.utils.translation import gettext_lazy as _
from .models import ServiceOrderSlackMessage
from operators.models import OperatorModel
from rest_framework.serializers import ValidationError
from django.http import JsonResponse
from django_q.models import Schedule
from dynamic_preferences.registries import global_preferences_registry

def manage_slack_messages(request):

    result = {}
    status_code = 200
    #Get

    if request.method == "GET":

        try:
            operator = OperatorModel.objects.get(ID=int(request.GET.get("operator")))
        except:
            raise ValidationError({'Operator': 'Invalid operator ID'})

        try:
            schedule_settings = ServiceOrderSlackMessage.objects.get(operator=operator)

        except:
            return JsonResponse(data=[], safe=False, status=200)

        data = {
            'operator': operator.ID,
            'type_repeats': schedule_settings.type_repeats,
            'number_repeats': schedule_settings.number_repeats,
            'start': schedule_settings.start
        }

        return JsonResponse(data=data, safe=False, status=200)

    #Post
    elif request.method == "POST":
        
        data = request.POST

        try:
            operator = OperatorModel.objects.get(ID=int(data.get("operator")))
        except:
            raise ValidationError({'Operator': 'Invalid operator ID'})

        try:
            instance = ServiceOrderSlackMessage.objects.get(operator=operator)

            for atribute in data:
                if atribute != "operator":
                    setattr(instance, atribute, data[atribute])

            instance.save()
            if instance.schedule:
                instance.schedule.delete()

        except:
            instance = ServiceOrderSlackMessage.objects.create(
                type_repeats=data.get("type_repeats"),
                number_repeats=data.get("number_repeats"),
                start=data.get("start"),
                operator=operator
            )

            instance.save()

            status_code = 201

        schedule_object: Schedule = Schedule.objects.create(
            func='iclass.api.send_slack_message',
            name=''.join(random.choice( string.ascii_uppercase + string.digits ) for _ in range(14)),
            schedule_type=instance.type_repeats,
            repeats=instance.number_repeats,
            next_run=instance.start,
            kwargs={
                'operator':instance.operator.ID
            },
            minutes=5,
        )

        instance.schedule = schedule_object
        instance.save()
        
        result = {
            'type_repeats' : instance.type_repeats,
            'number_repeats': instance.number_repeats,
            'start': instance.start
        }

    return JsonResponse(data=result, safe=False, status=status_code)

def send_slack_message(*args, **kwargs):
    """

        This function init communication cycle

        :param args: This param is args list
        :type args: list
        :param kwargs: This param is args dict
        :type kwargs: dict

    """

    if 'kwargs' in kwargs.keys():
        operator: int = kwargs['kwargs']['operator']

    else:
        operator: int = kwargs['operator']

    try:
        operator = OperatorModel.objects.get(ID=operator)
    except:
        raise ValidationError({'Operator': 'Invalid operator ID'})

    global_preferences: Dict[str, any] = global_preferences_registry.manager()

    url: str = global_preferences['iclass__iclass_url'] + "service_orders/send_reminder/"

    headers: Dict[str, str] = {
        'AUTHORIZATION': global_preferences['iclass__iclass_token']
    }

    token = operator.slack_token

    instance = ServiceOrderSlackMessage.objects.filter(operator=operator).first()

    data = {
        'operator': operator.ID,
        'token': token,
    }

    response = requests.post(
        url=url,
        headers=headers,
        verify=False,
        data=json.dumps(data)
    )