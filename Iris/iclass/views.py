
from .api import manage_slack_messages
from django.http import JsonResponse
from typing import List, Dict, Any
from dynamic_preferences.registries import global_preferences_registry
import requests
import json
from rest_framework.serializers import ValidationError
from rest_framework.decorators import api_view
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required

@api_view(['PATCH','POST','GET','DELETE'])
def proxy(request) -> JsonResponse:

    """

        This function sends request to the iclass microservice

        :param request: Values of user request
        :type request: request

        :returns: Response data user
        :rtype: JsonResponse
    
    """

    global_preferences: Dict[str, any] = global_preferences_registry.manager()

    origin_url: str = request.build_absolute_uri().split('/api/v1/iclass/')[1].split('?')
    url: str = global_preferences['iclass__iclass_url'] + origin_url[0]

    result: List[Dict[str, Any]] = []
    status_code: int = 200

    headers: Dict[str, str] = {
        'AUTHORIZATION': global_preferences['iclass__iclass_token']
    }

    if 'slack_msg' in url:
        return manage_slack_messages(request)

    if request.method == 'POST':

        if "delete_order" in url:

            response = requests.delete(
            url=url + '?motive=' + str(request.POST.dict().get('motive', "")),
            headers=headers,
            verify=False,
            )

        elif "comment_order" in url:

            response = requests.post(
            url=url + '?comment=' + str(request.POST.dict().get('comment', "")),
            headers=headers,
            verify=False,
            )

        elif "token" in url:

            headers: Dict [str, str]= {
                'AUTHORIZATION': global_preferences['iclass__iclass_token'],
                'Content-Type': 'application/x-www-form-urlencoded'
            }

            payload='username=' + request.POST.dict().get('username', "") + '&password=' + request.POST.dict().get('password', "") + '&grant_type=password'
            response = requests.request("POST", url, headers=headers, data=payload)
        
        else:

            response = requests.post(
                url=url,
                headers=headers,
                verify=False,
                data=json.dumps(request.POST)
            )

        status_code: int = response.status_code

        result = response.json()

    if request.method == 'GET':

        params = {}

        for param in request.GET:
            params[param] = request.GET[param]

        response = requests.get(
            url=url,
            headers=headers,
            verify=False,
            params=params
        )

        status_code: int = response.status_code

        if response.status_code != 200:
            raise ValidationError({
                "Iclass error": response.content,
            }) 

        else: 
            result = response.json()

    if request.method == 'DELETE':

        response = requests.delete(
            url=url,
            headers=headers,
            verify=False,
        )

        status_code: int = response.status_code

        if response.status_code != 200:
            raise ValidationError({
                "Iclass error": response.content,
            }) 

        else: 
            result = response.json()

    if request.method == 'PATCH':

        response = requests.patch(
            url=url,
            headers=headers,
            verify=False,
            data=json.dumps(request.data)
        )

        status_code: int = response.status_code

        if response.status_code != 200:
            raise ValidationError({
                "Iclass error": response.content,
            }) 

        else: 
            result = response.json()

    return JsonResponse(data=result, safe=False, status=status_code)
