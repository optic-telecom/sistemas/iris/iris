from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference, IntegerPreference, ChoicePreference

general = Section('iclass')

@global_preferences_registry.register
class StaticToken(StringPreference):

    section = general
    name: str = 'iclass_token'
    default: str = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwcnVlYmEiLCJleHAiOjE2MDg2NTU5MDZ9.fU5Uy2ReI0OazBMbHKUyHmgTRizB-KrDutU3eS7rnG0'
    required: bool = True

@global_preferences_registry.register
class SiteTitle(StringPreference):

    section = general
    name: str = 'iclass_url'
    default: str = 'localhost:7999/'
    required: bool = True