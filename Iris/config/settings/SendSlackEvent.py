import json
import urllib3 
from django.conf import settings
import os
from typing import List, Dict, Any

def send_slack_event(event, hint):

    http = urllib3.PoolManager()
    fields: List[Dict[str, Any]] = []
    
    fields.append({
        "title": "Función",
        "value": event['exception']['values'][-1]['stacktrace']['frames'][-1]['function'],
        "short": True
    })
    fields.append({
        "title": "Tipo",
        "value": event['exception']['values'][-1]['type'],
        "short": True
    })

    fields.append({
        "title": "Valor",
        "value": event['exception']['values'][-1]['value'],
        "short": True
    })

    fields.append({
        "title": "Archivo",
        "value": event['exception']['values'][-1]['stacktrace']['frames'][-1]['abs_path'] + ':' + str(event['exception']['values'][-1]['stacktrace']['frames'][-1]['lineno']),
        "short": False
    })

    data_slack: Dict[str, Any] = {
        "text": 'Proyecto: ' + settings.SITE_NAME,
        "attachments": [
        {
            "color": "#c82d10",
            "text": event['level'],
            "fields": fields
        }       
        ]
    }
    http.request(
        "POST",
        os.environ['SENTRY_SLACK_ERROR'],
        body=json.dumps(data_slack),
        headers={'Content-Type': 'application/json'}
    )

    return event