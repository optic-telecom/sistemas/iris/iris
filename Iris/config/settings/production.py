from .base import *
from multifiberpy.sentry_multifiber import send_slack_event
from os.path import join
from typing import List, Dict, Any

ENVIROMENT = "Production"
SITE_NAME = "Iris producción"

sentry_sdk.init(
    dsn=get_env_variable('SENTRY_URL'),
    integrations=[DjangoIntegration()],
    before_send=send_slack_event
)

ALLOWED_HOSTS: List[str] = ['iris7.cl', 'localhost', '127.0.0.1', '0.0.0.0', 'backendserver']

DEBUG: bool = False

BASE_DIR_LOG: str = "/home/iris/iris_backend/logs/django/debug_django.log"

LOGGING: Dict[str, Any] = {
    "version": 1,
    "formatters": {"verbose": {"format": "%(levelname)s:%(name)s: %(message)s"}},
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "file": {
            "level": "INFO",
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "filename": join(BASE_DIR_LOG, "debug_django.log"),
        },
    },
    "loggers": {
        "django.request": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": False,
        },
        "matplotlib": {
            "handlers": ["file", "console"],
            "level": "ERROR",
            "propagate": False,
        },
        "": {"level": "DEBUG", "handlers": ["console"],},
    },
}