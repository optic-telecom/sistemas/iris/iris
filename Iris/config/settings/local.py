from .base import *
from .SendSlackEvent import *

SITE_NAME = "Iris local"

ALLOWED_HOSTS: List[str] = ['*']

DEBUG: bool = True
