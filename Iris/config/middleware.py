from django.utils.deprecation import MiddlewareMixin
from django.core.cache import cache
from django.http import JsonResponse
import threading
import json
import re
from django.conf import settings
from django.http import HttpResponse


class CacheMiddleware(MiddlewareMixin):

    def __call__(self, request):

        def remove_items_cache(request):

            endpoint = request.build_absolute_uri().split('v1/')[1].split('/')
            init_endpoint = f'{endpoint[0]}/{endpoint[1]}*'
            
            for item in cache.keys(init_endpoint):
                cache.delete(item)

        def validate_url(request):
            
            url = request.build_absolute_uri()
            result = 'api/' in url

            if result:

                for regex in settings.CACHE_MIDDLEWARE['IGNORE']:
                   
                    if re.search(regex, url):
                        
                        result = False
                        break

            return result

        method = request.method
        
        if validate_url(request):

            if method == 'GET':
            
                data_cache = None
                response = None

                key = request.build_absolute_uri().split('v1/')[1]
                key = key.split('&_=')[0] if 'datatables' in key else key
                data_cache = cache.get( key )

                if data_cache:

                    response = HttpResponse(data_cache['content'], data_cache['content_type'])
                    response._headers = data_cache['headers']
                    response._closable_objects = data_cache['closable_objects']
                    response._reason_phrase = data_cache['reason_phrase']
                    response._charset = data_cache['charset']
                    response.status_code = 200

                else:

                    response = self.get_response(request)

                    key = request.build_absolute_uri().split('v1/')[1]
                    key = key.split('&_=')[0] if 'datatables' in key else key

                    data_cache = {

                        'content_type': response._headers['content-type'][1],
                        'headers': response._headers,
                        'content': response.content,
                        'closable_objects': response._closable_objects,
                        'reason_phrase': response._reason_phrase,
                        'charset': response._charset

                    }

                    cache.set( key, data_cache )

            else:

                if request.method in ['POST', 'PUT', 'DELETE']:

                    threading.Thread(
                        target=remove_items_cache,
                        args=[request]
                    ).start()

                response = self.get_response(request)

        else:

            response = self.get_response(request)

        return response