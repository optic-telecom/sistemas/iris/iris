from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls.static import static
from django.conf import settings
from rest_framework.permissions import BasePermission, AllowAny
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from dynamic_preferences.registries import global_preferences_registry
from typing import Dict, Any, Callable, List

class PermissionDocs(BasePermission):

    def has_permission(self, request, view):

        global_preferences: Dict[str, Any] = global_preferences_registry.manager()
        enviroment: str = global_preferences['general__Enviroment']

        return enviroment in ['local', 'development']


class PermissionDocsClass(AllowAny):

    def authenticate(self, request):

        return None

schema_view = get_schema_view(
   openapi.Info(
      title="Iris system api API",
      default_version='v1',
      description="Api, Iris project development",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="cristian.cantero@multifiber.cl"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(PermissionDocs,),
   authentication_classes=(PermissionDocsClass,),
)


urlpatterns: List[Callable] = [
    path(
        'api/v1/redoc/',
        schema_view.with_ui('redoc'),
        name='schema-redoc'
    ),
    path(
        'admin/',
        admin.site.urls
    ),
    path(
        'api/v1/communications/',
        include(('communications.urls', 'communications'), namespace='communications')
    ),
    path(
        'api/v1/tickets/',
        include(('tickets.urls', 'tickets'), namespace='tickets')
    ),
    path(
        'api/v1/user/',
        include(('users.urls', 'users'), namespace='users'),
    ),
    path(
        'api/v1/matrix/',
        include(('matrix.urls', 'matrix'), namespace='matrix'),
    ),
    path(
        'api/v1/customers/',
        include(('customers.urls', 'customers'), namespace='customers'),
    ),
    path(
        'api/v1/slackapp/',
        include(('slackapp.urls', 'slackapp'), namespace='slackapp'),
    ),
    path(
        'api/v1/drives/',
        include(('drives.urls', 'drives'), namespace='drives'),
    ),
    path(
        'api/v1/pulso/',
        include(('pulso.urls', 'pulso'), namespace='pulso'),
    ),
    path(
        'api/v1/escalation_ti/',
        include(('escalation_ti.urls', 'escalation_ti'), namespace='escalation_ti'),
    ),
    path(
        'api/v1/formerCustomers/',
        include(('formerCustomers.urls', 'formerCustomers'), namespace='formerCustomers'),
    ),
    path(
        'api/v1/iclass/',
        include(('iclass.urls', 'iclass'), namespace='iclass'),
    ),
    path(
        'api/v1/liveagent/',
        include(('liveagent.urls', 'liveagent'), namespace='liveagent'),
    ),
    path(
        'api/v1/voipnow/',
        include(('voipnow.urls', 'voipnow'), namespace='voipnow'),
    ),
    path(
        'api/v1/qc/',
        include(('qc.urls', 'qc'), namespace='qc'),
    ),
    path(
        'api/v1/tech_support/',
        include(('tech_support.urls', 'tech_support'), namespace='tech_support'),
    ),
    path(
        'api/v1/operators/',
        include(('operators.urls', 'operators'), namespace='operators'),
    ),
    path(
        'api/v1/retentions/',
        include(('retentions.urls', 'retentions'), namespace='retentions'),
    ),
    path(
        'api/v1/webhooks/',
        include(('webhooks.urls', 'webhooks'), namespace='webhooks'),
    ),
    path(
        'api/v1/escalation_finance/',
        include(('escalation_finance.urls', 'escalation_finance'), namespace='escalation_finance'),
    ),
] + static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )

urlpatterns + static(
                settings.STATIC_URL,
                document_root=settings.STATIC_ROOT
            )

