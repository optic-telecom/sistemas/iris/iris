from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference, IntegerPreference, ChoicePreference

general = Section('communications')


@global_preferences_registry.register
class MessageBirdKey(StringPreference):

    section = general
    name: str = 'Message_bird'
    default: str = 'FB2Pc3u5p3PIng9iSxQpsqupb'
    required: bool = True

@global_preferences_registry.register
class LowImpactCommunication(IntegerPreference):

    section = general
    name: str = 'low_impact'
    default: int = 100
    required: bool = False

@global_preferences_registry.register
class MediumImpactCommunication(IntegerPreference):

    section = general
    name: str = 'medium_impact'
    default: int = 1000
    required: bool = False

@global_preferences_registry.register
class HightImpactCommunication(IntegerPreference):

    section = general
    name: str = 'hight_impact'
    default: int = 100000
    required: bool = False

@global_preferences_registry.register
class SlackNotifications(StringPreference):
    
    section = general
    name: str = 'slack_notifications_massive_url'
    default: str = 'https://hooks.slack.com/services/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'
    required: bool = True

@global_preferences_registry.register
class WhatsappChannel(StringPreference):
    
    section = general
    name: str = 'whatsapp_channels'
    default: str = '618703f211ed4698b74897c106a7326c'
    required: bool = True

@global_preferences_registry.register
class SlackToken(StringPreference):
    
    section = general
    name: str = 'slack_token'
    default: str = 'xoxb-654700021921-1218871707361-TwPaGgjRywGCnUSctcqS3XI9'
    required: bool = True

@global_preferences_registry.register
class SlackTokenBandaancha(StringPreference):
    
    section = general
    name: str = 'slack_token_bandaancha'
    default: str = 'xoxb-381128721780-1480262424566-Vxj6edQ2no8k8Gbuitl2ORfG'
    required: bool = True

@global_preferences_registry.register
class NameSpace(StringPreference):
    
    section = general
    name: str = 'namespace'
    default: str = 'd732392d_4463_419b_8663_9736df56b8cc'
    required: bool = True

@global_preferences_registry.register
class TestEmail(StringPreference):
    
    section = general
    name: str = 'test_email'
    default: str = 'ariana.amador@multifiber.cl'
    required: bool = True