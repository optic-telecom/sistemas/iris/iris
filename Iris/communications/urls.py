from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import (ChatView, ConversationTagView, ConversationView, EmailConversationTablesView, EmailMessageView, EmailView, EventCallView, EventEmailView,
                    EventTextView, EventWhatsappView, FilesEmailView,
                    InternalChatView, MassiveCommunicationsView, NotificationView,
                    TemplateCallView, TemplateEmailView, TemplateTextView,
                    TemplateWhatsappView, TriggerEventView, CycleCommunicationsView, TextView, WhatsAppConversationTablesView, WhatsAppMessageView, WhatsappView, VoiceCallView,
                    EmailTableView, WhatsappTableView, TextTableView, VoiceCallTableView)

router = DefaultRouter()

router.register(r'cycle', CycleCommunicationsView, basename='cycle')
router.register(r'internal_chat', InternalChatView, basename='internal_chat')
router.register(r'template_email', TemplateEmailView, basename='template_email')
router.register(r'template_whatsapp', TemplateWhatsappView, basename='template_whatsapp')
router.register(r'template_text', TemplateTextView, basename='template_text')
router.register(r'template_voice_call', TemplateCallView, basename='template_voice_call')
router.register(r'chat', ChatView, basename='chat')
router.register(r'massive', MassiveCommunicationsView, basename='massive')
router.register(r'trigger_event', TriggerEventView, basename='trigger_event')

router.register(r'email_event', EventEmailView, basename='email_event')
router.register(r'whatsapp_event', EventWhatsappView, basename='whatsapp_event')
router.register(r'text_event', EventTextView, basename='text_event')
router.register(r'call_event', EventCallView, basename='call_event')

router.register(r'email', EmailView, basename='email')
router.register(r'email_tables', EmailTableView, basename='email_tables')
router.register(r'file_email', FilesEmailView, basename='file_email')
router.register(r'text', TextView, basename='text')
router.register(r'text_tables', TextTableView, basename='text_tables')
router.register(r'whatsapp', WhatsappView, basename='whatsapp')
router.register(r'whatsapp_tables', WhatsappTableView, basename='whatsapp_tables')
router.register(r'voice_call', VoiceCallView, basename='voice_call')
router.register(r'voice_call_tables', VoiceCallTableView, basename='voice_call_tables')

router.register(r'conversation', ConversationView, basename='conversation')
router.register(r'whatsapp_conversation_tables', WhatsAppConversationTablesView, basename='whatsapp_conversation_tables')
router.register(r'email_conversation_tables', EmailConversationTablesView, basename='email_conversation_tables')
router.register(r'whatsapp_message', WhatsAppMessageView, basename='whatsapp_message')
router.register(r'email_message', EmailMessageView, basename='email_message')
router.register(r'conversation_tag', ConversationTagView, basename='conversation_tag')

router.register(r'notification', NotificationView, basename='notification')

urlpatterns = router.urls
