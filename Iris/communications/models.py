from datetime import datetime
from functools import reduce
import calendar
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.core.validators import MinValueValidator
from django.core.validators import validate_email
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from operators.models import CompanyModel
from django_q.models import Schedule
from phone_field import PhoneField
from phonenumber_field.modelfields import PhoneNumberField
from rest_framework.serializers import ValidationError
from simple_history.models import HistoricalRecords
from dynamic_preferences.registries import global_preferences_registry
import requests
from typing import Dict, List, Tuple, Union, Any, Set, Callable
from common.utilitarian import get_token
from slack import WebClient
from common.models import BaseModel

import requests

class MessageModel(BaseModel):

    """

        Class define Model of message communications.
    
    """

    class Meta:

        abstract: bool = True

    # Message data
    message: str = models.TextField()

    # Extra message information
    sent: bool = models.BooleanField(default=False)
    inbox: bool = models.BooleanField(default=False)
    outbox: bool = models.BooleanField(default=False)
    viewed: bool = models.BooleanField(default=False)

class ChatModel(BaseModel):

    """

        Class define model to Chat of communications.
    
    """

    last_message: datetime = models.DateTimeField(default=datetime.now())
    new_messages: int = models.IntegerField(default=0)

    def __str__(self):

        return self.ID

class CommunicationModel(MessageModel):

    """

        Class define model base of communications.
    
    """

    chat: int = models.ForeignKey(
        ChatModel,
        models.SET_NULL,
        null=True,
        blank=False
    )

    TYPE: Tuple[Tuple[int, str]] = (
        (0, _('In')),
        (1, _('Out')),
    )

    _type : bool = models.BooleanField(default=1, choices=TYPE)

    event = models.ForeignKey(
        "TriggerEventModel",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        default=None
    )

    massive = models.ForeignKey(
        "MassiveCommunicationsModel",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        default=None
    )

    cycle = models.ForeignKey(
        "CycleCommunicationsModel",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        default=None
    )

    remove_equipament = models.PositiveIntegerField(
        null=True, 
        default=None
    )

    class Meta:

        abstract: bool = True

class InternalChatModel(CommunicationModel):

    """

        Class define model of internal communications.
    
    """

    pass

    def __str__(self):

        self.ID

class EmailModel(CommunicationModel):

    """

        Class define model of Email communications.
    
    """

    #Subject of email message
    subject: str = models.TextField()
    #Email receiver the email message
    receiver: str = models.EmailField()
    #Email sender the email message
    sender: str = models.EmailField()
    #Email has been seen
    main: bool = models.BooleanField(default=True)

    def __str__(self):

        return f"{self.subject} : De {self.sender} a {self.receiver}"

class TextModel(CommunicationModel):

    """

        Class define model of Text communications.
    
    """

    #Id returned for messagebird
    id_messagebird: str = models.TextField()
    #Telephone receiver the text message
    receiver: str = PhoneNumberField()

    def __str__(self):

        return f"{self.receiver}"

class WhatsappModel(CommunicationModel):

    """

        Class define model of Text communications.
    
    """

    #Id returned for messagebird
    id_messagebird: str = models.TextField()
    #Telephone receiver the whatsapp message
    receiver: str = PhoneNumberField()

    def __str__(self):

        return f"{self.receiver}"

class VoiceCallModel(CommunicationModel):

    """

        Class define model of Text communications.
    
    """

    #Id returned for messagebird
    id_messagebird: str = models.TextField()
    #Telephone receiver the text message
    receiver: str = PhoneNumberField()

    def __str__(self):

        return f"{self.receiver}"
    
class FilesEmailModel(BaseModel):

    """

        Class define model of Email Files communications.
    
    """

    email: int = models.ForeignKey(EmailModel, related_name='files_email', on_delete=models.PROTECT)
    files = models.FileField(upload_to='files/')

class MassiveCommunicationsModel(BaseModel):

    """

        Class define model of Massive Communications.
    
    """

    def clean(self):
        
        MassiveCommunicationsModel.validate_slack_channel(self.slack_channel, self.operator)

    def validate_slack_channel(value: str, operator: int):

        """

            This function, validate slack_channel field of MassiveCommunicationsModel

        """
        if value:

            token: str = operator.slack_token

            client = WebClient(token=token)
            exists: bool = False
            search: bool = True
            cursor: str = ''

            while search:
                response = client.conversations_list(token=token, cursor=cursor)
                if response["ok"]:
                    for channel in response["channels"]:
                            if channel["id"] == value:
                                exists = True
                                pass
                            pass
                    cursor = response.get("response_metadata",{}).get("next_cursor", '')
                    search = True if cursor != '' else False
                else:
                    raise ValidationError({
                        'Slack connection': "Unable to connect to Slack"
                    })

            if not exists:
                raise ValidationError({
                    'Error channel ID': "Channel not found"
                })


    DISJUNTIVE = 'D'
    CONJUNCTIVE = 'C'
    TYPE_FILTERS = (
        (DISJUNTIVE, _('Disjuntive')),
        (CONJUNCTIVE, _('Conjuntive'))
    )

    name: str = models.CharField(max_length=100, null=False, blank=False)
    #Type filters for select services
    type_filters: str = models.CharField(max_length=1, choices=TYPE_FILTERS, default=TYPE_FILTERS[0][0], verbose_name=_('Filters Type'), null=False, blank=False)
    #List of filters to get services
    filters: list = JSONField(null=False, default=list)
    #List of excludes customers
    excludes: list = JSONField(null=False, default=list)
    #List of includes services
    includes: list = JSONField(null=False, default=list)
    unpaid_ballot: bool = models.BooleanField(default=False)
    slack_channel: int = models.CharField(max_length=13, null=True)


    email_event: int = models.ForeignKey( 'EventEmailModel', on_delete=models.PROTECT, null=True)
    whatsapp_event: int = models.ForeignKey( 'EventWhatsappModel', on_delete=models.PROTECT, null=True)
    text_event: int = models.ForeignKey( 'EventTextModel', on_delete=models.PROTECT, null=True)
    call_event: int = models.ForeignKey( 'EventCallModel', on_delete=models.PROTECT, null=True)

    def __str__(self):

        return f"{self.name}"

class CycleCommunicationsModel(BaseModel):

    """

        Class define model of Cycle Communications.
    
    """

    def clean(self):
        
        CycleCommunicationsModel.validate_slack_channel(self.slack_channel, self.operator)

    def validate_slack_channel(value: str, operator):

        """

            This function, validate slack_channel field of CycleCommunicationsModel

        """

        token: str = operator.slack_token

        client = WebClient(token=token)
        exists: bool = False
        search: bool = True
        cursor: str = ''

        while search:
            response = client.conversations_list(token=token, cursor=cursor)
            if response["ok"]:
                for channel in response["channels"]:
                        if channel["id"] == value:
                            exists = True
                            pass
                        pass
                cursor = response.get("response_metadata",{}).get("next_cursor", '')
                search = True if cursor != '' else False
            else:
                raise ValidationError({
                    'Slack connection': "Unable to connect to Slack"
                })

        if not exists:
            raise ValidationError({
                'Error channel ID': "Channel not found"
            })

    def validator_massives_communications(value: Dict[str, Dict[str, str]]):

        """

            This function, validate massives_communications field of CycleCommunicationsModel

        """

        try:

            keys_positions: List[str] = [ int(key) for key in value.keys()]

        except Exception as e:

            raise ValidationError({
                'Key Error' : "The key can only be numerical",
            })

        if type(value) == dict:

            if value != {} and sorted(keys_positions) == list(range(0, max(keys_positions)+1)):

                ascending_datetime = None

                for position in range(0 , len(keys_positions) ):

                    item: Dict[str, str] = value[str(position)]

                    if not set(item.keys()).issubset(['massive', 'datetime']):

                        raise ValidationError({
                            f'Field {position}': "Does no contains one o more of the required fields: massive, datetime",
                        })

                    else:

                        if MassiveCommunicationsModel.objects.filter(ID=item['massive']):

                            try:

                                massive_datetime: str = datetime.strptime(item['datetime'], '%d/%m/%Y %H:%M')

                            except Exception as e:

                                raise ValidationError({
                                    f'Invalid datetime field': "Invalid datetime field format. The correct format is: '%d/%m/%Y %H:%M' ",
                                })

                            if not ascending_datetime :

                                ascending_datetime: str = massive_datetime

                            else:

                                if ascending_datetime >= massive_datetime:

                                    raise ValidationError({
                                        f'Invalid datetime field': "Dates must be ascending",
                                    })

                                else:

                                    ascending_datetime: str == massive_datetime

                        else:

                            raise ValidationError({
                                f'Invalid massive field': f"Invalid massive field id {item['massive']}, don't exist",
                            })
                    item['next_datetime']: str = item['datetime']

            else:

                raise ValidationError({
                    'Invalid order of massives': "The order must be consecutive, starting from 0",
                })

        else:

            raise ValidationError({
                'Invalid type': "Invalid data type",
            })

    ONCE = 'O'
    MINUTES = 'I'
    HOURLY = 'H'
    DAILY = 'D'
    WEEKLY = 'W'
    MONTHLY = 'M'
    QUARTERLY = 'Q'
    YEARLY = 'Y'
    TYPE = (
        (MINUTES, _('Minutos')),
        (ONCE, _('Una vez')),
        (HOURLY, _('Cada Hora')),
        (DAILY, _('Diariamente')),
        (WEEKLY, _('Semanalmente')),
        (MONTHLY, _('Mensualmente')),
        (QUARTERLY, _('Trimestralmente')),
        (YEARLY, _('Anualmente')),
    )

    name = models.CharField(max_length=100, verbose_name=_('Cycle name'), null=False, blank=False)
    type_repeats = models.CharField(max_length=1, choices=TYPE, default=TYPE[5][0], verbose_name=_('Type of repetitions'), null=False, blank=False)
    number_repeats = models.IntegerField(validators=[MinValueValidator(-1)], verbose_name=_('Number of repetitions'), null=False, blank=False)
    massives_communications = JSONField(validators=[validator_massives_communications], verbose_name=_('Massive communications'), null=False, blank=False)
    schedule = models.ForeignKey( Schedule, on_delete=models.SET_NULL, null=True)
    schedule_massive = models.ManyToManyField(Schedule, related_name="massive_schedules")
    slack_channel = models.CharField(max_length=13, null=True)

    slack_thread = models.CharField(verbose_name=_('Slack thread'), null=True, blank=True, max_length=100)
    active = models.BooleanField(default=False)
    test = models.BooleanField(default=False)

    def __str__(self):

        return f"{self.name}"

class TriggerEventModel(BaseModel):

    """

        Class define model of Trigger event.
    
    """

    def name_validator(value: str):

        """

            This function, validate name field of TriggerEventModel

        """

        alpha_and_underscore: str = 'abcdefghijklmnñopqrstuvwxyz_'

        if not reduce( lambda x,y: x and y, map( lambda x: x.lower() in alpha_and_underscore, value ) ):

            raise ValidationError({
                'Invalid name': "The name must be alphanumeric or the character '_'",
            })

    name: str = models.TextField(blank=False, null=False, unique=True, validators=[name_validator])
    description: str = models.TextField()
    
    email_event: int = models.ForeignKey( 'EventEmailModel', on_delete=models.PROTECT, null=True)
    whatsapp_event: int = models.ForeignKey( 'EventWhatsappModel', on_delete=models.PROTECT, null=True)
    text_event: int = models.ForeignKey( 'EventTextModel', on_delete=models.PROTECT, null=True)
    call_event: int = models.ForeignKey( 'EventCallModel', on_delete=models.PROTECT, null=True)

    def __str__(self):

        return f"{self.name}"

class TemplateEmailModel(BaseModel):

    """

        Class define model of Email Templates.
    
    """

    name: str = models.TextField(blank=False, null=False)
    #Subject of email message
    subject: str = models.TextField(blank=True, null=True, default='')
    #Template HTML of email message
    template_html: str = models.TextField(blank=True, null=True, default='{{}}')

    def __str__(self):

        return f"{self.name}"

class BaseNummericTemplates(BaseModel):

    """

        Class define base model of numeric Templates.
    
    """

    name: str = models.TextField(blank=False, null=False)

    #Template of email message
    template: str = models.TextField(blank=True, null=True, default='{{}}')

    class Meta:

        abstract: bool = True

    def __str__(self):

        return f"{self.name}"

class TemplateWhatsappModel(BaseModel):

    """

        Class define model of Whatsapp Templates.
    
    """

    def validator_order(value: str):

        """

            This function, validate name field of TriggerEventModel

        """

        list_fields: List[str] = [
            'activated_on', 'installed_on', 'technology_kind', 'number', 'due_day', 'status', 'composite_address', 'street', 'house_number', 'apartment_number',
            'tower', 'commune', 'location', 'activity', 'ssid', 'allow_auto_cut', 'seen_connected', 'mac_onu', 'network_mismatch', 'price_override', 'uninstalled_on',
            'expired_on', 'node__code', 'node__address', 'node__house_number', 'node__commune', 'node__is_building', 'node__is_optical_fiber', 'node__gpon',
            'node__gepon', 'node__pon_port', 'node__box_location', 'node__splitter_location', 'node__expected_power', 'node__towers', 'node__apartments', 'node__phone',
            'node__olt_config', 'plan__name', 'plan__price', 'plan__category', 'plan__uf', 'plan__active', 'customer__rut', 'customer__name', 'customer__email',
            'customer__phone', 'customer__default_due_day', 'customer__street', 'customer__house_number', 'customer__apartment_number', 'customer__tower',
            'customer__kind',
        ]

        if not type(value) == list:

            ValidationError({
                'Invalid type':'Order field must be type list'
            })

        for field in value:

            if not field in list_fields:

                ValidationError({
                    f'Invalid field {field}':f'{field} is not valid field of template'
                })

    name: str = models.CharField(blank=False, null=False, max_length=100)
    order: List[str] = JSONField(validators=[validator_order],null=True, default=list)
    template: str = models.TextField(default='')

    def __str__(self):

        return f"{self.name}"

class TemplateTextModel(BaseNummericTemplates):

    """

        Class define model of Text templates.
    
    """

    pass

class TemplateCallModel(BaseNummericTemplates):

    """

        Class define model of Voice call templates.
    
    """
    pass


class EventEmailModel(BaseModel):

    """

        Class define model of Event email.
    
    """
    
    #Email origin
    email_origin: str = models.EmailField(null=False, blank=False)
    #Email to send to test
    email_test: str = models.EmailField(null=False, blank=False)
    #Email to response
    email_response: str = models.EmailField(null=False, blank=False)
    #Email to copy
    email_copy: str = models.EmailField(null=True, blank=True)
    #Template of email
    template: int = models.ForeignKey( 'TemplateEmailModel', on_delete=models.PROTECT, null=False)

    def __str__(self):

        return f"De: {self.email_origin} . Responde a: {self.email_response} . Con copia a: {self.email_copy}"

class BaseNumericEvent(BaseModel):

    class Meta:

        abstract: bool = True

    phone_number_test: str = PhoneNumberField()

    def __str__(self):

        return f"Prueba: {self.phone_number_test} . Plantilla: {self.template}"

class EventWhatsappModel(BaseNumericEvent):

    template: int = models.ForeignKey( 'TemplateWhatsappModel', on_delete=models.PROTECT, null=False)

class EventTextModel(BaseNumericEvent):

    template: int = models.ForeignKey( 'TemplateTextModel', on_delete=models.PROTECT, null=False)

class EventCallModel(BaseNumericEvent):

    template: int = models.ForeignKey( 'TemplateCallModel', on_delete=models.PROTECT, null=False)

class EmailTablesModel(BaseModel):

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "subject":"str",
                    "sender":"str",
                    "receiver":"str",
                    "event__pk":"choices",
                    "massive__pk": "choices",
                    "cycle__pk": "choices"
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'created', 'sender', 'receiver', 'channel',
                                            'subject', '_type', 'message'
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class WhatsappTablesModel(BaseModel):

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "receiver":"str",
                    "event__pk":"choices",
                    "massive__pk": "choices",
                    "cycle__pk": "choices"
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'created', 'receiver', 'channel',
                                            '_type', 'message'
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class TextTablesModel(BaseModel):

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "receiver":"str",
                    "event__pk":"choices",
                    "massive__pk": "choices",
                    "cycle__pk": "choices"
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'created', 'receiver', 'channel',
                                            '_type', 'message'
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class VoiceCallTablesModel(BaseModel):

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "receiver":"str",
                    "event__pk":"choices",
                    "massive__pk": "choices",
                    "cycle__pk": "choices"
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'created', 'receiver', 'channel',
                                            '_type', 'message'
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class NotificationModel(models.Model):

    company = models.ForeignKey(
        CompanyModel,
        on_delete=models.CASCADE,
        blank=False
    )
    public: bool = models.BooleanField(default=True)
    message: str = models.TextField(null=False, default=" ")
    agent = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    data = JSONField(null=True, blank=True, default=dict)

class ConversationModel(BaseModel):

    """

        Class define model Company.
    
    """

    def clean(self):
        
        if len(ConversationModel.objects.filter(deleted=False, status=0, customer=self.customer)) > 1:

            raise ValidationError({
                    'Customer error': f"Theres already an active conversation with that customer"
                })

        if self.channel == 2:

            try:
                validate_email(self.customer)
            except ValidationError as e:
                raise ValidationError({'customer': 'Invalid customer email'})

        
    customer: str = models.TextField(null=False, default='')
    CHANNEL_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'Correo'),
        (1, 'Llamada'),
        (2, 'Facebook'),
        (3, 'Instagram'),
        (4, 'WhatsApp')
    )
    channel: int = models.PositiveIntegerField(default=0, choices=CHANNEL_CHOICES)

    services: List[int] = JSONField(default=list)

    STATUS_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'Abierto'),
        (1, 'Cerrado'),
        (2, 'Pendiente'),
    )
    status: int = models.PositiveIntegerField(default=0, choices=STATUS_CHOICES)
    last_message_date = models.DateTimeField(null=True, blank=True)
    last_received_message_date = models.DateTimeField(null=True, blank=True)
    tags: List[str]= JSONField(default=list)
    typify: str = models.IntegerField(null=True)
    agent = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    active: bool = models.BooleanField(default=True)
    last_message = models.TextField(null=False, default='')
    KIND_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'IN'),
        (1, 'OUT'),
    )
    last_message_kind: int = models.PositiveIntegerField(default=0, choices=KIND_CHOICES)
    chat =  models.IntegerField(null=True)

class WhatsAppMessageModel(BaseModel):

    """

        Class define model Company.
    
    """

    message: str = models.TextField(null=False, default='')
    conversation = models.ForeignKey(
        ConversationModel,
        null=True,
        blank=True,
        related_name='agent_user',
        on_delete=models.SET_NULL,
    )
    messagebird_conversation_id: str = models.TextField(null=True, default='')
    KIND_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'IN'),
        (1, 'OUT'),
    )
    kind: int = models.PositiveIntegerField(default=0, choices=KIND_CHOICES)
    STATUS_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'Enviado'),
        (1, 'Leído'),
        (2, 'Fallido'),
        (3, "Recibido")
    )
    status: int = models.PositiveIntegerField(default=0, choices=STATUS_CHOICES)
    media_url: Dict[Any,Any] = JSONField(default=list)
    messagebird_id: str = models.TextField(null=True, default='')

class EmailMessageModel(BaseModel):

    """

        Class define model Company.
    
    """

    message: str = models.TextField(null=False, default='')
    conversation = models.ForeignKey(
        ConversationModel,
        null=True,
        blank=True,
        related_name='conversation',
        on_delete=models.SET_NULL,
    )
    KIND_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'IN'),
        (1, 'OUT'),
    )
    kind: int = models.PositiveIntegerField(default=0, choices=KIND_CHOICES)
    messagebird_conversation_id: str = models.TextField(null=True, default='')
    STATUS_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'Enviado'),
        (1, 'Leído'),
        (2, 'Fallido'),
        (3, "Recibido")
    )
    status: int = models.PositiveIntegerField(default=0, choices=STATUS_CHOICES)
    media_url: Dict[Any,Any] = JSONField(default=list)
    subject: str = models.TextField(null=False, default='')
    is_html: bool = models.BooleanField(null=False, default=True)
    messagebird_id: str = models.TextField(null=True, default='')
    
class ConversationTagModel(BaseModel):

    """

        Class define model Company.
    
    """

    name: str = models.TextField(null=False, default='')

class WhatsAppConversationTablesModel(BaseModel):

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "receiver":"str",
                    "sender": "str",
                    "agent__pk": "choices",
                    "service": "int"
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'created', 'customer',
                                            'last_message_kind', 'service', 'ID', 'agent', 
                                            'last_message', 'last_message_date'
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class EmailConversationTablesModel(BaseModel):

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "receiver":"str",
                    "sender": "str",
                    "agent__pk": "choices",
                    "service": "int"
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'created', 'customer',
                                            'last_message_kind', 'service', 'ID', 'agent', 
                                            'last_message', 'last_message_date'
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)