import ast
import mimetypes
import pdb
import random
import re
import string
import threading
import base64
from datetime import datetime, timedelta
from functools import reduce
from typing import Any, Callable, Dict, List, Union

import pytz
import requests
from common.utilitarian import get_token, matrix_url, next_datetime
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.http import JsonResponse
from django.urls import reverse
from django.utils import timezone
from django_q.models import Schedule
from dynamic_preferences.registries import global_preferences_registry
from jinja2 import Template
from messagebird import Client, Feature
from messagebird.conversation_message import MESSAGE_TYPE_TEXT
from rest_framework.serializers import ValidationError
from sentry_sdk import capture_exception
from slack import WebClient
from validate_email import validate_email

from .models import (ChatModel, ConversationModel, CycleCommunicationsModel, EmailModel,
                     EventCallModel, EventEmailModel, EventTextModel,
                     EventWhatsappModel, FilesEmailModel,
                     MassiveCommunicationsModel, TextModel, VoiceCallModel,
                     WhatsappModel)


def email_task_queue(*args: list, **kwargs: dict) -> None:

    try:

        """

            This function send email

            :param args: List args how list
            :type args: list
            :param kwargs: List of args how dict
            :type kwargs: dict

        """

        # Get ID or pk of Email to send
        ID: int = int(kwargs['kwargs']['ID'])

        email_db: EmailModel = EmailModel.objects.get(ID=ID)
        files: FilesEmailModel = FilesEmailModel.objects.filter(email=email_db)

        mail: EmailMultiAlternatives = EmailMultiAlternatives(
            subject=email_db.subject,
            body=email_db.message,
            from_email=email_db.sender,
            to=[email_db.receiver],
            headers={"Reply-To": email_db.sender},
        )
        mail.content_subtype = 'html'

        # Add files to email
        for _file in files:

            mail.attach(
                _file.files.name,
                _file.files.read(),
                mimetypes.MimeTypes().guess_type(_file.files.path)[0]
            )

        mail.send()

        # Save email and indicates has been sent
        email_db: EmailModel = EmailModel.objects.get(ID=ID)
        email_db.sent: bool = True
        email_db.main: bool = email_db.main
        email_db.save()

    except Exception as e:

        capture_exception(e)


class Massive():

    def __init__(
        self,
        id_massive: int,
        id_cycle: int,
        test_massive: bool = True,
        test_cycle: bool = True,
        finish_cycle: bool = False
    ):

        massive_model: MassiveCommunicationsModel = MassiveCommunicationsModel.objects.get(
            ID=id_massive)

        channels: Dict[str, Any] = {}

        if (massive_model.email_event):

            channels['email'] = massive_model.email_event

        if (massive_model.whatsapp_event):

            channels['whatsapp'] = massive_model.whatsapp_event

        if (massive_model.text_event):

            channels['text'] = massive_model.text_event

        if (massive_model.call_event):

            channels['voice_call'] = massive_model.call_event

        # Cycle instance if is test
        self.cycle_instance: Union[CycleCommunicationsModel, None] = CycleCommunicationsModel.objects.get(
            ID=id_cycle) if id_cycle else None
        self.type_filters: str = massive_model.type_filters
        self.filters: List[list] = massive_model.filters
        self.operator: int = massive_model.operator
        self.reverse_filters_translation: dict = {
            v: k for k, v in self.filters_translation.items()}
        self.chat: ChatModel = None
        self.test_massive: bool = test_massive
        self.test_cycle: bool = test_cycle
        self.finish_cycle: bool = finish_cycle
        self.services_list: List[int] = []
        self.nodes_list: List[int] = []
        self.plans_list: List[int] = []
        self.customers_list: List[int] = []
        self.exclude_services: List[str] = massive_model.excludes
        self.include_services: List[int] = massive_model.includes
        self.massive_instance: MassiveCommunicationsModel = massive_model
        self.services_data: List[int] = []

        self.global_preferences = global_preferences_registry.manager()

        self.enviroment: str = self.global_preferences['general__Enviroment']
        self.user: User = User.objects.get(
            username=self.global_preferences['users__API_username']
        )

        self.send_channel: Dict[str, Any] = channels
        self.is_test: bool = self.global_preferences['general__Enviroment'] != 'production' or test_massive or test_cycle

        self.message_bird_client: Client = Client(
            access_key=self.global_preferences['communications__Message_bird'],
        )
        self.message_bird_client_whatsapp: Client = Client(
            access_key=self.global_preferences['communications__Message_bird'],
            features=[Feature.ENABLE_CONVERSATIONS_API_WHATSAPP_SANDBOX]
        )
        self.User: User = User.objects.get(
            username=self.global_preferences['users__API_username'])
        self.namespace_whatsapp_api: str = self.global_preferences['communications__namespace']

    # Translation Spanish name to filter to api
    filters_translation: Dict[str, str] = {
        'Tipo de tecnologia': 'technology_kind',
        'Numero de servicio': 'number',
        'Dia de pago': 'due_day',
        'Estado': 'status',
        'Ubicacion': 'location',
        'Autocorte': 'allow_auto_cut',
        'Tipo de documento': 'document_type',
        'Fecha de activacion': 'activated_on',
        'Balance de servicio': 'balance',

        'RUT del cliente': 'customer__rut',
        'Actividad del cliente': 'customer__commercial_activity__id',
        'Nombre del cliente': 'customer__name',
        'Correo del cliente': 'customer__email',
        'Dia de pago del cliente': 'customer__default_due_day',
        'Tipo de cliente': 'customer__kind',
        'Balance de cliente': 'customer__balance',

        'Plan del servicio': 'plan__id',
    }
    # Translation of name filter to type
    filters: Dict[str, str] = {
        'Fecha de activacion': 'date',
        'Fecha de instalacion': 'date',
        'Tipo de tecnologia': 'list',
        'Numero de servicio': 'int',
        'Dia de pago': 'int',
        'Estado': 'list',
        'Calle': 'str',
        'Numero de casa': 'str',
        'Numero de apartamento': 'str',
        'Torre': 'str',
        'Tipo de documento': 'list',
        'Ubicacion': 'str',
        'Actividad': 'int',
        'SSID': 'str',
        'Autocorte': 'bool',
        'Visto conectado': 'bool',
        'MAC de ONU': 'str',
        'Desajuste de red': 'bool',
        'Precio anulado': 'int',
        'Fecha de desintalacion': 'date',
        'Fecha de expiracion': 'date',
        'Balance de servicio':'int',

        'RUT del cliente': 'str',
        'Actividad del cliente': 'list',
        'Nombre del cliente': 'str',
        'Correo del cliente': 'str',
        'Telefono del cliente': 'str',
        'Dia de pago del cliente': 'int',
        'Calle del cliente': 'str',
        'Numero de casa del cliente': 'str',
        'Numero de apartamento del cliente': 'str',
        'Torre del cliente': 'str',
        'Tipo de cliente': 'list',
        'Balance de cliente':'int',

        'Plan del servicio': 'list',
        'Precio de plan': 'int',
        'Categoria de plan': 'list',
        'UF plan': 'bool',
        'Plan activo': 'bool',

        'Codigo del nodo': 'str',
        'Estado del nodo': 'int',
        'Direccion del nodo': 'str',
        'Calle del nodo': 'str',
        'Numero de casa del nodo': 'str',
        'Comuna del nodo': 'str',
        'Nodo en edificion': 'bool',
        'Nodo de fibra': 'bool',
        'Nodo GPON': 'bool',
        'Nodo GEPON': 'bool',
        'Nodo PON PORT': 'str',
        'Ubicacion de caja del nodo': 'str',
        'Ubicacion splitter del nodo': 'str',
        'Energia esperada del nodo': 'str',
        'Torres del nodo': 'int',
        'Apartamentos del nodo': 'int',
        'Telefono del nodo': 'str',
        'Configuracion de OLT al nodo': 'str',
    }
    # Translation of comparison value to url filter
    type_translation: Dict[str, str] = {
        'igual': 'equal',
        'mayor': 'gt',
        'mayor_igual': 'gte',
        'menor': 'lt',
        'menor_igual': 'lte',
        'distinto': 'different',
        'similar': 'iexact',
        'contiene': 'icontains',
        'inicia_con': 'istartswith',
        'termina_con': 'iendswith',
        'año': 'year',
        'mes': 'month',
        'dia': 'day',
        'dia_semana': 'week_day',
        'semana': 'week',
        'trimestre': 'quarter',
    }
    # Get lisr filters of type
    type_filters: Dict[str, List[str]] = {
        'int': ['igual', 'mayor', 'mayor_igual', 'menor', 'menor_igual',  'distinto'],
        'str': ['igual', 'similar', 'contiene', 'inicia_con', 'termina_con', 'distinto'],
        'date': ['igual', 'año', 'mes', 'dia', 'dia_semana', 'semana', 'trimestre', 'mayor', 'mayor_igual',
                 'menor', 'menor_igual', 'distinto'],
        'bool': ['igual', 'distinto'],
        'list': ['igual', 'distinto'],
    }

    def ticket_send_email(self, sesion, message: dict) -> None:
        """

            This function send email to customer

            :param self: This param is instace of Class Massive
            :type self: Massive
            :param sesion: Session user
            :type sesion: Session
            :param message: Dict with data of service
            :type message: dict

        """

        # Check is valid email
        if validate_email(message['receiver']):

            email: EmailModel = EmailModel()

            email.outbox: bool = True
            email.creator: User = self.user
            email.updater: User = self.user
            email.receiver: str = message['email_test'] if self.is_test else message['receiver']
            email.subject: str = message['subject']
            email.sender: str = message['email_origin']
            email.message: str = message['message']
            email.operator: int = self.massive_instance.operator
            if self.cycle_instance:
                email.cycle = self.cycle_instance
            else:
                email.massive = self.massive_instance
            email.save()

            mail = EmailMultiAlternatives(
                subject=email.subject,
                body=email.message,
                from_email=email.sender,
                to=[email.receiver],
                headers={"Reply-To": message['email_response']},
                bcc=[message['email_copy']] if message['email_copy'] else []
            )
            mail.content_subtype = 'html'
            mail.send()

    def ticket_send_ws(self, message: dict) -> None:
        """

            This function send sms communication

            :param self: This param is instace of Class Massive
            :type self: Massive
            :param message: This param contains data of message
            :type message: Dict

        """

        # Channel to send
        channel: str = self.global_preferences['communications__whatsapp_channels']
        token: str = self.global_preferences['communications__Message_bird']

        phone: str = message['phone_number_test'] if self.is_test else message['customer__phone']
        phone: str = phone.replace('+', '')
        # Create whatsapp model instance
        #
        # Get message
        whatsapp_model: WhatsappModel = WhatsappModel()
        whatsapp_model.message: str = message['message']
        whatsapp_model.receiver: str = phone
        whatsapp_model.outbox: bool = True
        whatsapp_model.creator: User = self.User
        whatsapp_model.updater: User = self.User
        whatsapp_model.operator: int = self.massive_instance.operator
        #
        ############################

        message_whatsapp = requests.post(
            url='https://conversations.messagebird.com/v1/conversations/start',
            headers={
                'Authorization': f'AccessKey {token}',
            },
            json={
                "to": phone,
                "type": "hsm",
                "channelId": f"{channel}",
                "content": {
                    "hsm": {
                        "namespace": self.namespace_whatsapp_api,
                        "templateName": message['template_name'],
                        "language": {
                            "policy": "deterministic",
                            "code": "es"
                        },
                        "params": message['params']
                    },
                }
            }
        )

        if message_whatsapp.status_code == 201:

            whatsapp_model.id_messagebird: int = message_whatsapp.json()['id']
            whatsapp_model.save()

    def ticket_send_call_voice(self, message: dict) -> None:
        """

            This function send call voice communication

            :param self: This param is instace of Class Massive
            :type self: Massive
            :param message: This param contains data of message
            :type message: Dict

        """

        phone: str = message['phone_number_test'] if self.is_test else message['customer__phone']

        # Create email model instance
        #
        voice_call_model: VoiceCallModel = VoiceCallModel()
        voice_call_model.message: str = message['message']
        voice_call_model.receiver: str = phone
        voice_call_model.outbox: bool = True
        voice_call_model.creator: User = self.User
        voice_call_model.updater: User = self.User
        voice_call_model.operator: int = self.massive_instance.operator
        #
        ############################

        # Send communications
        message = self.message_bird_client.voice_message_create(
            recipients=[phone.replace('+', '')],
            body=voice_call_model.message,
            params={
                'language': 'es-us',
                'voice': 'female',
                'ifMachine': 'continue',
                'repeat': 1,
            },
        )

        voice_call_model.id_messagebird: int = message.id
        voice_call_model.save()

    def ticket_send_sms(self, message: dict) -> None:
        """

            This function send sms communication

            :param self: This param is instace of Class Massive
            :type self: Massive
            :param message: This param contains data of message
            :type message: Dict

        """

        phone: str = message['phone_number_test'] if self.is_test else message['customer__phone']

        # Create text model instance
        #
        text_model: TextModel = TextModel()
        text_model.message: str = message['message']
        text_model.receiver: str = phone
        text_model.outbox: bool = True
        text_model.creator: User = self.User
        text_model.updater: User = self.User
        text_model.operator: int = self.massive_instance.operator
        #
        ############################

        # Send communications
        message = self.message_bird_client.message_create(
            originator='Banda Ancha',
            recipients=[phone.replace('+', '')],
            body=text_model.message,
        )

        text_model.id_messagebird: int = message.id
        text_model.save()

    def slack_notifications(self) -> None:
        """

            This function send Slack notification

            :param self: This param is instace of Class Massive
            :type self: Massive

        """

        def channels(massive: MassiveCommunicationsModel) -> str:

            channels: List[str] = []

            if massive.email_event:

                channels.append('Correo')

            if massive.whatsapp_event:

                channels.append('Whatsapp')

            if massive.text_event:

                channels.append('Texto')

            if massive.call_event:

                channels.append('Llamada')

            return ','.join(channels)

        channels: str = channels(self.massive_instance)
        
        token: str = self.massive_instance.operator.slack_token

        client = WebClient(token=token)

        count_services: int = len(self.services_data)
        massive_name: str = self.massive_instance.name

        execution_date = datetime.now().strftime("%H:%M")

        blocks2: List[Dict[str, Any]] = [
            {
                "type": "divider"
            },
            {
                "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":iris: *Iris* Comunicación Masiva"
                        }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f":heavy_check_mark:Se ha enviado la comunicación masiva: *{massive_name}*"
                        }
            },
            {
                "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"*Con un total de {count_services} servicios* \n:outbox_tray:*A través de*: {channels} \n:clock2:*A las *:{execution_date}"
                        }
            },
            {
                "type": "divider"
            }
        ]

        blocks1: List[Dict[str, Any]] = [
            {
                "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":iris: *Iris* Comunicación Masiva"
                        }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f":heavy_check_mark:Ha finalizado el envío de comunicaciónes programadas para el actual periodo del ciclo: *{massive_name}*"
                        }
            },
            {
                "type": "divider"
            }
        ]

        if self.test_massive:

            client.chat_postMessage(
                token=token,
                channel=self.massive_instance.slack_channel,
                blocks=blocks2,
                text="Comunicación masiva enviada"
            )

        else:

            if self.cycle_instance != None:

                client.chat_postMessage(
                    channel=self.cycle_instance.slack_channel,
                    blocks=blocks2,
                    thread_ts=self.cycle_instance.slack_thread,
                    text="Comunicación masiva enviada"
                )
            else:
                client.chat_postMessage(
                token=token,
                channel=self.massive_instance.slack_channel,
                blocks=blocks2,
                text="Comunicación masiva enviada"
            )

            if self.finish_cycle:

                client.chat_postMessage(
                    channel=self.cycle_instance.slack_channel,
                    blocks=blocks1,
                    thread_ts=self.cycle_instance.slack_thread,
                    text="Ciclo finalizado"
                )

                Schedule.objects.filter(
                    id__in=self.cycle_instance.schedule_massive.values_list('id')).delete()
                self.cycle_instance.schedule_massive.clear()
                self.cycle_instance.schedule.delete()

    def get_matrix_data(self) -> Dict[str, Any]:
           
        data: Dict[str, Any] = {
            "filters": self.filters,
            "includes": self.include_services,
            "excludes": self.exclude_services,
            "unpaid_ballot": self.massive_instance.unpaid_ballot,
            "criterion": self.massive_instance.type_filters
        }

        matrix_url: str = self.global_preferences['general__matrix_domain'] + 'services-filters/'

        response = requests.get(
            url=matrix_url,
            headers={
                "Authorization": self.global_preferences['matrix__matrix_token']
            },
            json=data,
            verify=False,
        )

        if response.status_code != 200:

            raise ValidationError({
                "Error matrix": "Error matrix filter"
            })

        self.services_data: List[Dict[str, Any]] = response.json()

        months: Dict[int, str] = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }

        weekdays: Dict[int, str] = {
            0: "Lunes",
            1: "Martes",
            2: "Miercoles",
            3: "Jueves",
            4: "Viernes",
            5: "Sabado",
            6: "Domingo",
        }

        current_datetime: datetime = datetime.now()
        

        for service in self.services_data:

            if 'service__number' in service:
                service['service__number_64'] = str(base64.b64encode(bytes(str(service['service__number']), 'utf-8')), 'utf-8')

            if 'customer__id' in service:
                service['customer__id_64'] = str(base64.b64encode(bytes(str(service['customer__id']), 'utf-8')), 'utf-8')

            service['current__month'] = months[current_datetime.month]
            service['current__year'] = current_datetime.year
            service['current__day'] = current_datetime.day
            service['current__week_day'] = weekdays[current_datetime.weekday()]
            service['current__datetime'] = current_datetime.strftime("%d/%m/%Y")
    
    def massive_message(self) -> None:
        """

            This function send massive communication

            :param self: This param is instace of Class Massive
            :type self: Massive

        """

        # Session user
        sesion = requests.Session()
        sesion.headers.update({"Authorization": get_token()})

        self.get_matrix_data()

        email_event: EventEmailModel = self.massive_instance.email_event
        whatsapp_event: EventWhatsappModel = self.massive_instance.whatsapp_event
        text_event: EventTextModel = self.massive_instance.text_event
        voice_call_event: EventCallModel = self.massive_instance.call_event

        for data in self.services_data:

            try:

                # Check if exist event email
                if email_event:

                    # Render message to customer with data
                    message: Dict[str, Any] = {}
                    message['message']: str = Template(
                        email_event.template.template_html).render(data)
                    message['subject']: str = email_event.template.subject
                    message['receiver']: str = data['customer__email']
                    message['email_origin']: str = email_event.email_origin
                    message['email_test']: bool = email_event.email_test
                    message['email_response']: str = email_event.email_response
                    message['email_copy']: str = email_event.email_copy

                    self.ticket_send_email(
                        sesion=sesion,
                        message=message,
                    )

            except Exception as e:

                capture_exception(e)

            # Check if exist whatsapp email
            if whatsapp_event:

                try:

                    # Render message to customer with data
                    message: Dict[str, Any] = {}
                    message['template_name']: str = whatsapp_event.template.name
                    message['message']: str = Template(
                        whatsapp_event.template.template).render(data)
                    message['phone']: str = data['customer__phone']
                    message['phone_number_test']: str = str(
                        whatsapp_event.phone_number_test)
                    message['params']: List[str] = [
                        {'default': data[param]} for param in whatsapp_event.template.order]

                    self.ticket_send_ws(
                        message=message,
                    )

                except Exception as e:

                    capture_exception(e)

            # Check if exist text email
            if text_event:

                try:

                    # Render message to customer with data
                    message: Dict[str, Any] = {}
                    message['message']: str = Template(
                        text_event.template.template).render(data)
                    message['phone']: str = data['customer__phone']
                    message['phone_number_test']: str = str(
                        text_event.phone_number_test)

                    self.ticket_send_sms(
                        message=message,
                    )

                except Exception as e:

                    capture_exception(e)

            # Check if exist voice call email
            if voice_call_event:

                try:

                    # Render message to customer with data
                    message: Dict[str, Any] = {}
                    message['message']: str = Template(
                        voice_call_event.template.template).render(data)
                    message['phone']: str = data['customer__phone']
                    message['phone_number_test']: str = str(
                        voice_call_event.phone_number_test)

                    self.ticket_send_call_voice(
                        message=message,
                    )

                except Exception as e:

                    capture_exception(e)

        # Send Slack notification
        self.slack_notifications()


def init_cycle_communications_message(*args, **kwargs):
    """

        This function init communication cycle

        :param args: This param is args list
        :type args: list
        :param kwargs: This param is args dict
        :type kwargs: dict

    """

    if 'kwargs' in kwargs.keys():

        pk_cycle: int = kwargs['kwargs']['pk_cycle']
        test_cycle: bool = kwargs['kwargs']['test_cycle']

    else:

        pk_cycle: int = kwargs['pk_cycle']
        test_cycle: bool = kwargs['test_cycle']

    # Get cycle instance
    cycle_communications: CycleCommunicationsModel = CycleCommunicationsModel.objects.get(
        ID=pk_cycle)


    # Delete references to massives
    Schedule.objects.filter(
        id__in=cycle_communications.schedule_massive.values_list('id')).delete()
    cycle_communications.schedule_massive.clear()

    # Count amount of massives
    length_massive = len(cycle_communications.massives_communications.items())

    # Get all massives
    massives: Dict[str, str] = cycle_communications.massives_communications
    # Get kind of reps
    type_cycle: str = cycle_communications.type_repeats

    for massive_position in range(0, length_massive):

        # Get massive settings
        massive_data: Dict[str, str] = massives[str(massive_position)]
        # Get datetime of massive communicatios
        datetime_start_str: str = massive_data['next_datetime']
        # Transform datetime str to datetime python object
        datetime_start_datetime: datetime = datetime.strptime(
            datetime_start_str, '%d/%m/%Y %H:%M')

        schedule_object: Schedule = Schedule.objects.create(
            func='communications.queue.init_massive_communications_message',
            name=''.join(random.choice(
                string.ascii_uppercase + string.digits) for _ in range(20)),
            schedule_type='D',
            repeats=1,
            next_run=datetime_start_datetime + timedelta(seconds=0),
            kwargs={
                'pk_cycle': int(cycle_communications.ID),
                'pk_massive': int(massive_data['massive']),
                'test_massive': False,
                'test_cycle': test_cycle,
                'finish_cycle': massive_position == length_massive - 1 and not cycle_communications.schedule.repeats,
            },
            minutes=5,
        )
        # Add schedule to cycle instance
        cycle_communications.schedule_massive.add(schedule_object)

        # Calculate next date to run massive
        massives[str(massive_position)]['next_datetime']: str = next_datetime(
            type_cycle, datetime_start_datetime).strftime('%d/%m/%Y %H:%M')

    # Set update datetime of massives
    cycle_communications.massives_communications: Dict[str, str] = massives

    global_preferences = global_preferences_registry.manager()
    # Get slack token
    token: str = cycle_communications.operator.slack_token

    # Instance of Slack client
    client = WebClient(token=token)
    # Channel of Slack
    channel: str = cycle_communications.slack_channel
    # Datetime start execution
    str_startdatetime: str = cycle_communications.massives_communications['0']['datetime']
    # Cycle name
    name_cycle: str = cycle_communications.name

    blocks: List[Dict[str, Any]] = [
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": f"Hola, se da inicio al ciclo de comunicaciones masivas llamado: *{name_cycle}* .\n\n La fecha de inicio para: *{str_startdatetime}* \n\n *Se han definido las siguientes comunicaciones masivas:*"
            }
        },
    ]

    masiveList: str = ""
    for massive_key in cycle_communications.massives_communications.keys():

        massive_id: str = cycle_communications.massives_communications[massive_key]['massive']
        name_massive: str = MassiveCommunicationsModel.objects.get(
            ID=int(massive_id)).name
        run_datetime: str = cycle_communications.massives_communications[massive_key]['datetime']

        masiveList = masiveList + f"• {name_massive}. :clock2:Fecha de ejecución *:{run_datetime}* \n"

       
       
    blocks.append(
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": masiveList
            },
        }
    )

    response = client.chat_postMessage(
        channel=channel,
        blocks=blocks,
        text="Inicio del ciclo de comunicaciones"
    )

    cycle_communications.slack_thread: float = response.data['ts']
    cycle_communications.save()


def finish_cycle_communications_message(*args, **kwargs):
    """

        This function finish communication cycle

        :param args: This param is args list
        :type args: list
        :param kwargs: This param is args dict
        :type kwargs: dict

    """

    task: Dict[str, Any] = args[0].kwargs

    pk_cycle: int = task['pk_cycle']

    # Instance of cycle
    cycle_communications: CycleCommunicationsModel = CycleCommunicationsModel.objects.get(
        ID=pk_cycle)

    # Set active cycle
    if cycle_communications.schedule.repeats == 0:

        cycle_communications.schedule.delete()
        cycle_communications.schedule: Union[Schedule, None] = None
        cycle_communications.active: bool = False
        cycle_communications.test: bool = False
        cycle_communications.save()


def init_massive_communications_message(*args, **kwargs):
    """

        This function init communication massive

        :param args: This param is args list
        :type args: list
        :param kwargs: This param is args dict
        :type kwargs: dict

    """

    if 'kwargs' in kwargs.keys():

        pk_cycle: int = kwargs['kwargs']['pk_cycle']
        pk_massive: int = kwargs['kwargs']['pk_massive']
        test_massive: bool = kwargs['kwargs']['test_massive']
        test_cycle: bool = kwargs['kwargs']['test_cycle']
        finish_cycle: bool = kwargs['kwargs']['finish_cycle']

    else:

        pk_cycle: int = kwargs['pk_cycle']
        pk_massive: int = kwargs['pk_massive']
        test_massive: bool = kwargs['test_massive']
        test_cycle: bool = kwargs['test_cycle']
        finish_cycle: bool = kwargs['finish_cycle']

    def threading_massive(
        id_massive: int,
        id_cycle: int,
        test_massive: bool = True,
        test_cycle: bool = True,
        finish_cycle: bool = False

    ):
            
        try:

            
            Massive(
                id_massive=id_massive,
                id_cycle=id_cycle,
                test_massive=test_massive,
                test_cycle=test_cycle,
                finish_cycle=finish_cycle,
            ).massive_message()

        except Exception as e:

            capture_exception(e)

    # Create threading
    threading.Thread(
        target=threading_massive,
        args=[pk_massive, pk_cycle, test_massive, test_cycle, finish_cycle]
    ).start()

def update_conversations(*args, **kwargs):

    try:
        ConversationModel.objects.filter(channel=1, deleted=False, active=True, last_received_message_date__lte=datetime.now() - timedelta(hours=24)).update(active=False, status=1)
    except:
        pass