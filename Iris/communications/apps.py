from django.apps import AppConfig


class CommunicationsConfig(AppConfig):

    """

        Class define documentation app Communications.
    
    """

    name: str = 'communications'
