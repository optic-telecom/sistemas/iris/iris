from typing import List, Dict, Any

from django.db.models import Q
from django.db.models.query import QuerySet
from dateutil.relativedelta import relativedelta
from common.dataTables import BaseDatatables
from django.http import JsonResponse
from rest_framework.serializers import ValidationError
from datetime import datetime, timedelta

from .models import (ConversationModel, CycleCommunicationsModel, EmailConversationTablesModel,
                     MassiveCommunicationsModel, TemplateCallModel,
                     TemplateEmailModel, TemplateTextModel,
                     TemplateWhatsappModel, TriggerEventModel, BaseModel, EmailModel, EmailTablesModel, WhatsAppConversationTablesModel, 
                     WhatsappTablesModel, WhatsappModel, TextModel, TextTablesModel, VoiceCallModel, VoiceCallTablesModel)


class EmailDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "receiver", "sender", "created", "subject", "_type", "event", "massive", "cycle"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "sender": "str",
        "receiver": "str",
        "subject": "str",
        "event__pk": "choices",
        "massive__pk": "choices",
        "cycle__pk":"choices"
    }

    model: BaseModel = EmailModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            qs: QuerySet = qs.filter(
                Q( receiver__icontains=search ) | Q( sender__icontains=search ) | Q(subject__icontains=search )
            ).select_related('event','massive','cycle')

        return qs
        
    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_email: QuerySet = EmailTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_email.filters

        data_email: QuerySet = super().get_filtered_queryset()

        if instance_tables_email.last_time_type and instance_tables_email.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_email: QuerySet = data_email.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_email.last_time_type] * instance_tables_email.last_time) )

        return data_email

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        new_messages = instance.chat.new_messages if instance.chat else 0

        return {
            "sender": instance.sender,
            "receiver": instance.receiver,
            "subject": instance.subject,
            "ID": instance.ID,
            "created": instance.created,
            "_type": 'IN' if instance._type == 0 else 'OUT',
            "channel": instance.event.name if instance.event else  instance.massive.name if instance.massive else instance.cycle.name if instance.cycle else None,
            'message': instance.message,
            "chat": instance.chat.ID if instance.chat else None,
            "new_messages": new_messages
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_tables_email: QuerySet = EmailTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"created",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "sender": "str",
            "receiver": "str",
            "subject": "str",
            "_type": "str",
            "event": "str",
            "massive": "str",
            "cycle": "str",
            "created": "datetime",

        }

        columns: Dict[str, Dict[str, Any]] = {
            'created':{
                "sortable": True,
                "name":"Fecha de creación",
                "width":100,
                "fixed":None
            },
            'sender':{
                "sortable": True,
                "name":"Emisor",
                "width":100,
                "fixed":None
            },
            'receiver':{
                "sortable": True,
                "name":"Receptor",
                "width":100,
                "fixed":None
            },
            'subject':{
                "sortable": True,
                "name":"Asunto",
                "width":100,
                "fixed":None
            },
            'message':{
                "sortable": False,
                "name":"Mensaje",
                "width":100,
                "fixed":None
            },
            '_type':{
                "sortable": True,
                "name":"Tipo",
                "width":100,
                "fixed":None
            },
            'channel': {
                "sortable": True,
                "name":"Canal",
                "width":100,
                "fixed":None
            }
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_email.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1
        
        data_struct["columns"] = data_struct_columns

        data_struct["filters"] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Emisor":{
                "type":"str",
                "name":"sender",
            },
            "Receptor":{
                "type":"str",
                "name":"receiver",
            },
            "Asunto":{
                "type":"str",
                "name":"subject",
            },
            "Evento":{
                "type":"choices",
                "name":"event__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/trigger_event/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Masivo":{
                "type":"choices",
                "name":"massive__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/massive/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Ciclo":{
                "type":"choices",
                "name":"cycle__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/cycle/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
        }

        return data_struct

    def get_initial_queryset(self) -> QuerySet:
        
        """

            This function, filter queryset by operator

        """

        qs: BaseModel = self.model.objects.all().exclude(deleted=True, main=False)
        
        #Select instances of QuerySet of indicates operator
        operator: str = self.request.data.get('operator', '')

        if operator.isnumeric() and operator != '0':  

            #Exclude by Operator
            qs: BaseModel = qs.filter( operator=int( operator ), deleted=False ).only('sender','receiver','subject','ID','created','_type','message','event','massive','cycle')

        else:

            ValidationError({
                "Error operator": "Don't send operator or invalid value",
            })

        return qs

    def get_ordered_queryset(self, qs: QuerySet) -> QuerySet:

        """

            This function, order queryset

        """

        if self.COLUMN_ORDER_FIELD and self.COLUMN_ORDER_TYPE:

            column_order: str = self.COLUMN_ORDER_FIELD

            if self.COLUMN_ORDER_TYPE == "desc":

                column_order: str = f"-{column_order}"

            qs: QuerySet = qs.order_by(column_order, '-chat__last_message')

        return qs

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """
        
        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )

        qs_size: int = len(qs)

        if qs_size > 0:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]
            
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )

        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

class TemplateTextDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
        "template": "str"
    }


    model: BaseModel = TemplateTextModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search) | Q(template__icontains=search) | Q(creator__username__icontains=search))

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "template": instance.template,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "template": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Plantilla":{
                "field": "template",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":300,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Plantilla":{
                "type":"str",
                "name":"template"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class TemplateCallDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = TemplateCallModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search) | Q(template__icontains=search))

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "template": instance.template,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "template": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Plantilla":{
                "field": "template",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":300,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class TemplateEmailDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = TemplateEmailModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search) | Q(template_html__icontains=search) | Q(subject__icontains=search))

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "template": instance.template_html,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "template": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class TemplateWhatsappDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = TemplateWhatsappModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search) | Q(template__icontains=search))

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "template": instance.template,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "template": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Plantilla":{
                "field": "template",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":300,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class TriggerEventDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name","description", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = TriggerEventModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search) | Q(description__icontains=search))

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        channels: List[str] = []

        if instance.email_event:

            channels.append('email')

        if instance.whatsapp_event:

            channels.append('whatsapp')

        if instance.text_event:

            channels.append('text')

        if instance.call_event:

            channels.append('call')

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "description": instance.description,
            "channels": channels
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":1000
        }

        data_struct["fields"] = {
            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "description": "str",
            "channels": "list",
        }

        data_struct["columns"] = {
            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Descripción":{
                "field": "description",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":300,
                "fixed":None
            },
            "Canales":{
                "field": "channels",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":200,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 6,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class MassiveCommunicationsModelDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = MassiveCommunicationsModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            qs: QuerySet = qs.filter( name__icontains=search )

            if search.isdigit():

                qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        channels: List[str] = []

        if instance.email_event:

            channels.append('email')

        if instance.whatsapp_event:

            channels.append('whatsapp')

        if instance.text_event:

            channels.append('text')

        if instance.call_event:

            channels.append('call')

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "slack_channel": instance.slack_channel,
            "channels": channels
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":1000
        }

        data_struct["fields"] = {
            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "slack_channel": "str",
            "channels": "list",
        }

        data_struct["columns"] = {
            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Canales":{
                "field": "channels",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":200,
                "fixed":None
            },
            "Activar":{
                "field": "ID_active",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Prueba":{
                "field": "ID",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 6,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 7,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class CycleCommunicationsModelDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator", "type_repeats"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
        "type_repeats":"choices",
        "active": "bool",
    }

    model: BaseModel = CycleCommunicationsModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            search_int: int = int(search)

            qs: QuerySet = qs.filter( Q( ID=search_int ) | Q( number_repeats=search_int ) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "slack_channel": instance.slack_channel,
            "active": instance.active,
            "test": instance.test,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":800
        }

        data_struct["fields"] = {
            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "slack_channel": "int",
            "active": "bool",
            "test": "bool",
        }

        data_struct["columns"] = {
            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Estado":{
                "field": "active",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":"right"
            },
            "Prueba":{
                "field": "test",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 6,
                "width":100,
                "fixed":"right"
            },
        }

        data_struct["filters"] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Estado":{
                "type":"bool",
                "name":"active"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }
        }

        return data_struct

class EmailTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]


    model: BaseModel = EmailTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs
        

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class WhatsappTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]


    model: BaseModel = WhatsappTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs
        

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class WhatsappDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "receiver", "created", "_type", "event", "massive", "cycle"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "receiver": "str",
        "event__pk": "choices",
        "massive__pk": "choices",
        "cycle__pk":"choices"
    }

    model: BaseModel = WhatsappModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            qs: QuerySet = qs.filter(
                Q( receiver__icontains=search )
            ).select_related('event','massive','cycle')

        return qs
        
    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_whatsapp: QuerySet = WhatsappTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_whatsapp.filters

        data_whatsapp: QuerySet = super().get_filtered_queryset()

        if instance_tables_whatsapp.last_time_type and instance_tables_whatsapp.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_whatsapp: QuerySet = data_whatsapp.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_whatsapp.last_time_type] * instance_tables_whatsapp.last_time) )

        return data_whatsapp

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "receiver": instance.receiver.raw_input,
            "ID": instance.ID,
            "created": instance.created,
            "_type": 'IN' if instance._type == 0 else 'OUT',
            "channel": instance.event.name if instance.event else  instance.massive.name if instance.massive else instance.cycle.name if instance.cycle else None,
            'message': instance.message
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_tables_whatsapp: QuerySet = WhatsappTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"created",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "sender": "str",
            "receiver": "str",
            "subject": "str",
            "_type": "str",
            "event": "str",
            "massive": "str",
            "cycle": "str",
            "created": "datetime",

        }

        columns: Dict[str, Dict[str, Any]] = {
            'created':{
                "sortable": True,
                "name":"Fecha de creación",
                "width":100,
                "fixed":None
            },
            'receiver':{
                "sortable": True,
                "name":"Receptor",
                "width":100,
                "fixed":None
            },
            'message':{
                "sortable": False,
                "name":"Mensaje",
                "width":100,
                "fixed":None
            },
            '_type':{
                "sortable": True,
                "name":"Tipo",
                "width":100,
                "fixed":None
            },
            'channel': {
                "sortable": True,
                "name":"Canal",
                "width":100,
                "fixed":None
            }
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_whatsapp.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1
        
        data_struct["columns"] = data_struct_columns

        data_struct["filters"] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Receptor":{
                "type":"str",
                "name":"receiver",
            },
            "Evento":{
                "type":"choices",
                "name":"event__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/trigger_event/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Masivo":{
                "type":"choices",
                "name":"massive__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/massive/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Ciclo":{
                "type":"choices",
                "name":"cycle__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/cycle/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
        }

        return data_struct

    def get_initial_queryset(self) -> QuerySet:
        
        """

            This function, filter queryset by operator

        """

        qs: BaseModel = self.model.objects.all().exclude(deleted=True)
        
        #Select instances of QuerySet of indicates operator
        operator: str = self.request.data.get('operator', '')

        if operator.isnumeric() and operator != '0':  

            #Exclude by Operator
            qs: BaseModel = qs.filter( operator=int( operator ), deleted=True ).only('receiver','ID','created','_type','message','event','massive','cycle')

        else:

            ValidationError({
                "Error operator": "Don't send operator or invalid value",
            })

        return qs

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """
        
        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )

        qs_size: int = len(qs)

        if qs_size > 0:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]
            
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )

        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

class TextTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]

    model: BaseModel = TextTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs
        

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class TextDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "receiver", "created", "_type", "event", "massive", "cycle"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "receiver": "str",
        "event__pk": "choices",
        "massive__pk": "choices",
        "cycle__pk":"choices"
    }

    model: BaseModel = TextModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            qs: QuerySet = qs.filter(
                Q( receiver__icontains=search )
            ).select_related('event','massive','cycle')

        return qs
        
    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_text: QuerySet = TextTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_text.filters

        data_text: QuerySet = super().get_filtered_queryset()

        if instance_tables_text.last_time_type and instance_tables_text.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_text: QuerySet = data_text.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_text.last_time_type] * instance_tables_text.last_time) )

        return data_text

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "receiver": instance.receiver.raw_input,
            "ID": instance.ID,
            "created": instance.created,
            "_type": 'IN' if instance._type == 0 else 'OUT',
            "channel": instance.event.name if instance.event else  instance.massive.name if instance.massive else instance.cycle.name if instance.cycle else None,
            'message': instance.message
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_tables_text: QuerySet = TextTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"created",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "sender": "str",
            "receiver": "str",
            "subject": "str",
            "_type": "str",
            "event": "str",
            "massive": "str",
            "cycle": "str",
            "created": "datetime",

        }

        columns: Dict[str, Dict[str, Any]] = {
            'created':{
                "sortable": True,
                "name":"Fecha de creación",
                "width":100,
                "fixed":None
            },
            'receiver':{
                "sortable": True,
                "name":"Receptor",
                "width":100,
                "fixed":None
            },
            'message':{
                "sortable": False,
                "name":"Mensaje",
                "width":100,
                "fixed":None
            },
            '_type':{
                "sortable": True,
                "name":"Tipo",
                "width":100,
                "fixed":None
            },
            'channel': {
                "sortable": True,
                "name":"Canal",
                "width":100,
                "fixed":None
            }
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_text.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1
        
        data_struct["columns"] = data_struct_columns

        data_struct["filters"] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Receptor":{
                "type":"str",
                "name":"receiver",
            },
            "Evento":{
                "type":"choices",
                "name":"event__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/trigger_event/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Masivo":{
                "type":"choices",
                "name":"massive__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/massive/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Ciclo":{
                "type":"choices",
                "name":"cycle__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/cycle/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
        }

        return data_struct

    def get_initial_queryset(self) -> QuerySet:
        
        """

            This function, filter queryset by operator

        """

        qs: BaseModel = self.model.objects.all().exclude(deleted=True)
        
        #Select instances of QuerySet of indicates operator
        operator: str = self.request.data.get('operator', '')

        if operator.isnumeric() and operator != '0':  

            #Exclude by Operator
            qs: BaseModel = qs.filter( operator=int( operator ), deleted=True ).only('receiver','ID','created','_type','message','event','massive','cycle')

        else:

            ValidationError({
                "Error operator": "Don't send operator or invalid value",
            })

        return qs

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """
        
        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )
        
        qs_size: int = len(qs)

        if qs_size > 0:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]
            
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )

        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

class VoiceCallTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]

    model: BaseModel = VoiceCallTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs
        

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class VoiceCallDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "receiver", "created", "_type", "event", "massive", "cycle"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "receiver": "str",
        "event__pk": "choices",
        "massive__pk": "choices",
        "cycle__pk":"choices"
    }

    model: BaseModel = VoiceCallModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            qs: QuerySet = qs.filter(
                Q( receiver__icontains=search )
            ).select_related('event','massive','cycle')

        return qs
        
    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_call: QuerySet = VoiceCallTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_call.filters

        data_voice: QuerySet = super().get_filtered_queryset()

        if instance_tables_call.last_time_type and instance_tables_call.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_voice: QuerySet = data_voice.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_call.last_time_type] * instance_tables_call.last_time) )

        return data_voice

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "receiver": instance.receiver.raw_input,
            "ID": instance.ID,
            "created": instance.created,
            "_type": 'IN' if instance._type == 0 else 'OUT',
            "channel": instance.event.name if instance.event else  instance.massive.name if instance.massive else instance.cycle.name if instance.cycle else None,
            'message': instance.message
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_tables_call: QuerySet = VoiceCallTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"created",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "sender": "str",
            "receiver": "str",
            "subject": "str",
            "_type": "str",
            "event": "str",
            "massive": "str",
            "cycle": "str",
            "created": "datetime",

        }

        columns: Dict[str, Dict[str, Any]] = {
            'created':{
                "sortable": True,
                "name":"Fecha de creación",
                "width":100,
                "fixed":None
            },
            'receiver':{
                "sortable": True,
                "name":"Receptor",
                "width":100,
                "fixed":None
            },
            'message':{
                "sortable": False,
                "name":"Mensaje",
                "width":100,
                "fixed":None
            },
            '_type':{
                "sortable": True,
                "name":"Tipo",
                "width":100,
                "fixed":None
            },
            'channel': {
                "sortable": True,
                "name":"Canal",
                "width":100,
                "fixed":None
            }
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_tables_call.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1
        
        data_struct["columns"] = data_struct_columns

        data_struct["filters"] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Receptor":{
                "type":"str",
                "name":"receiver",
            },
            "Evento":{
                "type":"choices",
                "name":"event__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/trigger_event/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Masivo":{
                "type":"choices",
                "name":"massive__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/massive/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Ciclo":{
                "type":"choices",
                "name":"cycle__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/cycle/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
        }

        return data_struct

    def get_initial_queryset(self) -> QuerySet:
        
        """

            This function, filter queryset by operator

        """

        qs: BaseModel = self.model.objects.all().exclude(deleted=True)
        
        #Select instances of QuerySet of indicates operator
        operator: str = self.request.data.get('operator', '')

        if operator.isnumeric() and operator != '0':  

            #Exclude by Operator
            qs: BaseModel = qs.filter( operator=int( operator ), deleted=True ).only('receiver','ID','created','_type','message','event','massive','cycle')

        else:

            ValidationError({
                "Error operator": "Don't send operator or invalid value",
            })

        return qs

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """
        
        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )

        qs_size: int = len(qs)

        if qs_size > 0:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]
            
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )

        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

class ConversationWhatsAppDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "created", "_type", "receiver", "sender", "agent", "service", "last_message", "last_message_date"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "receiver": "str",
        "sender": "str",
        "agent__pk": "choices",
        "service": "int",
        "last_message_date":"datetime",
        "tags": "str"
    }

    model: BaseModel = ConversationModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            qs: QuerySet = qs.filter(
                Q( customer__icontains=search )
            )

        return qs
        
    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_conversation_table: QuerySet = WhatsAppConversationTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_conversation_table.filters

        tags_filters = [item for item in self.REQUEST_FILTERS if "tags" in item]
        
        self.REQUEST_FILTERS = [item for item in self.REQUEST_FILTERS if "tags" not in item]

        data_conversation: QuerySet = super().get_filtered_queryset()

        for item in tags_filters:
            if "equal" in item:
                data_conversation = data_conversation.filter(tags__icontains=item[2])
            else:
                data_conversation = data_conversation.exclude(tags__icontains=item[2])

        if instance_conversation_table.last_time_type and instance_conversation_table.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_conversation: QuerySet = data_conversation.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_conversation_table.last_time_type] * instance_conversation_table.last_time) )

        return data_conversation

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "ID": instance.ID,
            "created": instance.created,
            "last_message_date": instance.last_message_date,
            "customer": instance.customer,
            "agent": instance.agent.username if instance.agent else None,
            "last_message": instance.last_message,
            "service": instance.services,
            "last_message_kind": instance.get_last_message_kind_display(),
            "custom_chat": ','.join(map(str, instance.services)) if isinstance(instance.services, list) and instance.services != [] else instance.customer
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_conversation_table: QuerySet = WhatsAppConversationTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"created",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "sender": "str",
            "receiver": "str",
            "agent": "str",
            "_type": "str",
            "created": "datetime",
            "last_message": "str"

        }

        columns: Dict[str, Dict[str, Any]] = {
            'ID':{
                "sortable": True,
                "name":"ID",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Fecha de creación",
                "width":100,
                "fixed":None
            },
            'customer':{
                "sortable": True,
                "name":"Cliente",
                "width":100,
                "fixed":None
            },
            'agent':{
                "sortable": True,
                "name":"Agente",
                "width":100,
                "fixed":None
            },
            'service':{
                "sortable": True,
                "name":"Número de servicio",
                "width":100,
                "fixed":None
            },
            'last_message':{
                "sortable": False,
                "name":"Último Mensaje",
                "width":100,
                "fixed":None
            },
            'last_message_date':{
                "sortable": False,
                "name":"Fecha Último Mensaje",
                "width":100,
                "fixed":None
            },
            'last_message_kind':{
                "sortable": True,
                "name":"Tipo",
                "width":100,
                "fixed":None
            }
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_conversation_table.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1
        
        data_struct_columns["Conversación"] = {
            "field": "custom_chat",
            "sortable": True,
            "visible": True,
            "position": position +1,
            "width":100,
            "fixed":None
        }

        data_struct["columns"] = data_struct_columns

        data_struct["filters"] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Cliente":{
                "type":"str",
                "name":"customer",
            },
            "Servicio":{
                "type":"str",
                "name":"service",
            },
            "Agente":{
                "type":"choices",
                "name":"agent__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Tipo Último Mensaje":{
                "type":"choices",
                "name":"last_message_kind",
                "type_choices": "choices",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:"IN",
                        1: "OUT"
                    }
                }
            },
            "Fecha de último mensaje":{
                "type":"datetime",
                "name":"last_message_date",
                "format":"%d/%m/%Y %H:%M",
            },
            "Tags":{
                "type":"choices",
                "name":"tags",
                "type_choices": "str",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/conversation_tag",
                        "read_data":{
                            "value":"name",
                            "human_readable": "name"
                        }
                    }
                }
            },
        }

        return data_struct

    def get_initial_queryset(self) -> QuerySet:
        
        """

            This function, filter queryset by operator

        """

        qs: BaseModel = self.model.objects.all().exclude(deleted=True).filter(channel=4)
        
        #Select instances of QuerySet of indicates operator
        operator: str = self.request.data.get('operator', '')

        if operator.isnumeric() and operator != '0':  

            #Exclude by Operator
            qs: BaseModel = qs.filter( operator=int( operator ), deleted=False ).only('customer','ID','created','last_message_kind','last_message', 'agent')

        else:

            ValidationError({
                "Error operator": "Don't send operator or invalid value",
            })

        return qs

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """

        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )

        qs_size: int = len(qs)

        if qs_size > 0:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]
            
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )

        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

class WhatsAppConversationsTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]

    model: BaseModel = WhatsAppConversationTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs
        

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class EmailConversationsTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]

    model: BaseModel = EmailConversationTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs
        

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class ConversationEmailDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "created", "_type", "receiver", "sender", "agent", "service", "last_message", "last_message_date"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "receiver": "str",
        "sender": "str",
        "agent__pk": "choices",
        "service": "int",
        "last_message_date":"datetime",
        "tags": "str"
    }

    model: BaseModel = ConversationModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            qs: QuerySet = qs.filter(
                Q( customer__icontains=search )
            )

        return qs
        
    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_conversation_table: QuerySet = EmailConversationTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_conversation_table.filters

        tags_filters = [item for item in self.REQUEST_FILTERS if "tags" in item]
        
        self.REQUEST_FILTERS = [item for item in self.REQUEST_FILTERS if "tags" not in item]

        data_conversation: QuerySet = super().get_filtered_queryset()

        for item in tags_filters:
            if "equal" in item:
                data_conversation = data_conversation.filter(tags__icontains=item[2])
            else:
                data_conversation = data_conversation.exclude(tags__icontains=item[2])

        if instance_conversation_table.last_time_type and instance_conversation_table.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_conversation: QuerySet = data_conversation.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_conversation_table.last_time_type] * instance_conversation_table.last_time) )

        return data_conversation

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "ID": instance.ID,
            "created": instance.created,
            "last_message_date": instance.last_message_date,
            "customer": instance.customer,
            "agent": instance.agent.username if instance.agent else None,
            "last_message": instance.last_message if len(instance.last_message) < 15 else instance.last_message[:14],
            "service": instance.services,
            "last_message_kind": instance.get_last_message_kind_display(),
            "custom_chat": instance.services[0] if isinstance(instance.services, list) and instance.services != [] else instance.customer
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_conversation_table: QuerySet = EmailConversationTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"created",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":900
        }

        data_struct["fields"] = {

            "ID": "int",
            "sender": "str",
            "receiver": "str",
            "agent": "str",
            "_type": "str",
            "created": "datetime",
            "last_message": "str"

        }

        columns: Dict[str, Dict[str, Any]] = {
            'ID':{
                "sortable": True,
                "name":"ID",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Fecha de creación",
                "width":100,
                "fixed":None
            },
            'customer':{
                "sortable": True,
                "name":"Cliente",
                "width":100,
                "fixed":None
            },
            'agent':{
                "sortable": True,
                "name":"Agente",
                "width":100,
                "fixed":None
            },
            'service':{
                "sortable": True,
                "name":"Número de servicio",
                "width":100,
                "fixed":None
            },
            'last_message':{
                "sortable": False,
                "name":"Último Mensaje",
                "width":100,
                "fixed":None
            },
            'last_message_date':{
                "sortable": False,
                "name":"Fecha Último Mensaje",
                "width":100,
                "fixed":None
            },
            'last_message_kind':{
                "sortable": True,
                "name":"Tipo",
                "width":100,
                "fixed":None
            }
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}

        position: int = 0

        for _column in instance_conversation_table.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1
        
        data_struct_columns["Conversación"] = {
            "field": "custom_chat",
            "sortable": True,
            "visible": True,
            "position": position +1,
            "width":100,
            "fixed":None
        }

        data_struct["columns"] = data_struct_columns

        data_struct["filters"] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Cliente":{
                "type":"str",
                "name":"customer",
            },
            "Servicio":{
                "type":"str",
                "name":"service",
            },
            "Agente":{
                "type":"choices",
                "name":"agent__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Tipo Último Mensaje":{
                "type":"choices",
                "name":"last_message_kind",
                "type_choices": "choices",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0:"IN",
                        1: "OUT"
                    }
                }
            },
            "Fecha de último mensaje":{
                "type":"datetime",
                "name":"last_message_date",
                "format":"%d/%m/%Y %H:%M",
            },
            "Tags":{
                "type":"choices",
                "name":"tags",
                "type_choices": "str",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "communications/conversation_tag",
                        "read_data":{
                            "value":"name",
                            "human_readable": "name"
                        }
                    }
                }
            },
        }

        return data_struct

    def get_initial_queryset(self) -> QuerySet:
        
        """

            This function, filter queryset by operator

        """

        qs: BaseModel = self.model.objects.all().exclude(deleted=True).filter(channel=0)
        
        #Select instances of QuerySet of indicates operator
        operator: str = self.request.data.get('operator', '')

        if operator.isnumeric() and operator != '0':  

            #Exclude by Operator
            qs: BaseModel = qs.filter( operator=int( operator ), deleted=False ).only('customer','ID','created','last_message_kind','last_message', 'agent')

        else:

            ValidationError({
                "Error operator": "Don't send operator or invalid value",
            })

        return qs

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """

        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )

        qs_size: int = len(qs)

        if qs_size > 0:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]
            
            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )

        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)
