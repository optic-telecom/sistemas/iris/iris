import json
import base64
from typing import IO, Any, Dict, List, Union
from datetime import datetime
from django.urls.base import set_script_prefix
import requests

from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import ValidationError
from django.contrib.auth.models import User
from django_q.tasks import async_task
from operators.models import OperatorModel
from communications.models import ChatModel
from jinja2 import Template
from .queue import email_task_queue
from .models import EmailModel, FilesEmailModel, WhatsappModel, TextModel, VoiceCallModel
from operators.models import CommunicationsSettingsModel

def create_email(data, user, files={}, test=True):

    #Check if the user id or instance was provided. If it was an id, get the instance
    if type(user) == int or type(user) == str:
        try:
            user = User.objects.get(ID=user)
        except:
            raise ValidationError({'email error': 'Invalid user'})
    try:
        
        if not data.get('chat'):
            chat_instance = ChatModel.objects.create(operator=OperatorModel.objects.get(ID=data.get('operator')))
            data['chat'] = chat_instance
            
        email: EmailModel = EmailModel.objects.create(
            subject=data.get('subject'),
            sender=data.get('sender'),
            receiver=data.get('receiver'),
            operator=OperatorModel.objects.get(ID=data.get('operator')),
            message=data.get('message'),
            chat=data.get('chat',None),
            main=True,
            viewed=True
            )
    except:
        raise ValidationError({'email error': 'Unable to create email'})

    email.creator: User = user
    email.updater: User = user
    email.save()

    for data_file in files:

        file_model: FilesEmailModel = FilesEmailModel()

        file_model.email: EmailModel = email
        file_model.creator: User = user
        file_model.kupdater: User = user
        file_model.operator: int = OperatorModel.objects.get(ID=data.get('operator'))
        file_model.files.save(data_file.name, data_file.file)
        file_model.save()

    #Create Async task
    async_task(func=email_task_queue, kwargs={'ID': email.ID})

    return email

def get_customer_communications(customer_instance):

    customer_phones: List[str] = [ phone[0] for phone in customer_instance.phones.values_list('phone') ]
    customer_emails: List[str] = [ email[0] for email in customer_instance.emails.values_list('email') ]

    whatsapp_messages: List[Dict[str, str]] = [ { 'created': text.created, 'creator': text.creator.username if text.creator else '', 'message': text.message, 'type':'whatsapp' } for text in WhatsappModel.objects.filter(receiver__in=customer_phones) ]
    text_messages: List[dict] = [ { 'created': text.created, 'creator': text.creator.username if text.creator else '', 'message': text.message, 'type':'text' } for text in TextModel.objects.filter(receiver__in=customer_phones) ]
    voice_messages: List[Dict[str, str]] = [ { 'created': text.created, 'creator': text.creator.username if text.creator else '', 'message': text.message, 'type':'voice_call' } for text in VoiceCallModel.objects.filter(receiver__in=customer_phones) ]
    email_messages: List[Dict[str, str]] = [ { 'created': text.created, 'creator': text.creator.username if text.creator else '', 'message': text.message, 'type':'email' } for text in EmailModel.objects.filter(receiver__in=customer_emails) ]

    #Get all messages
    all_communications: List[Dict[str, Dict[str,str]]] = whatsapp_messages + text_messages + voice_messages + email_messages
    #Sort data
    all_communications.sort( key=lambda x: x['created'] )
    
    return all_communications

def send_whatsapp_message(*args, **kwargs):

    """

        This function init communication massive

        :param args: This param is args list
        :type args: list
        :param kwargs: This param is args dict
        :type kwargs: dict

    """

    if 'kwargs' in kwargs.keys():

        receiver: int = kwargs['kwargs']['receiver']
        message: int = kwargs['kwargs']['message']
        operator: int = kwargs['kwargs']['operator']
        params: int = kwargs['kwargs']['params']
        template: int = kwargs['kwargs']['template']
        media_url: int = kwargs['kwargs']['media_url']
        message_instance = kwargs['kwargs']['message_instance']

    else:

        receiver: int = kwargs['receiver']
        message: int = kwargs['message']
        operator: int = kwargs['operator']
        params: int = kwargs['params']
        template: int = kwargs['template']
        media_url: int = kwargs['media_url']
        message_instance = kwargs['message_instance']

    communications_settings = CommunicationsSettingsModel.objects.filter(company=operator.company).first()
    global_preferences = global_preferences_registry.manager()
    reportUrl = "https://5bd8-179-63-247-10.ngrok.io/" + "api/v1/communications/whatsapp_message/update_message/"

    if communications_settings.kind == 0:

        channels = communications_settings.extra_data.get("channels")
        namespace = communications_settings.extra_data.get("namespace")
        response = None

        if template:

            response = requests.post(
                url='https://conversations.messagebird.com/v1/conversations/start',
                headers={
                    'Authorization': f'AccessKey {communications_settings.token}',
                },
                json={
                    "to":receiver,
                    "reportUrl": reportUrl,
                    "type":"hsm",
                    "channelId":next(item for item in channels if item["operator"] == operator.ID and item["type"] == "whatsapp").get("channel"),
                    "content": {
                        "hsm": {
                            "namespace": namespace,
                            "templateName": template,
                            "language": {
                                "policy": "deterministic",
                                "code": "es"
                            },
                            "params": params
                        },
                    }
                }
            )

        else:

            if media_url != []:

                url = media_url[0].replace("http://127.0.0.1:8000/media\\","https://ff04-190-120-253-192.ngrok.io/media/")

                data = { 
                    "to":receiver,
                    "channelId":next(item for item in channels if item["operator"] == operator.ID and item["type"] == "whatsapp").get("channel"),
                    "type":"image" if url.endswith("jpg") or url.endswith("jpeg") or url.endswith("png") else "file",
                    "content":{ 
                        "image" if url.endswith("jpg") or url.endswith("jpeg") or url.endswith("png") else "file":{
                            "url": url
                        }
                    }
                }

                response = requests.post(
                    url='https://conversations.messagebird.com/v1/conversations/start',
                    headers={
                        'Authorization': f'AccessKey {communications_settings.token}',
                    },
                    json=data
                )

            else:

                data = {
                    "to":receiver,
                    "reportUrl": reportUrl, 
                    "type":"text",
                    "channelId":next(item for item in channels if item["operator"] == operator.ID and item["type"] == "whatsapp").get("channel"),
                    "content": {
                        "text": message
                    }  
                }
                
                response = requests.post(
                    url='https://conversations.messagebird.com/v1/conversations/start',
                    headers={
                        'Authorization': f'AccessKey {communications_settings.token}',
                    },
                    json=data
                )

        try: 
            
            response2 = requests.get(
                url=response.json()["messages"]["href"],
                headers={
                    'Authorization': f'AccessKey {communications_settings.token}',
                },
                params={"limit": 1}
            )
            message_instance.messagebird_id = response2.json()["items"][0]["id"]
            message_instance.messagebird_conversation_id = response.json()["id"]
            if response.json()["status"]== "accepted":
                message_instance.status = 0
            message_instance.save()
        except:
            message_instance.status = 2
            message_instance.save()

def send_email_message(*args, **kwargs):

    """

        This function init communication massive

        :param args: This param is args list
        :type args: list
        :param kwargs: This param is args dict
        :type kwargs: dict

    """

    if 'kwargs' in kwargs.keys():

        receiver: int = kwargs['kwargs']['receiver']
        message: int = kwargs['kwargs']['message']
        subject: int = kwargs['kwargs']['subject']
        operator: int = kwargs['kwargs']['operator']
        template: int = kwargs['kwargs']['template']
        media_url: int = kwargs['kwargs']['media_url']
        service: int = kwargs['kwargs']['service']
        instance: int = kwargs['kwargs']['instance']

    else:

        receiver: int = kwargs['receiver']
        message: int = kwargs['message']
        subject: int = kwargs['subject']
        operator: int = kwargs['operator']
        template: int = kwargs['template']
        media_url: int = kwargs['media_url']
        service: int = kwargs['service']
        instance: int = kwargs['instance']

    communications_settings = CommunicationsSettingsModel.objects.filter(company=operator.company).first()
    reportUrl = "https://3c12-190-120-253-193.ngrok.io/" + "api/v1/communications/email_message/receive_message/"
    
    if communications_settings.kind == 0:

        channel = communications_settings.extra_data.get("channels")
        email_channel = next(item for item in channel if item["operator"] == operator.ID and item["type"] == "email").get("channel")
        namespace = communications_settings.extra_data.get("namespace")

        if template:
            
            global_preferences: Dict[str, any] = global_preferences_registry.manager()
            
            def get_matrix_data(service):

                filters = [['number', 'equal', service]]

                data: Dict[str, Any] = {
                    "filters": filters,
                    "includes": [],
                    "excludes": [],
                    "unpaid_ballot": False,
                    "criterion": 'D'
                }

                matrix_url: str = global_preferences['general__matrix_domain'] + 'services-filters/'

                response = requests.get(
                    url=matrix_url,
                    headers={
                        "Authorization": global_preferences['matrix__matrix_token']
                    },
                    json=data,
                    verify=False,
                )

                if response.status_code != 200:

                    raise ValidationError({
                        "Error matrix": "Error matrix filter"
                    })

                result = response.json()[0]

                if 'service__number' in result:
                    result['service__number_64'] = str(base64.b64encode(bytes(str(result['service__number']), 'utf-8')), 'utf-8')

                if 'customer__id' in result:
                    result['customer__id_64'] = str(base64.b64encode(bytes(str(result['customer__id']), 'utf-8')), 'utf-8')

                return result
            
            def current_time_tags(data: Dict[str, Any]) -> Dict[str, Any]:

                months: Dict[int, str] = {
                    1: "Enero",
                    2: "Febrero",
                    3: "Marzo",
                    4: "Abril",
                    5: "Mayo",
                    6: "Junio",
                    7: "Julio",
                    8: "Agosto",
                    9: "Septiembre",
                    10: "Octubre",
                    11: "Noviembre",
                    12: "Diciembre",
                }

                weekdays: Dict[int, str] = {
                    0: "Lunes",
                    1: "Martes",
                    2: "Miercoles",
                    3: "Jueves",
                    4: "Viernes",
                    5: "Sabado",
                    6: "Domingo",
                }

                current_datetime: datetime = datetime.now()

                data['current__month'] = months[int(current_datetime.month)]
                data['current__year'] = current_datetime.year
                data['current__day'] = current_datetime.day
                data['current__week_day'] = weekdays[int(current_datetime.weekday())]
                data['current__datetime'] = current_datetime.strftime("%d/%m/%Y")

                return data

            data: Dict[str, Any] = get_matrix_data(service)
            data: Dict[str, Any] = current_time_tags(data)

            message =  Template(template).render(data)

            data = {
                "subject":subject,
                "to":[
                    {
                    "name": "Customer",
                    "address": receiver
                    }
                ],
                "from": {
                    "name": operator.company.name,
                    "address": "ariana.amador@neo8.cl"
                },
                "content":{
                    "html": message
                },
                "attachments": media_url,
                "reportUrl": reportUrl
            }

            response = requests.post(
                url='https://email.messagebird.com/v1/send',
                headers={
                    'Authorization': f'AccessKey {communications_settings.token}',
                },
                json=data
            )

        else:

            data = {
                "subject":subject,
                "to":[
                    {
                    "name": "Customer",
                    "address": receiver
                    }
                ],
                "from": {
                    "name": operator.company.name,
                    "address": "ariana.amador@neo8.cl"
                },
                "content":{
                    "html": message
                },
                "attachments": media_url,
                "reportUrl": reportUrl
            }

            response = requests.post(
                url='https://email.messagebird.com/v1/send',
                headers={
                    'Authorization': f'AccessKey {communications_settings.token}',
                },
                json=data
            )

        instance.messagebird_id = response.json().get("id")
        instance.save()