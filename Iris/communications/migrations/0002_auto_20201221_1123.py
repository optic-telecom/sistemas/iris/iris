# Generated by Django 2.1.7 on 2020-12-21 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('communications', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cyclecommunicationsmodel',
            name='slack_channel',
            field=models.CharField(max_length=9, null=True),
        ),
        migrations.AlterField(
            model_name='historicalcyclecommunicationsmodel',
            name='slack_channel',
            field=models.CharField(max_length=9, null=True),
        ),
        migrations.AlterField(
            model_name='historicalmassivecommunicationsmodel',
            name='slack_channel',
            field=models.CharField(max_length=9, null=True),
        ),
        migrations.AlterField(
            model_name='massivecommunicationsmodel',
            name='slack_channel',
            field=models.CharField(max_length=9, null=True),
        ),
    ]
