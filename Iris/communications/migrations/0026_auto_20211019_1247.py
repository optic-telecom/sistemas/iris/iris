# Generated by Django 3.2.2 on 2021-10-19 16:47

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('operators', '0010_update_communication_settings'),
        ('communications', '0025_auto_20211007_0838'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailmessagemodel',
            name='messagebird_id',
            field=models.TextField(default='', null=True),
        ),
        migrations.AddField(
            model_name='historicalemailmessagemodel',
            name='messagebird_id',
            field=models.TextField(default='', null=True),
        ),
        migrations.AddField(
            model_name='historicalwhatsappmessagemodel',
            name='messagebird_id',
            field=models.TextField(default='', null=True),
        ),
        migrations.AddField(
            model_name='whatsappmessagemodel',
            name='messagebird_id',
            field=models.TextField(default='', null=True),
        ),
        migrations.AlterField(
            model_name='chatmodel',
            name='last_message',
            field=models.DateTimeField(default=datetime.datetime(2021, 10, 19, 12, 47, 48, 13075)),
        ),
        migrations.AlterField(
            model_name='emailmessagemodel',
            name='status',
            field=models.PositiveIntegerField(choices=[(0, 'Enviado'), (1, 'Leído'), (2, 'Fallido')], default=0),
        ),
        migrations.AlterField(
            model_name='historicalchatmodel',
            name='last_message',
            field=models.DateTimeField(default=datetime.datetime(2021, 10, 19, 12, 47, 48, 13075)),
        ),
        migrations.AlterField(
            model_name='historicalemailmessagemodel',
            name='status',
            field=models.PositiveIntegerField(choices=[(0, 'Enviado'), (1, 'Leído'), (2, 'Fallido')], default=0),
        ),
        migrations.AlterField(
            model_name='historicalwhatsappmessagemodel',
            name='status',
            field=models.PositiveIntegerField(choices=[(0, 'Enviado'), (1, 'Leído'), (2, 'Recibido')], default=0),
        ),
        migrations.AlterField(
            model_name='whatsappmessagemodel',
            name='status',
            field=models.PositiveIntegerField(choices=[(0, 'Enviado'), (1, 'Leído'), (2, 'Recibido')], default=0),
        )
    ]
