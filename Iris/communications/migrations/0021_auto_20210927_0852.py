# Generated by Django 3.2.2 on 2021-09-27 12:52

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('operators', '0009_create_communication_settings'),
        ('communications', '0020_auto_20210923_1156'),
    ]

    operations = [
        migrations.AddField(
            model_name='conversationmodel',
            name='chat',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='historicalconversationmodel',
            name='chat',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='chatmodel',
            name='last_message',
            field=models.DateTimeField(default=datetime.datetime(2021, 9, 27, 8, 52, 20, 501189)),
        ),
        migrations.AlterField(
            model_name='historicalchatmodel',
            name='last_message',
            field=models.DateTimeField(default=datetime.datetime(2021, 9, 27, 8, 52, 20, 501189)),
        )
    ]
