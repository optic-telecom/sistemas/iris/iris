import ast
import base64
import json
import random
import re
import string
import threading
from datetime import datetime, timedelta
from functools import reduce
from mimetypes import guess_extension
from typing import IO, Any, Dict, List, Union
from operators.models import CompanyModel

import arrow
import pytz
import requests
import urllib3
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import get_token
from common.viewsets import BaseViewSet
from django.conf import settings
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.mail import EmailMultiAlternatives
from django.http import JsonResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django_q.models import Schedule
from django_q.tasks import async_task, schedule
from drf_yasg.openapi import Response as Response_Openapi
from drf_yasg.utils import swagger_auto_schema
from dynamic_preferences.registries import global_preferences_registry
from escalation_ti.models import EscalationModel
from formerCustomers.models import RemoveEquipamentEmailModel
from jinja2 import Template
from magic import from_buffer
from messagebird import Client
from operators.models import OperatorModel
from phonenumber_field.modelfields import PhoneNumberField
from rest_framework import serializers, viewsets
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.parsers import FileUploadParser
from rest_framework.serializers import ValidationError
from sentry_sdk import capture_exception
from slack import WebClient
from tickets.models import TypifyModel
from url_filter.integrations.drf import DjangoFilterBackend
from validate_email import validate_email
from communications.models import ChatModel

from .api import create_email
from operators.models import CommunicationsSettingsModel
from escalation_finance.models import FinanceEmailModel, FinanceEscalationModel
from .datatables import (ConversationEmailDatatable, ConversationWhatsAppDatatable, CycleCommunicationsModelDatatable, EmailConversationsTablesDatatable, EmailDatatable,
                         EmailTablesDatatable,
                         MassiveCommunicationsModelDatatable,
                         TemplateCallDatatable, TemplateEmailDatatable,
                         TemplateTextDatatable, TemplateWhatsappDatatable,
                         TextDatatable, TextTablesDatatable,
                         TriggerEventDatatable, VoiceCallDatatable,
                         VoiceCallTablesDatatable, WhatsAppConversationsTablesDatatable, WhatsappDatatable,
                         WhatsappTablesDatatable)
from .documentation import (DocumentationCycleCommunications,
                            DocumentationMassiveCommunications,
                            DocumentationTemplateCall,
                            DocumentationTemplateEmail,
                            DocumentationTemplateText,
                            DocumentationTemplateWhatsapp,
                            DocumentationTriggerEvent)
from .models import (BaseNumericEvent, ChatModel, ConversationModel, ConversationTagModel, CycleCommunicationsModel, EmailConversationTablesModel, EmailMessageModel,
                     EmailModel, EmailTablesModel, EventCallModel,
                     EventEmailModel, EventTextModel, EventWhatsappModel,
                     FilesEmailModel, InternalChatModel,
                     MassiveCommunicationsModel, NotificationModel, TemplateCallModel,
                     TemplateEmailModel, TemplateTextModel,
                     TemplateWhatsappModel, TextModel, TextTablesModel,
                     TriggerEventModel, VoiceCallModel, VoiceCallTablesModel, WhatsAppConversationTablesModel, WhatsAppMessageModel,
                     WhatsappModel, WhatsappTablesModel)
from .queue import Massive, init_massive_communications_message
from .serializers import (ChatSerializer, ConversationSerializer, ConversationTagSerializer, CycleCommunicationsSerializer, EmailConversationTablesSerializer, EmailMessageSerializer,
                          EmailSerializer, EmailTablesSerializer,
                          EventCallSerializer, EventEmailSerializer,
                          EventTextSerializer, EventWhatsappSerializer,
                          FilesEmailSerializer, InternalChatSerializer,
                          MassiveCommunicationsSerializer, NotificationSerializer,
                          TemplateCallSerializer, TemplateEmailSerializer,
                          TemplateTextSerializer, TemplateWhatsappSerializer,
                          TextSerializer, TextTablesSerializer,
                          TriggerEventSerializer, VoiceCallSerializer,
                          VoiceCallTablesSerializer, WhatsAppConversationTablesSerializer, WhatsAppMessageSerializer,  WhatsappSerializer,
                          WhatsappTablesSerializer)


class CycleCommunicationsView(BaseViewSet):

    """

        Class define ViewSet of CycleCommunicationsModel.
    
    """

    queryset: BaseModel = CycleCommunicationsModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = CycleCommunicationsSerializer
    documentation_class = DocumentationCycleCommunications()

    @method_decorator(permission_required('communications.add_cyclecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_create()
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_cyclecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_update()
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_cyclecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_update()
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.delete_cyclecommunicationsmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('communications.view_cyclecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=False, methods=['post'])
    def datatables(self, request):

        """
        
            This funtion return datatables data

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView
            :param request: data of request
            :type request: request
        
        """

        return CycleCommunicationsModelDatatable(request).get_data()

    @documentation_class.documentation_datables_struct()
    @method_decorator(permission_required('communications.view_cyclecommunicationsmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(CycleCommunicationsModelDatatable(request).get_struct(), safe=True)

    @documentation_class.documentation_activate()
    @method_decorator(permission_required('communications.change_cyclecommunicationsmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def activate(self, request, pk) -> JsonResponse:
        
        """
            This funtion active cycle communications

            :param self: Instance of Class CycleCommunicationsView
            :type self: CycleCommunicationsView
            :param request: data of request
            :type request: request
            :param pk: Instance pk
            :type pk: int
        """
  
        try:

            #Cycle instance
            cycle_communications: CycleCommunicationsModel = CycleCommunicationsModel.objects.get(ID=pk)

            #Check if cycle is active
            if not cycle_communications.schedule and not cycle_communications.active and not cycle_communications.test:

                #Delete old schedules
                Schedule.objects.filter(pk__in=cycle_communications.schedule_massive.values_list('id')).delete()
                cycle_communications.schedule_massive.clear()

                #Get start datetime and transform to python datetime
                datetime_start_str: str = cycle_communications.massives_communications['0']['datetime']
                datetime_start_datetime: datetime = datetime.strptime(datetime_start_str, '%d/%m/%Y %H:%M')
                
                #Check if the start datetime is greather than current datetime

                if datetime_start_datetime < datetime.now():

                    raise ValidationError({
                        "Datetime error": "Start time already passed"
                    })

                else:
                    
                    schedule_object: Schedule = Schedule.objects.create(
                        func='communications.queue.init_cycle_communications_message',
                        hook='communications.queue.finish_cycle_communications_message',
                        name=''.join(random.choice( string.ascii_uppercase + string.digits ) for _ in range(14)),
                        schedule_type=cycle_communications.type_repeats,
                        repeats=cycle_communications.number_repeats,
                        next_run=datetime_start_datetime - timedelta(seconds=120),
                        kwargs={
                            'pk_cycle':cycle_communications.ID,
                            'test_cycle':False
                        },
                        minutes=5,
                    )

                    #Save Schedule object of cycle
                    cycle_communications.schedule: Schedule = schedule_object
                    cycle_communications.active: bool = True
                    cycle_communications.test: bool = False
                    cycle_communications.save()

            else:

                #Raise error if cycle is active
                raise ValidationError({
                    "Cycle active": "The cycle is active o running a test"
                })

        except ObjectDoesNotExist:

            #Raise error if cycle does exist
            raise ValidationError({
                "Pk error": "Does not exist item"
            })

        return JsonResponse(data={}, safe=True)

    @documentation_class.documentation_inactive()
    @method_decorator(permission_required('communications.change_cyclecommunicationsmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def inactive(self, request, pk) -> JsonResponse:
        
        """
            This funtion inactive cycle communications

            :param self: Instance of Class CycleCommunicationsView
            :type self: CycleCommunicationsView
            :param request: data of request
            :type request: request
            :param pk: Instance pk
            :type pk: int
        """

        try:

            #Instance of cycle
            cycle_communications: CycleCommunicationsModel = CycleCommunicationsModel.objects.get(ID=pk)

            #Check if contains schedule instance an delete
            if isinstance(cycle_communications.schedule, Schedule):

               cycle_communications.schedule.delete()
               cycle_communications.schedule = None

            #Remove all instance related shcedules
            Schedule.objects.filter(pk__in=cycle_communications.schedule_massive.values_list('id')).delete()
            cycle_communications.schedule_massive.clear()
            cycle_communications.active: bool = False
            cycle_communications.test: bool = False
            cycle_communications.save()

        except ObjectDoesNotExist:

            #Raise error if cycle does exist
            raise ValidationError({
                "Pk error": "Does not exist item"
            })

        return JsonResponse(data={}, safe=True)

    @documentation_class.documentation_test()
    @action(detail=True, methods=['post'])
    @method_decorator(permission_required('communications.change_cyclecommunicationsmodel',raise_exception=True))
    def test(self, request, pk) -> JsonResponse:

        """
            This funtion test cycle communications

            :param self: Instance of Class CycleCommunicationsView
            :type self: CycleCommunicationsView
            :param request: data of request
            :type request: request
            :param pk: Instance pk
            :type pk: int
        """
  
        try:

            #Cycle instance
            cycle_communications: CycleCommunicationsModel = CycleCommunicationsModel.objects.get(ID=pk)

            #Check if cycle is active
            if not cycle_communications.schedule and not cycle_communications.active and not cycle_communications.test:

                #Delete old schedules
                Schedule.objects.filter(pk__in=cycle_communications.schedule_massive.values_list('id')).delete()
                cycle_communications.schedule_massive.clear()

                #Get start datetime and transform to python datetime
                datetime_start_str: str = cycle_communications.massives_communications['0']['datetime']
                datetime_start_datetime: datetime = datetime.strptime(datetime_start_str, '%d/%m/%Y %H:%M')
                
                #Check if the start datetime is greather than current datetime

                if datetime_start_datetime < datetime.now():

                    raise ValidationError({
                        "Datetime error": "Start time already passed"
                    })

                else:
                    
                    schedule_object: Schedule = Schedule.objects.create(
                        func='communications.queue.init_cycle_communications_message',
                        name=''.join(random.choice( string.ascii_uppercase + string.digits ) for _ in range(14)),
                        schedule_type=cycle_communications.type_repeats,
                        repeats=cycle_communications.number_repeats,
                        next_run=datetime_start_datetime - timedelta(seconds=120),
                        kwargs={
                            'pk_cycle':cycle_communications.ID,
                            'test_cycle':True
                        },
                        minutes=5,
                    )

                    #Save Schedule object of cycle
                    cycle_communications.schedule: Schedule = schedule_object
                    cycle_communications.active: bool = False
                    cycle_communications.test: bool = True
                    cycle_communications.save()

            else:

                #Raise error if cycle is active
                raise ValidationError({
                    "Cycle active": "The cycle is active o running a test"
                })

        except ObjectDoesNotExist:

            #Raise error if cycle does exist
            raise ValidationError({
                "Pk error": "Does not exist item"
            })

        return JsonResponse(data={}, safe=True)

class MassiveCommunicationsView(BaseViewSet):

    """

        Class define ViewSet of MassiveCommunicationsModel.
    
    """

    queryset: BaseModel = MassiveCommunicationsModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = MassiveCommunicationsSerializer
    documentation_class = DocumentationMassiveCommunications()

    @method_decorator(permission_required('communications.add_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_create()
    def create(self, request, *args, **kwargs):  
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_update()
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_update()
    def partial_update(self, request, *args, **kwargs):

        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.delete_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_delete()
    def destroy(self, request, *args, **kwargs):

        """
        
            This funtion destroy instance and Schedule element

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView
            :param request: data of request
            :type request: request
            :param args: list args
            :type args: list
            :param kwargs: dict args
            :type kwargs: dict
        
        """

        status: int = 500
        result: List[str, List[str]] = {
            "non_field_errors": [
                "Unexpected"
            ]
        }

        try:

            instance: MassiveCommunicationsModel = self.get_object()

            instance.status: bool = False
            instance.deleted: bool = True
            instance.save()

            result: dict = {}
            status: int = 200

        except ObjectDoesNotExist:

            result: List[str, List[str]] = {
                "non_field_errors": [
                    "Does not exist item"
                ]
            }

        return JsonResponse(data=result, safe=True, status=status)

    @method_decorator(permission_required('communications.view_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_filters()
    @action(detail=False, methods=['get'])
    def filters(self, request) -> JsonResponse:

        """
        
            This funtion return filters

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView
            :param request: data of request
            :type request: request

            :returns: Returns JsonResponse, responser to user
            :rtype: JsonResponse
        
        """

        return JsonResponse({
            'filters': Massive.filters,
            'filters_translation': Massive.filters_translation,
            'type_filters': Massive.type_filters,
            'type_filters_tranlation': Massive.type_translation
        })
    
    @method_decorator(permission_required('communications.view_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView
            :param request: data of request
            :type request: request
        
        """

        return MassiveCommunicationsModelDatatable(request).get_data()

    @method_decorator(permission_required('communications.view_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_datables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(MassiveCommunicationsModelDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('communications.change_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_test()
    @action(detail=True, methods=['post'])
    def test(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView
            :param request: data of request
            :type request: request
            :param pk: Instance pk
            :type pk: int

        """

        try:
            
            massive: MassiveCommunicationsModel = MassiveCommunicationsModel.objects.get(ID=pk)

            #Create Async event of massive message
            
            init_massive_communications_message(kwargs={
                'pk_massive': massive.ID,
                'pk_cycle': None,
                'test_cycle':False,
                'test_massive': True,
                'finish_cycle': False,
            })

        except ObjectDoesNotExist:

            #Raise error if cycle does exist
            raise ValidationError({
                "Pk error": "Does not exist item"
            })

        return JsonResponse(data={})

    @method_decorator(permission_required('communications.change_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_add_channel()
    @action(detail=True, methods=['post'])
    def add_channel(self, request, pk) -> JsonResponse:

        """
        
            This funtion add event to Trigger event

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView
            :param request: data of request
            :type request: request
            :param request: pk of MassiveCommunicationsModel
            :type request: int
        
        """

        class ValidatorEmail(BaseSerializer):

            """
            
                Class validate email event
            
            """

            class Meta:

                #Class model of serializer
                model: BaseModel = EventEmailModel
                #Fields of serializer
                fields: Union[list, str] = ['email_test', 'email_response', 'email_copy', 'template', 'email_origin', 'operator']

        class ValidatorWhatsapp(BaseSerializer):

            """
            
                Class validate whatsapp event
            
            """
            
            class Meta:

                #Class model of serializer
                model: BaseModel = EventWhatsappModel
                #Fields of serializer
                fields: Union[list, str] = ['phone_number_test', 'template', 'operator']

        class ValidatorText(BaseSerializer):

            """
            
                Class validate text event
            
            """
            
            class Meta:

                #Class model of serializer
                model: BaseModel = EventTextModel
                #Fields of serializer
                fields: Union[list, str] = ['phone_number_test', 'template', 'operator']

        class ValidatorCall(BaseSerializer):
            
            """
            
                Class validate call event
            
            """

            class Meta:

                #Class model of serializer
                model: BaseModel = EventCallModel
                #Fields of serializer
                fields: Union[list, str] = ['phone_number_test', 'template', 'operator']

        channels: Dict[str, Dict[str, Any]] = {
            'email': {
                'validator':ValidatorEmail,
                'name_file': 'email_event'
            },
            'whatsapp': {
                'validator':ValidatorWhatsapp,
                'name_file': 'whatsapp_event'
            },
            'text': {
                'validator':ValidatorText,
                'name_file': 'text_event'
            },
            'call': {
                'validator':ValidatorCall,
                'name_file': 'call_event'
            },
        }  

        if 'channel' not in request.POST.keys():

            raise serializers.ValidationError( 
        
                {
                    "channel": [
                        "Required 'channel' field."
                    ],
                }

            )

        #Channel to add
        channel: Dict[str, dict] = request.POST['channel']

        if channel not in channels.keys():

            raise serializers.ValidationError( 
        
                {
                    "channel": [
                        "No valid 'channel' field value."
                    ],
                }

            )

        #Get serializer object with data
        validator = channels[ channel ]['validator']( data=request.POST )
        #Add request to serializer
        validator.context['request'] = request

        if validator.is_valid():

            #Create instance of massive communications
            instance_event: BaseModel = validator.create( validator.validated_data )
            #Get instance of MassiveCommunicationsModel
            instance_massive: MassiveCommunicationsModel = MassiveCommunicationsModel.objects.get(ID=pk)

            #Save event and user
            instance_massive.updater: User = request.user
            setattr(instance_massive, channels[ channel ]['name_file'], instance_event)
            instance_massive.save()

        else:

            raise serializers.ValidationError( validator.errors )

        return JsonResponse(data={})

    @method_decorator(permission_required('communications.change_massivecommunicationsmodel',raise_exception=True))
    @documentation_class.documentation_remove_channel()
    @action(detail=True, methods=['post'])
    def remove_channel(self, request, pk) -> JsonResponse:

        """
        
            This funtion remove event to Trigger event

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView
            :param request: data of request
            :type request: request
            :param request: pk of MassiveCommunicationsModel
            :type request: int
        
        """

        #Dict of name on model of event
        channels: Dict[str, str] = {
            'email': 'email_event',
            'whatsapp': 'whatsapp_event',
            'text': 'text_event',
            'call': 'call_event',
        }

        #Get instance of MassiveCommunicationsModel
        instance_massive: MassiveCommunicationsModel = MassiveCommunicationsModel.objects.get(ID=pk)
        #Get instance of event
        instance_event: Union[EventEmailModel, EventWhatsappModel, EventTextModel, EventCallModel ] = getattr(instance_massive, channels[request.POST['channel']])

        if instance_event:

            #Set null to fields of event
            setattr(instance_massive, channels[request.POST['channel']], None)
            instance_massive.save()

            #Delete instance
            instance_event.delete()

        return JsonResponse(data={})

    @documentation_class.documentation_activate()
    @method_decorator(permission_required('communications.change_massivecommunicationsmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def activate(self, request, pk) -> JsonResponse:
        
        """
            This funtion active massive

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView
            :param request: data of request
            :type request: request
            :param pk: Instance pk
            :type pk: int
        """

        try:
            
            massive: MassiveCommunicationsModel = MassiveCommunicationsModel.objects.get(ID=pk)

            #Create Async event of massive message
            
            init_massive_communications_message(kwargs={
                'pk_massive': massive.ID,
                'pk_cycle': None,
                'test_cycle':False,
                'test_massive': False,
                'finish_cycle': False,
            })

        except ObjectDoesNotExist:

            #Raise error if cycle does exist
            raise ValidationError({
                "Pk error": "Does not exist item"
            })

        return JsonResponse(data={})

class TemplateEmailView(BaseViewSet):

    """

        Class define ViewSet of TemplateEmailModel.
    
    """

    queryset: BaseModel = TemplateEmailModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TemplateEmailSerializer
    documentation_class = DocumentationTemplateEmail()

    @method_decorator(permission_required('communications.add_templateemailmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_templateemailmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_templateemailmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.delete_templateemailmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):

        triggerQuery = TriggerEventModel.objects.filter(email_event__template__pk = kwargs['pk'], deleted=False)
        massiveQuery = MassiveCommunicationsModel.objects.filter(email_event__template__pk = kwargs['pk'], deleted=False)
        remove_equipment = RemoveEquipamentEmailModel.objects.filter(template_email__pk = kwargs['pk'], deleted=False)

        if len(triggerQuery) > 0:
            raise ValidationError({'pk error': f'This template is associated to the event {triggerQuery[0].name}'})
        elif len(massiveQuery) > 0:
            raise ValidationError({'pk error': f'This template is associated to the massive {massiveQuery[0].name}'})
        elif len(remove_equipment) > 0:
            raise ValidationError({'pk error': f'This template is assigned to remove equipment emails'})

        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('communications.view_templateemailmodel',raise_exception=True))
    @documentation_class.documentation_datables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TemplateEmailDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_templateemailmodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:
        
        return TemplateEmailDatatable(request).get_data()

    @method_decorator(permission_required('communications.add_templateemailmodel',raise_exception=True))
    @documentation_class.documentation_test()
    @action(detail=False, methods=['post'])
    def test_template(self, request):

        """
        
            This funtion test template

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        #Get values
        from_email: str = request.POST.get('from_email', '')
        to_email: str = request.POST.get('to_email', '')
        template: str = request.POST.get('template', '')
        subject: str = request.POST.get('subject', '')
        data: Dict[str, str] = request.POST.get('data', None)

        if data == None: 
            data: Dict = {}
        else:
            try:
                data: Dict = json.loads(data)
            except:
                raise ValidationError({'Invalid tags data': 'A valid dictionary wasnt supplied'})

        #check if contains params
        if validate_email(from_email) and validate_email(to_email):

            mail = EmailMultiAlternatives(
                subject=subject,
                body=Template(template).render(data),
                from_email=from_email,
                to=[to_email],
            )
            mail.content_subtype = 'html'
            mail.send()

        else:

            raise ValidationError({
                "Requeried fields":" 'from_email' and 'to_email' fields is required "
            })

        return JsonResponse({})

    @method_decorator(permission_required('communications.add_templateemailmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def preview_template(self, request):

        """
        
            This funtion returns the preview of a template

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        #Get values
        html: str = request.POST.get('template', '')
        data: Dict[str, str] = request.POST.get('data', None)
        
        if data == None: 
            data: Dict = {}
        else:
            try:
                data: Dict = json.loads(data)
            except:
                raise ValidationError({'Invalid tags data': 'A valid dictionary wasnt supplied'})

        body = Template(html).render(data)

        return JsonResponse(data={'template': body})

    @method_decorator(permission_required('communications.add_templateemailmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def tags(self, request):

        data: List[Dict[str, str]] = [
            {"label": "Tipo de tecnología", "name": "service__technology_kind" , "type": "str"},
            {"label": "Número de servicio", "name": "service__number" , "type": "int"},
            {"label": "Fecha tope del servicio", "name": "service__due_day" , "type": "datetime"},
            {"label": "Estado del servicio", "name": "service__status" , "type": "str"},
            {"label": "Dir. completa del servicio", "name": "service__composite_address" , "type": "str"},
            {"label": "Calle del servicio", "name": "service__street" , "type": "str"},
            {"label": "Nro. casa del servicio", "name": "service__house_number" , "type": "int"},
            {"label": "Nro. apto del servicio", "name": "service__apartment_number" , "type": "int"},
            {"label": "Nro. torre del servicio", "name": "service__tower" , "type": "int"},
            {"label": "Comuna del servicio", "name": "service__commune" , "type": "str"},
            {"label": "Ubicación del servicio", "name": "service__location" , "type": "int"},
            {"label": "Tipo de Actividad", "name": "service__activity" , "type": "str"},
            {"label": "Código del nodo", "name": "node__code" , "type": "str"},
            {"label": "Dirección del nodo", "name": "node__address" , "type": "str"},
            {"label": "Número de casa del nodo", "name": "node__house_number" , "type": "int"},
            {"label": "Comuna del nodo", "name": "node__commune" , "type": "str"},
            {"label": "¿Nodo gpon?", "name": "node__gpon" , "type": "str"},
            {"label": "¿Nodo gepon?", "name": "node__gepon" , "type": "str"},
            {"label": "Puerto del nodo", "name": "node__pon_port" , "type": "str"},
            {"label": "Ubicación de caja del nodo", "name": "node__box_location" , "type": "str"},
            {"label": "Ubicación splitter del nodo", "name": "node__splitter_location" , "type": "str"},
            {"label": "Voltaje esperado del nodo", "name": "node__expected_power" , "type": "str"},
            {"label": "Número de la torre del nodo", "name": "node__towers" , "type": "int"},
            {"label": "Nro. apto del nodo", "name": "node__apartments" , "type": "int"},
            {"label": "Teléfono del nodo", "name": "node__phone" , "type": "phone"},
            {"label": "Nombre del plan", "name": "plan__name" , "type": "str"},
            {"label": "Precio del plan", "name": "plan__price" , "type": "int"},
            {"label": "Tipo de plan", "name": "plan__category" , "type": "str"},
            {"label": "¿Plan UF?", "name": "plan__uf" , "type": "str"},
            {"label": "¿Plan activo?", "name": "plan__active" , "type": "str"},
            {"label": "Rut del cliente", "name": "customer__rut" , "type": "str"},
            {"label": "Nombre del cliente", "name": "customer__name" , "type": "str"},
            {"label": "Correo del cliente", "name": "customer__email" , "type": "email"},
            {"label": "Teléfono del cliente", "name": "customer__phone" , "type": "phone"},
            {"label": "Fecha tope del cliente", "name": "customer__default_due_day" , "type": "datetime"},
            {"label": "Calle del cliente", "name": "customer__street" , "type": "str"},
            {"label": "Nro. casa del cliente", "name": "customer__house_number" , "type": "int"},
            {"label": "Nro. apto del cliente", "name": "customer__apartment_number" , "type": "int"},
            {"label": "Nro. torre del cliente", "name": "customer__tower" , "type": "int"},
            {"label": "Tipo de cliente", "name": "customer__kind" , "type": "str"},
        ]

        return JsonResponse(data={'tags': data})

class TemplateWhatsappView(BaseViewSet):

    """

        Class define ViewSet of TemplateWhatsappModel.
    
    """

    queryset: BaseModel = TemplateWhatsappModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TemplateWhatsappSerializer
    documentation_class = DocumentationTemplateWhatsapp()

    @method_decorator(permission_required('communications.add_templatewhatsappmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_templatewhatsappmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_templatewhatsappmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.delete_templatewhatsappmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):

        triggerQuery = TriggerEventModel.objects.filter(whatsapp_event__template__pk = kwargs['pk'])
        massiveQuery = MassiveCommunicationsModel.objects.filter(whatsapp_event__template__pk = kwargs['pk'])

        if len(triggerQuery) > 0:
            raise ValidationError({'pk error': f'This template is associated to the event {triggerQuery[0].name}'})
        elif len(massiveQuery) > 0:
            raise ValidationError({'pk error': f'This template is associated to the massive {massiveQuery[0].name}'})

        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('communications.view_templatewhatsappmodel',raise_exception=True))
    @documentation_class.documentation_datables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TemplateWhatsappDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_templatewhatsappmodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:
        
        return TemplateWhatsappDatatable(request).get_data()

    @method_decorator(permission_required('communications.change_templatewhatsappmodel',raise_exception=True))
    @documentation_class.documentation_test()
    @action(detail=False, methods=['post'])
    def test_template(self, request):

        """
        
            This funtion test voice call

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """
        
        #Get values
        telephone: str = request.POST.get('telephone', None)
        template: str = request.POST.get('template_name', None)
        params: List[str] = ast.literal_eval( request.POST.get('params', "[]") )

        data: Dict[str, str] = {
            "service__technology_kind":"Hogar",
            "service__number":"5000",
            "service__due_day":"25/05/2019",
            "service__status":"Activo",
            "service__composite_address":"San Miguel, Jiron Arica, 126",
            "service__street":"Arica",
            "service__house_number":"126",
            "service__apartment_number":"126",
            "service__tower":"126",
            "service__commune":"Arica",
            "service__location":"5000",
            "service__activity":"Hotel",
            "node__code":"codeXXX",
            "node__address":"San Miguel, Jiron Arica, 126",
            "node__house_number":"126",
            "node__commune":"Arica",
            "node__gpon":"Si",
            "node__gepon":"No",
            "node__pon_port":"Port 5000",
            "node__box_location":"San Miguel",
            "node__splitter_location":"San Miguel",
            "node__expected_power":"500w",
            "node__towers":"500",
            "node__apartments":"500",
            "node__phone":"+51362548965",
            "plan__name":"Plan solidario",
            "plan__price":"19990",
            "plan__category":"Hogar",
            "plan__uf":"Si",
            "plan__active":"Si",
            "customer__rut":"15.265.659-k",
            "customer__name":"Antonio Jose",
            "customer__email":"antonio@gmail.com",
            "customer__phone":"+5196589536",
            "customer__default_due_day":"25/06/2019",
            "customer__street":"San Miguel, Jiron Arica, 126",
            "customer__house_number":"126",
            "customer__apartment_number":"126",
            "customer__tower":"126",
            "customer__kind":"Hogar",
        }

        #check if contains params
        if telephone and template:

            #Get credentials
            global_preferences = global_preferences_registry.manager()
            channel: str = "618703f211ed4698b74897c106a7326c"
            token: str = global_preferences['communications__Message_bird']
            namespace_whatsapp_api: str = global_preferences['communications__namespace']

            params_data: List[Dict[str, Any]] = [ {'default': data[param]} for param in  params ]

            requests.post(
                url='https://conversations.messagebird.com/v1/conversations/start',
                headers={
                    'Authorization': f'AccessKey {token}',
                },
                json={
                    "to":telephone,
                    "type":"hsm",
                    "channelId":f"{channel}",
                    "content": {
                        "hsm": {
                            "namespace": namespace_whatsapp_api,
                            "templateName": template,
                            "language": {
                                "policy": "deterministic",
                                "code": "es"
                            },
                            "params": params_data
                        },
                    }
                }
            )

        else:

            raise ValidationError({
                "Requeried fields":" 'telephone' and 'template_name' fields is required "
            })

        return JsonResponse({})

    @method_decorator(permission_required('communications.add_templatewhatsappmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def preview_template(self, request):

        """
        
            This funtion returns the preview of a template

            :param self: Instance of Class TemplateWhatsappView
            :type self: TemplateWhatsappView
            :param request: data of request
            :type request: request
        
        """

        #Get values
        template: str = request.POST.get('template', '')
        data: Dict[str, str] = request.POST.get('data', None)
        
        if data == None: 
            data: Dict = []
        else:
            try:
                data: Dict = json.loads(data)
                tags: Dict = {}
                for i,element in enumerate(data):
                    tags["{{" + str(i+1) + "}}"] = element
            except:
                raise ValidationError({'Invalid tags data': 'A valid dictionary wasnt supplied'})

        for i,j in tags.items():
            template = template.replace(i,str(j))

        return JsonResponse(data={'template': template})
        
    @method_decorator(permission_required('communications.view_templatewhatsappmodel',raise_exception=True))
    @documentation_class.documentation_tags()
    @action(detail=False, methods=['get'])
    def tags(self, request):

        return  JsonResponse([
            {"value":"service__technology_kind", "name":"Tipo de tecnología del servicio"},
            {"value":"service__number", "name":"Número de servicio"},
            {"value":"service__due_day", "name":"Dìa de pago del servicio"},
            {"value":"service__status", "name":"Estado del servicio"},
            {"value":"service__composite_address", "name":"Dirección del servicio"},
            {"value":"service__street", "name":"Calle del servicio"},
            {"value":"service__house_number", "name":"Número de casa del servicio"},
            {"value":"service__apartment_number", "name":"Número de apartamento del servicio"},
            {"value":"service__tower", "name":"Torre del servicio"},
            {"value":"service__commune", "name":"Comuna del servicio"},
            {"value":"service__location", "name":"Ubicación del servicio"},
            {"value":"service__activity", "name":"Actividad del servicio"},

            {"value":"node__code", "name":"Código del nodo"},
            {"value":"node__address", "name":"Dirección del nodo"},
            {"value":"node__house_number", "name":"Número de casa de nodo"},
            {"value":"node__commune", "name":"Comuna del nodo"},
            {"value":"node__gpon", "name":"GPON"},
            {"value":"node__gepon", "name":"GEPON"},
            {"value":"node__pon_port", "name":"PON port"},
            {"value":"node__box_location", "name":"Ubicación de caja del nodo"},
            {"value":"node__splitter_location", "name":"Ubicación del splitter del nodo"},
            {"value":"node__expected_power", "name":"Potencia esperada del nodo"},
            {"value":"node__towers", "name":"Torres del nodo"},
            {"value":"node__apartments", "name":"Apartamentos del nodo"},
            {"value":"node__phone", "name":"Telefono del nodo"},

            {"value":"plan__name", "name":"Nombre del plan"},
            {"value":"plan__price", "name":"Precio del plan"},
            {"value":"plan__category", "name":"Categoría del plan"},
            {"value":"plan__uf", "name":"UF plan"},
            {"value":"plan__active", "name":"¿Esta activo el plan?"},

            {"value":"customer__rut", "name":"Rut del cliente"},
            {"value":"customer__name", "name":"Nombre del cliente"},
            {"value":"customer__email", "name":"Correo del cliente"},
            {"value":"customer__phone", "name":"Telefono del cliente"},
            {"value":"customer__default_due_day", "name":"Día de pago del cliente"},
            {"value":"customer__street", "name":"Calle del cliente"},
            {"value":"customer__house_number", "name":"Número de casa del cliente"},
            {"value":"customer__apartment_number", "name":"Número de apartamento del cliente"},
            {"value":"customer__tower", "name":"Torre del cliente"},
            {"value":"customer__kind", "name":"Tipo de cliente"},
            
        ], safe=False)

class TemplateTextView(BaseViewSet):

    """

        Class define ViewSet of TemplateTextModel.
    
    """

    queryset: BaseModel = TemplateTextModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TemplateTextSerializer
    documentation_class = DocumentationTemplateText()

    @method_decorator(permission_required('communications.add_templatetextmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_templatetextmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_templatetextmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.delete_templatetextmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):

        triggerQuery = TriggerEventModel.objects.filter(text_event__template__pk = kwargs['pk'])
        massiveQuery = MassiveCommunicationsModel.objects.filter(text_event__template__pk = kwargs['pk'])

        if len(triggerQuery) > 0:
            raise ValidationError({'pk error': f'This template is associated to the event {triggerQuery[0].name}'})
        elif len(massiveQuery) > 0:
            raise ValidationError({'pk error': f'This template is associated to the massive {massiveQuery[0].name}'})

        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('communications.view_templatetextmodel',raise_exception=True))
    @documentation_class.documentation_datables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables struct

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TemplateTextDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_templatetextmodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:
        
        """
        
            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return TemplateTextDatatable(request).get_data()

    @method_decorator(permission_required('communications.change_templatetextmodel',raise_exception=True))
    @documentation_class.documentation_test()
    @action(detail=False, methods=['post'])
    def test_template(self, request):

        """
        
            This funtion test template

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """
        
        #Get values
        telephone: str = request.POST.get('telephone', None)
        template: str = request.POST.get('template', None)
        data: Dict[str, str] = request.POST.get('data', None)

        if data == None: 
            data: Dict = {}
        else:
            try:
                data: Dict = json.loads(data)
            except:
                raise ValidationError({'Invalid tags data': 'A valid dictionary wasnt supplied'})

        #check if contains params
        if telephone and template:

            #Get client
            global_preferences = global_preferences_registry.manager()
            client_message_bird = Client( access_key=global_preferences['communications__Message_bird'] )

            client_message_bird.message_create(
                originator='Banda Ancha',
                recipients=[ telephone.replace( '+', '') ],
                body=Template(template).render(data),
            )

        else:

            raise ValidationError({
                "Requeried fields":" 'telephone' and 'template' fields is required "
            })

        return JsonResponse({})

    @method_decorator(permission_required('communications.add_templateemailmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def preview_template(self, request):

        """
        
            This funtion returns the preview of a template

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        #Get values
        template: str = request.POST.get('template', '')
        data: Dict[str, str] = request.POST.get('data', None)
        
        if data == None: 
            data: Dict = {}
        else:
            try:
                data: Dict = json.loads(data)
            except:
                raise ValidationError({'Invalid tags data': 'A valid dictionary wasnt supplied'})

        body = Template(template).render(data)

        return JsonResponse(data={'template': body})

class TemplateCallView(BaseViewSet):

    """

        Class define ViewSet of TemplateCallModel.
    
    """

    queryset: BaseModel = TemplateCallModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TemplateCallSerializer
    documentation_class = DocumentationTemplateCall()

    @method_decorator(permission_required('communications.add_templatecallmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_templatecallmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_templatecallmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.delete_templatecallmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):

        triggerQuery = TriggerEventModel.objects.filter(call_event__template__pk = kwargs['pk'])
        massiveQuery = MassiveCommunicationsModel.objects.filter(call_event__template__pk = kwargs['pk'])

        if len(triggerQuery) > 0:
            raise ValidationError({'pk error': f'This template is associated to the event {triggerQuery[0].name}'})
        elif len(massiveQuery) > 0:
            raise ValidationError({'pk error': f'This template is associated to the massive {massiveQuery[0].name}'})
        
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('communications.view_templatecallmodel',raise_exception=True))
    @documentation_class.documentation_datables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request)  -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TemplateCallDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_templatecallmodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:
        
        return TemplateCallDatatable(request).get_data()

    @method_decorator(permission_required('communications.change_templatecallmodel',raise_exception=True))
    @documentation_class.documentation_test()
    @action(detail=False, methods=['post'])
    def test_template(self, request) -> JsonResponse:

        """
        
            This funtion test voice call

            :param self: Instance of Class TemplateCallView
            :type self: TemplateCallView
            :param request: data of request
            :type request: request
        
        """

        #Get values
        telephone: str = request.POST.get('telephone', None)
        template: str = request.POST.get('template', None)
        data: Dict[str, str] = request.POST.get('data', None)

        if data == None: 
            data: Dict = {}
        else:
            try:
                data: Dict = json.loads(data)
            except:
                raise ValidationError({'Invalid tags data': 'A valid dictionary wasnt supplied'})

        #check if contains params
        if telephone and template:

            #Get client
            global_preferences = global_preferences_registry.manager()
            client_message_bird = Client( access_key=global_preferences['communications__Message_bird'] )

            client_message_bird.voice_message_create(
                recipients=[ telephone.replace('+', '') ],
                body=Template(template).render(data),
                params={
                    'language':'es-mx',
                    'voice': 'female',
                    'ifMachine':'continue',
                    'repeat':1,
                },
            )

        else:

            raise ValidationError({
                "Requeried fields":" 'telephone', 'template' and 'data' fields is required "
            })

        return JsonResponse({})

    @method_decorator(permission_required('communications.add_templatecallmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def preview_template(self, request):

        """
        
            This funtion returns the preview of a template

            :param self: Instance of Class TemplateCallView
            :type self: TemplateCallView
            :param request: data of request
            :type request: request
        
        """

        #Get values
        template: str = request.POST.get('template', '')
        data: Dict[str, str] = request.POST.get('data', None)
        
        if data == None: 
            data: Dict = {}
        else:
            try:
                data: Dict = json.loads(data)
            except:
                raise ValidationError({'Invalid tags data': 'A valid dictionary wasnt supplied'})

        body = Template(template).render(data)

        return JsonResponse(data={'template': body})

class InternalChatView(BaseViewSet):

    """

        Class define ViewSet of InternalChatModel.
    
    """

    queryset: BaseModel = InternalChatModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = InternalChatSerializer

    @method_decorator(permission_required(['communications.add_internalchatmodel', 'tickets.add_typifymodel'],raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required(['communications.change_internalchatmodel', 'tickets.add_typifymodel'],raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required(['communications.change_internalchatmodel', 'tickets.add_typifymodel'],raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required(['communications.delete_internalchatmodel', 'tickets.add_typifymodel'],raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        
        def send_message(thread: str, channel: str, instance: InternalChatModel, username: str):

            global_preferences = global_preferences_registry.manager()

            #Get the right access token based on the operator
            client = WebClient(token=instance.operator.slack_token)

            blocks: List[Dict[str, Any]] =  [
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":iris: *Iris* Tickets"
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":writing_hand:*" + username + "* ha eliminado un comentario: *" + instance.message + "*"
                    }
                }
            ]

            client.chat_postMessage(
                blocks=blocks,
                thread_ts=thread,
                channel=channel,
                text=f" " + username + " ha eliminado un comentario: " + instance.message 
            )

        if self.get_object().creator != self.request.user:
            raise ValidationError({"invalid user": "only the creator can edit this message"})

        typify_instance = TypifyModel.objects.filter(ID=self.get_object().chat.ID).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                send_message(thread, typify_instance.slack_thread[thread], self.get_object(), self.request.user.username)

        escalation_instance = EscalationModel.objects.filter(ID=self.get_object().chat.ID).first()
        if escalation_instance and escalation_instance.slack_thread != None:
            for thread in escalation_instance.slack_thread:
                send_message(thread, escalation_instance.slack_thread[thread], self.get_object(), self.request.user.username)

        return super().destroy(request, *args, **kwargs)


class EmailView(BaseViewSet):

    """

        Class define ViewSet of EmailModel.
    
    """

    parser_class: tuple = (FileUploadParser,)

    queryset: BaseModel = EmailModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EmailSerializer

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @method_decorator(permission_required('communications.view_emailmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def get_messages(self, request):

        """
        
            This funtion return text data

            :param self: Instance of Class EmailView
            :type self: EmailView
            :param request: data of request
            :type request: request
        
        """

        emails: Union[List[str], None] = request.POST.getlist('emails', None)

        if not isinstance(emails, list):

            raise ValidationError({
                'Error emails': 'Required emails field'
            })

        result: List[Dict[str, str]] = [ { 'created': text.created, 'message': text.message, 'type':'email' } for text in EmailModel.objects.filter(receiver__in=emails) ]

        return JsonResponse( result, safe=False )

    @method_decorator(permission_required('communications.view_emailmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EmailDatatable
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        email_datatables = EmailDatatable(request)
        email_datatables.ID_TABLE = pk

        return JsonResponse(email_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_emailmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EmailDatatable
            :type self: EmailDatatable
            :param request: data of request
            :type request: request
        
        """

        email_datatables = EmailDatatable(request)
        email_datatables.ID_TABLE = pk

        return email_datatables.get_data()

    @method_decorator(permission_required('communications.view_emailmodel',raise_exception=True))   
    @action(detail=True, methods=['get'])
    def chat_emails(self, request, pk) -> JsonResponse:

        try:
            email_instance = EmailModel.objects.get(ID=pk)
        except:
            raise ValidationError({'Invalid pk': 'Theres no instance with that ID'})

        email_query = EmailModel.objects.filter(chat=email_instance.chat)

        if email_instance.chat == None:
            response = []
            return JsonResponse(response, safe=False)
            
        chat_instance = email_instance.chat
        chat_instance.new_messages == 0
        chat_instance.save()

        response = []

        for instance in email_query:

            if not instance.viewed:
                instance.viewed = True
                instance.save()

            response.append({
                "sender": instance.sender,
                "receiver": instance.receiver,
                "subject": instance.subject,
                "ID": instance.ID,
                "created": instance.created,
                "_type": 'IN' if instance._type == 0 else 'OUT',
                "channel": instance.event.name if instance.event else  instance.massive.name if instance.massive else instance.cycle.name if instance.cycle else None,
                'message': instance.message
            })
        
        return JsonResponse(response, safe=False)

    @method_decorator(permission_required('communications.add_emailmodel',raise_exception=True))   
    @action(detail=False, methods=['post'])
    def send_email(self, request) -> JsonResponse:

        template_id = request.data.get("template")
        sender = request.data.get("sender")
        receiver = request.data.get("receiver")
        service_number = request.data.get("service_number")
        subject = request.data.get("subject")
        operator = request.data.get("operator")
        chat_id = request.data.get("chat",0)

        try:
            template = TemplateEmailModel.objects.get(ID=template_id)
        except:
            raise ValidationError({'Invalid template': 'Theres no instance with that ID'})

        try:
            chat_instance = ChatModel.objects.get(ID=chat_id)
        except:
            chat_instance=None

        global_preferences = global_preferences_registry.manager()

        matrix_filters = [['number', 'equal', service_number]]

        data: Dict[str, Any] = {
            "filters": matrix_filters,
            "includes": [],
            "excludes": [],
            "unpaid_ballot": False,
            "criterion": 'D'
        }

        matrix_url: str = global_preferences['general__matrix_domain'] + 'services-filters/'

        response = requests.get(
            url=matrix_url,
            headers={
                "Authorization": global_preferences['matrix__matrix_token']
            },
            json=data,
            verify=False,
        )

        if response.status_code != 200:

            raise ValidationError({
                "Error matrix": "Error matrix filter"
            })

        data = response.json()[0]

        months: Dict[int, str] = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
            10: "Octubre",
            11: "Noviembre",
            12: "Diciembre",
        }

        weekdays: Dict[int, str] = {
            0: "Lunes",
            1: "Martes",
            2: "Miercoles",
            3: "Jueves",
            4: "Viernes",
            5: "Sabado",
            6: "Domingo",
        }

        current_datetime: datetime = datetime.now()

        data['current__month'] = months[int(current_datetime.month)]
        data['current__year'] = current_datetime.year
        data['current__day'] = current_datetime.day
        data['current__week_day'] = weekdays[int(current_datetime.weekday())]
        data['current__datetime'] = current_datetime.strftime("%d/%m/%Y")

        json_data = {
            'subject': subject,
            'sender': sender,
            'receiver': receiver,
            'operator': OperatorModel.objects.get(ID=operator),
            'message': Template(template.template_html).render(data),
            'chat': chat_instance
        }

        create_email(json_data, request.user)

        return JsonResponse({'success': 'Email sent'})

@method_decorator((permission_required('communications.view_filesemailmodel',raise_exception=True)), name='dispatch')
class FilesEmailView(BaseViewSet):

    """

        Class define ViewSet of FilesEmailModel.
    
    """

    queryset: BaseModel = FilesEmailModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = FilesEmailSerializer
    http_method_names = ['get']

class TextView(BaseViewSet):

    """

        Class define ViewSet of TextModel.
    
    """

    queryset: BaseModel = TextModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TextSerializer

    @method_decorator(permission_required('communications.view_textmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def get_messages(self, request):

        """
        
            This funtion return text data

            :param self: Instance of Class TextView
            :type self: TextView
            :param request: data of request
            :type request: request
        
        """

        phones: str = request.POST.getlist('phones', None)

        if not isinstance(phones, list):

            raise ValidationError({
                'Error phones': 'Required phones field'
            })

        result: List[dict] = [ { 'created': text.created, 'message': text.message, 'type':'text' } for text in TextModel.objects.filter(receiver__in=phones) ]

        return JsonResponse( result, safe=False )

    @method_decorator(permission_required('communications.view_textmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextView
            :type self: TextView
            :param request: data of request
            :type request: request
        
        """

        text_datatables = TextDatatable(request)
        text_datatables.ID_TABLE = pk

        return JsonResponse(text_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_textmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextView
            :type self: TextView
            :param request: data of request
            :type request: request
        
        """

        text_datatables = TextDatatable(request)
        text_datatables.ID_TABLE = pk

        return text_datatables.get_data()

class WhatsappView(BaseViewSet):

    """

        Class define ViewSet of WhatsappModel.
    
    """

    queryset: BaseModel = WhatsappModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = WhatsappSerializer

    @action(detail=False, methods=['get'])
    def whatsapp_templates(self, request) -> JsonResponse:

        global_preferences = global_preferences_registry.manager()

        token: str = global_preferences['communications__Message_bird']
        
        list_templates: List[Dict[str, str]] = requests.get(
            url='https://integrations.messagebird.com/v1/public/whatsapp/templates',
            headers={
                'Authorization': f'AccessKey {token}',
            },
        ).json()

        list_templates: List[Dict[str, str]] =  list( map(
                                                    lambda x: {'name':x['name'], 'template':x['content']},
                                                    filter(lambda x: x["status"] == "APPROVED", list_templates)
                                                ) )

        return JsonResponse( list_templates, safe=False )


    @action(detail=False, methods=['post'])
    def get_messages(self, request):

        """
        
            This funtion return text data

            :param self: Instance of Class WhatsappView
            :type self: WhatsappView
            :param request: data of request
            :type request: request
        
        """

        phones: Union[str, None] = request.POST.getlist('phones', None)

        if not isinstance(phones, list):

            raise ValidationError({
                'Error phones': 'Required phones field'
            })

        result: List[Dict[str, str]] = [ { 'created': text.created, 'message': text.message, 'type':'whatsapp' } for text in WhatsappModel.objects.filter(receiver__in=phones) ]

        return JsonResponse( result, safe=False )

    @method_decorator(permission_required('communications.view_whatsappmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class WhatsappView
            :type self: WhatsappView
            :param request: data of request
            :type request: request
        
        """

        whatsapp_datatables = WhatsappDatatable(request)
        whatsapp_datatables.ID_TABLE = pk

        return JsonResponse(whatsapp_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_whatsappmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class WhatsappView
            :type self: WhatsappView
            :param request: data of request
            :type request: request
        
        """

        whatsapp_datatables = WhatsappDatatable(request)
        whatsapp_datatables.ID_TABLE = pk

        return whatsapp_datatables.get_data()

class VoiceCallView(BaseViewSet):

    """

        Class define ViewSet of VoiceCallModel.
    
    """

    queryset: BaseModel = VoiceCallModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = VoiceCallSerializer

    @method_decorator(permission_required('communications.view_voicecallmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def get_messages(self, request):

        """
        
            This funtion return text data

            :param self: Instance of Class VoiceCallView
            :type self: VoiceCallView
            :param request: data of request
            :type request: request
        
        """

        phones: str = request.POST.getlist('phones', None)

        if not isinstance(phones, list):

            raise ValidationError({
                'Error phones': 'Required phones field'
            })

        result: List[Dict[str, str]] = [ { 'created': text.created, 'message': text.message, 'type':'voice_call' } for text in VoiceCallModel.objects.filter(receiver__in=phones) ]

        return JsonResponse( result, safe=False )

    @method_decorator(permission_required('communications.view_voicecallmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class VoiceCallView
            :type self: VoiceCallView
            :param request: data of request
            :type request: request
        
        """

        call_datatables = VoiceCallDatatable(request)
        call_datatables.ID_TABLE = pk

        return JsonResponse(call_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_voicecallmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class VoiceCallView
            :type self: VoiceCallView
            :param request: data of request
            :type request: request
        
        """

        call_datatables = VoiceCallDatatable(request)
        call_datatables.ID_TABLE = pk

        return call_datatables.get_data()

class ChatView(BaseViewSet):

    """

        Class define ViewSet of ChatModel.
    
    """

    queryset: BaseModel = ChatModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = ChatSerializer

def current_time_tags(data: Dict[str,Any]) -> Dict[str,Any] :

    months : Dict[int, str] = {
        1: "Enero",
        2: "Febrero",
        3: "Marzo",
        4: "Abril",
        5: "Mayo",
        6: "Junio",
        7: "Julio",
        8: "Agosto",
        9: "Septiembre",
        10: "Octubre",
        11: "Noviembre",
        12: "Diciembre",
    }

    weekdays: Dict[int, str] = {
        0:"Lunes",
        1:"Martes",
        2:"Miercoles",
        3:"Jueves",
        4:"Viernes",
        5:"Sabado",
        6:"Domingo",
    }

    current_datetime: datetime = datetime.now()

    data['current__month'] = months[current_datetime.month]
    data['current__year'] = current_datetime.year
    data['current__day'] = current_datetime.day
    data['current__week_day'] = weekdays[current_datetime.weekday()]
    data['current__datetime'] = current_datetime.strftime("%d/%m/%Y")

    return data

class TriggerEventView(BaseViewSet):

    """

        Class define ViewSet of TriggerEventModel.
    
    """

    queryset: BaseModel = TriggerEventModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TriggerEventSerializer
    documentation_class = DocumentationTriggerEvent()

    @method_decorator(permission_required('communications.add_triggereventmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_triggereventmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_triggereventmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.delete_triggereventmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('communications.view_triggereventmodel',raise_exception=True))
    @documentation_class.documentation_datables_struct()
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TriggerEventView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TriggerEventDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_triggereventmodel',raise_exception=True))
    @documentation_class.documentation_datables()
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TriggerEventView
            :type self: TriggerEventView
            :param request: data of request
            :type request: request
        
        """

        return TriggerEventDatatable(request).get_data()

    @documentation_class.documentation_active()
    @action(detail=False, methods=['post'])
    def active(self, request) -> JsonResponse:

        """
        
            This funtion activate event

            :param self: Instance of Class TriggerEventView
            :type self: TriggerEventView
            :param request: data of request
            :type request: request
        
        """

        if set(["event","info"]) - set(request.data.keys()):

            raise ValidationError({
                "Required fields": "Required fields: event, info"
            })

        #Get event name
        name: str = request.data['event']
        #Get services data

        services: List[Dict[str, Any]] = request.data['info']

        def send_data(message:dict, services: List[dict], trigger: TriggerEventModel):

            """
        
                This funtion send communications

                :param message: Multiples values required
                :type message: dict
                :param services: Values list of services
                :type services: list
            
            """

            def send_data_email(message: Dict[str, Any], services: List[Dict[str, Any]], trigger: TriggerEventModel):

                """
        
                    This funtion send emails

                    :param message: Multiples values required
                    :type message: dict
                    :param services: Values list of services
                    :type services: list
                
                """

                def ticket_send_email(message: Dict[str, Any], trigger: TriggerEventModel):

                    """
                
                        This function send email to customer

                        :param message: Dict with data of service
                        :type message: dict

                    """

                    def base64_to_file(str_file: str) -> ContentFile:

                        """
                
                            This function return file object

                            :param str_file: Str base 64
                            :type str_file: Str

                            :returns: File of base64
                            :rtype: File

                        """

                        file_64: str = base64.b64decode(str_file, validate=True)

                        mime_type = from_buffer(file_64, mime=True)
                        file_ext = guess_extension(mime_type)

                        file_object: ContentFile = ContentFile(file_64, name='file_email.' + file_ext )

                        return file_64 , 'file_email.' + file_ext

                    #Check is valid email
                    if validate_email(message['receiver']):

                        email: EmailModel = EmailModel.objects.create(operator=message['operator'])
                        
                        email.outbox: bool = True
                        email.creator: User = message['user']
                        email.updater: User = message['user']
                        email.receiver: str = message['email_test'] if message['is_test'] else message['receiver']
                        email.subject: str = message['subject']
                        email.sender: str = message['email_origin']
                        email.message: str = message['message']
                        email.event: TriggerEventModel = trigger
                        email.viewed: bool = True
                        email.main: bool = True
                        chat_instance = ChatModel.objects.create(operator=message['operator'])
                        email.chat = chat_instance
                        email.save()

                        mail = EmailMultiAlternatives(
                            subject=email.subject,
                            body=  email.message,
                            from_email=email.sender,
                            to=[email.receiver],
                            headers={"Reply-To": message['email_response']},
                            bcc=[ message['email_copy'] ] if message['email_copy'] else []
                        )
                        mail.content_subtype = 'html'

                        for file_email in message['files']:

                            #Get file
                            file_email, name_file = base64_to_file(file_email)
                            
                            #Create file object
                            file_object: FilesEmailModel = FilesEmailModel()
                            file_object.email: EmailModel = email
                            file_object.files: ContentFile = ContentFile(file_email, name_file)
                            file_object.operator: int = message['operator']
                            file_object.save()

                            #Add file to email object
                            mail.attach( name_file, file_email )
                        
                        mail.send()

                email_event: EventEmailModel = message['email_event']

                for data in services:

                    data: Dict[str, Any] = current_time_tags(data)

                    if 'service__number' in data:

                        data['service__number_64'] = str(base64.b64encode(bytes(str(data['service__number']), 'utf-8')), 'utf-8')

                    if 'customer__id' in data:
                        
                        data['customer__id_64'] = str(base64.b64encode(bytes(str(data['customer__id']), 'utf-8')), 'utf-8')

                    #Render message to customer with data
                    message: Dict[str, Any] = message
                    message['message']: str = Template(email_event.template.template_html).render(data)
                    message['subject']: str = email_event.template.subject
                    message['receiver']: str = data['customer__email']
                    message['files']: List[str] = data.get('files', [])
                    message['operator'] = email_event.operator

                    ticket_send_email(
                        message=message,
                        trigger=trigger
                    )

            def send_data_whatsapp(message: Dict[str, Any], services: List[Dict[str, Any]]):

                """
        
                    This funtion send whatsapp

                    :param message: Multiples values required
                    :type message: dict
                    :param services: Values list of services
                    :type services: list
                
                """

                def ticket_send_ws(message: Dict[str, Any]) -> None:
                    
                    """
                
                        This function send email to customer

                        :param message: Dict with data of service
                        :type message: dict

                    """

                    #Channel to send
                    channel: str = message['channel']
                    token: str = message['token']
                    namespace_whatsapp_api: str = message['namespace_whatsapp_api']

                    phone: str = message['phone_number_test_whatsapp'] if message['is_test'] else message['customer__phone']
                    phone: str = phone.replace( '+', '' )
                    #Create whatsapp model instance
                    #
                    #Get message
                    whatsapp_model: WhatsappModel = WhatsappModel()
                    whatsapp_model.message: str = message['message']
                    whatsapp_model.receiver: str = phone
                    whatsapp_model.outbox: bool = True
                    whatsapp_model.creator: User = message['user']
                    whatsapp_model.updater: User = message['user']
                    whatsapp_event.operator: int = message['whatsapp_event'].operator
                    #
                    ############################
                    
                    message_whatsapp = requests.post(
                        url='https://conversations.messagebird.com/v1/conversations/start',
                        headers={
                            'Authorization': f'AccessKey {token}',
                        },
                        json={
                            "to":phone,
                            "type":"hsm",
                            "channelId":f"{channel}",
                            "content": {
                                "hsm": {
                                    "namespace": namespace_whatsapp_api,
                                    "templateName": message['template_name'],
                                    "language": {
                                        "policy": "deterministic",
                                        "code": "es"
                                    },
                                    "params": message['params']
                                },
                            }
                        }
                    )

                    whatsapp_model.id_messagebird: int = message_whatsapp.json()['id']
                    whatsapp_model.save()

                whatsapp_event: EventWhatsappModel = message['whatsapp_event']

                for data in services:

                    data: Dict[str, Any] = current_time_tags(data)

                    if 'service__number' in data:

                        data['service__number_64'] = str(base64.b64encode(bytes(str(data['service__number']), 'utf-8')), 'utf-8')

                    if 'customer__id' in data:
                        
                        data['customer__id_64'] = str(base64.b64encode(bytes(str(data['customer__id']), 'utf-8')), 'utf-8')

                    #Render message to customer with data
                    message: Dict[str, Any] = message
                    message['message']: str = Template(whatsapp_event.template.template).render(data)
                    message['customer__phone']: str = data['customer__phone']
                    message['params']: List[Dict[str, Any]] = [ {'default': data[param]} for param in  whatsapp_event.template.order]

                    ticket_send_ws(
                        message=message,
                    )

            def send_data_call_voice(message: Dict[str, Any], services: List[Dict[str, Any]]):
                
                """
        
                    This funtion send voice call

                    :param message: Multiples values required
                    :type message: dict
                    :param services: Values list of services
                    :type services: list
                
                """

                def ticket_send_call_voice(message: dict) -> None:
                    
                    """
                
                        This function send email to customer

                        :param message: Dict with data of service
                        :type message: dict

                    """

                    phone: str = message['phone_number_test_voice_call'] if message['is_test'] else message['customer__phone']

                    #Create email model instance
                    #
                    voice_call_model: VoiceCallModel = VoiceCallModel()
                    voice_call_model.message: str = message['message']
                    voice_call_model.receiver: str = phone
                    voice_call_model.outbox: bool = True
                    voice_call_model.creator: User = message['user']
                    voice_call_model.updater: User = message['user']
                    voice_call_model.operator: int = message['voice_call_event'].operator
                    #
                    ############################

                    #Send communications
                    message = message['message_bird_client'].voice_message_create(
                        recipients=[ phone.replace('+', '') ],
                        body=voice_call_model.message,
                        params={
                            'language':'es-mx',
                            'voice': 'female',
                            'ifMachine':'continue',
                            'repeat':1,
                        },
                    )

                    voice_call_model.id_messagebird: int = message.id
                    voice_call_model.save()

                voice_call_event: EventCallModel = message['voice_call_event']

                for data in services:

                    data: Dict[str, Any] = current_time_tags(data)

                    if 'service__number' in data:

                        data['service__number_64'] = str(base64.b64encode(bytes(str(data['service__number']), 'utf-8')), 'utf-8')

                    if 'customer__id' in data:
                        
                        data['customer__id_64'] = str(base64.b64encode(bytes(str(data['customer__id']), 'utf-8')), 'utf-8')

                    #Render message to customer with data
                    message: Dict[str, Any] = message
                    message['message']: str = Template(voice_call_event.template.template).render(data)
                    message['customer__phone']: str = data['customer__phone']

                    ticket_send_call_voice(
                        message=message,
                    )
            
            def send_data_sms(message: Dict[str, Any], services: List[Dict[str, Any]]):

                """
        
                    This funtion send sms

                    :param message: Multiples values required
                    :type message: dict
                    :param services: Values list of services
                    :type services: list
                
                """

                def ticket_send_sms(message: Dict[str, Any]):
                
                    """
                
                        This function send email to customer

                        :param message: Dict with data of service
                        :type message: dict

                    """

                    phone: str = message['phone_number_test_sms'] if message['is_test'] else message['customer__phone']

                    #Create text model instance
                    #
                    text_model: TextModel = TextModel()
                    text_model.message: str = message['message']
                    text_model.receiver: str = phone
                    text_model.outbox: bool = True
                    text_model.creator: User = message['user']
                    text_model.updater: User = message['user']
                    text_model.operator: int = message['text_event']
                    #
                    ############################
                    
                    #Send communications
                    message = message['message_bird_client'].message_create(
                        originator='Banda Ancha',
                        recipients=[ phone.replace( '+', '') ],
                        body=text_model.message,
                    )

                    text_model.id_messagebird: int = message.id
                    text_model.save()

                text_event: EventTextModel = message['text_event']

                for data in services:

                    data: Dict[str, Any] = current_time_tags(data)

                    if 'service__number' in data:

                        data['service__number_64'] =str(base64.b64encode(bytes(str(data['service__number']), 'utf-8')), 'utf-8')

                    if 'customer__id' in data:
                        
                        data['customer__id_64'] = str(base64.b64encode(bytes(str(data['customer__id']), 'utf-8')), 'utf-8')

                    #Render message to customer with data
                    message: Dict[str, Any] = message
                    
                    message['message']: str = Template(text_event.template.template).render(data)
                    message['customer__phone']: str = data['customer__phone']

                    ticket_send_sms(
                        message=message,
                    )

            if message['email_event']:

                send_data_email(message.copy(), services, trigger)

            if message['whatsapp_event']:

                send_data_whatsapp(message.copy(), services)

            if message['voice_call_event']:

                send_data_call_voice(message.copy(), services)

            if message['text_event']:

                send_data_sms(message.copy(), services)
            
        if len(services):

            #Get Trigger instance
            trigger: TriggerEventModel = TriggerEventModel.objects.get(name=name)

            global_preferences = global_preferences_registry.manager()
            user: User = User.objects.get(username=global_preferences['users__API_username'])
            enviroment: str = global_preferences['general__Enviroment']
            is_test: bool = enviroment in ['local', 'development', 'stage']
            email_event: EventEmailModel = trigger.email_event
            whatsapp_event: EventWhatsappModel = trigger.whatsapp_event
            text_event: EventTextModel = trigger.text_event
            voice_call_event: EventCallModel = trigger.call_event

            message: Dict[str, Any] = {
                'user':user,
                'is_test':is_test,
                'channel':global_preferences['communications__whatsapp_channels'],
                'token':global_preferences['communications__Message_bird'],
                'namespace_whatsapp_api':global_preferences['communications__namespace'],
                'message_bird_client': Client( access_key=global_preferences['communications__Message_bird'] ),
                'email_event': None,
                'whatsapp_event': None,
                'text_event': None,
                'voice_call_event': None,
            }
            
            #Set email defaults values
            if email_event:

                message['email_origin']: str = email_event.email_origin
                message['email_test']: str = email_event.email_test
                message['email_response']: str = email_event.email_response
                message['email_copy']: str = email_event.email_copy
                message['email_event']: EventEmailModel = email_event

            #Set whatsapp defaults values
            if whatsapp_event:

                message['template_name']: str = whatsapp_event.template.name
                message['phone_number_test_whatsapp']: str = str(whatsapp_event.phone_number_test)
                message['whatsapp_event']: EventWhatsappModel = whatsapp_event

            #Set text defaults values
            if text_event:

                message['phone_number_test_sms']: str = str(text_event.phone_number_test)
                message['text_event']: EventTextModel = text_event

            #Set voice call defaults values
            if voice_call_event:

                message['phone_number_test_voice_call']: str = str(voice_call_event.phone_number_test)
                message['voice_call_event']: EventCallModel = voice_call_event
                
            #Send communications
            send_data(message, services, trigger)
            
        return JsonResponse(data={})

    @method_decorator(permission_required('communications.change_triggereventmodel',raise_exception=True))
    @documentation_class.documentation_add_channel()
    @action(detail=True, methods=['post'])
    def add_channel(self, request, pk) -> JsonResponse:

        """
        
            This funtion add event to Trigger event

            :param self: Instance of Class TriggerEventView
            :type self: TriggerEventView
            :param request: data of request
            :type request: request
            :param request: pk of TriggerEventModel
            :type request: int
        
        """

        class ValidatorEmail(BaseSerializer):

            """
            
                Class validate email event
            
            """

            class Meta:

                #Class model of serializer
                model: BaseModel = EventEmailModel
                #Fields of serializer
                fields: Union[list, str] = ['email_test', 'email_response', 'email_copy', 'template', 'email_origin', 'operator']

        class ValidatorWhatsapp(BaseSerializer):

            """
            
                Class validate whatsapp event
            
            """
            
            class Meta:

                #Class model of serializer
                model: BaseModel = EventWhatsappModel
                #Fields of serializer
                fields: Union[list, str] = ['phone_number_test', 'template', 'operator']

        class ValidatorText(BaseSerializer):

            """
            
                Class validate text event
            
            """
            
            class Meta:

                #Class model of serializer
                model: BaseModel = EventTextModel
                #Fields of serializer
                fields: Union[list, str] = ['phone_number_test', 'template', 'operator']

        class ValidatorCall(BaseSerializer):
            
            """
            
                Class validate call event
            
            """

            class Meta:

                #Class model of serializer
                model: BaseModel = EventCallModel
                #Fields of serializer
                fields: Union[list, str] = ['phone_number_test', 'template', 'operator']

        channels = {
            'email': {
                'validator':ValidatorEmail,
                'name_file': 'email_event'
            },
            'whatsapp': {
                'validator':ValidatorWhatsapp,
                'name_file': 'whatsapp_event'
            },
            'text': {
                'validator':ValidatorText,
                'name_file': 'text_event'
            },
            'call': {
                'validator':ValidatorCall,
                'name_file': 'call_event'
            },
        }  

        if 'channel' not in request.POST.keys():

            raise serializers.ValidationError( 
        
                {
                    "channel": [
                        "Required 'channel' field."
                    ],
                }

            )

        #Channel to add
        channel: Dict[str, Any] = request.POST['channel']

        if channel not in channels.keys():

            raise serializers.ValidationError( 
        
                {
                    "channel": [
                        "No valid 'channel' field value."
                    ],
                }

            )

        #Get serializer object with data
        validator = channels[ channel ]['validator']( data=request.POST )
        #Add request to serializer
        validator.context['request'] = request

        if validator.is_valid():

            #Create instence of event
            instance_event: BaseModel = validator.create( validator.validated_data )
            #Get instance of TriggerEventModel
            instance_trigger: TriggerEventModel = TriggerEventModel.objects.get(ID=pk)

            #Save event and user
            instance_trigger.updater: User = request.user
            setattr(instance_trigger, channels[ channel ]['name_file'], instance_event)
            instance_trigger.save()

        else:

            raise serializers.ValidationError( validator.errors )

        return JsonResponse(data={})

    @method_decorator(permission_required('communications.change_triggereventmodel',raise_exception=True))
    @documentation_class.documentation_remove_channel()
    @action(detail=True, methods=['post'])
    def remove_channel(self, request, pk) -> JsonResponse:

        """
        
            This funtion remove event to Trigger event

            :param self: Instance of Class TriggerEventView
            :type self: TriggerEventView
            :param request: data of request
            :type request: request
            :param request: pk of TriggerEventModel
            :type request: int
        
        """

        #Dict of name on model of event
        channels: Dict[str, str] = {
            'email': 'email_event',
            'whatsapp': 'whatsapp_event',
            'text': 'text_event',
            'call': 'call_event',
        }

        #Get instance of TriggerEventModel
        trigger_event: TriggerEventModel = TriggerEventModel.objects.get(ID=pk)

        #Get instance of event
        instance_event: Union[EventEmailModel, EventWhatsappModel, EventTextModel, EventCallModel] = getattr(trigger_event, channels[request.POST['channel']])

        if instance_event:

            #Set null to fields of event
            setattr(trigger_event, channels[request.POST['channel']], None)
            trigger_event.save()

            #Delete instance
            instance_event.delete()

        return JsonResponse(data={})

# @method_decorator((permission_required('communications.view_eventemailmodel',raise_exception=True)), name='dispatch')
class EventEmailView(BaseViewSet):

    """

        Class define ViewSet of EventEmailModel.
    
    """

    queryset: BaseModel = EventEmailModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EventEmailSerializer

# @method_decorator((permission_required('communications.view_eventwhatsappmodel',raise_exception=True)), name='dispatch')
class EventWhatsappView(BaseViewSet):

    """

        Class define ViewSet of EventWhatsappModel.
    
    """

    queryset: BaseModel = EventWhatsappModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EventWhatsappSerializer

# @method_decorator((permission_required('communications.view_eventtextmodel',raise_exception=True)), name='dispatch')
class EventTextView(BaseViewSet):

    """

        Class define ViewSet of EventTextModel.
    
    """

    queryset: BaseModel = EventTextModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EventTextSerializer

# @method_decorator((permission_required('communications.view_eventcallmodel',raise_exception=True)), name='dispatch')
class EventCallView(BaseViewSet):

    """

        Class define ViewSet of EventCallModel.
    
    """

    queryset: BaseModel = EventCallModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EventCallSerializer

class EmailTableView(BaseViewSet):

    """

        Class define ViewSet of EmailTablesModel.
    
    """

    queryset: BaseModel = EmailTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EmailTablesSerializer

    # @method_decorator(permission_required('formerCustomers.view_removeequipamentmodel',raise_exception=False))
    # def get_queryset(self, *args, **kwargs):
    #     if self.request.user.has_perm('formerCustomers.view_removeequipamenttablesmodel'):
    #         return self.queryset
    #     else:
    #         return HttpResponseForbidden()
    
    
    @method_decorator(permission_required('communications.add_emailtablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_emailtablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_emailtablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.add_emailtablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('communications.view_emailtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Emisor":{
                    "type":"str",
                    "name":"sender",
                },
                "Receptor":{
                    "type":"str",
                    "name":"receiver",
                },
                "Asunto":{
                    "type":"str",
                    "name":"subject",
                },
                "Evento":{
                    "type":"choices",
                    "name":"event__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/trigger_event/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Masivo":{
                    "type":"choices",
                    "name":"massive__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/massive/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Ciclo":{
                    "type":"choices",
                    "name":"cycle__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/cycle/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'sender': "Emisor",
                'receiver': "Receptor",
                'subject': "Asunto",
                '_type': "Tipo",
                'channel': "Canal",
                'message': "Mensaje"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('communications.view_emailtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EmailTableView
            :type self: EmailTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(EmailTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('communications.view_emailtablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class EmailTableView
            :type self: EmailTableView
            :param request: data of request
            :type request: request
        
        """

        return EmailTablesDatatable(request).get_data()

class WhatsappTableView(BaseViewSet):

    """

        Class define ViewSet of EmailTablesModel.
    
    """

    queryset: BaseModel = WhatsappTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = WhatsappTablesSerializer
      
    @method_decorator(permission_required('communications.add_whatsapptablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_whatsapptablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_whatsapptablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.add_whatsapptablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('communications.view_whatsapptablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Receptor":{
                    "type":"str",
                    "name":"receiver",
                },
                "Evento":{
                    "type":"choices",
                    "name":"event__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/trigger_event/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Masivo":{
                    "type":"choices",
                    "name":"massive__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/massive/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Ciclo":{
                    "type":"choices",
                    "name":"cycle__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/cycle/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'receiver': "Receptor",
                '_type': "Tipo",
                'channel': "Canal",
                'message': "Mensaje"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('communications.view_whatsapptablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class WhatsappTableView
            :type self: WhatsappTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(WhatsappTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('communications.view_whatsapptablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class WhatsappTableView
            :type self: WhatsappTableView
            :param request: data of request
            :type request: request
        
        """

        return WhatsappTablesDatatable(request).get_data()

class TextTableView(BaseViewSet):

    """

        Class define ViewSet of TextTablesModel.
    
    """

    queryset: BaseModel = TextTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = TextTablesSerializer
      
    @method_decorator(permission_required('communications.add_texttablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_texttablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_texttablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.add_texttablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('communications.view_texttablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Receptor":{
                    "type":"str",
                    "name":"receiver",
                },
                "Evento":{
                    "type":"choices",
                    "name":"event__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/trigger_event/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Masivo":{
                    "type":"choices",
                    "name":"massive__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/massive/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Ciclo":{
                    "type":"choices",
                    "name":"cycle__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/cycle/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'receiver': "Receptor",
                '_type': "Tipo",
                'channel': "Canal",
                'message': "Mensaje"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('communications.view_texttablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextTableView
            :type self: TextTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(TextTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('communications.view_texttablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextTableView
            :type self: TextTableView
            :param request: data of request
            :type request: request
        
        """

        return TextTablesDatatable(request).get_data()

class VoiceCallTableView(BaseViewSet):

    """

        Class define ViewSet of VoiceCallTablesModel.
    
    """

    queryset: BaseModel = VoiceCallTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = VoiceCallTablesSerializer
      
    @method_decorator(permission_required('communications.add_voicecalltablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_voicecalltablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_voicecalltablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.add_voicecalltablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('communications.view_voicecalltablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Receptor":{
                    "type":"str",
                    "name":"receiver",
                },
                "Evento":{
                    "type":"choices",
                    "name":"event__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/trigger_event/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Masivo":{
                    "type":"choices",
                    "name":"massive__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/massive/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Ciclo":{
                    "type":"choices",
                    "name":"cycle__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "communications/cycle/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'receiver': "Receptor",
                '_type': "Tipo",
                'channel': "Canal",
                'message': "Mensaje"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('communications.view_voicecalltablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class VoiceCallTableView
            :type self: VoiceCallTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(VoiceCallTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('communications.view_voicecalltablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class VoiceCallTableView
            :type self: VoiceCallTableView
            :param request: data of request
            :type request: request
        
        """

        return VoiceCallTablesDatatable(request).get_data()

class NotificationView(BaseViewSet):

    """

        Class define ViewSet of ConversationTagModel.
    
    """

    queryset = NotificationModel.objects.all()
    serializer_class: BaseSerializer = NotificationSerializer

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

class ConversationView(BaseViewSet):

    """

        Class define ViewSet of ConversationModel.
    
    """

    queryset: BaseModel = ConversationModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = ConversationSerializer

class WhatsAppMessageView(BaseViewSet):

    """

        Class define ViewSet of WhatsAppMessageModel.
    
    """

    @method_decorator(permission_required('communications.view_whatsappmessagemodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextView
            :type self: TextView
            :param request: data of request
            :type request: request
        
        """

        text_datatables = ConversationWhatsAppDatatable(request)
        text_datatables.ID_TABLE = pk

        return JsonResponse(text_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_whatsappmessagemodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextView
            :type self: TextView
            :param request: data of request
            :type request: request
        
        """

        text_datatables = ConversationWhatsAppDatatable(request)
        text_datatables.ID_TABLE = pk

        return text_datatables.get_data()

    @action(detail=False, methods=['post'])
    def receive_message(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        if request.data.get("type") == "message.created":

            if request.data.get('message').get("direction") == "received":
                
                message = request.data.get('message')
                if len(WhatsAppMessageModel.objects.filter(messagebird_id=message.get("id")).all()) > 0:
                    return JsonResponse({"success": True}, safe=False)

                for item in CommunicationsSettingsModel.objects.filter(kind=0).all():
                    
                    if item.extra_data.get("channels"):
                        companies = [d for d in item.extra_data.get("channels") if d['channel'] == message.get("channelId")]
                        if len(companies) > 0:
                            operator = OperatorModel.objects.get(pk=companies[0]["operator"])

                receiver = message.get("from")

                new_message = WhatsAppMessageModel.objects.create(
                    message=message.get("content").get("text", " "),
                    kind=0,
                    status=3,
                    messagebird_conversation_id=message.get("conversartionId"),
                    operator=operator,
                    messagebird_id=message.get("id")
                )

                new_message.media_url = [message.get("content").get("image").get("url")] if message.get("content").get("image",False) else []
                new_message.save()

                try:

                    existent_conversation = ConversationModel.objects.filter(deleted=False, customer=message.get("from"), status=0).first()
                    existent_conversation.last_received_message_date = datetime.now()
                    existent_conversation.last_message_date = datetime.now()
                    existent_conversation.last_message = message.get("content").get("text", " ")
                    existent_conversation.last_message_kind = 0
                    existent_conversation.save()
                    new_message.conversation = existent_conversation
                    new_message.save()

                    new_notification = NotificationModel.objects.create(
                        company=operator.company,
                        public=False,
                        agent=existent_conversation.agent,
                        message="Nuevo mensaje de Whatsapp en la conversación " + str(existent_conversation.ID),
                        data={"type": "whatsapp", "conversation_ID": existent_conversation.ID}
                    )

                except:

                    chatInstance = ChatModel.objects.create(operator=operator)

                    new_conversation = ConversationModel.objects.create(
                        operator=operator,
                        customer=receiver,
                        channel=4,
                        status=0,
                        last_message_date=datetime.now(),
                        last_received_message_date=datetime.now(),
                        active=True,
                        last_message=message.get("content").get("text", ""),
                        last_message_kind=0,
                        chat=chatInstance.ID
                    )

                    new_message.conversation = new_conversation
                    new_message.save()

                    new_notification = NotificationModel.objects.create(
                        company=operator.company,
                        public=True,
                        message="Nuevo mensaje de Whatsapp",
                        data={"type": "whatsapp", "conversation_ID": new_conversation.ID}
                    )

                return JsonResponse({"success": True}, safe=False)

        elif request.data.get("type") == "message.updated":
            
            if request.data.get('message').get("direction") == "sent":
                
                message_data = request.data.get("message")

                try:
                    message = WhatsAppMessageModel.objects.get(messagebird_id=message_data.get("id"))
                    if message == None: message = WhatsAppMessageModel.objects.get(messagebird_id='', message=message_data.get("content").get("text"),messagebird_conversation_id=request.data.get("conversation").get("id"))
                    if message_data.get("status") == "sent":
                        message.status = 0
                    elif message_data.get("status") == "failed":
                        message.status = 2
                    elif message_data.get("status") == "read":
                        message.status = 1
                    message.save()
                except:
                    pass

        return JsonResponse({"success": True}, safe=False)

    @method_decorator(permission_required('communications.view_whatsappmessagemodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def get_messages(self, request, pk) -> JsonResponse:

        result = []
        if pk != "null":
            messages = WhatsAppMessageModel.objects.filter(conversation__pk=pk, deleted=False).order_by("created").all()
            if len(messages) > 0: NotificationModel.objects.filter(data__type="whatsapp",data__conversation_ID=int(pk)).delete()

            for message in messages:

                if message.kind == 0 and message.status == 3:

                    try:
                        message_notification = NotificationModel.objects.filter(public=False, data={"type": "whatsapp", "converastion_ID": pk}).delete()
                    except:
                        pass

                    message.status = 1
                    message.save()
                    
                result += [{
                    "ID": message.ID,
                    "message": message.message,
                    "kind": message.kind,
                    "media_url": message.media_url,
                    "status": message.status,
                    "creator": message.creator.username if message.creator else None,
                    "is_html": False
                }]

        return JsonResponse(result, safe=False)

    queryset: BaseModel = WhatsAppMessageModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = WhatsAppMessageSerializer

class ConversationTagView(BaseViewSet):

    """

        Class define ViewSet of ConversationTagModel.
    
    """

    @method_decorator(permission_required('communications.view_conversationtagmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def update_tags(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        tags = json.loads(request.data.get('tags'))
        existent_tags = ConversationTagModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.data.get('operator'))).all()
        for tag in existent_tags:
            if tag.name not in tags:
                tag.delete()
        
        for tag in tags:
            if len(existent_tags.filter(name=tag, deleted=False)) == 0:
                ConversationTagModel.objects.create(
                    creator=request.user, 
                    name=tag,
                    operator=OperatorModel.objects.get(pk=request.data.get('operator'))
                    )
        result = [instance.name for instance in ConversationTagModel.objects.filter(deleted=False, operator=OperatorModel.objects.get(pk=request.data.get('operator')))]
        return JsonResponse(result, safe=False)

    queryset: BaseModel = ConversationTagModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = ConversationTagSerializer

class WhatsAppConversationTablesView(BaseViewSet):

    """

        Class define ViewSet of ConversationTablesModel.
    
    """

    queryset: BaseModel = WhatsAppConversationTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = WhatsAppConversationTablesSerializer
      
    @method_decorator(permission_required('communications.add_conversationtablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_conversationtablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_conversationtablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.add_conversationtablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('communications.view_conversationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de inicio":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Fecha de fin":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Cliente":{
                    "type":"str",
                    "name":"customer",
                },
                "Servicio":{
                    "type":"str",
                    "name":"service",
                },
                "Agente":{
                    "type":"choices",
                    "name":"agent__pk",
                    "type_choices": "choices",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Tipo Último Mensaje":{
                    "type":"choices",
                    "name":"last_message_kind",
                    "type_choices": "choices",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0:"IN",
                            1: "OUT"
                        }
                    }
                },
                "Fecha de último mensaje":{
                    "type":"datetime",
                    "name":"last_message_date",
                    "format":"%d/%m/%Y %H:%M",
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'last_message_date': "Fecha Último Mensaje",
                'customer': "Cliente",
                'agent': "Agente",
                'last_message': "Último Mensaje",
                'service': "Número de Servicio",
                'last_message_kind': "Tipo Último Mensaje"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('communications.view_conversationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextTableView
            :type self: TextTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(WhatsAppConversationsTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('communications.view_conversationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextTableView
            :type self: TextTableView
            :param request: data of request
            :type request: request
        
        """

        return WhatsAppConversationsTablesDatatable(request).get_data()

class EmailConversationTablesView(BaseViewSet):

    """

        Class define ViewSet of ConversationTablesModel.
    
    """

    queryset: BaseModel = EmailConversationTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EmailConversationTablesSerializer
      
    @method_decorator(permission_required('communications.add_conversationtablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_conversationtablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.change_conversationtablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('communications.add_conversationtablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    # @documentation_class.documentation_definition()
    @method_decorator(permission_required('communications.view_conversationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {
            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de inicio":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Fecha de fin":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Cliente":{
                    "type":"str",
                    "name":"customer",
                },
                "Servicio":{
                    "type":"str",
                    "name":"service",
                },
                "Agente":{
                    "type":"choices",
                    "name":"agent__pk",
                    "type_choices": "choices",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "user/data/information/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Tipo Último Mensaje":{
                    "type":"choices",
                    "name":"last_message_kind",
                    "type_choices": "choices",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0:"IN",
                            1: "OUT"
                        }
                    }
                },
                "Fecha de último mensaje":{
                    "type":"datetime",
                    "name":"last_message_date",
                    "format":"%d/%m/%Y %H:%M",
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'last_message_date': "Fecha Último Mensaje",
                'customer': "Cliente",
                'agent': "Agente",
                'last_message': "Último Mensaje",
                'service': "Número de Servicio",
                'last_message_kind': "Tipo Último Mensaje"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('communications.view_conversationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextTableView
            :type self: TextTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(EmailConversationsTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('communications.view_conversationtablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextTableView
            :type self: TextTableView
            :param request: data of request
            :type request: request
        
        """

        return EmailConversationsTablesDatatable(request).get_data()

class EmailMessageView(BaseViewSet):

    """

        Class define ViewSet of WhatsAppMessageModel.
    
    """

    @method_decorator(permission_required('communications.view_emailmessagemodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextView
            :type self: TextView
            :param request: data of request
            :type request: request
        
        """

        text_datatables = ConversationEmailDatatable(request)
        text_datatables.ID_TABLE = pk

        return JsonResponse(text_datatables.get_struct(), safe=True)

    @method_decorator(permission_required('communications.view_emailmessagemodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TextView
            :type self: TextView
            :param request: data of request
            :type request: request
        
        """

        text_datatables = ConversationEmailDatatable(request)
        text_datatables.ID_TABLE = pk

        return text_datatables.get_data()

    @action(detail=False, methods=['post'])
    def receive_message(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketView
            :type self: TicketView
            :param request: data of request
            :type request: request
        
        """

        if request.data.get("type") != None:

            if request.data.get("type") == "message.opened":

                try:
                    email_instance = EmailMessageModel.objects.get(messagebird_id=request.data.get("id"))
                    email_instance.status = 1
                    email_instance.save()
                except: 
                    pass
        else:
            if request.data.get('message').get("direction") == "inbound" and request.data.get("message") != None:

                email = request.data.get('message')

                for item in CommunicationsSettingsModel.objects.filter(kind=0).all():
                    
                    if item.extra_data.get("whatsapp_channel"):
                        companies = [d for d in item.extra_data.get("whatsapp_channel") if d['channel'] == email.get("channelId")]
                        if len(companies) > 0:
                            operator = OperatorModel.objects.get(pk=companies[0]["operator"])

                sender = email.get("from").get("address")
                receiver = email.get("to")[0].get("address")

                content = email.get("content").get("html") if  email.get("content").get("html") != None else email.get("content").get("text")
                is_html = email.get("content").get("html") != None

                new_message = EmailMessageModel.objects.create(
                    message=content,
                    kind=0,
                    status=3,
                    is_html=is_html,
                    subject=email.get("subject"),
                    operator=operator,
                    messagebird_conversation_id=email.get("threadId")
                )

                new_message.media_url = [email.get("attachments").get("url")] if email.get("attachments") != None and len(email.get("attachments")) > 0 else []
                new_message.save()

                try:
                    existent_conversation = ConversationModel.objects.filter(deleted=False, customer=email.get("from").get("address"), status=0).first()
                    new_message.conversation = existent_conversation
                    existent_conversation.last_received_message_date = datetime.now()
                    existent_conversation.last_message_date = datetime.now()
                    existent_conversation.last_message = content
                    existent_conversation.last_message_kind = 0
                    existent_conversation.save()
                    new_message.save()

                    new_notification = NotificationModel.objects.create(
                        company=operator.company,
                        public=False,
                        agent=existent_conversation.agent,
                        message="Nuevo mensaje de correo en la conversación " + str(existent_conversation.ID),
                        data={"type": "email", "conversation_ID": existent_conversation.ID}
                    )
                except:

                    chatInstance = ChatModel.objects.create(operator=operator)

                    new_conversation = ConversationModel.objects.create(
                        operator=operator,
                        customer=sender,
                        channel=0,
                        status=0,
                        last_message_date=datetime.now(),
                        last_received_message_date=datetime.now(),
                        active=True,
                        last_message=content,
                        last_message_kind=0,
                        chat=chatInstance.ID
                    )

                    new_message.conversation = new_conversation
                    new_message.save()

                    new_notification = NotificationModel.objects.create(
                        company=operator.company,
                        public=True,
                        message="Nuevo mensaje de correo",
                        data={"type": "email","conversation_ID": existent_conversation.ID}
                    )

                    if len(FinanceEmailModel.objects.filter(deleted=False, operator=operator, email=receiver).all()) > 0:
                        typify_instance = TypifyModel.objects.create(
                            operator=operator,
                            channel=0
                        )
                        FinanceEscalationModel.objects.create(
                            operator=operator,
                            typify=typify_instance.ID,
                            email_conversation=new_conversation,
                            autoclose_typify=True
                        )

                return JsonResponse({"success": True}, safe=False)

            else:
                return JsonResponse({"success": True}, safe=False)
        
        return JsonResponse({"success": True}, safe=False)

    @method_decorator(permission_required('communications.view_emailmessagemodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def get_messages(self, request, pk) -> JsonResponse:
        result = []
        if pk != "null":

            messages = EmailMessageModel.objects.filter(conversation__pk=pk, deleted=False).order_by("created").all()
            if len(messages) > 0: NotificationModel.objects.filter(data__type="whatsapp",data__conversation_ID=pk).delete()

            for message in messages:

                if message.kind == 0 and message.status == 3: 

                    try:
                        message_notification = NotificationModel.objects.filter(public=False, data={"type": "email", "conversation_ID": pk}).delete()
                    except:
                        pass

                    message.status = 1
                    message.save()

                result += [{
                    "ID": message.ID,
                    "message": message.message,
                    "kind": message.kind,
                    "media_url": message.media_url,
                    "status": message.status,
                    "creator": message.creator.username if message.creator else None,
                    "is_html": message.is_html,
                    "creator": message.creator.username if message.creator else None,
                    "subject": message.subject
                }]
        return JsonResponse(result, safe=False)

    queryset: BaseModel = EmailMessageModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = EmailMessageSerializer