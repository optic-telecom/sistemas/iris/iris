import ast
import os
import threading
import random
import string
from datetime import datetime, timedelta
from typing import IO, List, Union, Dict, Any
from django.core.mail import message
from slack import WebClient
from jinja2 import Template

import openpyxl
import requests
from django.contrib.auth.models import User
from django.db.models import Q
from communications.api import send_whatsapp_message, send_email_message
from django_q.models import Schedule
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.core.exceptions import ObjectDoesNotExist
from django_q.tasks import async_task
from rest_framework import serializers
from rest_framework.serializers import (ValidationError, SerializerMethodField)
from validate_email import validate_email
from dynamic_preferences.registries import global_preferences_registry
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import get_token, matrix_url, next_datetime
from tickets.models import TypifyModel
from escalation_ti.models import EscalationModel
from operators.models import OperatorModel, CommunicationsSettingsModel

from .models import (ChatModel, ConversationModel, ConversationTagModel, CycleCommunicationsModel, EmailConversationTablesModel, EmailMessageModel, EmailModel,
                     EventCallModel, EventEmailModel, EventTextModel,
                     EventWhatsappModel, FilesEmailModel, InternalChatModel,
                     MassiveCommunicationsModel, NotificationModel, TemplateCallModel,
                     TemplateEmailModel, TemplateTextModel,
                     TemplateWhatsappModel, TextModel, TriggerEventModel,
                     VoiceCallModel, WhatsAppConversationTablesModel, WhatsAppMessageModel, WhatsappModel, EmailTablesModel, WhatsappTablesModel,
                     TextTablesModel, VoiceCallTablesModel)
from .queue import Massive, email_task_queue


class CycleCommunicationsSerializer(BaseSerializer):

    """

        Class define serializer of TemplateEmailView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = CycleCommunicationsModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Any] = {
            'schedule_massive': {
                'read_only': True,
            },
            'active': {
                'read_only': True,
            },
        }

    def validate_massives_communications(self, value) -> Dict[str, dict]:

        """

            This function, validate massives_communications field of CycleCommunicationsModel

        """

        def get_type_repeats(self) -> str:

            """

                This function, return type_repeats value

            """

            def get_type_repeats_of_instance(self) -> str:

                """

                    This function, return type_repeats instance value

                """

                return self.instance.type_repeats

            method: str = self.context['request'].method
            type_repeats = None

            if method == 'PATCH':

                type_repeats: str = get_type_repeats_of_instance(self)

            elif method == 'PUT':

                if 'type_repeats' in self.initial_data.keys():

                    type_repeats: str = self.initial_data['type_repeats']

                else:

                    type_repeats: str = get_type_repeats_of_instance(self)

            elif method == 'POST':

                type_repeats: str = self.initial_data['type_repeats']

            return type_repeats

        #Avoid overlapping dates
        start_datetime: str = value['0']['datetime']
        start_datetime: datetime = datetime.strptime(start_datetime, '%d/%m/%Y %H:%M')

        finish_datetime: str = value[ str(len(value) - 1) ]['datetime']
        finish_datetime: datetime = datetime.strptime(finish_datetime, '%d/%m/%Y %H:%M')

        type_cycle: str = get_type_repeats(self)

        if finish_datetime > next_datetime( type_cycle, start_datetime):

            raise ValidationError({
                'Overlapping dates': 'The end date is greater than the next run from the start date'
            })

        return value

    def validate_active(self, value) -> bool:

        return False

    def create(self, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and create instance

            :param self: CycleCommunicationsSerializer instance
            :type self: CycleCommunicationsSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        same_name_cycles = CycleCommunicationsModel.objects.filter(name=validated_data['name'])

        if len(same_name_cycles) > 0:
            raise ValidationError({'name': 'There is already a cycle with that name'})

        return super().create(validated_data)

    def update(self, instance, validated_data):

        """

            This function, validate if can update instance and update

        """

        if instance.active or instance.test:

            raise ValidationError({
                'Cycle is active or test in running': "It cannot be modified, while the cycle is active or the test is running."
            })
        
        return  super().update(instance, validated_data)

class MassiveCommunicationsSerializer(BaseSerializer):

    """

        Class define serializer of MassiveCommunicationsView.
    
    """

    email_event = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='communications:email_event-detail',
    )
    whatsapp_event = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='communications:whatsapp_event-detail',
    )
    text_event = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='communications:text_event-detail',
    )
    call_event = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='communications:call_event-detail',
    )

    #File files get email and services
    excludes_file: List[int] = serializers.FileField(required=False)
    includes_file: List[int] = serializers.FileField(required=False)
    channels: List[str] = serializers.JSONField(required=False, write_only=True)

    def validate_filters(self, value: List[List[str]]) -> List[List[str]]:

        """
    
            This funtion validate template

            :param self: Instance of Class MassiveCommunicationsSerializer
            :type self: MassiveCommunicationsSerializer
            :param value: value for create instance
            :type value: dict

            :returns: Returns list with valid filters
            :rtype: list

        """

        #Get filters
        try:

            filters_data: List[List[str]] = value

        except Exception as e:

            raise ValidationError({
                "Data error":"Error data"
            })

        # Convert 'filters' to list of list

        filters: List[str] = [ value for key, value in Massive.filters_translation.items() ]
        #Get list filters

        filters_translation: Dict[str, str] = { value:key for key, value in Massive.filters_translation.items() }
        #Get dict filters_translation reverse

        type_translation: Dict[str, str] = { value:key for key, value in Massive.type_translation.items() }
        #Get dict type_translation reverse

        for _filter in filters_data:

            #Check length list
            if len(_filter) != 3:

                raise ValidationError({
                    "Invalid length":"List must contain three elements"
                })

            # Check if is valid filter
            if not _filter[0] in filters:

                raise ValidationError({
                    "Invalid filter":f"Don't exist filter {_filter[0]}"
                })
                

            # Check if is valid comparison for filter and exist 
            if not type_translation[ _filter[1] ] in Massive.type_filters[ Massive.filters[ filters_translation[ _filter[0] ] ] ]:

                raise ValidationError({
                    "Invalid comparison":f"Invalid comparison item {_filter[1]} for filter '{_filter[0]}'"
                })

        return filters_data

    def validate_excludes(self, value: IO) -> List[int]:

        """
    
            This funtion validate template

            :param self: Instance of Class MassiveCommunicationsSerializer
            :type self: MassiveCommunicationsSerializer
            :param value: value for create instance
            :type value: dict

            :returns: Returns list with valid excludes value
            :rtype: list

        """

        excludes_list: List[int] = []

        if 'excludes_file' in self.context['request'].FILES.keys():
            
            excel_file: IO = self.context['request'].FILES["excludes_file"]

            try:

                wb: IO = openpyxl.load_workbook(excel_file)
                #Get file excel

            except:

                raise ValidationError("Don't is valid excludes excel file")

            #Check 'excludes_files' sheet exist
            if not 'excludes_files' in [ sheet.lower() for sheet in wb.get_sheet_names()]:

                raise ValidationError({
                    "Error sheet excludes":"Don't exist sheet named 'excludes_files'"
                })

            ws: IO = wb.get_sheet_by_name('excludes_files')

            #Get list services
            sesion = requests.Session()
            sesion.headers.update({"Authorization": get_token()})

            params: Dict[str, str] = {
                'fields':'id',
            }
            response = sesion.get(
                            url=matrix_url('services'),
                            params=params, 
                            verify=False,   
                        )
            services: List[int] = [ service['id'] for service in response.json() ]
            
            #Go through column A of the file
            for row in ws.iter_cols(min_row=1, max_col=1, max_row=ws.max_row):

                for cell in row:

                    try:

                        excludes_list.append( int( cell.value ) )

                    except:

                        pass

            excludes_list: List[int] = list(set(services).intersection(set(excludes_list)))

        return excludes_list

    def validate_includes(self, value: IO) -> List[int]:

        """
    
            This funtion validate template

            :param self: Instance of Class MassiveCommunicationsSerializer
            :type self: MassiveCommunicationsSerializer
            :param value: value for create instance
            :type value: dict

            :returns: Returns dict with valid includes value
            :rtype: dict

        """

        includes_list: List[int] = []

        if 'includes_file' in self.context['request'].FILES.keys():
            
            excel_file: IO = self.context['request'].FILES["includes_file"]

            try:

                wb: IO = openpyxl.load_workbook(excel_file)
                #Get file excel

            except:

                raise serializers.ValidationError("Don't is valid includes excel file")

            #Check 'includes_files' sheet exist
            if not 'includes_files' in [ sheet.lower() for sheet in wb.get_sheet_names()]:

                raise ValidationError({
                        "Error sheet includes":"Don't exist sheet named 'includes_files'"
                    })

            ws: IO = wb.get_sheet_by_name('includes_files')

            #Get list services
            sesion = requests.Session()
            sesion.headers.update({"Authorization": get_token()})

            params: Dict[str, str] = {
                'fields':'id',
            }
            response = sesion.get(
                            url=matrix_url('services'),
                            params=params,
                            verify=False,    
                        )
            services: List[int] = [ service['id'] for service in response.json() ]
            
            #Go through column A of the file
            for row in ws.iter_cols(min_row=1, max_col=1, max_row=ws.max_row):

                for cell in row:

                    try:

                        includes_list.append( int( cell.value ) )

                    except:

                        pass

            includes_list: List[int] = list(set(services).intersection(set(includes_list)))

        return includes_list

    def validate(self, data: dict) -> dict:

        def validate_channels(self, value: Dict[str, dict]) -> Dict[str, Any]:

            """
        
                This funtion validate channels

                :param self: Instance of Class MassiveCommunicationsSerializer
                :type self: MassiveCommunicationsSerializer
                :param value: value for create instance
                :type value: dict

                :returns: Returns dict with valid channels
                :rtype: dict
        
            """

            #Get channels send data
            channels: Dict[str, str] = value
            result: Dict[str, Any] = {
                'email_event': None,
                'whatsapp_event': None,
                'text_event': None,
                'call_event': None,
            }

            def validate_emails(data: Dict[str, str]) -> EventEmailModel:

                """
        
                    This funtion validate email event

                    :param data: Dict with data of email event
                    :type data: dict

                    :returns: Returns EventEmailModel
                    :rtype: EventEmailModel
        
                """

                def validate_item_email(item: str, data: Dict[str, str]) -> str:

                    """
        
                        This funtion validate email event

                        :param item: Name of key
                        :type item: str
                        :param data: Dict with data of emails
                        :type data: dict

                        :returns: validated email
                        :rtype: str
        
                    """
                    
                    #Check if exist item in dict
                    if item not in data.keys():

                        raise ValidationError({
                            f"{item} required": f"Required field {item}"
                        })

                    #Check if email is valid
                    if not validate_email(data[item]):

                        raise ValidationError({
                            f"{item} invalid": f"Required valid email field {item}"
                        })

                    return data[item]

                email_event: EventEmailModel = EventEmailModel()

                #Set emails
                email_event.email_origin: str = validate_item_email('email_origin', data)
                email_event.email_test: str = validate_item_email('email_test', data)
                email_event.email_response: str = validate_item_email('email_response', data)

                if 'email_copy' in data.keys() and validate_email( data['email_copy'] ):

                    email_event.email_copy: str = validate_item_email('email_copy', data)

                #Search template of model
                if not 'template' in data.keys() or not TemplateEmailModel.objects.filter(ID=data['template']):

                    raise ValidationError({
                        'template email error': "Required valid template field of email event"
                    })

                else:
                    
                    email_event.template: str = TemplateEmailModel.objects.get(ID=data['template'])

                email_event.save()

                return email_event

            def validate_whatsapp(data: Dict[str, str]) -> EventWhatsappModel:

                """
        
                    This funtion validate whatsapp event

                    :param data: Dict with data of email event
                    :type data: dict

                    :returns: Returns EventWhatsappModel
                    :rtype: EventWhatsappModel
        
                """
                
                whatsapp_event:EventWhatsappModel = EventWhatsappModel()

                #Set template of whatsapp
                if not 'template' in data.keys() or not TemplateWhatsappModel.objects.filter(ID=data['template']):

                    raise ValidationError({
                        'template whatsapp error': "Required valid template field of whatsapp event"
                    })

                else:

                    whatsapp_event.template: TemplateWhatsappModel = TemplateWhatsappModel.objects.get(ID=data['template'])

                #Set telephone number
                whatsapp_event.phone_number_test: str = data['phone_number_test']

                try:

                    whatsapp_event.save()

                except Exception as e:

                    raise ValidationError({
                        'phone_number_test whatsapp': "Invalid Whatsapp phone format"
                    })
                
                return whatsapp_event

            def validate_voice_call(data: Dict[str, str]) -> EventCallModel:
                
                """
        
                    This funtion validate voice call event

                    :param data: Dict with data of voice call event
                    :type data: dict

                    :returns: Returns EventCallModel
                    :rtype: EventCallModel
        
                """

                voice_call_event:EventCallModel = EventCallModel()

                #Search template of model
                if not 'template' in data.keys() or not TemplateCallModel.objects.filter(ID=data['template']):

                    raise ValidationError({
                        'template voice call error': "Required valid template field of voice call event"
                    })

                else:

                    voice_call_event.template: TemplateCallModel = TemplateCallModel.objects.get(ID=data['template'])

                #Set telephone number
                voice_call_event.phone_number_test: str = data['phone_number_test']

                try:

                    voice_call_event.save()

                except Exception as e:

                    raise ValidationError({
                        'phone_number_test voice call': "Invalid Call phone format"
                    })

                return voice_call_event

            def validate_text(data: Dict[str, str]) -> EventTextModel:

                """
        
                    This funtion validate text event

                    :param data: Dict with data of text event
                    :type data: dict

                    :returns: Returns EventTextModel
                    :rtype: EventTextModel
        
                """

                text_event:EventTextModel = EventTextModel()

                #Search template of model
                if not 'template' in data.keys() or not TemplateTextModel.objects.filter(ID=data['template']):

                    raise ValidationError({
                        'template text error': "Required valid template field of text event"
                    })

                else:

                    text_event.template: TemplateTextModel = TemplateTextModel.objects.get(ID=data['template'])

                #Set telephone number
                text_event.phone_number_test: str = data['phone_number_test']

                try:

                    text_event.save()

                except Exception as e:

                    raise ValidationError({
                        'phone_number_test voice call': "Invalid Text phone format"
                    })

                return text_event

            #If contains email event, search values
            if 'email_event' in channels.keys():

                result['email_event']: EventEmailModel = validate_emails( channels['email_event'] )

            #If contains whatsapp event, search values
            if 'whatsapp_event' in channels.keys():

                result['whatsapp_event']: EventWhatsappModel = validate_whatsapp( channels['whatsapp_event'] )

            #If contains text event, search values
            if 'text_event' in channels.keys():

                result['text_event']: EventTextModel = validate_text( channels['text_event'] )

            #If contains call event, search values
            if 'call_event' in channels.keys():

                result['call_event']: EventCallModel = validate_voice_call( channels['call_event'] )

            return result

        if 'channels' in data:

            data.update( validate_channels(self, data['channels'] ) )
            del data['channels']

        return data

    def update(self, instance: MassiveCommunicationsModel, validated_data: Dict[str, Any]) -> MassiveCommunicationsModel:

        def del_attr(before_instance, new_instance, field):

            if getattr(before_instance, field) != getattr(new_instance, field):

                getattr(before_instance, field).delete()

        new_instance: MassiveCommunicationsModel = super().update(instance, validated_data)

        for field in ['email_event', 'whatsapp_event', 'text_event', 'call_event']:

            del_attr(instance, new_instance, field)
        
        return new_instance

    def create(self, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and create instance

            :param self: MassiveCommunicationsSerializer instance
            :type self: MassiveCommunicationsSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        same_name_massives = MassiveCommunicationsModel.objects.filter(name=validated_data['name'])

        if len(same_name_massives) > 0:
            raise ValidationError({'name': 'There is already a massive with that name'})

        return super().create(validated_data)

    class Meta:

        #Class model of serializer
        model: BaseModel = MassiveCommunicationsModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'
        #Args of serializer
        extra_kwargs: dict = {
            'email': {'required': False},
            'schedule_args': {'required': False},
            'schedule_kwargs': {'required': False},
            'status': {'required': False},
            'schedule': {'required': False},
            'excludes': {'required': False},
        }

class TemplateEmailSerializer(BaseSerializer):

    """

        Class define serializer of TemplateEmailView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = TemplateEmailModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class TemplateTextSerializer(BaseSerializer):

    """

        Class define serializer of TemplateTextView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = TemplateTextModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class TemplateWhatsappSerializer(BaseSerializer):

    """

        Class define serializer of TemplateWhatsappView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = TemplateWhatsappModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class TemplateCallSerializer(BaseSerializer):

    """

        Class define serializer of TemplateCallModelView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = TemplateCallModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class InternalChatSerializer(BaseSerializer):

    """

        Class define serializer of InternalChatView.
    
    """

    def send_message(self, thread: str, channel: str, request, created: bool):

        global_preferences = global_preferences_registry.manager()

        #Get the right access token based on the operator
        operator_instance = OperatorModel.objects.get(pk=int(request.data.get('operator','2')))
        
        client = WebClient(token=operator_instance.slack_token)

        action: str = "añadido" if created else "actualizado"

        blocks: List[Dict[str, Any]] =  [
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": ":iris: *Iris* Tickets"
                }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f":writing_hand:*" + request.user.username + "* ha " + action + " un comentario: *" + str(request.data.get('message')) + "*"
                }
            }
        ]

        client.chat_postMessage(
            blocks=blocks,
            thread_ts=thread,
            channel=channel,
            text=f" " + request.user.username + " ha " + action + " un comentario: " + str(request.data.get('message')) 
        )

    def create(self, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and create instance

            :param self: InternalChatSerializer instance
            :type self: InternalChatSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        typify_instance = TypifyModel.objects.filter(ID=self.context['request'].data.get('chat')).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                self.send_message(thread, typify_instance.slack_thread[thread], self.context['request'], True)

        return super().create(validated_data)

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        if self.context['request'].user != instance.creator:
            raise ValidationError({"invalid user": "only the creator can edit this message"})

        typify_instance = TypifyModel.objects.filter(ID=self.context['request'].data.get('chat')).first()
        if typify_instance and typify_instance.slack_thread != None:
            for thread in typify_instance.slack_thread:
                self.send_message(thread, typify_instance.slack_thread[thread], self.context['request'], False)
        
        escalation_instance = EscalationModel.objects.filter(ID=self.context['request'].data.get('chat')).first()
        if escalation_instance and escalation_instance.slack_thread != None:
            for thread in escalation_instance.slack_thread:
                self.send_message(thread, escalation_instance.slack_thread[thread], self.context['request'], False)

        return super().update(instance, validated_data)
    
    class Meta:

        #Class model of serializer
        model: BaseModel = InternalChatModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'
        #Args of serializer
        extra_kwargs: dict = {
            'chat': {'required': True},
        }

class FilesEmailSerializer(BaseSerializer):

    """

        Class define serializer of InternalChatView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = FilesEmailModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class EmailSerializer(BaseSerializer):

    """

        Class define serializer of EmailChatView.
    
    """

    files_email = FilesEmailSerializer(many=True, read_only=True)

    class Meta:

        #Class model of serializer
        model: BaseModel = EmailModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

    def create(self, validated_data: Dict[str, Any]) -> EmailModel:

        """
        
            This funtion validate values

            :param self: Instance of Class EmailSerializer
            :type self: EmailSerializer
            :param validated_data: validated data
            :type validated_data: dict

            :returns: Returns instance EmailModel
            :rtype: EmailModel
        
        """
        
        if self.context['request'].data.get("domain", None):
            try:
                validated_data['operator'] = OperatorModel.objects.get(domain=self.context['request'].data.get("domain"))
            except:
                raise ValidationError({'invalid domain': 'The domain is not assigned to any operator'})
                
        email: EmailModel = EmailModel.objects.create(**validated_data)
        email.outbox: bool = True

        if email.chat != None:
            chat_instance = email.chat
            chat_instance.last_updated = datetime.now()
            chat_instance.save()
        else:
            email_query = EmailModel.objects.filter(created__gte=datetime.now() - timedelta(hours=12)).filter(Q(receiver=email.sender) | Q(sender=email.sender))
            if len(email_query) > 1:
                email.chat = email_query[0].chat
                email.main = False
                chat_instance = email.chat
                if chat_instance:
                    chat_instance.new_messages += 1
                    chat_instance.last_updated = datetime.now()
                    chat_instance.save()

            else:
                chatInstance = ChatModel.objects.create(operator=email.operator)
                email.chat = chatInstance

        email.save()

        if email._type == 1:
        
            user: User = self.context['request'].user

            for data_file in self.context['request']._files.values():

                file_model: FilesEmailModel = FilesEmailModel()

                file_model.email: EmailModel = email
                file_model.creator: User = user
                file_model.updater: User = user
                file_model.operator: int = validated_data["operator"]
                file_model.files.save(data_file.name, data_file.file)
                file_model.save()

            #Create Async task
            async_task(func=email_task_queue, kwargs={'ID': email.ID})

        return email

class WhatsappSerializer(BaseSerializer):

    """

        Class define serializer of WhatsappView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = WhatsappModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class TextSerializer(BaseSerializer):

    """

        Class define serializer of TextView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = TextModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class VoiceCallSerializer(BaseSerializer):

    """

        Class define serializer of VoiceCallView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = VoiceCallModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class ChatSerializer(BaseSerializer):

    """

        Class define serializer of ChatView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = ChatModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'


class EventEmailSerializer(BaseSerializer):

    """

        Class define serializer of EventEmailView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = EventEmailModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class EventWhatsappSerializer(BaseSerializer):

    """

        Class define serializer of EventWhatsappView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = EventCallModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class EventTextSerializer(BaseSerializer):

    """

        Class define serializer of EventTextView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = EventTextModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class EventCallSerializer(BaseSerializer):

    """

        Class define serializer of EventCallView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = EventCallModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'


class TriggerEventSerializer(BaseSerializer):

    """

        Class define serializer of TriggerEventView.
    
    """

    email_event = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='communications:email_event-detail',
    )
    whatsapp_event = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='communications:whatsapp_event-detail',
    )
    text_event = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='communications:text_event-detail',
    )
    call_event = serializers.HyperlinkedRelatedField(
        many=False,
        read_only=True,
        view_name='communications:call_event-detail',
    )

    class Meta:

        #Class model of serializer
        model: BaseModel = TriggerEventModel
        #Fields of serializer
        fields: List[str] = '__all__'

class EmailTablesSerializer(BaseSerializer):

    """

        Class define serializer of EmailTableView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = EmailTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: EmailTablesModel, validated_data: Dict[str, Any]) -> EmailTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class WhatsappTablesSerializer(BaseSerializer):

    """

        Class define serializer of WhatsappTableView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = WhatsappTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: WhatsappTablesModel, validated_data: Dict[str, Any]) -> WhatsappTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class TextTablesSerializer(BaseSerializer):

    """

        Class define serializer of WhatsappTableView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = TextTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: TextTablesModel, validated_data: Dict[str, Any]) -> TextTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class VoiceCallTablesSerializer(BaseSerializer):

    """

        Class define serializer of WhatsappTableView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = VoiceCallTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: VoiceCallTablesModel, validated_data: Dict[str, Any]) -> VoiceCallTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class NotificationSerializer(BaseSerializer):

    """

        Class define serializer of CompanyView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = NotificationModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            }
        }


class ConversationSerializer(BaseSerializer):

    """

        Class define serializer of ConversationView.
    
    """

    agent_username = SerializerMethodField()
    
    def get_agent_username(self,instance_model: ConversationModel) -> str:

        return instance_model.agent.username if instance_model.agent else None

    def create(self, validated_data: Dict[str, Any]) -> ConversationModel:

        """
        
            This funtion validate and create instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        result = super().create(validated_data)
        chatInstance = ChatModel.objects.create(operator=result.operator)
        result.chat = chatInstance.ID
        result.save()
        return result

    class Meta:

        #Class model of serializer
        model: BaseModel = ConversationModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class WhatsAppMessageSerializer(BaseSerializer):

    """

        Class define serializer of WhatsAppMessageView.
    
    """

    def create(self, validated_data: Dict[str, Any]) -> WhatsAppMessageModel:

        """
        
            This funtion validate and create instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        communications_settings = CommunicationsSettingsModel.objects.filter(company=validated_data["operator"].company)

        if len(communications_settings) != 0:
            communications_settings = communications_settings.first()
        else:
            raise ValidationError({'settings': 'Theres no communications api settings for that company '})
            
        if validated_data.get("conversation",None) == None:

            conversations = ConversationModel.objects.filter(active=True, customer=self.context.get("request").data["receiver"], deleted=False)

            if len (conversations)  > 0:
                validated_data["conversation"] = conversations.first()
            else:
                new_conversation = ConversationModel.objects.create(
                    operator=validated_data["operator"],
                    customer=self.context.get("request").data["receiver"],
                    channel=4,
                    status=0,
                    last_message_date=datetime.now(),
                    agent=self.context['request'].user,
                    creator=self.context['request'].user,
                    updater=self.context['request'].user,
                    active=True,
                    last_message=validated_data.get("message"," "),
                    last_message_kind=1,
                )

                chatInstance = ChatModel.objects.create(operator=new_conversation.operator)
                new_conversation.chat = chatInstance.ID
                new_conversation.save()

                validated_data["conversation"] = new_conversation

        else:

            conversation = validated_data["conversation"]
            conversation.last_message = validated_data.get("message", " ")
            conversation.last_message_date = datetime.now()
            conversation.last_message_kind = 1
            conversation.save()

        media_url = []
        for data_file in self.context['request']._files.values():
    
            global_preferences = global_preferences_registry.manager()
            path = default_storage.save('messagebird_files/' + data_file.name, ContentFile(data_file.read()))
            messagebird_file = os.path.join(settings.MEDIA_ROOT, path)
            media_url = [global_preferences['general__iris_backend_domain'] + messagebird_file.split("Iris\\")[1]]
            validated_data["media_url"] = media_url
            
        result = super().create(validated_data)

        send_whatsapp_message(kwargs={
                'receiver': self.context["request"].data.get("receiver"),
                'message': validated_data.get("message", " "),
                'operator': validated_data["operator"],
                'params': [],
                'template': self.context['request'].data.get("template",None),
                'media_url': media_url,
                'message_instance': result
                })


        # schedule_object: Schedule = Schedule.objects.create(
        #     func='communications.api.send_whatsapp_message',
        #     name=''.join(random.choice(
        #         string.ascii_uppercase + string.digits) for _ in range(20)),
        #     schedule_type='O',
        #     repeats=1,
        #     next_run=datetime.now() + timedelta(seconds=4),
        #     kwargs={
        #         'receiver': validated_data["receiver"],
        #         'message': validated_data["message"],
        #         'operator': validated_data["operator"],
        #         'params': [],
        #         'template': self.context['request'].date.get("template",None),
        #         'media_url': media_url
        #         },
        #     minutes=5,
        # )

        return result

    def update(self, instance, validated_data):

        """

            This function, validate if can update instance and update

        """

        if validated_data.get("agent"):

            NotificationModel.objects.filter(data__type="whatsapp",data__conversation_ID=instance.ID).delete()

        
        return  super().update(instance, validated_data)


    class Meta:

        #Class model of serializer
        model: BaseModel = WhatsAppMessageModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class ConversationTagSerializer(BaseSerializer):

    """

        Class define serializer of ConversationTagView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = ConversationTagModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'

class WhatsAppConversationTablesSerializer(BaseSerializer):

    """

        Class define serializer of ConversationTablesView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = WhatsAppConversationTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: WhatsAppConversationTablesModel, validated_data: Dict[str, Any]) -> WhatsAppConversationTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class EmailConversationTablesSerializer(BaseSerializer):

    """

        Class define serializer of ConversationTablesView.

    """

    class Meta:

        # Class model of serializer
        model: BaseModel = EmailConversationTablesModel
        # Fields of serializer
        fields: Union[List[str], str] = '__all__'
        # Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            },
        }

    def update(self, instance: EmailConversationTablesModel, validated_data: Dict[str, Any]) -> EmailConversationTablesModel:

        if 'last_time' in validated_data or 'last_time_type' in validated_data:

            if validated_data.get("last_time_type", None) == None:
                raise ValidationError({ 
                'last_time_type': ['This field is required.']})
            
            if validated_data.get("last_time", None) == None:
                raise ValidationError({ 
                'last_time': ['This field is required.']})

        return super().update(instance, validated_data)

class EmailMessageSerializer(BaseSerializer):

    """

        Class define serializer of WhatsAppMessageView.
    
    """

    def create(self, validated_data: Dict[str, Any]) -> EmailMessageModel:

        """
        
            This funtion validate and create instance

            :param self: TicketSerializer instance
            :type self: TicketSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        communications_settings = CommunicationsSettingsModel.objects.filter(company=validated_data["operator"].company)

        if len(communications_settings) != 0:
            communications_settings = communications_settings.first()
        else:
            raise ValidationError({'settings': 'Theres no communications api settings for that company '})
        

        if validated_data.get("conversation",None) == None:

            conversations = ConversationModel.objects.filter(active=True, customer=self.context.get("request").data["receiver"], deleted=False)

            if len (conversations)  > 0:
                validated_data["conversation"] = conversations.first()
            else:

                chatInstance = ChatModel.objects.create(operator=new_conversation.operator)

                new_conversation = ConversationModel.objects.create(
                    operator=validated_data["operator"],
                    customer=self.context.get("request").data["receiver"],
                    channel=0,
                    status=0,
                    last_message_date=datetime.now(),
                    agent=self.context['request'].user,
                    creator=self.context['request'].user,
                    updater=self.context['request'].user,
                    active=True,
                    last_message=validated_data["message"],
                    last_message_kind=1,
                    chat=chatInstance.ID
                )

                validated_data["conversation"] = new_conversation

        media_url = []

        for data_file in self.context['request']._files.values():

                with data_file.open('rb') as f:
        
                    response_upload = requests.post(
                        url='https://conversations.messagebird.com/v1/files',
                        headers={
                            'Authorization': f'AccessKey {communications_settings.token}',
                            'Content-Disposition': 'attachment',
                            'filename': "testing_image.png",
                            'Content-Type': 'image/png'
                        },
                        files=f
                    )

                    meadia_url = media_url + [{"ID": response_upload.get("id")}]

        result = super().create(validated_data)

        send_email_message(kwargs={
            'receiver': self.context.get("request").data["receiver"],
            'message': validated_data["message"],
            'operator': validated_data["operator"],
            'subject': validated_data["subject"],
            'params': [],
            'template': self.context['request'].data.get("template",None),
            'media_url': media_url,
            'service':  self.context['request'].data.get("service",None),
            'instance': result
        })

        # schedule_object: Schedule = Schedule.objects.create(
        #     func='communications.api.send_email_message',
        #     name=''.join(random.choice(
        #         string.ascii_uppercase + string.digits) for _ in range(20)),
        #     schedule_type='O',
        #     repeats=1,
        #     next_run=datetime.now() + timedelta(seconds=4),
        #     kwargs={
        #         'receiver': validated_data["receiver"],
        #         'message': validated_data["message"],
        #         'operator': validated_data["operator"],
        #         'params': [],
        #         'template': self.context['request'].date.get("template",None),
        #         'media_url': media_url
        #         },
        #     minutes=5,
        # )
        
        return result

    def update(self, instance, validated_data):

        """

            This function, validate if can update instance and update

        """

        if validated_data.get("agent"):

            NotificationModel.objects.filter(data__type="email",data__conversation_ID=instance.ID).delete()

        
        return  super().update(instance, validated_data)

    class Meta:

        #Class model of serializer
        model: BaseModel = EmailMessageModel
        #Fields of serializer
        fields: Union[list, str] = '__all__'
