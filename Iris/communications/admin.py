from typing import List

from django.contrib import admin

from .models import (CycleCommunicationsModel, EmailModel, EventCallModel,
                     EventEmailModel, EventTextModel, EventWhatsappModel,
                     MassiveCommunicationsModel, TemplateCallModel,
                     TemplateEmailModel, TemplateTextModel,
                     TemplateWhatsappModel, TextModel, TriggerEventModel,
                     VoiceCallModel, WhatsappModel, EmailTablesModel)


class EmailAdmin(admin.ModelAdmin):

    fields: List[str] = ["subject", "receiver", "sender", "message", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID","subject", "receiver", "sender","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID","subject", "receiver", "sender", "message"]

    #Define model data list ordening
    ordering: List[str] = ["subject", "receiver", "sender"]

class TextAdmin(admin.ModelAdmin):

    fields: List[str] = [ "receiver", "message", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "receiver","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "receiver", "message"]

    #Define model data list ordening
    ordering: List[str] = [ "receiver"]

class WhatsappAdmin(admin.ModelAdmin):

    fields: List[str] = [ "receiver", "message", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "receiver","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "receiver", "message"]

    #Define model data list ordening
    ordering: List[str] = [ "receiver"]

class VoiceCallAdmin(admin.ModelAdmin):

    fields: List[str] = [ "receiver", "message", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "receiver","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "receiver", "message"]

    #Define model data list ordening
    ordering: List[str] = [ "receiver"]

class MassiveAdmin(admin.ModelAdmin):

    fields: List[str] = [ "name", "type_filters", "slack_channel", "email_event", "whatsapp_event", "text_event", "call_event", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "name", "type_filters", "slack_channel","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "name"]

    #Define model data list ordening
    ordering: List[str] = [ "name", "type_filters"]

class TriggerEventAdmin(admin.ModelAdmin):

    fields: List[str] = [ "name", "description", "email_event", "whatsapp_event", "text_event", "call_event", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "name", "description","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "name", "description"]

    #Define model data list ordening
    ordering: List[str] = [ "name"]

class EventEmailAdmin(admin.ModelAdmin):

    fields: List[str] = [ "email_origin", "email_test", "email_response", "email_copy", "template", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "email_origin", "email_test", "email_response", "email_copy", "operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "email_origin", "email_test", "email_response", "email_copy"]

    #Define model data list ordening
    ordering: List[str] = [ "email_origin", "email_test", "email_response", "email_copy", "template"]

class EventWhatsappAdmin(admin.ModelAdmin):

    fields: List[str] = [ "phone_number_test", "template", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "phone_number_test", "operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "phone_number_test" ]

    #Define model data list ordening
    ordering: List[str] = [ "phone_number_test", "template"]

class EventTextAdmin(admin.ModelAdmin):

    fields: List[str] = [ "phone_number_test", "template", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "phone_number_test","operator" ]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "phone_number_test" ]

    #Define model data list ordening
    ordering: List[str] = [ "phone_number_test", "template"]

class EventCallAdmin(admin.ModelAdmin):

    fields: List[str] = [ "phone_number_test", "template", "operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["ID", "phone_number_test", "operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["ID", "phone_number_test" ]

    #Define model data list ordening
    ordering: List[str] = [ "phone_number_test", "template"]

class CycleCommunicationsAdmin(admin.ModelAdmin):

    fields = ['name', 'type_repeats', 'number_repeats', 'massives_communications', 'slack_channel', "operator"]

    #list of fields to display in django admin
    list_display = ['ID','name', 'type_repeats', 'number_repeats','active', 'test', 'operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

class TemplateWhatsappAdmin(admin.ModelAdmin):

    fields = ['name', 'template', 'order', "operator"]

    #list of fields to display in django admin
    list_display = ['ID','name','operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

class TemplateTextAdmin(admin.ModelAdmin):

    fields = ['name', 'template', "operator"]

    #list of fields to display in django admin
    list_display = ['ID','name','operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

class TemplateCallAdmin(admin.ModelAdmin):

    fields  = ['name', 'template', "operator"]

    #list of fields to display in django admin
    list_display = ['ID','name','operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

class TemplateEmailAdmin(admin.ModelAdmin):

    fields = ['name', 'template_html', 'subject', "operator" ]

    #list of fields to display in django admin
    list_display = ['ID','name', 'subject','operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name', 'subject']

    #to define model data list ordering
    ordering = ['name', 'subject']

class EmailTablesAdmin(admin.ModelAdmin):

    fields = ['name', 'columns', 'last_time', "last_time_type", "operator", "create_me", "assigned_to_me", "filters" ]

    #list of fields to display in django admin
    list_display = ['ID','name','operator']

    #if you want django admin to show the search bar, just add this line
    search_fields = ['ID','name']

    #to define model data list ordering
    ordering = ['name']

admin.site.register(CycleCommunicationsModel, CycleCommunicationsAdmin)
admin.site.register(EmailModel, EmailAdmin)
admin.site.register(TextModel, TextAdmin)
admin.site.register(WhatsappModel, WhatsappAdmin)
admin.site.register(VoiceCallModel, VoiceCallAdmin)
admin.site.register(MassiveCommunicationsModel, MassiveAdmin)
admin.site.register(TriggerEventModel, TriggerEventAdmin)
admin.site.register(EventEmailModel, EventEmailAdmin)
admin.site.register(EventWhatsappModel, EventWhatsappAdmin)
admin.site.register(EventTextModel, EventTextAdmin)
admin.site.register(EventCallModel, EventCallAdmin)

admin.site.register(TemplateWhatsappModel, TemplateWhatsappAdmin)
admin.site.register(TemplateTextModel, TemplateTextAdmin)
admin.site.register(TemplateCallModel, TemplateCallAdmin)
admin.site.register(TemplateEmailModel, TemplateEmailAdmin)

admin.site.register(EmailTablesModel, EmailTablesAdmin)
