from typing import Union, List

from drf_yasg.openapi import Response as Response_Openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers

from .models import CycleCommunicationsModel, MassiveCommunicationsModel


class DocumentationSerializerDatatables(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """

        start: int = serializers.IntegerField()
        offset: int = serializers.IntegerField()
        order_field: str = serializers.CharField(default="ID")
        order_type: str = serializers.CharField(default="desc")
        filters: List[list] = serializers.JSONField(default=[['ID','equal','1']])
        
        class Meta:

            fields: Union[list, str] = '__all__'


class DocumentationCycleCommunications(object):

    """

        Class define documentation to ViewSet CycleCommunicationsView.
    
    """

    class DocumentationCycleSerializerResponse(serializers.ModelSerializer):

        """

            Class define serializer for Response documentation of MassiveCommunicationsView.
    
        """

        massives_communications = serializers.JSONField(default={
                "0": {
                    "massive": "1",
                    "datetime": "07/07/2020 17:46",
                    "next_datetime": "07/07/2020 17:47"
                },
                "1": {
                    "massive": "1",
                    "datetime": "07/07/2020 17:47",
                    "next_datetime": "07/07/2020 17:48"
                }
            }
        )

        class Meta:

            model: CycleCommunicationsModel = CycleCommunicationsModel
            fields: Union[list, str] = '__all__'

    class DocumentationCycleSerializerBody(serializers.ModelSerializer):

        """

            Class define serializer for Body Request documentation of MassiveCommunicationsView.
    
        """

        massives_communications = serializers.JSONField(default={
                "0": {
                    "massive": "1",
                    "datetime": "07/07/2020 17:46",
                    "next_datetime": "07/07/2020 17:47"
                },
                "1": {
                    "massive": "1",
                    "datetime": "07/07/2020 17:47",
                    "next_datetime": "07/07/2020 17:48"
                }
            }
        )

        class Meta:

            model: CycleCommunicationsModel = CycleCommunicationsModel
            fields: Union[list, str] = '__all__'

    class DocumentationCycleSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of MassiveCommunicationsView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []
    
    serializer_class_response = DocumentationCycleSerializerResponse
    serializer_class_body = DocumentationCycleSerializerBody
    serializer_class_void = DocumentationCycleSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatables

    def documentation_create(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationCycleCommunications
            :type self: DocumentationCycleCommunications

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create, cycle communications',
            operation_description="""

                This endpoint create cycle record. 

            """,
            responses = {
                200: Response_Openapi(
                        'Creation cycle instance',
                        self.serializer_class_response,
                    ),
                500:Response_Openapi(
                        description = 'Error create massive instance',
                        examples = {
                            "Key Error":"The key can only be numerical",
                            "Field name_field":"Does no contains one o more of the required fields: massive, datetime",
                            "Invalid datetime field":"Invalid datetime field format. The correct format is: '%d/%m/%Y %H:%M' ",
                            "Invalid datetime field":"Dates must be ascending",
                            "Invalid massive field":"Invalid massive field id id_massive_instance, don't exist",
                            "Invalid order of massives":"The order must be consecutive, starting from 0",
                            "Invalid type":"Invalid data type",
                            "Overlapping dates":"The end date is greater than the next run from the start date",
                        },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_update(self):

        """

            This function return information of documentation view 'Update'

            :param self: Instance of Class DocumentationCycleCommunications
            :type self: DocumentationCycleCommunications

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Update, cycle communications',
            operation_description="""

                This endpoint Update cycle record. 

            """,
            responses = {
                200: Response_Openapi(
                        'Creation cycle instance',
                        self.serializer_class_response,
                    ),
                500:Response_Openapi(
                        description = 'Error Update massive instance',
                        examples = {
                            "Key Error":"The key can only be numerical",
                            "Field name_field":"Does no contains one o more of the required fields: massive, datetime",
                            "Invalid datetime field":"Invalid datetime field format. The correct format is: '%d/%m/%Y %H:%M' ",
                            "Invalid datetime field":"Dates must be ascending",
                            "Invalid massive field":"Invalid massive field id id_massive_instance, don't exist",
                            "Invalid order of massives":"The order must be consecutive, starting from 0",
                            "Invalid type":"Invalid data type",
                            "Overlapping dates":"The end date is greater than the next run from the start date",
                            "Overlapping dates":"The end date is greater than the next run from the start date",
                        },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_activate(self):

        """

            This function return information of documentation view 'activate'

            :param self: Instance of Class DocumentationCycleCommunications
            :type self: DocumentationCycleCommunications

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Activate, cycle communications',
            operation_description="""

                This endpoint active cycle record. 

            """,
            responses = {
                200: Response_Openapi(
                        'Active cycle instance',
                        self.serializer_class_void,
                    ),
                500:Response_Openapi(
                        description = 'Error active massive instance',
                        examples = {
                            "Datetime error":"Start time already passed",
                            "Cycle active":"The cycle is active o running a test",
                            "Pk error":"Does not exist item",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_void,
        )

    def documentation_inactive(self):

        """

            This function return information of documentation view 'inactive'

            :param self: Instance of Class DocumentationCycleCommunications
            :type self: DocumentationCycleCommunications

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Inactive, cycle communications',
            operation_description="""

                This endpoint inactive cycle record. 

            """,
            responses = {
                200: Response_Openapi(
                        'inactive cycle instance',
                        self.serializer_class_void,
                    ),
                500:Response_Openapi(
                        description = 'Error inactive massive instance',
                        examples = {
                            "Pk error":"Does not exist item",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_void,
        )

    def documentation_test(self):

        """

            This function return information of documentation view 'test'

            :param self: Instance of Class DocumentationCycleCommunications
            :type self: DocumentationCycleCommunications

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Test, cycle communications',
            operation_description="""

                This endpoint test cycle record. 

            """,
            responses = {
                200: Response_Openapi(
                        'Test cycle instance',
                        self.serializer_class_void,
                    ),
                500:Response_Openapi(
                        description = 'Error test massive instance',
                        examples = {
                            "Datetime error":"Start time already passed",
                            "Cycle test":"The cycle is active o running a test",
                            "Pk error":"Does not exist item",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_void,
        )

    def documentation_datables_struct(self):

        """
            This function return struct datatables cycle communications

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, cycle communications',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, cycle communications',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "slack_channel": "str",
                            "type_repeats": "choices",
                            "active": "bool",
                            "test": "bool",
                        },
                        "columns": {
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Tipo de repeticiones":{
                                "field": "type_repeats",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Estado":{
                                "field": "active",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Prueba":{
                                "field": "test",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 6
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 7
                            },
                        },
                        "filters": {
                            "Fecha de inicio":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Fecha de fin":{
                                "type":"datetime",
                                "name":"created",
                                "format":"%d/%m/%Y %H:%M",
                            },
                            "Nombre":{
                                "type":"str",
                                "name":"name"
                            },
                            "Estado":{
                                "type":"bool",
                                "name":"active"
                            },
                            "Tipo de repeticiones":{
                                "type":"choices",
                                "name":"type_repeats",
                                "type_choices": "choices",
                                "values": {
                                    "type": "static",
                                    "extra_data":{
                                        "I":"Minutos",
                                        "O":"Una vez",
                                        "H":"Cada Hora",
                                        "D":"Diariamente",
                                        "W":"Semanalmente",
                                        "M":"Mensualmente",
                                        "Q":"Trimestralmente",
                                        "Y":"Anualmente",
                                    }
                                }
                            },
                            "Creador":{
                                "type":"choices",
                                "name":"creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data":{
                                        "url": "user/data/information/",
                                        "read_data":{
                                            "value":"ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationCycleCommunications
            :type self: DocumentationCycleCommunications

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, cycle',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, cycle',
                    examples = [
                        {
                            "name": "Ciclo cobro",
                            "ID": "10",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "slack_channel": "Iris channel",
                            "type_repeats": "Minutos",
                            "active": True,
                            "test": False,
                        },
                        {
                            "name": "Ciclo cobro",
                            "ID": "10",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "slack_channel": "Iris channel",
                            "type_repeats": "Minutos",
                            "active": True,
                            "test": False,
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

class DocumentationMassiveCommunications(object):

    """

        Class define documentation to ViewSet MassiveCommunicationsView.
    
    """

    class DocumentationMassiveSerializerResponse(serializers.ModelSerializer):

        """

            Class define serializer for Response documentation of MassiveCommunicationsView.
    
        """

        excludes = serializers.JSONField(default=['1000','5963'])
        includes = serializers.JSONField(default=['1000','5963'])
        filters = serializers.JSONField(default=[ ['number', 'equal', 10], ['number', 'gte', 100]])

        class Meta:

            model: MassiveCommunicationsModel = MassiveCommunicationsModel
            fields: Union[list, str] = '__all__'

    class DocumentationMassiveSerializerBody(serializers.ModelSerializer):

        """

            Class define serializer for Body Request documentation of MassiveCommunicationsView.
    
        """

        #Indicates field of file type
        excludes_file = serializers.CharField(default='This is a FILE field')
        #Indicates field of file type
        includes_file = serializers.CharField(default='This is a FILE field')
        filters = serializers.JSONField(default=[ ['number', 'equal', 10], ['number', 'gte', 100]])

        class Meta:

            model: MassiveCommunicationsModel = MassiveCommunicationsModel
            fields: Union[list,str] = [

                "excludes_file",
                "includes_file", 
                "filters",
                "type_filters",
                "operator",

            ]
            extra_kwargs: dict = {

                'excludes': {
                    'required': False
                },
                'includes': {
                    'required': False
                },

            }

    class DocumentationMassiveSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of MassiveCommunicationsView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []
    
    class DocumentationSerializerDeleteChannelMassiveVoid(serializers.Serializer):

        """

            Class define void Serializer of TriggerEvent.
    
        """

        choices: list = [
            ('email', 'email'),
            ('text', 'text'),
            ('whatsapp', 'whatsapp'),
            ('call', 'call'),
        ]

        channel : str = serializers.ChoiceField(choices)
        
        class Meta:

            fields: Union[list, str] = []

    serializer_class_response = DocumentationMassiveSerializerResponse
    serializer_class_body = DocumentationMassiveSerializerBody
    serializer_class_void = DocumentationMassiveSerializerVoid
    serializer_class_datatables = DocumentationSerializerDatatables
    serializer_class_delete_channel = DocumentationSerializerDeleteChannelMassiveVoid


    def documentation_activate(self):

        """

            This function return information of documentation view 'activate'

            :param self: Instance of Class DocumentationCycleCommunications
            :type self: DocumentationCycleCommunications

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Activate, massive',
            operation_description="""

                This endpoint active massive. 

            """,
            responses = {
                200: Response_Openapi(
                        'Active massive',
                        self.serializer_class_void,
                    ),
                500:Response_Openapi(
                        description = 'Error active massive instance',
                        examples = {
                            "Pk error":"Does not exist item",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_void,
        )

    def documentation_create(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationMassiveCommunications
            :type self: DocumentationMassiveCommunications

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create, massive communications',
            operation_description="""

                This endpoint create massive record. 

            """,
            responses = {
                200: Response_Openapi(
                        'Creation massive instance',
                        self.serializer_class_response,
                    ),
                500:Response_Openapi(
                        description = 'Erros create massive instance',
                        examples = {
                            "Data error":"Error data",
                            "Invalid length": "List must contain three elements",
                            "Invalid filter": "Don't exist filter {{name_filter}}",
                            "Invalid comparison": "Invalid comparison item {{name_comparison}} for filter {{name_filter}}",
                            "Error sheet excludes": "Don't exist sheet named 'excludes_files'",
                            "Error sheet includes": "Don't exist sheet named 'includes_files'",
                            "email_origin required":"Required field email_origin",
                            "email_origin invalid":"Required valid email field email_origin",
                            "email_test required":"Required field email_test",
                            "email_test invalid":"Required valid email field email_test",
                            "email_response required":"Required field email_response",
                            "email_response invalid":"Required valid email field email_response",
                            "email_copy required":"Required field email_copy",
                            "email_copy invalid":"Required valid email field email_copy",
                            "template email error":"Required valid template field of email event",
                            "template whatsapp error":"Required valid template field of whatsapp event",
                            "phone_number_test whatsapp":"Invalid Whatsapp phone format",
                            "template voice call error":"Required valid template field of voice call event",
                            "phone_number_test voice call":"Invalid Call phone format",
                            "template text error":"Required valid template field of text event",
                            "phone_number_test voice call":"Invalid Text phone format",
                        },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.DocumentationMassiveSerializerBody,
        )

    def documentation_update(self):

        """

            This function return information of documentation view 'update'

            :param self: Instance of Class DocumentationMassiveCommunications
            :type self: DocumentationMassiveCommunications

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Update, massive communications',
            operation_description="""

                This endpoint Update massive record. 

            """,
            responses = {
                200: Response_Openapi(
                        'Creation massive instance',
                        self.serializer_class_response,
                    ),
                500:Response_Openapi(
                        description = 'Erros Update massive instance',
                        examples = {
                            "Data error":"Error data",
                            "Invalid length": "List must contain three elements",
                            "Invalid filter": "Don't exist filter {{name_filter}}",
                            "Invalid comparison": "Invalid comparison item {{name_comparison}} for filter {{name_filter}}",
                            "Error sheet excludes": "Don't exist sheet named 'excludes_files'",
                            "Error sheet includes": "Don't exist sheet named 'includes_files'",
                            "email_origin required":"Required field email_origin",
                            "email_origin invalid":"Required valid email field email_origin",
                            "email_test required":"Required field email_test",
                            "email_test invalid":"Required valid email field email_test",
                            "email_response required":"Required field email_response",
                            "email_response invalid":"Required valid email field email_response",
                            "email_copy required":"Required field email_copy",
                            "email_copy invalid":"Required valid email field email_copy",
                            "template email error":"Required valid template field of email event",
                            "template whatsapp error":"Required valid template field of whatsapp event",
                            "phone_number_test whatsapp":"Invalid Whatsapp phone format",
                            "template voice call error":"Required valid template field of voice call event",
                            "phone_number_test voice call":"Invalid Call phone format",
                            "template text error":"Required valid template field of text event",
                            "phone_number_test voice call":"Invalid Text phone format",
                        },
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.DocumentationMassiveSerializerBody,
        )

    def documentation_delete(self):

        """
            This function return information of documentation view 'delete'

            :param self: Instance of Class DocumentationMassiveCommunications
            :type self: DocumentationMassiveCommunications

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Delete, massive communications',
            operation_description="""

                This endpoint Delete massive record. 

            """,
            responses = {
                500:Response_Openapi(
                        description = 'Error Update massive instance',
                        examples = {
                            "Unexpected":"Unexpected Error",
                            "Does not exist item": "Instance to delete don't exist",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_void,
        )

    def documentation_filters(self):

        """
            This function return information of documentation view 'filters'

            :param self: Instance of Class DocumentationMassiveCommunications
            :type self: DocumentationMassiveCommunications

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Filter, massive communications',
            operation_description="""

                This endpoint return filters information. This method does not expect any value. 

                \n**filters_translation:** Dict filters, The key is the name in spanish, the value is correct value to send
                \n**filters:** Dict of filters in spanish as key, and the value is the type
                \n**type_filters:** Type of comparisons, the key is the spanish representation and the value is the correct value to send
                \n**type_filters_tranlation:** List of comparison according to type

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Dict with filters',
                    examples = {
                        "result":{
                            'filters_translation': {
                                'Fecha de activacion': 'activated_on',
                                'Fecha de instalacion': 'installed_on',
                                'Tipo de tecnologia': 'technology_kind',
                            },
                            'filters': {
                                'Fecha de activacion': 'date',
                                'Fecha de instalacion': 'date',
                                'Tipo de tecnologia': 'list',
                            },
                            'type_filters': {
                                'date': ['igual', 'año', 'mes', 'dia', 'dia_semana', 'semana', 'trimestre', 'mayor', 'mayor_igual','menor', 'menor_igual', 'distinto'],
                                'list': ['igual', 'distinto'],
                            },
                            'type_filters_tranlation': {
                                'igual': 'equal',
                                'mayor': 'gt',
                                'mayor_igual': 'gte',
                                'menor': 'lt',
                                'menor_igual': 'lte',
                            }
                        },
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationMassiveCommunications
            :type self: DocumentationMassiveCommunications

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, massive',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, massive',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "slack_channel": "Iris",
                            "channels": ["email", "text"]
                        },
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "slack_channel": "Iris",
                            "channels": ["email", "text"]
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_test(self):

        """
            This function return information of documentation view 'test'

            :param self: Instance of Class DocumentationMassiveCommunications
            :type self: DocumentationMassiveCommunications

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Test, massive communications',
            operation_description="""

                This endpoint Test massive record. 

            """,
            responses = {
                500:Response_Openapi(
                        description = 'Error Test massive instance',
                        examples = {
                            "Pk error": "Instance to delete don't exist",
                        },
                ),
                200:Response_Openapi(
                        description = 'Does not exist item',
                        examples = {},
                )
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_destroy(self):

        """
            This function return information of documentation view 'destroy'

            :param self: Instance of Class DocumentationMassiveCommunications
            :type self: DocumentationMassiveCommunications

            :returns: Funttion, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Destroy, massive communications',
            operation_description="""

                This endpoint destroy and inactive massive record 

            """,
            responses = {
                500:Response_Openapi(
                        description = 'Error destroy massive instance',
                        examples = {
                            "Unexpected":"Unexpected Error",
                            "Does not exist item": "Instance to delete don't exist",
                        },
                ),
                200:Response_Openapi(
                        description = 'Test massive instance',
                        examples = {},
                )
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datables_struct(self):

        """
            This function return struct datatables massive communications

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, massive communications',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, massive communications',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "slack_channel": "str",
                            "channels": "list",
                        },
                        "columns": {
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Canales":{
                                "field": "channels",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Fecha de fin": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Nombre": {
                                "type": "str",
                                "name": "name"
                            },
                            "Creador": {
                                "type": "choices",
                                "name": "creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data": {
                                        "url": "user/data/information/",
                                        "read_data": {
                                            "value": "ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_add_channel(self):

        """
            This function return information of documentation view 'active'

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Add event to massive',
            operation_description="""

                This endpoint add event to massive.

                    This required 'channel' field . Its values ​​are: 'email', 'text', 'whatsapp', 'call'

                    If the value of the field 'channel' is 'email' then send  the following parameters: 
                        
                        field: email_test
                        type: email
                        required: True

                        field: email_response
                        type: email
                        required: True

                        field: email_copy
                        type: email
                        required: False

                        field: template
                        type: int
                        required: True

                    If the value of the field 'channel' is different from 'email' then send  the following parameters:

                        field: phone_number_test
                        type: phone
                        required: True

                        field: template
                        type: int
                        required: True

            """,
            responses = {
                200:Response_Openapi(
                    description = 'add event',
                    examples = {
                        
                    },
                ),
                500:Response_Openapi(
                        description = 'Erros create massive instance',
                        examples = {
                            "Enter a valid email address":"The email_test, email_response and email_copy fields require a valid email address",
                            "This field may not be blank.": "The email_test, email_response and channel fields may not be blank",
                            "This field may not be null.": "The template fields may not be null",
                            "Required 'channel' field.": "The channel fields is required",
                            "No valid 'channel' field value.": "Valid values is: email, whatsapp, text, call"
                        },
                )
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_remove_channel(self):

        """
            This function delete channel of trigger

            :param self: Instance of Class MassiveCommunicationsView
            :type self: MassiveCommunicationsView

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Delete channel of massive',
            operation_description="""

                This endpoint delete channel of massive. 

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Delete channel',
                    examples = {
                        
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_delete_channel,
        )

class DocumentationTemplateEmail(object):

    """

        Class define documentation of TemplateEmailView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []

    class DocumentationSerializerTestEmailVoid(serializers.Serializer):

        """

            Class define void Serializer of TemplateCallView.
    
        """

        template: str = serializers.CharField(write_only=True)
        subject: str = serializers.CharField(write_only=True)
        from_email: str = serializers.EmailField(write_only=True)
        to_email: str = serializers.EmailField(write_only=True)
        
        class Meta:

            fields: Union[list, str] = '__all__'
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_test = DocumentationSerializerTestEmailVoid
    serializer_class_datatables = DocumentationSerializerDatatables
    
    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, email template',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, email template',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "template": "Hola {{customer__name}}",
                        },
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "template": "Hola {{customer__name}}",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )
   
    def documentation_test(self):

        """
            This function return email

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Test, email template',
            operation_description="""

                This endpoint test, email

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Test, email template',
                    examples = {},
                ),
                500:Response_Openapi(
                        description = 'Erros test email template',
                        examples = {
                            "Requeried fields":" 'telephone' and 'template' fields is required ",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_test,
        )

    def documentation_datables_struct(self):

        """
            This function return struct datatables email

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, email template',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, email template',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "template": "str"
                        },
                        "columns": {
                            "Nombre": {
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador": {
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación": {
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Plantilla": {
                                "field": "template",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar": {
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar": {
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            }
                        },
                        "filters": {
                            "Fecha de inicio": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Fecha de fin": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Nombre": {
                                "type": "str",
                                "name": "name"
                            },
                            "Creador": {
                                "type": "choices",
                                "name": "creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data": {
                                        "url": "user/data/information/",
                                        "read_data": {
                                            "value": "ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationTemplateWhatsapp(object):

    """

        Class define documentation of TemplateWhatsappView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []
    
    class DocumentationSerializerTestWhatsappVoid(serializers.Serializer):

        """

            Class define void Serializer of TemplateCallView.
    
        """

        template_name: str = serializers.CharField(write_only=True)
        telephone: str = serializers.CharField(write_only=True)
        params: List[str] = serializers.JSONField(write_only=True, default=['customer__name', 'customer__rut'])
        
        class Meta:

            fields: Union[list, str] = '__all__'
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_test = DocumentationSerializerTestWhatsappVoid
    serializer_class_datatables = DocumentationSerializerDatatables

    def documentation_datables_struct(self):

        """
            This function return struct datatables whatsapp

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, whatsapp template',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, whatsapp template',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "template": "str"
                        },
                        "columns": {
                            "Nombre": {
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador": {
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación": {
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Plantilla": {
                                "field": "template",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar": {
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar": {
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            }
                        },
                        "filters": {
                            "Fecha de inicio": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Fecha de fin": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Nombre": {
                                "type": "str",
                                "name": "name"
                            },
                            "Creador": {
                                "type": "choices",
                                "name": "creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data": {
                                        "url": "user/data/information/",
                                        "read_data": {
                                            "value": "ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, whatsapp template',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, whatsapp template',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "template": "Hola {{customer__name}}",
                        },
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "template": "Hola {{customer__name}}",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_test(self):

        """
            This function return test whatsapp

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Test, whatsapp template',
            operation_description="""

                This endpoint test, whatsapp

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Test, whatsapp template',
                    examples = {},
                ),
                500:Response_Openapi(
                        description = 'Erros test whatsapp template',
                        examples = {
                            "Requeried fields":" 'telephone' and 'template' fields is required ",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_test,
        )

    def documentation_tags(self):

        """
            This function return tags whatsapp

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Tags, whatsapp template',
            operation_description="""

                This endpoint return tags list

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Tags, whatsapp template',
                    examples = [
                        "service__tower",
                        "service__commune",
                        "service__location",
                        "service__activity",
                        "node__code",
                        "node__address",
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationTemplateText(object):

    """

        Class define documentation of TemplateTextView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TemplateTextView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []
    
    class DocumentationSerializerTestTextVoid(serializers.Serializer):

        """

            Class define void Serializer of TemplateCallView.
    
        """

        template: str = serializers.CharField(write_only=True)
        telephone: str = serializers.CharField(write_only=True)
        
        class Meta:

            fields: Union[list, str] = '__all__'
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_test = DocumentationSerializerTestTextVoid
    serializer_class_datatables = DocumentationSerializerDatatables
    
    def documentation_datables_struct(self):

        """
            This function return struct datatables text

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, text template',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, text template',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "template": "str"
                        },
                        "columns": {
                            "Nombre": {
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador": {
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación": {
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Plantilla": {
                                "field": "template",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar": {
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar": {
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            }
                        },
                        "filters": {
                            "Fecha de inicio": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Fecha de fin": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Nombre": {
                                "type": "str",
                                "name": "name"
                            },
                            "Creador": {
                                "type": "choices",
                                "name": "creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data": {
                                        "url": "user/data/information/",
                                        "read_data": {
                                            "value": "ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_test(self):

        """
            This function return test text

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Test, text template',
            operation_description="""

                This endpoint test, text

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Test, text template',
                    examples = {},
                ),
                500:Response_Openapi(
                        description = 'Erros test text template',
                        examples = {
                            "Requeried fields":" 'telephone' and 'template' fields is required ",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_test,
        )

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, text template',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, text template',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "template": "Hola {{customer__name}}",
                        },
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "template": "Hola {{customer__name}}",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

class DocumentationTemplateCall(object):

    """

        Class define documentation of TemplateCallView.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TemplateCallView.
    
        """
        
        class Meta:

            fields: Union[list, str] = []

    class DocumentationSerializerTestCallVoid(serializers.Serializer):

        """

            Class define void Serializer of TemplateCallView.
    
        """

        template: str = serializers.CharField(write_only=True)
        telephone: str = serializers.CharField(write_only=True)
        
        class Meta:

            fields: Union[list, str] = '__all__'
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_test = DocumentationSerializerTestCallVoid
    serializer_class_datatables = DocumentationSerializerDatatables

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, voice call template',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, voice call template',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "template": "Hola {{customer__name}}",
                        },
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "template": "Hola {{customer__name}}",
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_test(self):

        """
            This function return test voice call

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
        
        return swagger_auto_schema(
            operation_id='Test, voice call template',
            operation_description="""

                This endpoint test, voice call

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Test, voice call template',
                    examples = {},
                ),
                500:Response_Openapi(
                        description = 'Erros test voice call template',
                        examples = {
                            "Requeried fields":" 'telephone' and 'template' fields is required ",
                        },
                )
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_test,
        )

    def documentation_datables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, voice call template',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, voice call template',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "template": "str"
                        },
                        "columns": {
                            "Nombre": {
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador": {
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación": {
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Plantilla": {
                                "field": "template",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar": {
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar": {
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            }
                        },
                        "filters": {
                            "Fecha de inicio": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Fecha de fin": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Nombre": {
                                "type": "str",
                                "name": "name"
                            },
                            "Creador": {
                                "type": "choices",
                                "name": "creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data": {
                                        "url": "user/data/information/",
                                        "read_data": {
                                            "value": "ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

class DocumentationTriggerEvent(object):

    """

        Class define documentation of TriggerEvent.
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void Serializer of TriggerEvent.
    
        """
        
        class Meta:

            fields: Union[list, str] = []

    class DocumentationSerializerDeleteChannelVoid(serializers.Serializer):

        """

            Class define void Serializer of TriggerEvent.
    
        """

        choices: list = [
            ('email', 'email'),
            ('text', 'text'),
            ('whatsapp', 'whatsapp'),
            ('call', 'call'),
        ]

        channel : str = serializers.ChoiceField(choices)
        
        class Meta:

            fields: Union[list, str] = []

    class DocumentationTriggerEventSerializer(serializers.Serializer):

        """

            Class define Serializer of TriggerEvent.
    
        """
        
        service = serializers.IntegerField(write_only=True, required=True)
        operator = serializers.IntegerField(write_only=True, required=True)

        class Meta:

            fields: Union[list, str] = []
    
    serializer_class_void = DocumentationSerializerVoid
    serializer_class_body = DocumentationTriggerEventSerializer
    serializer_class_datatables = DocumentationSerializerDatatables
    serializer_class_delete_channel = DocumentationSerializerDeleteChannelVoid

    def documentation_datables(self):

        """
            This function return information of documentation view 'datables'

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Datatables, trigger event',
            operation_description="""

                This endpoint return datatable information. This method expect value 'search[value]' as int and other values of datatable plugin. If 'search[value]' is number, it returns only record with that this ID, but returns all.

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Datatables, trigger event',
                    examples = [
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "description": "Este evento es de ...",
                            "channels": ["email", "text"]
                        },
                        {
                            "ID": "10",
                            "name": "Cobranza",
                            "created": "2020-06-17T14:40:00.568523-05:00",
                            "creator": "cristian@optic.cl",
                            "description": "Este evento es de ...",
                            "channels": ["email", "whatsapp"]
                        }
                    ],
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_datatables,
        )

    def documentation_active(self):

        """
            This function return information of documentation view 'active'

            :param self: Instance of Class TriggerEvent
            :type self: TriggerEvent

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Active, massive communications',
            operation_description="""

                This endpoint Active massive record.

                    Params:

                        name: event name
                        operator: Operator pk
                        info: list of dict. Each dictionary must contain: 'email' field and 'customer__phone' field.
                              The 'customer__phone' field must be: +(code)(Last nine digits of the phone)

                            Example:

                                [
                                    {
                                        'number': '13149', 
                                        'street': '',
                                        'house_number': '',
                                        'apartment_number': '',
                                        'tower': '',
                                        'location': '13101',
                                        'price_override': '0',
                                        'due_day': '5',
                                        'activated_on': '',
                                        'installed_on': '',
                                        'uninstalled_on': '',
                                        'expired_on': '',
                                        'status': '',
                                        'network_mismatch': 'Si',
                                        'ssid': '13149.bandaancha.cl',
                                        'technology_kind': '2',
                                        'allow_auto_cut': '',
                                        'seen_connected': '',
                                        'mac_onu': '3C:78:43:FA:C0:D1',
                                        'composite_address': 'Lord Cochrane 635 dpto 403 torre A',
                                        'commune': 'Santiago',
                                        'node__code': 'LC635',
                                        'node__address': 'Lord Cochrane 635',
                                        'node__street': 'Lord Cochrane',
                                        'node__house_number': '635',
                                        'node__commune': 'Santiago',
                                        'node__olt_config': '',
                                        'node__is_building': 'No',
                                        'node__is_optical_fiber': 'Si',
                                        'node__gpon': 'Si',
                                        'node__gepon': 'No',
                                        'node__pon_port': '',
                                        'node__box_location': '',
                                        'node__splitter_location': '',
                                        'node__expected_power': '',
                                        'node__towers': '2',
                                        'node__apartments': '700',
                                        'node__phone': '+5622662 7737',
                                        'plan__name': 'BA-FIBRA-GPON-10Megas',
                                        'plan__price': '12990.00',
                                        'plan__category': '',
                                        'plan__uf': 'Si',
                                        'plan__active': 'Si',
                                        'customer__rut': '26.754.935-4',
                                        'customer__name': 'Salvador',
                                        'customer__email': 'salvador@multifiber.cl',
                                        'customer__street': 'Lord Cochrane',
                                        'customer__house_number': '635',
                                        'customer__apartment_number': '403',
                                        'customer__tower': 'A',
                                        'customer__location': '13101',
                                        'customer__phone': '56964088525',
                                        'customer__default_due_day': '5',
                                        'customer__composite_address': 'Lord Cochrane 635 dpto 403 torre A',
                                        'customer__commune': 'Santiago',
                                        'files':[
                                            'https://www.nemours.org/content/dam/nemours/www/filebox/service/preventive/nhps/heguide.pdf'
                                        ]
                                    },
                                    {
                                        'number': '13149', 
                                        'street': '',
                                        'house_number': '',
                                        'apartment_number': '',
                                        'tower': '',
                                        'location': '13101',
                                        'price_override': '0',
                                        'due_day': '5',
                                        'activated_on': '',
                                        'installed_on': '',
                                        'uninstalled_on': '',
                                        'expired_on': '',
                                        'status': '',
                                        'network_mismatch': 'Si',
                                        'ssid': '13149.bandaancha.cl',
                                        'technology_kind': '2',
                                        'allow_auto_cut': '',
                                        'seen_connected': '',
                                        'mac_onu': '3C:78:43:FA:C0:D1',
                                        'composite_address': 'Lord Cochrane 635 dpto 403 torre A',
                                        'commune': 'Santiago',
                                        'node__code': 'LC635',
                                        'node__address': 'Lord Cochrane 635',
                                        'node__street': 'Lord Cochrane',
                                        'node__house_number': '635',
                                        'node__commune': 'Santiago',
                                        'node__olt_config': '',
                                        'node__is_building': 'No',
                                        'node__is_optical_fiber': 'Si',
                                        'node__gpon': 'Si',
                                        'node__gepon': 'No',
                                        'node__pon_port': '',
                                        'node__box_location': '',
                                        'node__splitter_location': '',
                                        'node__expected_power': '',
                                        'node__towers': '2',
                                        'node__apartments': '700',
                                        'node__phone': '+5622662 7737',
                                        'plan__name': 'BA-FIBRA-GPON-10Megas',
                                        'plan__price': '12990.00',
                                        'plan__category': '',
                                        'plan__uf': 'Si',
                                        'plan__active': 'Si',
                                        'customer__rut': '26.754.935-4',
                                        'customer__name': 'Liseth',
                                        'customer__email': 'liseth.gonzalez@multifiber.cl',
                                        'customer__street': 'Lord Cochrane',
                                        'customer__house_number': '635',
                                        'customer__apartment_number': '403',
                                        'customer__tower': 'A',
                                        'customer__location': '13101',
                                        'customer__phone': '584242560256',
                                        'customer__default_due_day': '5',
                                        'customer__composite_address': 'Lord Cochrane 635 dpto 403 torre A',
                                        'customer__commune': 'Santiago',
                                        'files':[
                                            'https://www.nemours.org/content/dam/nemours/www/filebox/service/preventive/nhps/heguide.pdf'
                                        ]
                                    },
                                ]
            """,
            responses = {
                200:Response_Openapi(
                    description = 'Activated event',
                    examples = {
                        
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_add_channel(self):

        """
            This function return information of documentation view 'active'

            :param self: Instance of Class TriggerEvent
            :type self: TriggerEvent

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Add event to trigger',
            operation_description="""

                This endpoint add event to trigger.

                    This required 'channel' field . Its values ​​are: 'email', 'text', 'whatsapp', 'call'

                    If the value of the field 'channel' is 'email' then send  the following parameters: 
                        
                        field: email_test
                        type: email
                        required: True

                        field: email_response
                        type: email
                        required: True

                        field: email_copy
                        type: email
                        required: False

                        field: template
                        type: int
                        required: True

                    If the value of the field 'channel' is different from 'email' then send  the following parameters:

                        field: phone_number_test
                        type: phone
                        required: True

                        field: template
                        type: int
                        required: True

            """,
            responses = {
                200:Response_Openapi(
                    description = 'add event',
                    examples = {
                        
                    },
                ),
                500:Response_Openapi(
                        description = 'Erros create massive instance',
                        examples = {
                            "Enter a valid email address":"The email_test, email_response and email_copy fields require a valid email address",
                            "This field may not be blank.": "The email_test, email_response and channel fields may not be blank",
                            "This field may not be null.": "The template fields may not be null",
                            "Required 'channel' field.": "The channel fields is required",
                            "No valid 'channel' field value.": "Valid values is: email, whatsapp, text, call"
                        },
                )
            },
            query_serializer = self.serializer_class_void,
        )

    def documentation_remove_channel(self):

        """
            This function delete channel of trigger

            :param self: Instance of Class TriggerEvent
            :type self: TriggerEvent

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Delete channel of trigger',
            operation_description="""

                This endpoint delete channel of trigger. 

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Delete channel',
                    examples = {
                        
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
            request_body = self.serializer_class_delete_channel,
        )

    def documentation_datables_struct(self):

        """
            This function return struct datatables voice call

            :param self: Instance of Class DocumentationTemplate
            :type self: DocumentationTemplate

            :returns: Function, obtains documentation information
            :rtype: function
        
        """
    
        return swagger_auto_schema(
            operation_id='Struct datatables, voice call template',
            operation_description="""

                This endpoint return struct datatables

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Struct, voice call template',
                    examples = {
                        "id_table": None,
                        "fields": {
                            "ID": "int",
                            "name": "str",
                            "created": "datetime",
                            "creator": "str",
                            "description": "str",
                            "channels": "list",
                        },
                        "columns": {
                            "Nombre":{
                                "field": "name",
                                "sortable": True,
                                "visible": True,
                                "position": 0
                            },
                            "Creador":{
                                "field": "creator",
                                "sortable": True,
                                "visible": True,
                                "position": 1
                            },
                            "Fecha de creación":{
                                "field": "created",
                                "sortable": True,
                                "visible": True,
                                "position": 2
                            },
                            "Descripción":{
                                "field": "description",
                                "sortable": False,
                                "visible": True,
                                "position": 3
                            },
                            "Modificar":{
                                "field": "custom_change",
                                "sortable": False,
                                "visible": True,
                                "position": 4
                            },
                            "Eliminar":{
                                "field": "custom_delete",
                                "sortable": False,
                                "visible": True,
                                "position": 5
                            },
                        },
                        "filters": {
                            "Fecha de inicio": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Fecha de fin": {
                                "type": "datetime",
                                "name": "created",
                                "format": "%d/%m/%Y %H:%M"
                            },
                            "Nombre": {
                                "type": "str",
                                "name": "name"
                            },
                            "Creador": {
                                "type": "choices",
                                "name": "creator__pk",
                                "type_choices": "choices",
                                "values": {
                                    "type": "dinamic",
                                    "extra_data": {
                                        "url": "user/data/information/",
                                        "read_data": {
                                            "value": "ID",
                                            "human_readable": "name"
                                        }
                                    }
                                }
                            }
                        },
                        "comparisons": {
                            "Igual": {
                                "name": "equal",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool",
                                    "choices"
                                ]
                            },
                            "Año": {
                                "name": "year",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mes": {
                                "name": "month",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día": {
                                "name": "day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Día de la semana": {
                                "name": "week_day",
                                "type": [
                                    "int"
                                ]
                            },
                            "Semana": {
                                "name": "week",
                                "type": [
                                    "int"
                                ]
                            },
                            "Trimestre": {
                                "name": "quarter",
                                "type": [
                                    "int"
                                ]
                            },
                            "Mayor": {
                                "name": "gt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Mayor o igual": {
                                "name": "gte",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor": {
                                "name": "lt",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Menor o igual": {
                                "name": "lte",
                                "type": "datetime"
                            },
                            "Distinto": {
                                "name": "different",
                                "type": [
                                    "datetime",
                                    "str",
                                    "int",
                                    "bool"
                                ]
                            },
                            "Similar": {
                                "name": "iexact",
                                "type": "str"
                            },
                            "Contiene": {
                                "name": "icontains",
                                "type": "str"
                            },
                            "Inicia con": {
                                "name": "istartswith",
                                "type": "str"
                            },
                            "Termina con": {
                                "name": "iendswith",
                                "type": "str"
                            }
                        },
                        "type_of_comparisons": {
                            "datetime": [
                                "Igual",
                                "Año",
                                "Mes",
                                "Día",
                                "Día de la semana",
                                "Semana",
                                "Trimestre",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "str": [
                                "Igual",
                                "Similar",
                                "Contiene",
                                "Inicia con",
                                "Termina con",
                                "Distinto"
                            ],
                            "int": [
                                "Igual",
                                "Mayor",
                                "Mayor o igual",
                                "Menor",
                                "Menor o igual",
                                "Distinto"
                            ],
                            "bool": [
                                "Igual",
                                "Distinto"
                            ],
                            "choices": [
                                "Igual",
                                "Distinto"
                            ]
                        },
                        "defaults": {
                            "order_field": "ID",
                            "order_type": "desc",
                            "start": 0,
                            "offset": 0
                        }
                    },
                ),
            },
            query_serializer = self.serializer_class_void,
        )
