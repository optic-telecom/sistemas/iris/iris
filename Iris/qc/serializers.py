import re
from datetime import datetime, timezone, timedelta
from typing import Any, List, Union, Dict
import requests
import random
import string
import json
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import get_ticket_SLA
from django.db.models.query import QuerySet
from rest_framework import serializers
from rest_framework.serializers import ValidationError
from schema import And, Schema, Use
from dynamic_preferences.registries import global_preferences_registry
from .models import (QAChannelModel, QAIndicatorModel, QAInspectionModel, 
QAInspectionTablesModel, QAInspectionAnswerModel, QAInspectionAnswerChildModel, QAMonitoringModel, QAMonitoringRecordModel)

class QAIndicatorSerializer(BaseSerializer):

    """

        Class define serializer of QAIndicatorView.
    
    """

    def create(self, validated_data: Dict[str, Any]) -> QAIndicatorModel:

        """
        
            This funtion validate and create instance

            :param self: QAIndicatorSerializer instance
            :type self: QAIndicatorSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        result = super().create(validated_data)

        if result.parent == None:
            inspections = QAInspectionModel.objects.filter(kind=result.channel)

            for inspection in inspections:
                
                answer_instance = QAInspectionAnswerModel.objects.create(
                    indicator=result,
                    inspection=inspection,
                    operator=result.operator,
                    satisfied= True
                )

        else:

            parent_answers = QAInspectionAnswerModel.objects.filter(indicator=result.parent)

            for parent in parent_answers:

                child_instance = QAInspectionAnswerChildModel.objects.create(
                    indicator=result,
                    satisfied=True,
                    operator=result.operator,
                    parent=parent
                )
        

        return result

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: QAIndicatorSerializer instance
            :type self: QAIndicatorSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """
        if "parent" in validated_data:
            raise ValidationError({"parent": "updating an indicator parent is not allowed"})

        result = super().update(instance, validated_data)

    class Meta:

        #Class model of serializer
        model: BaseModel = QAIndicatorModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class QAChannelSerializer(BaseSerializer):

    """

        Class define serializer of QAchannelView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = QAChannelModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class QAInspectionSerializer(BaseSerializer):

    """

        Class define serializer of QAchannelView.
    
    """

    answers = serializers.SerializerMethodField()
    satisfied = serializers.SerializerMethodField()
    parent_info = serializers.SerializerMethodField()

    def get_parent_info(self, instance_model: QAInspectionModel):
        
        parent_info = {}
        answer_parents = QAInspectionAnswerModel.objects.filter(inspection=instance_model)

        for parent in answer_parents:
            
            parent_info[parent.indicator.ID] = parent.satisfied

        return parent_info
            
    def get_answers(self, instance_model : QAInspectionModel):
        
        answers = []
        answer_parents = QAInspectionAnswerModel.objects.filter(inspection=instance_model)

        for parent in answer_parents:

            children_list = []

            children = QAInspectionAnswerChildModel.objects.filter(parent=parent)

            for child in children:

                children_list.append({"id": child.ID, "indicator": child.indicator.ID, "result": child.satisfied})
            
            answers.append({'parent_indicator': parent.indicator.ID, 'children': children_list})

        return answers

    def get_satisfied(self, instance_model: QAInspectionModel):

        answer_parents = QAInspectionAnswerModel.objects.filter(inspection=instance_model)
        satisfied: bool = True
        for parent in answer_parents:
            if parent.satisfied == False: satisfied = False

        return satisfied

    def create(self, validated_data: Dict[str, Any]) -> QAInspectionAnswerModel:

        """
        
            This funtion validate and create instance

            :param self: TypifySerializer instance
            :type self: TypifySerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        result = super().create(validated_data)

        #Create parents
        answers = self.context['request'].data.get('answers_data')
        answers = json.loads(answers)
        for parent in answers:

            try:
                parent_indicator = QAIndicatorModel.objects.get(ID=parent['id_parent'])
            except:
                raise ValidationError({'parent': 'Invalid parent id'})
            parent_instance = QAInspectionAnswerModel.objects.create(
                indicator=parent_indicator,
                inspection=result,
                operator=result.operator,
                satisfied= True
            )

            for child in parent['children']:
                try:
                    indicator = QAIndicatorModel.objects.get(ID=child['id'])
                except:
                    raise ValidationError({'parent': 'Invalid parent id'})

                child_instance = QAInspectionAnswerChildModel.objects.create(
                    indicator=indicator,
                    satisfied=child['result'],
                    operator=result.operator,
                    parent=parent_instance
                )
                   
                if child_instance.satisfied != True and parent_instance.satisfied == True: 
                    parent_instance.satisfied = False
                    parent_instance.save()
        
        return result

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: QAInspectionSerializer instance
            :type self: QAInspectionSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """
        if "kind" in validated_data:
            raise ValidationError({"kind": "updating an inspection kind is not allowed"})

        return super().update(instance, validated_data)

    class Meta:

        #Class model of serializer
        model: BaseModel = QAInspectionModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class QAInspectionTablesSerializer(BaseSerializer):

    """

        Class define serializer of QAInspectionQuestionView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = QAInspectionTablesModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class QAInspectionAnswerChildSerializer(BaseSerializer):

    """

        Class define serializer of QAInspectionAnswerChildView.
    
    """

    def update(self, instance, validated_data: Dict[str, Any]):

        """
        
            This funtion validate and update instance

            :param self: QAInspectionAnswerChildSerializer instance
            :type self: QAInspectionAnswerChildSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        result = super().update(instance, validated_data)

        answer_parent = instance.parent

        children = QAInspectionAnswerChildModel.objects.filter(parent=answer_parent)
        satisfied: bool = True

        for child in children:
            
            if child.satisfied == False:
                satisfied = False 

        answer_parent.satisfied = satisfied
        answer_parent.save()

        return result

    class Meta:

        #Class model of serializer
        model: BaseModel = QAInspectionAnswerChildModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class QAMonitoringSerializer(BaseSerializer):

    """

        Class define serializer of QAMonitoringview.
    
    """

    sla = serializers.SerializerMethodField()

    def get_sla(self, instance_model : QAMonitoringModel ) -> float:

        closed_status_list = [1,2]
        return get_ticket_SLA(instance_model, closed_status_list, 'status')


    class Meta:

        #Class model of serializer
        model: BaseModel = QAMonitoringModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class QAMonitoringRecordSerializer(BaseSerializer):

    """

        Class define serializer of QAMonitoringview.
    
    """

    def create(self, validated_data: Dict[str, Any]) -> QAIndicatorModel:

        """
        
            This funtion validate and create instance

            :param self: QAIndicatorSerializer instance
            :type self: QAIndicatorSerializer
            :param validated_data: dict of instance data
            :type validated_data: dict
        
        """

        monitoring = validated_data["monitoring"]

        if monitoring.status != 0:
            raise ValidationError({'invalid monitoring': 'The monitoring instance has been closed'})

        return super().create(validated_data)

    class Meta:

        #Class model of serializer
        model: BaseModel = QAMonitoringRecordModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

