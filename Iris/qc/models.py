from datetime import datetime
from typing import Any, Callable, Dict, List, Set, Tuple, Union
import requests
from phonenumber_field.modelfields import PhoneNumberField
from common.models import BaseModel
from tickets.models import TypifyModel, CategoryModel
from common.utilitarian import (communications_url, get_token, iris_url,
                                matrix_url)
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import gettext_lazy as _
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import ValidationError

class QAIndicatorModel(BaseModel):

    """

        Class define model QAIndicator.
    
    """

    def clean(self):

        if self.parent == None and self.channel == None:
            raise ValidationError({'Invalid data': 'Every indicator must have a parent or a channel'})

    description: str = models.TextField(blank=False, null=False)
    parent = models.ForeignKey(
        'self',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    TYPE_CHOICES = (
        (0, 'Llamada'),
        (1, 'Canal Escrito'),
    )
    channel: int = models.PositiveSmallIntegerField(choices=TYPE_CHOICES, null=True, blank=True)

class QAChannelModel(BaseModel):

    """

        Class define model QAChannel.
    
    """
    
    name: str = models.TextField(blank=False, null=False)

class QAInspectionModel(BaseModel):

    """

        Class define model QAInspection.
    
    """

    def clean(self):
        
        if self.kind == 0:
            if self.agent_extension == None or self.agent_extension == '':
                raise ValidationError({'extension': 'A VoipNow extension is required'}) 
        else:
            self.agent_extension = None
            if self.accept_date == None or self.answer_date == None:
                raise ValidationError({'Missing data': 'accept_date and answer_date are required for this kind of inspections'}) 
            
    contact_date: datetime = models.DateTimeField()
    accept_date: datetime = models.DateTimeField(null=True, blank=True)
    answer_date: datetime = models.DateTimeField(null=True, blank=True)
    agent_extension: int = models.PositiveSmallIntegerField(null=True, blank=True)
    TYPE_CHOICES = (
        (0, 'Llamada'),
        (1, 'Canal Escrito'),
    )
    kind: int = models.PositiveSmallIntegerField(choices=TYPE_CHOICES)
    customer_phone: str = PhoneNumberField(null=True, blank=True)
    customer_email: str = models.EmailField(null=True, blank=True)
    customer_rut: List[int] = JSONField(default=list, null=True)
    channel =  models.ForeignKey(
        QAChannelModel,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
    )
    typify = models.ForeignKey(
        TypifyModel, 
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    services: List[int] = JSONField(default=list, null=True, blank=True)
    HIRING_CHOICES = (
        (0, 'NO'),
        (1, 'SÍ'),
        (2, 'N/A'),
    )
    service_hiring: int = models.SmallIntegerField(choices=HIRING_CHOICES)
    address: str = models.CharField(max_length=100)
    commentaries: str = models.TextField(null=True)
    agent = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )
    category = models.ForeignKey(
        CategoryModel, 
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    
class QAInspectionAnswerModel(BaseModel):

    """

        Class define model QAInspectionAnswer
    
    """

    indicator = models.ForeignKey(
        QAIndicatorModel, 
        null=False,
        blank=False,
        on_delete=models.PROTECT
    )
    satisfied = models.BooleanField()
    inspection = models.ForeignKey(
        QAInspectionModel, 
        null=False,
        blank=False,
        on_delete=models.PROTECT
    )

class QAInspectionAnswerChildModel(BaseModel):

    """

        Class define model QAInspectionAnswerChild.
    
    """

    satisfied = models.BooleanField()
    indicator = models.ForeignKey(
        QAIndicatorModel, 
        null=False,
        blank=False,
        on_delete=models.PROTECT
    )
    parent = models.ForeignKey(
        QAInspectionAnswerModel, 
        null=False,
        blank=False,
        on_delete=models.PROTECT
    )

class QAInspectionTablesModel(BaseModel):

    def clean(self):

        if (self.last_time_type != None and self.last_time == None) or (self.last_time_type == None and self.last_time != None):
            if self.last_time == None:
                raise ValidationError({
                    'last_time': ["This field is required."]
                })
            else:
                raise ValidationError({
                    'last_time_type': ["This field is required."]
                })

        def validation_filters(value: List[str]):

            def scheme_filter( _filter: List[str]) -> List[Any]:

                """
                    
                    This funtion, return validate filter or raise exception with errors

                """

                FIELDS_FILTERS: Dict[str, str] = {
                    "created":"datetime",
                    "creator__pk":"choices",
                    "category__pk":"choices",
                    "service_hiring":"choices",
                }

                TYPE_OF_FILTERS: Dict[str, List[str]] = {
                    "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
                    "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
                    "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
                    "bool": [ "equal", "different" ],
                    "choices": [ "equal", "different" ],
                    "location": [ "equal", "different"],
                }

                def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                    """
                
                        This funtion, return validated filter value or raise exception with errors

                    """

                    def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                        """
                
                            This funtion, return validated datetime value or raise exception with errors

                        """

                        if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                            result: int = int(value)

                            if comparison_filter == "year" and 2100 >= result <= 2000:

                                raise ValidationError({})

                            elif comparison_filter == "month" and 1 >= result <= 12:

                                raise ValidationError({})

                            elif comparison_filter == "day":

                                max_days: int = 366 if calendar( datetime.now().year ) else 365

                                if not 1 >= result <= max_days:

                                    raise ValidationError({})

                            elif comparison_filter == "week_day" and 0 >= result <= 6:

                                raise ValidationError({})

                            elif comparison_filter == "quarter" and 1 >= result <= 4:

                                raise ValidationError({})

                        else:

                            datetime.strptime(value, '%d/%m/%Y %H:%M')

                        return value

                    def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                        """
                        
                            This function, return validated bool value or raise exception with errors
                        
                        """

                        if not value in ["True", "False"]:

                            raise Exception("Invalid bool value")

                        else:

                            return True if value == "True" else False

                    def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated str value or raise exception with errors
                        
                        """

                        return str(value)

                    def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                        """
                        
                            This function, return validated int value or raise exception with errors
                        
                        """

                        return int(value)

                    def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                        """
                        
                            This function, return validated lcoation value or raise exception with errors
                        
                        """

                        result: List[str] = value.split(':')

                        if not ( len(result) == 2  and result[0] in ['region','commune','street'] and isinstance(result[1], int) ):

                            raise Exception("Invalid location value")

                    dict_validators: Dict[str, Callable] = {
                        "int":validate_int,
                        "str":validate_str,
                        "datetime":validate_datetime,
                        "bool":validate_bool,
                        "choices":validate_choices,
                        "location":validate_location,
                    }
                    
                    try:

                        value: Any = dict_validators[_type](value, _type, comparison_filter)

                    except Exception as e:

                        raise ValidationError({
                            "Invalid value type": f"Invalid value: {value}, for filter {name}",
                        })

                    return value

                if len(_filter) == 3:

                    name_filter: str = _filter[0]
                    comparison_filter: str = _filter[1]
                    value_filter: str = _filter[2]

                    #Get filter name or None
                    field_type: str = FIELDS_FILTERS.get(name_filter)

                    if field_type:

                        #Get filter comparison or None
                        comparisons: List[str] = TYPE_OF_FILTERS[field_type]

                        if comparison_filter in comparisons:

                            value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                        else:

                            raise ValidationError({
                                "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                            })

                    else:

                        raise ValidationError({
                            "Invalid filter name": f"Invalid filter name {name_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter": "Invalid filter length",
                    })

                return [field_type, comparison_filter, value_filter]

            results: List[ List[Any] ] = []

            for _filter in value:

                results.append( scheme_filter( _filter ) )

            return results


        validation_filters( self.filters )

        if not self.last_time_type:

            self.last_time = None

        elif not isinstance(self.last_time, int):

            raise ValidationError({
                'Invalid last_time': "Required int value"
            })

        valid_columns : Set[str] =  set( [
                                            'ID', 'agent_extension', 'created', 'contact_date', 
                                            'customer_phone', 'customer_email', 'channel',
                                            'customer_rut', 'services', 'creator', 'accept_date',
                                            'answer_date'
                                    ] )

        invalid_columns: List[str] = list( set( self.columns ) - valid_columns )

        if invalid_columns:

            raise ValidationError({
                'Invalid columns': ', '.join(invalid_columns)
            })

    filters: List[List[Union[str, int]]] = JSONField(default=list)
    assigned_to_me: bool = models.BooleanField(default=False)
    create_me: bool = models.BooleanField(default=False)
    
    LAST_TIME_TYPE_CHOICES = (
        (0, _('Minutos')),
        (1, _('Horas')),
        (2, _('Dias')),
        (3, _('Semanas')),
        (4, _('Meses')),
        (5, _('Trimestre')),
        (6, _('Año')),
    )

    last_time_type: int = models.PositiveIntegerField(choices=LAST_TIME_TYPE_CHOICES, null=True)
    last_time: int = models.PositiveIntegerField(null=True)

    columns: List[List[Union[str, int]]] = JSONField(default=list)

    name : str = models.CharField(max_length=100, blank=False)

class QAMonitoringModel(BaseModel):

    """

        Class define model QAMonitoring.
    
    """
    
    name: str = models.CharField(max_length=100, null=False, blank=False)
    
    STATUS_CHOICES: Tuple[Tuple[int, str]] = (
        (0, 'ABIERTO'),
        (1, 'CERRADO'),
        (2, 'CANCELADO')
    )
    status: int = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=0)

class QAMonitoringRecordModel(BaseModel):

    """

        Class define model QAMonitoringRecord.
    
    """

    def clean(self):
        
        if self.kind == 0:
            if self.agent_extension == None or self.agent_extension == '':
                raise ValidationError({'extension': 'A VoipNow extension is required'}) 

    contact_date: datetime = models.DateTimeField()
    accept_date: datetime = models.DateTimeField(null=True, blank=True)
    answer_date: datetime = models.DateTimeField(null=True, blank=True)
    agent_extension: int = models.PositiveSmallIntegerField(null=True, blank=True)
    customer_phone: str = PhoneNumberField(null=True, blank=True)
    customer_email: str = models.EmailField(null=True, blank=True)
    customer_rut: List[int] = JSONField(default=list, null=True)
    services: List[int] = JSONField(default=list, null=True, blank=True)
    address: str = models.CharField(max_length=100)
    commentaries: str = models.TextField(null=True)
    motive: str = models.TextField(null=True)
    category = models.ForeignKey(
        CategoryModel, 
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    monitoring = models.ForeignKey(
        QAMonitoringModel,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
    )
    agent = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )
    channel =  models.ForeignKey(
        QAChannelModel,
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    TYPE_CHOICES = (
        (0, 'Llamada'),
        (1, 'Canal Escrito'),
    )
    kind: int = models.PositiveSmallIntegerField(choices=TYPE_CHOICES)