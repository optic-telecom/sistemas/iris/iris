import threading
from typing import Any, Dict, List

from django.contrib.auth.models import User
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.viewsets import BaseViewSet
from django.http import JsonResponse
from rest_framework.decorators import action
from rest_framework.serializers import ValidationError
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required


from .datatables import (QAIndicatorDatatable, QAChannelDatatable, QAInspectionDatatable, QAInspectionTablesDatatable, QAMonitoringDatatable,
QAMonitoringRecordDatatable)
from .models import (QAIndicatorModel, QAChannelModel, QAInspectionModel, QAInspectionTablesModel, 
QAInspectionAnswerChildModel, QAMonitoringModel, QAMonitoringRecordModel)
from .serializers import (QAIndicatorSerializer, QAChannelSerializer, QAInspectionSerializer, 
                        QAInspectionTablesSerializer, QAInspectionAnswerChildSerializer, QAMonitoringSerializer, QAMonitoringRecordSerializer)
from .documentation import DocumentationQAIndicator

class QAIndicatorView(BaseViewSet):

    """

        Class define ViewSet of QAIndicatorModel.
    
    """

    queryset: BaseModel = QAIndicatorModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = QAIndicatorSerializer
    documentation_class = DocumentationQAIndicator()

    @method_decorator(permission_required('qc.delete_qaindicatormodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('qc.add_qaindicatormodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('qc.change_qaindicatormodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qaindicatormodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.view_qaindicatormodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAIndicatorView
            :type self: QAIndicatorView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(QAIndicatorDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('qc.view_qaindicatormodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAIndicatorView
            :type self: QAIndicatorView
            :param request: data of request
            :type request: request
        
        """

        return QAIndicatorDatatable(request).get_data()

class QAChannelView(BaseViewSet):

    """

        Class define ViewSet of QAIndicatorModel.
    
    """

    queryset: BaseModel = QAChannelModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = QAChannelSerializer

    @method_decorator(permission_required('qc.delete_qachannelmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('qc.add_qachannelmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qachannelmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qachannelmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.view_qachannelmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAIndicatorView
            :type self: QAIndicatorView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(QAChannelDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('qc.view_qaindicatormodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAIndicatorView
            :type self: QAIndicatorView
            :param request: data of request
            :type request: request
        
        """

        return QAChannelDatatable(request).get_data()

class QAInspectionView(BaseViewSet):

    """

        Class define ViewSet of QAInspectionModel.
    
    """

    queryset: BaseModel = QAInspectionModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = QAInspectionSerializer

    @method_decorator(permission_required('qc.delete_qainspectionmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('qc.add_qainspectionmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qainspectionmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qainspectionmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.view_qainspectionmodel',raise_exception=True))
    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAInspectionView
            :type self: QAInspectionView
            :param request: data of request
            :type request: request
        
        """
        inspection_datatables = QAInspectionDatatable(request)
        inspection_datatables.ID_TABLE = pk

        return JsonResponse(inspection_datatables.get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('qc.view_qainspectionmodel',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAInspectionView
            :type self: QAInspectionView
            :param request: data of request
            :type request: request
        
        """

        inspection_datatables = QAInspectionDatatable(request)
        inspection_datatables.ID_TABLE = pk

        return inspection_datatables.get_data()

class QAInspectionTablesView(BaseViewSet):

    """

        Class define ViewSet of QAInspectionTablesModel.
    
    """

    queryset: BaseModel = QAInspectionTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = QAInspectionTablesSerializer

    @method_decorator(permission_required('qc.delete_qainspectiontablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('qc.add_qainspectiontablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        
        return super().create(request, *args, **kwargs)
        
    @method_decorator(permission_required('qc.change_qainspectiontablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qainspectiontablesmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.view_qainspectiontablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def definition(self, request) -> JsonResponse:
        data_struct : Dict[str, Dict[str, str]] = {

            "comparisons": {
                "Igual": {
                    "name": "equal",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Año": {
                    "name": "year",
                    "type": ["int"]
                },
                "Mes": {
                    "name": "month",
                    "type": ["int"]
                },
                "Día": {
                    "name": "day",
                    "type": ["int"]
                },
                "Día de la semana": {
                    "name": "week_day",
                    "type": ["int"]
                },
                "Semana": {
                    "name": "week",
                    "type": ["int"]
                },
                "Trimestre": {
                    "name": "quarter",
                    "type": ["int"]
                },
                "Mayor": {
                    "name": "gt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Mayor o igual": {
                    "name": "gte",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor": {
                    "name": "lt",
                    "type": ["datetime", "str", "int", "bool"]
                },
                "Menor o igual": {
                    "name": "lte",
                    "type":"datetime"
                },
                "Distinto": {
                    "name": "different",
                    "type": ["datetime", "str", "int", "bool", "choices", "location"]
                },
                "Similar": {
                    "name": "iexact",
                    "type":"str"
                },
                "Contiene": {
                    "name": "icontains",
                    "type":"str"
                },
                "Inicia con": {
                    "name": "istartswith",
                    "type":"str"
                },
                "Termina con": {
                    "name": "iendswith",
                    "type":"str"
                }
            },
            "type_of_comparisons": {
                "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
                "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
                "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
                "bool": [ "Igual", "Distinto" ],
                "choices": [ "Igual", "Distinto" ],
                "location": [ "Igual", "Distinto" ]
            },
            "filters": {
                "Fecha de creación":{
                    "type":"datetime",
                    "name":"created",
                    "format":"%d/%m/%Y %H:%M",
                },
                "Categoría de Tipificaciones":{
                    "type":"choices",
                    "name":"category__pk",
                    "type_choices": "int",
                    "values": {
                        "type": "dinamic",
                        "extra_data":{
                            "url": "tickets/category/",
                            "read_data":{
                                "value":"ID",
                                "human_readable": "name"
                            }
                        }
                    }
                },
                "Venta":{
                    "type":"choices",
                    "name":"service_hiring",
                    "type_choices": "int",
                    "values": {
                        "type": "static",
                        "extra_data":{
                            0: "No",
                            1: "Sí",
                            2: "N/A"
                        }
                    }
                },
            },
            "columns":  {
                'created': "Fecha de creación",
                'creator': "Creador",
                'agent_extension': "Extensión VoipNow",
                'contact_date': "Fecha de Contacto",
                'accept_date': "Fecha de Aceptación",
                'answer_date': "Fecha de Respuesta",
                'customer_phone': "Teléfono Cliente",
                'customer_email': "Correo Cliente", 
                'customer_rut': "Rut Cliente",
                'services': "Servicios",
                'channel': "Canal"
            }
        }

        return JsonResponse(data_struct, safe=True)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('qc.view_qainspectiontablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAInspectionTablesView
            :type self: QAInspectionTablesView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(QAInspectionTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('qc.view_qainspectiontablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAInspectionTablesView
            :type self: QAInspectionTablesView
            :param request: data of request
            :type request: request
        
        """

        return QAInspectionTablesDatatable(request).get_data()

class QAInspectionAnswerChildView(BaseViewSet):

    """

        Class define ViewSet of QAInspectionAnswerChildModel.
    
    """

    queryset: BaseModel = QAInspectionAnswerChildModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = QAInspectionAnswerChildSerializer

    @method_decorator(permission_required('qc.delete_qainspectionanswerchildmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('qc.add_qainspectionanswerchildmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qainspectionanswerchildmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qainspectionanswerchildmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

class QAMonitoringView(BaseViewSet):

    """

        Class define ViewSet of QAInspectionAnswerChildModel.
    
    """

    queryset: BaseModel = QAMonitoringModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = QAMonitoringSerializer

    @method_decorator(permission_required('qc.delete_qamonitoringmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('qc.add_qamonitoringmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qamonitoringmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qamonitoringmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.view_qamonitoringmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAMonitoringView
            :type self: QAMonitoringView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(QAMonitoringDatatable(request).get_struct(), safe=True)

    @method_decorator(permission_required('qc.view_qamonitoringmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAMonitoringView
            :type self: QAMonitoringView
            :param request: data of request
            :type request: request
        
        """

        return QAMonitoringDatatable(request).get_data()

class QAMonitoringRecordView(BaseViewSet):

    """

        Class define ViewSet of QAMonitoringRecordModel.
    
    """

    queryset: BaseModel = QAMonitoringRecordModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = QAMonitoringRecordSerializer

    @method_decorator(permission_required('qc.delete_qamonitoringrecordmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @method_decorator(permission_required('qc.add_qamonitoringrecordmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qamonitoringrecordmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.change_qamonitoringrecordmodel',raise_exception=True))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @method_decorator(permission_required('qc.view_qachannelmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAIndicatorView
            :type self: QAIndicatorView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(QAMonitoringRecordDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('qc.view_qainspectionmode',raise_exception=True))
    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class QAInspectionView
            :type self: QAInspectionView
            :param request: data of request
            :type request: request
        
        """

        monitoring_record_table = QAMonitoringRecordDatatable(request)
        monitoring_record_table.MONITORING = pk

        return monitoring_record_table.get_data()