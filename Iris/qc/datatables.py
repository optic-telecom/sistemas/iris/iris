from typing import List, Dict, Any, Union
from functools import reduce

from django.db.models import Q
from django.db.models.query import QuerySet
from dateutil.relativedelta import relativedelta
from common.dataTables import BaseDatatables
from django.http import JsonResponse
from common.models import BaseModel
from .models import (QAIndicatorModel, QAChannelModel, QAInspectionModel, QAInspectionTablesModel, QAMonitoringModel,
QAMonitoringRecordModel)
from .serializers import QAInspectionSerializer,QAMonitoringRecordSerializer

class QAIndicatorDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = QAIndicatorModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(parent__description__icontains=search) | Q(description__icontains=search))

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "description": instance.description,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "parent": instance.parent.description if instance.parent else '',
            "channel": instance.get_channel_display()
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "template": "str",

        }

        data_struct["columns"] = {

            "ID":{
                "field": "ID",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Descripción":{
                "field": "description",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":150,
                "fixed":None
            },
            "Padre":{
                "field": "parent",
                "sortable": True,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Canal":{
                "field": "channel",
                "sortable": True,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 6,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Descripción":{
                "type":"str",
                "name":"description"
            },
            "Padre":{
                "type":"choices",
                "name":"parent__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "qc/indicator/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "description"
                        }
                    }
                }
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class QAChannelDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = QAChannelModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search) )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "template": "str",

        }

        data_struct["columns"] = {

            "ID":{
                "field": "ID",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class QAInspectionTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator"]


    model: BaseModel = QAInspectionTablesModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( name__icontains=search )

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {}

        return data_struct

class QAInspectionDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = [
        "ID", "created", "creator", "agent_extension", "contact_date", "customer_rut", 
        "services", "accept_date", "answer_date"
    ]

    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "creator__pk":"choices",
        "customer_email":"str",
        "customer_phone":"str",
        "agent_extension":"int",
    }

    model: BaseModel = QAInspectionModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            if search.isdigit():

                int_value: int = int(search)
                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: int_value in x.services, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(ID=int_value) | 
                    Q(id__in=list_typify_id)
                )
            
            else:

                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: search in x.customer_rut, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(agent_extension__icontains=search) | Q(ID__in=list_typify_id)
                )

        return qs

    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        instance_tables_inspection: QuerySet = QAInspectionTablesModel.objects.get(ID=self.ID_TABLE)

        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + instance_tables_inspection.filters

        data_inspection: QuerySet = super().get_filtered_queryset()

        #Tickets created to request user
        if instance_tables_inspection.create_me:

            data_inspection: QuerySet = data_inspection.filter(creator=self.request.user)

        #Tickets time interval
        if instance_tables_inspection.last_time_type and instance_tables_inspection.last_time:

            TYPE_TIMEDELTA : Dict[str, timedelta] = {
                0: relativedelta(minutes=1),
                1: relativedelta(hours=1),
                2: relativedelta(days=1),
                3: relativedelta(weeks=1),
                4: relativedelta(months=1),
                5: relativedelta(months=3),
                6: relativedelta(years=1),
            }

            data_inspection: QuerySet = data_inspection.filter( created__gte=datetime.now() - (TYPE_TIMEDELTA[instance_tables_inspection.last_time_type] * instance_tables_inspection.last_time) )

        return data_inspection

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        result: Dict[str, Union[str, int, float]] = QAInspectionSerializer(instance).data

        result["creator"]: str = instance.creator.username
        result["updater"]: str = instance.updater.username if instance.updater else ''
        result["kind"]: str = instance.get_kind_display()
        result["service_hiring"]: str = instance.get_service_hiring_display()
        result["channel"]: str = instance.channel.name

        return result

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        instance_tables_inspection: QuerySet = QAInspectionTablesModel.objects.get(ID=self.ID_TABLE)

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "download":False
        }

        data_struct["fields"] = {
            "ID": "int",
            "created": "datetime",
            "updated": "datetime",
            "contact_date": "datetime",
            "operator": "int",
            "services": "list",
            "customer_rut": "str",
            "customer_phone": "str",
            "customer_email": "str",
            "creator": "str",
            "updater": "str",
            "service_hiring": "bool",
            "agent_extension": "int"
        }

        #Init column definition#
        ########################

        columns: Dict[str, Dict[str, Any]] = {
            'creator':{
                "sortable": True,
                "name":"Creador",
                "width":100,
                "fixed":None
            },
            'created':{
                "sortable": True,
                "name":"Creado",
                "width":100,
                "fixed":None
            },
            'ID':{
                "sortable": True,
                "name":"ID",
                "width":100,
                "fixed":None
            },
            'agent_extension':{
                "sortable": True,
                "name":"Extensión VoipNow",
                "width":100,
                "fixed":None
            },
            'contact_date':{
                "sortable": True,
                "name":"Fecha de Contacto",
                "width":100,
                "fixed":None
            },
            'accept_date':{
                "sortable": True,
                "name":"Fecha de Aceptación",
                "width":100,
                "fixed":None
            },
            'answer_date':{
                "sortable": True,
                "name":"Fecha de Respuesta",
                "width":100,
                "fixed":None
            },
            'customer_phone':{
                "sortable": False,
                "name":"Teléfono Cliente",
                "width":200,
                "fixed":None
            }, 
            'customer_email':{
                "sortable": False,
                "name":"Correo Cliente",
                "width":100,
                "fixed":None
            },
            'customer_rut':{
                "sortable": True,
                "name":"Rut Cliente",
                "width":100,
                "fixed":None
            },
            'services':{
                "sortable": True,
                "name":"Servicios",
                "width":100,
                "fixed":None
            },    
            'channel':{
                "sortable": True,
                "name":"Canal",
                "width":100,
                "fixed":None
            },   
        }

        data_struct_columns: Dict[str, Dict[str, Dict[str, Any]]] = {}
        
        data_struct_columns["ID"] = {
            "field": "ID",
            "sortable": True,
            "visible": True,
            "position": 0,
            "width":100,
        }

        position: int = 1

        for _column in instance_tables_inspection.columns:

            data_struct_columns[columns[_column]['name']] = {
                "field": _column,
                "sortable": columns[_column]['sortable'],
                "visible": True,
                "position": position,
                "width":columns[_column]['width'],
                "fixed":columns[_column]['fixed']
            }

            position += 1

        data_struct_columns["Modificar"] = {
            "field": "custom_change",
            "sortable": False,
            "visible": True,
            "position": position,
            "width":100,
            "fixed":"right",
            "download": True,
        }

        data_struct_columns["Eliminar"] = {
            "field": "custom_delete",
            "sortable": False,
            "visible": True,
            "position": position+1,
            "width":100,
            "fixed":"right",
            "download": True,
        }

        data_struct["columns"] = data_struct_columns

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll": reduce(lambda x, y: x + y, [ column["width"] for column in  data_struct_columns.values()] ) + 100
        }

        #End  column definition#
        ########################

        #Init filters definition#
        #########################

        filters: Dict[str, Dict[str, Any]] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
             "Teléfono Cliente":{
                "type":"str",
                "name":"customer_phone",
            },
             "Correo Cliente":{
                "type":"str",
                "name":"customer_email",
            },
            "Extensión VoipNow":{
                "type":"int",
                "name":"agent_extension",
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "int",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
        }

        filters_names : Dict[str, str] = {
            'created': ["Fecha de inicio", "Fecha de fin"],
            'agent_extension':["Extensión VoipNow"],
            'creator__pk':["Creador"],
        }

        for _filter in instance_tables_inspection.filters:

            for _field in filters_names[_filter[0]]:

                del filters[_field]

        data_struct["filters"] = filters

        #End filters definition#
        ########################

        return data_struct

class QAMonitoringDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["ID", "name", "created", "creator", "status"]
    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "created":"datetime",
        "name":"str",
        "creator__pk":"choices",
    }

    model: BaseModel = QAMonitoringModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        qs: QuerySet = qs.filter( Q(name__icontains=search))

        if search.isdigit():

            qs: QuerySet = qs.filter( ID=int(search) )

        return qs

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "status": instance.get_status_display(),
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"ID",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "template": "str",

        }

        data_struct["columns"] = {

            "ID":{
                "field": "ID",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Estado":{
                "field": "status",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Registro":{
                "field": "add_record",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Estado":{
                "type":"choices",
                "name":"status",
                "type_choices": "int",
                "values": {
                    "type": "static",
                    "extra_data":{
                        0: "Abierto",
                        1: "Cerrado",
                        2: "Cancelado"
                    }
                }
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct

class QAMonitoringRecordDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = [
        "ID", "created", "creator","contact_date", "services", "accept_date", "answer_date"
    ]

    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "creator__pk":"choices",
        "monitoring__pk": "choices",
        "customer_phone": "str",
        "customer_email": "str",
    }

    model: BaseModel = QAMonitoringRecordModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        if search:

            if search.isdigit():

                int_value: int = int(search)
                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: int_value in x.services, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(ID=int_value) | 
                    Q(id__in=list_typify_id)
                )

            else:

                list_typify_id: List[int] = list( map( lambda x: x.ID, filter( lambda x: search in x.customer_rut, qs ) ) )

                qs: QuerySet = qs.filter( 
                    Q(agent_extension__icontains=search) |
                    Q(ID__in=list_typify_id)
                )

        return qs

    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """
        
        #Add filters
        self.REQUEST_FILTERS = self.REQUEST_FILTERS + [["monitoring__pk","equal",self.MONITORING]]

        data_inspection: QuerySet = super().get_filtered_queryset()


        return data_inspection

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        result: Dict[str, Union[str, int, float]] = QAMonitoringRecordSerializer(instance).data

        result["creator"]: str = instance.creator.username
        result["updater"]: str = instance.updater.username if instance.updater else ''
        result["monitoring"]: str = instance.monitoring.name

        return result

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"created",
            "order_type":"asc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",

        }

        data_struct["columns"] = {

            "ID":{
                "field": "ID",
                "sortable": True,
                "visible": True,
                "position": 0,
                "width":100,
                "fixed":None
            },
            "Fecha de Creación":{
                "field": "created",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Creador":{
                "field": "creator",
                "sortable": True,
                "visible": True,
                "position": 2,
                "width":100,
                "fixed":None
            },
            "Fecha de Contacto":{
                "field": "contact_date",
                "sortable": True,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Fecha de Aceptación":{
                "field": "accept_date",
                "sortable": True,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":None
            },
            "Fecha de Respuesta":{
                "field": "answer_date",
                "sortable": True,
                "visible": True,
                "position": 5,
                "width":100,
                "fixed":None
            },
            "Extensión VoipNow":{
                "field": "agent_extension",
                "sortable": False,
                "visible": True,
                "position": 6,
                "width":100,
                "fixed":None
            },
            "Teléfono del Cliente":{
                "field": "customer_phone",
                "sortable": False,
                "visible": True,
                "position": 7,
                "width":100,
                "fixed":None
            },
            "Email del Cliente":{
                "field": "customer_email",
                "sortable": False,
                "visible": True,
                "position": 8,
                "width":100,
                "fixed":None
            },
            "Rut del Cliente":{
                "field": "customer_rut",
                "sortable": False,
                "visible": True,
                "position": 9,
                "width":100,
                "fixed":None
            },
            "Servicios":{
                "field": "services",
                "sortable": True,
                "visible": True,
                "position": 10,
                "width":100,
                "fixed":None
            },
            "Motivo":{
                "field": "motive",
                "sortable": False,
                "visible": True,
                "position": 11,
                "width":100,
                "fixed":None
            },
            "Categoría":{
                "field": "category",
                "sortable": True,
                "visible": True,
                "position": 12,
                "width":100,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 13,
                "width":100,
                "fixed":"right"
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 14,
                "width":100,
                "fixed":"right"
            },

        }

        data_struct["filters"] = {

            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Motivo":{
                "type":"str",
                "name":"motive"
            },
            "Teléfono Cliente":{
                "type":"str",
                "name":"customer_phone"
            },
            "Correo Cliente":{
                "type":"str",
                "name":"customer_email"
            },
            "Categoría":{
                "type":"choices",
                "name":"category__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "tickets/category/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            },
            "Creador":{
                "type":"choices",
                "name":"creator__pk",
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        }

        return data_struct