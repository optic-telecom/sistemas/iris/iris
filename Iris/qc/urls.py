from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import (QAIndicatorView, QAChannelView, QAInspectionView, QAInspectionTablesView, 
QAInspectionAnswerChildView, QAMonitoringView, QAMonitoringRecordView)

router = DefaultRouter()

router.register(r'indicator', QAIndicatorView, basename='indicator')
router.register(r'qa_channel', QAChannelView, basename='qa_channel')
router.register(r'inspection', QAInspectionView, basename='inspection')
router.register(r'inspection_tables', QAInspectionTablesView, basename='inspection_tables')
router.register(r'inspection_answer', QAInspectionAnswerChildView, basename='inspection_answer')
router.register(r'monitoring', QAMonitoringView, basename='monitoring')
router.register(r'monitoring_record', QAMonitoringRecordView, basename='monitoring_record')

urlpatterns = router.urls
 