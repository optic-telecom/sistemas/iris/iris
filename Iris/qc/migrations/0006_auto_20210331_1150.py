# Generated by Django 2.1.7 on 2021-03-31 15:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('qc', '0005_auto_20210324_1533'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalqainspectionmodel',
            name='channel',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='qc.QAChannelModel'),
        ),
        migrations.AddField(
            model_name='qainspectionmodel',
            name='channel',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='qc.QAChannelModel'),
            preserve_default=False,
        ),
    ]
