# Generated by Django 3.2.2 on 2021-05-31 16:07

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('qc', '0019_auto_20210506_0837'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalqamonitoringrecordmodel',
            name='accept_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='historicalqamonitoringrecordmodel',
            name='agent',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalqamonitoringrecordmodel',
            name='answer_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='historicalqamonitoringrecordmodel',
            name='channel',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='qc.qachannelmodel'),
        ),
        migrations.AddField(
            model_name='historicalqamonitoringrecordmodel',
            name='kind',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Llamada'), (1, 'Canal Escrito')], default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='qamonitoringrecordmodel',
            name='accept_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='qamonitoringrecordmodel',
            name='agent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='qamonitoringrecordmodel',
            name='answer_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='qamonitoringrecordmodel',
            name='channel',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='qc.qachannelmodel'),
        ),
        migrations.AddField(
            model_name='qamonitoringrecordmodel',
            name='kind',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Llamada'), (1, 'Canal Escrito')], default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='historicalqamonitoringrecordmodel',
            name='customer_email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AlterField(
            model_name='historicalqamonitoringrecordmodel',
            name='customer_phone',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None),
        ),
        migrations.AlterField(
            model_name='qamonitoringrecordmodel',
            name='customer_email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AlterField(
            model_name='qamonitoringrecordmodel',
            name='customer_phone',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None),
        ),
    ]
