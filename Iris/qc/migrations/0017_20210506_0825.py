# Generated by Django 2.2 on 2021-04-30 12:36

import django.contrib.postgres.fields.jsonb
from django.db import migrations
from django.db.models import Q

def rut_str_to_list(apps, scheme_editor):
    
    InspectionModel = apps.get_model('qc', 'QAInspectionModel')

    for inspection in InspectionModel.objects.all():
        if inspection.customer_rut != None:
            inspection.rut_list = [inspection.customer_rut]
            inspection.save()

    MonitoringRecordModel = apps.get_model('qc', 'QAMonitoringRecordModel')

    for monitoring_record in MonitoringRecordModel.objects.all():
        if monitoring_record.customer_rut != None:
            monitoring_record.rut_list = [monitoring_record.customer_rut]
            monitoring_record.save()

def rut_list_to_str(apps, scheme_editor):

    InspectionModel = apps.get_model('qc', 'QAInspectionModel')

    for inspection in InspectionModel.objects.all():
        try:
            inspection.customer_rut = inspection.rut_list[0]
            inspection.save()
        except: 
            pass
    
    MonitoringRecordModel = apps.get_model('qc', 'QAMonitoringRecordModel')

    for monitoring_record in MonitoringRecordModel.objects.all():
        try:
            monitoring_record.customer_rut = monitoring_record.rut_list[0]
            monitoring_record.save()
        except: 
            pass

class Migration(migrations.Migration):

    dependencies = [
        ('qc', '0016_auto_20210506_0825'),
    ]

    operations = [
        migrations.RunPython(rut_str_to_list, rut_list_to_str),
    ]
