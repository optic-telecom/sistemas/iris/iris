from rest_framework.routers import DefaultRouter

from .views import (WebhookTableView)

router = DefaultRouter()

router.register(r'webhook_tables', WebhookTableView, basename='webhook_tables')

urlpatterns = router.urls
