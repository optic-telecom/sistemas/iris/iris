from django.db import models

# Create your models here.
from datetime import datetime
from typing import Any, Callable, Dict, List, Set, Tuple, Union
from django.utils.translation import gettext_lazy as _
from common.models import BaseModel
from django.contrib.auth.models import User, Group
from django.contrib.postgres.fields import JSONField
from django.db import models
from rest_framework.serializers import ValidationError

class WebhookTablesModel(BaseModel):

    name : str = models.CharField(max_length=100, blank=False)
    hook_title: str = models.TextField(blank=False)