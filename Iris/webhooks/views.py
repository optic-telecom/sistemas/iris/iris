import threading
from typing import Any, Dict, List

from django.contrib.auth.models import User, Group
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.viewsets import BaseViewSet
from django.http import JsonResponse, HttpResponseForbidden
from rest_framework.decorators import action
from rest_framework.serializers import ValidationError
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from dynamic_preferences.registries import global_preferences_registry

from .models import WebhookTablesModel
from .serializers import WebhookTablesSerializer
from .datatables import WebhookTablesDatatable

class WebhookTableView(BaseViewSet):

    """

        Class define ViewSet of TicketTablesModel.
    
    """

    queryset: BaseModel = WebhookTablesModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = WebhookTablesSerializer
    
    @method_decorator(permission_required('webhooks.add_webhooktablesmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(permission_required('webhooks.change_webhooktablesmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(permission_required('webhooks.delete_webhooktablesmodel',raise_exception=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    #@documentation_class.documentation_datatables_struct()
    @method_decorator(permission_required('webhooks.add_webhooktablesmodel',raise_exception=True))
    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketTableView
            :type self: TicketTableView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(WebhookTablesDatatable(request).get_struct(), safe=True)

    #@documentation_class.documentation_datatables()
    @method_decorator(permission_required('webhooks.add_webhooktablesmodel',raise_exception=True))
    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TicketTableView
            :type self: TicketTableView
            :param request: data of request
            :type request: request
        
        """

        return WebhookTablesDatatable(request).get_data()