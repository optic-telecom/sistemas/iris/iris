from typing import Any, Dict, List, Union

from common.models import BaseModel
from common.serializers import BaseSerializer
from django.contrib.auth.models import User, Group
from django.db.models.query import QuerySet
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import (SerializerMethodField,ValidationError)
from rest_framework import serializers

from .models import (WebhookTablesModel)

class WebhookTablesSerializer(BaseSerializer):

    """

        Class define serializer of WebhookTablesView.
    
    """

    filters = SerializerMethodField()
    
    def get_filters(self,instance_model: WebhookTablesModel) -> str:
        return []

    class Meta:

        #Class model of serializer
        model: BaseModel = WebhookTablesModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {
                'required': False
            },
            'updater': {
                'required': False
            }
        }