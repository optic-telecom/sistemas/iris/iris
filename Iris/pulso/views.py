from typing import Any, Dict, List

import requests

from django.http import JsonResponse
from dynamic_preferences.registries import global_preferences_registry


def proxy(request) -> JsonResponse:

    """

        This function return information of pulso request

        :param request: Values of user request
        :type request: request

        :returns: Response data user
        :rtype: JsonResponse
    
    """

    global_preferences: Dict[str, Any] = global_preferences_registry.manager()

    origin_url: str = request.build_absolute_uri().split('/api/v1/pulso/')
    url: str = global_preferences['pulso__domain_url'] + '/api/v1/' + origin_url[1]

    headers: Dict[str, str] = {
        'AUTHORIZATION': global_preferences['matrix__matrix_token'],
        'content-type': 'application/json'
    }

    result: List[Dict[str, Any]] = []
    status_code: int = 500

    if request.method == 'GET':

        response = requests.get(
            url=url,
            headers=headers,
            verify=False
        )

        status_code: int = response.status_code

        if response.status_code == 200:
            
            result: List[Dict[str, Any]] = response.json()

    else:

        if request.method == 'POST':

            response = requests.post(
                url=url,
                headers=headers,
                verify=False,
                data=request.body.decode('utf-8')
            )

            status_code: int = response.status_code

            try:

                result: List[Dict[str, Any]] = response.json()

            except:

                pass

    return JsonResponse(data=result, safe=False, status=status_code)
