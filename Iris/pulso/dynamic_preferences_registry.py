from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference

general = Section('pulso')


@global_preferences_registry.register
class StaticToken(StringPreference):

    section = general
    name: str = 'domain_url'
    default: int = "http://pulso.multifiber.cl"
    required: bool = True

@global_preferences_registry.register
class PulsoToken(StringPreference):

    section = general
    name: str = 'pulso_check_token'
    default: str = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    required: bool = True