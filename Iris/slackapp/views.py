import os
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from django.http import JsonResponse
from typing import List, Dict, Any
from dynamic_preferences.registries import global_preferences_registry
import requests
from slack import WebClient
from operators.models import OperatorModel

def proxy(request) -> JsonResponse:

    """

        This function returns information of slack request

        :param request: Values of user request
        :type request: request

        :returns: Response data user
        :rtype: JsonResponse
    
    """

    global_preferences: Dict[str, any] = global_preferences_registry.manager()

    origin_url: str = request.build_absolute_uri().split('/api/v1/slackapp/')[1].split('?')
    url: str = global_preferences['slackapp__slack_domain'] + origin_url[0]

    operator_instance = OperatorModel.objects.get(pk=int(request.GET['operator']))
    token = operator_instance.slack_token

    if request.GET.get("support",False) != False:
        token: str =  global_preferences["tech_support__slack_bot_token"]

    request_get = request.GET.copy()
    request_get.pop('operator',None)
    request_get.pop('support',None)

    params: Dict[str, str] = {}

    for param in request_get:
        params[param] = request_get[param]

    result: List[Dict[str, Any]] = []
    status_code: int = 500

    if request.method == 'GET':

        response = requests.get(
            url=url,
            verify=False,
            params=params,
            headers={'Authorization': 'Bearer ' + token}
        )

        status_code: int = response.status_code

        if response.status_code == 200:
            
            response = response.json()

            result: List[Dict[str, Any]] = response

        else:
            
            result: List[Dict[str, Any]] = []

    else:

        data = {}
        
        for item in request.POST:
            data[item] = request.POST[item]

        data.pop('operator',None)

        if 'files.upload' in url:

            if len(request._files) < 1:
                result = {"Files": "No files were provided"}

            for data_file in request._files.values():

                path = default_storage.save('tmp/' + data_file.name, ContentFile(data_file.read()))
                tmp_file = os.path.join(settings.MEDIA_ROOT, path)

                client = WebClient(token=token)

                response = client.files_upload(
                    channels=params['channel'],
                    initial_comment=params.get('text',''),
                    thread_ts=params.get('thread_ts',''),
                    file=tmp_file,
                )

                result = response.data
                status_code = 200 if result['ok'] else 400

        else:

            response = requests.post(
                url=url,
                params=params,
                json=data,
                verify=False,
            )

            status_code: int = response.status_code

        if  status_code == 200:

            if result == []:
                response = response.json()

                result: List[Dict[str, Any]] = response


    return JsonResponse(data=result, safe=False, status=status_code)
