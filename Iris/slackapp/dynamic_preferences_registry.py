from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference, IntegerPreference, ChoicePreference

general = Section('slackapp')

@global_preferences_registry.register
class SiteTitle(StringPreference):

    section = general
    name: str = 'slack_domain'
    default: str = 'https://slack.com/api/'
    required: bool = True