from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import (obtain_jwt_token, refresh_jwt_token,
                                      verify_jwt_token)

from .views import UserView, user_recover, UserDataView

router = DefaultRouter()
router.register(r'data', UserView, basename='user')
router.register(r'user_description', UserDataView, basename='user_description')

urlpatterns = [
    path(
        route='token/auth/',
        view=obtain_jwt_token,
        name='get_token',
    ),
    path(
        route='token/refresh/',
        view=refresh_jwt_token,
        name='refresh_token',
    ),
    path(
        route='token/verify/',
        view=verify_jwt_token,
        name='verify_token',
    ),
    path(
        route='user_recover/',
        view=user_recover,
        name='user_recover',
    ),
] + router.urls
