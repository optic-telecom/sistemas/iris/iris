import json
import random
import string
from json import JSONEncoder
from typing import Any, Dict, List

import requests
from common.models import BaseModel
from common.serializers import BaseSerializer
from common.utilitarian import communications_url, get_token
from common.viewsets import BaseViewSet
from communications.api import create_email
from django.contrib.auth.models import Permission, User
from django.db import models
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.serializers import ModelSerializer, ValidationError
from validate_email import validate_email
from operators.models import OperatorModel

from .models import UserDataModel, UserSlackIDModel
from .serializers import UserDataSerializer, UserSerializer


class UserView(viewsets.ModelViewSet):

    """

        Class define ViewSet of User.
    
    """

    queryset: models = User.objects.all()
    serializer_class: ModelSerializer = UserSerializer

    @action(detail=False, methods=['get'])
    def information(self, request) -> JsonResponse:

        """
        
            This funtion return users data

            :param self: Instance of Class UserView
            :type self: UserView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse([ {'ID':item.pk, 'name':item.username} for item in self.queryset], safe=False)

    @action(detail=False, methods=['get'])
    def information_group(self, request) -> JsonResponse:

        """
        
            This funtion return users data

            :param self: Instance of Class UserView
            :type self: UserView
            :param request: data of request
            :type request: request
        
        """

        group_name: str = request.GET.get("group_name","")

        return JsonResponse([ {'ID':item.pk, 'name':item.username} for item in self.queryset.filter(groups__name__icontains=group_name)], safe=False)

    @action(detail=False, methods=['get'])
    def user_information(self, request) -> JsonResponse:

        user: User = request.user

        return JsonResponse({
            'ID':user.pk,
            'username':user.username,
            'groups':[group[0] for group in user.groups.values_list('name')],
            },
            safe=False
        )

    @action(detail=False, methods=['get'])
    def user_permissions(self, request) -> JsonResponse:

        message: List[str] = list(request.user.get_all_permissions())
        status: int = 200
        
        return JsonResponse(message, safe=False, status=status)

@require_http_methods(["POST"])
def user_recover(request) -> JsonResponse:

    sesion = requests.Session()
    sesion.headers.update({"Authorization": get_token()})

    #Create random key
    password: str = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))

    username: str = request.POST.get('username', None)

    message: Dict[str, str] = {}

    if username == None:

        message: Dict[str, str] = {
            'Invalid user': 'Void username params'
        }
        status: int = 500

    else:

        agent: User = User.objects.filter(username=username).first()
        message : Dict[str, str] = {}

        if not agent:

            message: Dict[str, str] = {
                'Invalid user': 'Invalid user'
            }
            status: int = 500
            
        else:
        

            data: Dict[str, Any]  = {
                "subject": 'Usuario de Iris',
                "receiver": username,
                "sender": 'IrisSystem@optic.cl',
                "message": f'Su nueva clave en Iris es: {password}',
                "operator": OperatorModel.objects.get(ID=1)
            }

            create_email(data, agent, {}, False)

            agent.set_password(password)
            agent.save()
            status: int = 200

    return JsonResponse({}, safe=False, status=status)

class UserDataView(BaseViewSet):

    """

        Class define ViewSet of UserDataModel.
    
    """

    queryset: BaseModel = UserDataModel.objects.filter(deleted=False)
    serializer_class: BaseSerializer = UserDataSerializer

    @action(detail=False, methods=['get'])
    def voip_extentions(self, request):

        data: List[Dict[str, Any]] = []

        for description in UserDataModel.objects.filter(deleted=False):
            
            for extention in description.voip_extentions:

                data.append({"extention": extention, "username": description.user.username})
            
        return JsonResponse(data={'data': data})

    @action(detail=False, methods=['get'])
    def slack_uids(self, request):

        data: List[Dict[str, Any]] = []

        for slack_id in UserSlackIDModel.objects.filter(deleted=False):

            data.append({slack_id.uid: slack_id.user.username})
            
        return JsonResponse(data={'data': data})

    @action(detail=True, methods=['get'])
    def user_system_groups(self, request, pk):

        groups: List[Dict[str, Any]] = []

        user = User.objects.get(pk=pk)

        for group in user.groups.all().filter(name__startswith="Sistema"):

            groups.append(group.name)
            
        return JsonResponse(data={'groups': groups})
