from typing import List

from django.contrib import admin

from .models import UserDataModel, UserSlackIDModel

class UserDataAdmin(admin.ModelAdmin):

    fields: List[str] = ["voip_extentions", "user"]
    list_display: List[str] = ["voip_extentions", "user"]
    search_fields: List[str] = ["voip_extentions"]
    ordering: List[str] = ["user"]

class UserSlackIDAdmin(admin.ModelAdmin):

    fields: List[str] = ["user","uid","operator"]
    list_display: List[str] = ["user","uid"]
    search_fields: List[str] = ["uid"]
    ordering: List[str] = ["user"]

admin.site.register(UserDataModel, UserDataAdmin)
admin.site.register(UserSlackIDModel, UserSlackIDAdmin)