

import pytest
import requests
from django.contrib.auth.models import User, Group
from django.urls import reverse
from typing import Any, Dict, List
import json
from dynamic_preferences.registries import global_preferences_registry
from django.test.client import RequestFactory
from users.permissions import Permission, Authentication
from users.views import UserView 


@pytest.mark.django_db
class TestUserApi():

    def reverse_url(self, url: str, args: List[Any]):

        try:

            url: str = reverse(url, args=args)

        except:

            assert False

        return url

    def test_user_information(self, admin_client):

        url_base: str = self.reverse_url('users:user-list', []) + 'information/'

        response = admin_client.get( 
            path=url_base,
            content_type='application/json',
        )

        assert len(User.objects.all()) == len(response.json())

    def test_user_information(self, admin_client):

        url_base: str = self.reverse_url('users:user-list', []) + 'information_group/'

        test_group: Group = Group.objects.create(name="test")

        test_user: User = User.objects.first()
        test_user.groups.add(test_group)

        data = {
            "group_name":"test"
        }

        response = admin_client.get( 
            path=url_base,
            params=data,
            content_type='application/json',
        )

        assert 1 == len(response.json())

    def test_user_information(self, admin_client):

        url_base: str = self.reverse_url('users:user-list', []) + 'user_information/'

        test_group: Group = Group.objects.create(name="test")

        test_user: User = User.objects.first()
        test_user.groups.add(test_group)

        data = {
            'ID':test_user.pk,
            'username':test_user.username,
            'groups':['test'],
        }

        response = admin_client.get( 
            path=url_base,
            content_type='application/json',
        )

        assert data == response.json()

    def test_user_permissions(self, admin_client):

        url_base: str = self.reverse_url('users:user-list', []) + 'user_permissions/'

        test_user: User = User.objects.first()

        response = admin_client.get( 
            path=url_base,
            content_type='application/json',
        )

        assert list(test_user.get_all_permissions()) == response.json()

@pytest.mark.django_db
class TestPermission():

    def test_autentication(self):

        global_preferences = global_preferences_registry.manager()

        request = RequestFactory()
        get_request = request.get('/api/v1/formerCustomers/remove_equipament/')

        token: str = global_preferences['users__API_token']

        get_request.META['HTTP_AUTHORIZATION'] = token

        user = User.objects.create(username='api@api.com')

        autentication = Authentication()
        assert autentication.authenticate(get_request)



@pytest.mark.django_db
class TestAuthentication():

    def test_autentication(self):

        global_preferences = global_preferences_registry.manager()

        request = RequestFactory()
        get_request = request.get('/api/v1/formerCustomers/remove_equipament/')

        token: str = global_preferences['users__API_token']

        get_request.META['HTTP_AUTHORIZATION'] = token

        user = User.objects.create(username='api@api.com')

        autentication = Authentication()
        assert (user, None) == autentication.authenticate(get_request)

    def test_autentication_has_permission(self):

        user = User.objects.create(username='api@api.com')

        request = RequestFactory()
        get_request = request.get('/api/v1/formerCustomers/remove_equipament/')
        get_request.user = user

        assert Authentication().has_permission(get_request, None)


        

  