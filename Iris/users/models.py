from django.db import models
from django.contrib.auth.models import User
from common.models import BaseModel
from django.contrib.postgres.fields import JSONField
from typing import List

class UserDataModel(BaseModel):

    """

        Class define model UserDescription.
    
    """

    voip_extentions: List[int] = JSONField(default=list)
    user = models.OneToOneField(
        User, 
        null=False,
        blank=False,
        on_delete=models.PROTECT
    )
    
class UserSlackIDModel(BaseModel):

    """

        Class define model UserSlackID.
    
    """

    uid: str = models.CharField(max_length=20, null=False, blank=False)
    user = models.OneToOneField(
        User, 
        null=False,
        blank=False,
        on_delete=models.PROTECT
    )