from typing import List

from django.contrib.auth.models import Group, User
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.authentication import BaseAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from rest_framework.serializers import ValidationError


class Authentication(BaseAuthentication):

    def authenticate(self, request):

        global_preferences = global_preferences_registry.manager()

        token: str = global_preferences['users__API_token']
        user_name: str = global_preferences['users__API_username']

        if request.META.get('HTTP_AUTHORIZATION', '') == token:

            user = User.objects.get(username=user_name)

        else:

            webhook =  "email_message/receive_message" in request.get_full_path()
            if webhook:
                user = User.objects.get(username=user_name)
            else:
                raise ValidationError({'Error': 'Validation error'})

        return (user, None)

    def has_permission(self, request, view):

        """

            This funtion ckeck exist user and is authenticated
    
        """        

        return request.user and request.user.is_authenticated

class Permission(IsAuthenticated):

    def has_permission(self, request, view):

        """

            This funtion ckeck exist user and is authenticated
    
        """        

        global_preferences = global_preferences_registry.manager()

        token: str = global_preferences['users__API_token']

        result: bool = False
        
        if request.META.get('HTTP_AUTHORIZATION', '') == token:

            result = True

        else:

            result = super().has_permission(request, view)

        return result
