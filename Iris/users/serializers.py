import random
import string
from typing import Union, Dict, Any

import requests
from django.contrib.auth.models import Group, User
from rest_framework import serializers
from common.serializers import BaseSerializer
from common.models import BaseModel
from common.utilitarian import *
from .models import UserDataModel


class UserSerializer(serializers.ModelSerializer):

    """

        Class define serializer of UserView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = User
        #Fields of serializer
        fields: Union[tuple, str] = ('username', 'is_active', 'is_superuser')
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'is_active': {'required': False},
            'is_superuser': {'required': False},
        }

    def partial_update(self, request, *args, **kwargs):

        kwargs['partial']: bool = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):

        instance: User = self.get_object()
        instance.deleted: bool = True
        instance.save()

    def create(self, validated_data: dict):

        sesion = requests.Session()
        sesion.headers.update({"Authorization": get_token()})

        #Create random key
        password: str = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        
        username: str = validated_data['username']

        sesion.post(
            url=communications_url('email-list'),
            data={
                "subject": 'Usuario de Iris',
                "receiver": validated_data['username'],
                "sender": 'IrisSystem@optic.cl',
                "message": f'Su usuario en Iris es: {username} . Su clave asignada es: {password}',
                "chat": sesion.post(communications_url('chat-list')).json()['ID'],
            },
            verify=False,
        )

        result = super().create(validated_data)
        agent: User = User.objects.get(username=username)
        agent.set_password(password)
        agent.save()

        return result

class UserDataSerializer(BaseSerializer):

    """

        Class define serializer of UserDataView.
    
    """

    class Meta:

        #Class model of serializer
        model: BaseModel = UserDataModel
        #Fields of serializer
        fields: Union[List[str], str] = '__all__'
        #Args of serializer
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }