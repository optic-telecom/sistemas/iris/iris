from django.apps import AppConfig


class UsersConfig(AppConfig):

    """

        Class define documentation app Users.
    
    """

    name: str = 'users'
