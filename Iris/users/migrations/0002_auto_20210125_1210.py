# Generated by Django 2.1.7 on 2021-01-25 17:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profileusers',
            name='user',
        ),
        migrations.DeleteModel(
            name='ProfileUsers',
        ),
    ]
