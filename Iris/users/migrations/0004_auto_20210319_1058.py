# Generated by Django 2.1.7 on 2021-03-19 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_historicaluserdatamodel_userdatamodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicaluserdatamodel',
            name='operator',
            field=models.SmallIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='userdatamodel',
            name='operator',
            field=models.SmallIntegerField(blank=True, null=True),
        ),
    ]
