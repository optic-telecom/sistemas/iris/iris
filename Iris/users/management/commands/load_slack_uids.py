from typing import IO, List, Union, Dict, Any
import openpyxl
import string    
import random
from django.contrib.auth.models import User, Group
from django.core.management.base import BaseCommand
from dynamic_preferences.registries import global_preferences_registry
from users.models import UserSlackIDModel
from operators.models import OperatorModel

class Command(BaseCommand):

    help = 'Uploads users slack id'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('file')

    def handle(self, **options):

        def iter_rows(ws):
            for row in ws.iter_rows():
                yield [cell.value for cell in row]

        failed_items = []
        wb = openpyxl.load_workbook(filename=options['file'], data_only=True)
        o_sheet = wb["Sheet"]

        for index,element in enumerate(iter_rows(o_sheet)):

            if index == 0 or element[1] == "-" or element[0] == None:
                continue
            
            try:
                user = User.objects.get(username=element[0]) 
            except:
                S = 10
                password = ''.join(random.choices(string.ascii_uppercase + string.digits, k = S))    
                user = User.objects.create(username=element[0], password=password)
            
            operator = 1 if element[2] == 'MULTIFIBER' else 2

            try:
                UserSlackIDModel.objects.create(
                    operator=OperatorModel.objects.get(ID=operator),
                    user=user,
                    uid=element[1]
                )
            except:
                pass

            if len(element) > 3:
                try:
                    for index in range(3,len(element)):
                        group = Group.objects.filter(name=element[index]).first()
                        group.user_set.add(user)
                except:
                    pass

