if (!String.prototype.format) {
	  			String.prototype.format = function() {
		    		var args = arguments;
		    		return this.replace(/{(\d+)}/g, function(match, number) {
			      		return typeof args[number] != 'undefined'
			        	? args[number]
			        	: match
			      		;
		    		});
	  			};
			}

url_base = "http://127.0.0.1:8000/{0}"

		function callView(url_view){

			content = $("#content")

			content.empty()

			$.ajax({
			  url: url_view,
			  success: function (body_html){
			  	$("#content").html(body_html)
			  },
			  beforeSend: function (request) {
    		  	
    			request.setRequestHeader("Authorization", 'jwt {0}'.format(Cookies.get('token')))
  			  },
			  error: function (){

			  		$.gritter.add({
					title: 'Error',
					text: 'No se puedo cargar el recurso',
					sticky: false,
					time: ''
				});

			  }
			});
		}

		function sendData(form, event, success, error){

			event.preventDefault()
	    	var url = form.attr('action');
	    	debugger
	    	
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: form.serialize(),
	           beforeSend: function (request) {
    		  	debugger
    			request.setRequestHeader("Authorization", 'JWT {0}'.format(Cookies.get('token')))
  			  	},
	           success: function()
	           {
	           	    $.gritter.add({
						title: 'Resultado:',
						text: success,
						sticky: false,
						time: ''
					});
	           },
	           error: function()
	           {

	           		$.gritter.add({
						title: 'Resultado:',
						text: error,
						sticky: false,
						time: ''
					});
	           },
	         });


		}