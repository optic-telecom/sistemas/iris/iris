Highlight.init();

var dt_table = $('#table-typing').dataTable({
    order: [[ 0, "desc" ]],
    columnDefs: [
        {
            orderable: true,
            searchable: true,
            targets: [0,2]
        },
    ],
    columns: [
        { "data": "name"},
        { "data": "parent"},
        { "data": "classification"},
    ],
    searching: true,
    processing: true,
    serverSide: true,
    stateSave: true,
    ajax: { url: url_base.format('api/v1/tickets/datatables/typing/'), dataSrc: "" }
});