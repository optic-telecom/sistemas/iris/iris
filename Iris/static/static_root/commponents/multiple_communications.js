$("#page-container").addClass("page-content-full-height")
$("#content").addClass("content-full-width")
$("#content").addClass("inbox")

var types = []
var filters = []
var filters_types = {}
var add_flag = true
cont = 0

$.ajax({
    url: url_base.format('api/v1/tickets/massive/'),
    type: "GET",
    success: function (data){
       
        types = data['filters']
        filters = Object.keys(data['filters'])
        filters_types = data['type_filters']


    },
    error: function (a){

        $.gritter.add({
        title: 'Error',
        text: 'No se puedo cargar el recurso',
        sticky: false,
        time: ''
    });

    }
});

$("#formData").on('submit', function( event ) {

    event.preventDefault()
    var url = $(this).attr('action');

    data = $(this).serializeArray()
    var result = {}
    var ckeck = []
    var tags = []
    var before = ''
    var temporal = []

    data.forEach(function(item, index){
        switch(item['name']){

            case 'template':

                result['template'] = item['value']

                break;

            case 'subject_message':

                result['subject_message'] = item['value']

                break;

            case 'mincheck[]':

                ckeck.push(item['value'])

                break;

            default:
                
                if (before == ''){

                    before = item['name']

                }
                else
                {
                    if (before == item['name']){

                        temporal.push(item['value'])

                    }
                    else
                    {
                        tags.push(temporal)
                        temporal = []
                        before = item['name']

                    }

                }


        }

    })

    tags.push(temporal)

    result['mincheck[]'] = ckeck
    result['tags[]'] = tags

    debugger

    $.ajax({
           type: "POST",
           url: url,
           data: result,
           beforeSend: function (request) {

            request.setRequestHeader("Authorization", 'JWT {0}'.format(Cookies.get('token')))
            },
           success: function()
           {
                $.gritter.add({
                    title: 'Resultado:',
                    text: success,
                    sticky: false,
                    time: ''
                });
           },
           error: function()
           {

                $.gritter.add({
                    title: 'Resultado:',
                    text: error,
                    sticky: false,
                    time: ''
                });
           },
         });

})

function createTagit(node, defaulsTags, array)
{
    new_element = $( "<ul class='primary line-mode'></ul>" )
    node.after( new_element )
    fieldName = "tags{0}[]".format(cont++)

    new_element.tagit({
        fieldName: fieldName,
        availableTags:array,
        autocomplete: {delay: 0, minLength: 1},
        showAutocompleteOnFocus:true,
        allowDuplicates:false,
        placeholderText: "Ingrese filtros",
        tabIndex:-1,
        
        beforeTagAdded: function(event, ui){
            
            tags = $(this).tagit("assignedTags")
            switch (tags.length)
            {

                case 1:
                    
                    
                    if ( !filters.includes(ui.tagLabel) )
                    {
                        event.preventDefault()
                        $.gritter.add({
                            title: 'Error',
                            text: 'El filtro no es valido',
                            sticky: false,
                            time: ''
                        });

                    }
                    else
                    {
                        if ( add_flag ){
                            event.preventDefault()
                            add_flag = false
                            createTagit($(this) , ['X', ui.tagLabel], filters_types[types[ui.tagLabel]] )
                            $(this).remove()
                            add_flag = true 

                        }
                        
                    }
                    break;

                case 2:

                    if (!(add_flag && filters_types[types[tags[1]]].includes(ui.tagLabel)))
                    {
                        event.preventDefault()
                        $.gritter.add({
                            title: 'Error',
                            text: 'El criterio no es valido',
                            sticky: false,
                            time: ''
                        });

                    }
                    
                    break;

                default:

            }
        },
        beforeTagRemoved: function(event, ui) {
            
            
            currentTags = $(this).tagit("assignedTags")
            positionTag = currentTags.indexOf(ui.tagLabel)
            
            switch (positionTag)
            {

                case 0:

                    event.preventDefault()
                    $(this).remove()
                    break;

                case 1:

                    event.preventDefault()
                    createTagit($(this), ['X'], filters)

                    $(this).remove()
                    break

                case 2:

                    event.preventDefault()
                    createTagit($(this), ['X', currentTags[1]], filters_types[types[currentTags[1]]])
                    debugger
                    $(this).remove()
                    break

                default:

            }
            
        },
    })

    defaulsTags.forEach(function (item, index) {
        new_element.tagit("createTag", item);
    });

}

$( "#add-filter" ).on( "click", function(event){

    event.preventDefault()

    createTagit($( "#filters" ), ['X'], filters )

} );