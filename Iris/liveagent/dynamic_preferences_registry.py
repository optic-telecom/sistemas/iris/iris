from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference, IntegerPreference, ChoicePreference

general = Section('liveagent')


@global_preferences_registry.register
class LiveagentKey(StringPreference):

    section = general
    name: str = 'Liveagent'
    default: str = 'xxxx-xxxx-xxxx'
    required: bool = True