from typing import List, Dict, Any

import requests
from django.http import JsonResponse
from dynamic_preferences.registries import global_preferences_registry
from django.http.multipartparser import MultiPartParser


def proxy(request) -> JsonResponse:

    """

        This function return information of liveagent request

        :param request: Values of user request
        :type request: request

        :returns: Response data user
        :rtype: JsonResponse
    
    """

    global_preferences: Dict[str, any] = global_preferences_registry.manager()

    origin_url: str = request.build_absolute_uri().split('/api/v1/liveagent/')
    url: str = 'https://optic.ladesk.com/api/v3/' + origin_url[1]

    headers: Dict[str, str] = {
        'apikey': global_preferences['liveagent__Liveagent']
    }

    result: List[Dict[str, Any]] = []

    if request.method == 'GET':

        response = requests.get(
            url=url,
            headers=headers
        )

        status_code: int = response.status_code

        if response.status_code == 200:
            
            response = response.json()

            if type(response) != list and 'results' in response.keys():

                result: List[Dict[str, Any]] = response['results']

            else:

                result: List[Dict[str, Any]] = response

        else:
            
            result: List[Dict[str, Any]] = []

        result: JsonResponse = JsonResponse(data=result, safe=False, status=status_code)

    else:

        if request.method == 'POST':

            response = requests.post(
                url=url,
                headers=headers,
                verify=False,
                data=request.body.decode('utf-8')
            )

        status_code: int = response.status_code

        try:

            result: JsonResponse = JsonResponse(data=response.json(), safe=False, status=status_code)

        except:

            result: JsonResponse = JsonResponse(safe=False, status=status_code, data={})

    return result
