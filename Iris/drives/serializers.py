from rest_framework import serializers

from common.serializers import BaseSerializer
from typing import Dict, Any, Union, List
from .models import DriveDefinitionModel, DriveRegisterModel

class DriveDefinitionSerializer(BaseSerializer):

    class Meta:

        model = DriveDefinitionModel
        fields: Union[List[str], str] = '__all__'
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

class DriveRegisterSerializer(BaseSerializer):

    class Meta:

        model = DriveRegisterModel
        fields: Union[List[str], str] = '__all__'
        extra_kwargs: Dict[str, Dict[str, Any]] = {
            'creator': {'required': False},
            'updater': {'required': False},
        }

    def update(self, instance: DriveRegisterModel, validated_data: Dict[str, Any ] ):

        """
            This function updated instance

            :param self: Instance of Class DriveRegisterSerializer
            :type self: DriveRegisterSerializer
            :param instance: Instance of Class DriveRegisterModel
            :type instance: DriveRegisterModel
            :param validated_data: Dict object with validated data
            :type validated_data: dict

            :returns: Updated instance of DriveRegisterModel class
            :rtype: DriveRegisterModel
        
        """

        #Remove drive key if exist
        if "drive" in validated_data:

            del validated_data["drive"]

        current_definition: Dict[str, Any] = instance.register


        if "register" in validated_data:

            for key, value in validated_data["register"].items():

                if key in current_definition:

                    current_definition[key]: Any = value

            validated_data["register"]: Dict[str, Any] = current_definition
        
        return super().update(instance, validated_data)