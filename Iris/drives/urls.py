from rest_framework.routers import DefaultRouter

from .views import DriveDefinitionView, DriveRegisterView

router = DefaultRouter()

router.register(r'definition', DriveDefinitionView, basename='definition')
router.register(r'register', DriveRegisterView, basename='register')

urlpatterns = router.urls