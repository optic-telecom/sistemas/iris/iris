from rest_framework.serializers import ValidationError
from datetime import datetime
import phonenumbers
from typing import List, Any, Dict, Callable, Union
from validate_email import validate_email
import requests
from common.utilitarian import get_token, matrix_url
from django.contrib.auth.models import User
from dynamic_preferences.registries import global_preferences_registry

def prueba():

        pass

def validate_int( default_value_description: int ):

    """

        This function validate value int type

        :param default_value_description: Description value
        :type default_value_description: int

    """

    if isinstance(default_value_description, bool) or not isinstance(default_value_description, int):

        raise ValidationError({
            'Error int value': "Value must be int type"
        })

def validate_float( default_value_description: float ):

    """

        This function validate value float type

        :param default_value_description: Description value
        :type default_value_description: float

    """

    if isinstance(default_value_description, bool) or not (isinstance(default_value_description, float) or isinstance(default_value_description, int)) :

        raise ValidationError({
            'Error float value': "Value must be float type"
        })

def validate_logic( default_value_description: bool ):

    """

        This function validate value bool type

        :param default_value_description: Description value
        :type default_value_description: bool

    """

    if not default_value_description in [True, False]:

        raise ValidationError({
            'Error logic value': "Value must be bool type"
        })

def validate_select_static( default_value_description: List [ Dict[ Union[int, float, bool, str], str] ] ):

    """

        This function validate value choices type

        :param default_value_description: Description value
        :type default_value_description: dict

    """

    def validate_value( default_value_description: List [ Dict[ Union[int, float, bool, str], str] ] ):

        if len(default_value_description['values']) == 0:

            raise ValidationError({
                'Description error: static selects': "There must be at least one option"
            })

        for value in default_value_description['values']:


            if not ( isinstance(value, dict) and "value" in value and "name" in value and isinstance(value["name"], str) and isinstance(value["value"], str ) ):

                raise ValidationError({
                    'Description default_value error: static selects': f"Description default_value of static is not valid, must be list of dict type"
                })

        if not [ x for x in default_value_description['values'] if x['value'] == default_value_description['default_value'] ] and default_value_description['default_value'] != None:

            raise ValidationError({
                'Description default_value error: static selects': "Default value, must be in the list"
            })

        if not len(default_value_description['values']) == len(set([x['value'] for x in default_value_description['values']])):

            raise ValidationError({
                'Description default_value error: static selects': "Invalid repeats values"
            })

    if isinstance(default_value_description['values'], list) and validate_value( default_value_description ):

        raise ValidationError({
            'Description default_value error: static selects': "Description default_value of static is not valid, must be list of dict type"
        })

def validate_datetime( default_value_description: str ):

    """

        This function validate value str type

        :param default_value_description: Description value
        :type default_value_description: str

    """

    if not isinstance(default_value_description, str):

        raise ValidationError({
            'Error default_value value: datetime type': "Description default_value is not valid, must be str type"
        })

    try:

        datetime.strptime(default_value_description, '%Y/%m/%d %H:%M:%S')

    except:

        raise ValidationError({
            'Error default_value format: datetime format': "Description default_valueis not valid, must be datatime this format: %Y/%m/%d %H:%M:%S"
        })

def validate_phone( default_value_description: str ):

    """

        This function validate value phone type

        :param default_value_description: Description value
        :type default_value_description: str

    """

    if not isinstance(default_value_description, str):

        raise ValidationError({
            'Error default_value value: phone type': "Description default_value is not valid, must be str type"
        })

    try:

        phonenumbers.parse(default_value_description, None)

    except:

        raise ValidationError({
            'Error default_value format: phone format': "Description default_value is not valid, Invalid phone format"
        })

def validate__email( default_value_description: str ):

    """

        This function validate value email type

        :param default_value_description: Description value
        :type default_value_description: str

    """

    if not ( isinstance(default_value_description, str) and validate_email(default_value_description) ):

        raise ValidationError({
            'Error default_value value: email': "Description default_value is not valid, must be str type and valid email"
        })

def validate_string( default_value_description: str ):

    """

        This function validate value str type

        :param default_value_description: Description value
        :type default_value_description: str

    """

    if not  isinstance(default_value_description, str):

        raise ValidationError({
            'Description default_value error: string': "Description default_value is not valid, must be str type"
        })

def validate_dynamic_services( default_value_description: int ):

    """

        This function validate value service type

        :param default_value_description: Description value
        :type default_value_description: int

    """

    if not isinstance(default_value_description, int):

        raise ValidationError({
            'Description default_value error: dynamic_services type': "Description default_value is not valid, must be int type"
        })

    url_services: str = matrix_url('services')
    url_services: str = f'{url_services}{default_value_description}/'

    response = requests.get(
        url=url_services,
        verify=False,
        headers={
            "Authorization": get_token()
        },
    )

    if response.status_code != 200:

        raise ValidationError({
            'Error dynamic_services value': "Must be an existing value"
        })

def validate_dynamic_customers( default_value_description: str ):

    """

        This function validate value customer type

        :param default_value_description: Description value
        :type default_value_description: int

    """

    if not isinstance(default_value_description, str):

        raise ValidationError({
            'Description default_value error: dynamic_customers type': "Description default_value is not valid, must be str type"
        })

    url_customers: str = matrix_url('customers')
    url_customers: str = f'{url_customers}'

    params: Dict[str, str] = {
        'fields':'id',
        'rut':default_value_description
    }

    response = requests.get(
        url=url_customers,
        verify=False,
        headers={
            "Authorization": get_token()
        },
        params=params
    )

    if response.status_code != 200 and response.json():

        raise ValidationError({
            'Error dynamic_customers value': "Must be an existing value"
        })

def validate_dynamic_users_iris( default_value_description: int ):

    """

        This function validate value iris user type

        :param default_value_description: Description value
        :type default_value_description: int

    """

    if not isinstance(default_value_description, int):

        raise ValidationError({
            'Description default_value error: dynamic_users_iris type': "Description default_value is not valid, must be int type"
        })

    try:

        User.objects.get(pk=default_value_description)

    except:

        raise ValidationError({
            'Error dynamic_users_iris value': "Must be an existing value"
        })

def validate_location( default_value_description: int ):

    """

        This function validate value location type

        :param default_value_description: Description value
        :type default_value_description: int

    """

    if not isinstance(default_value_description, str):

        raise ValidationError({
            'Description default_value error: location type': "Description default_value is not valid, must be int type"
        })

    global_preferences: Dict[str, Any] = global_preferences_registry.manager()

    #Get headers and token
    headers: Dict[str] = {
        'AUTHORIZATION': global_preferences['matrix__matrix_token']
    }

    #Get url of pulso
    url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility/'

    try:

        street_location = default_value_description.split(':')[0]
        operator = default_value_description.split(':')[1]

    except Exception as e:

        raise ValidationError({
            'Error location value': "Must be an a valid format : street_location:operator"
        })

    #Check feasibility
    response_feasibility: Dict[str] = requests.get(
        url=url,
        headers=headers,
        verify=False,
        params={
            'street_location':street_location,
            'operator':operator,
        },
    )

    response_feasibility_data: Dict[str, str] = response_feasibility.json()
    response_feasibility_status: int = response_feasibility.status_code

    if response_feasibility_status != 200 or response_feasibility_data.get('error', ''):

        raise ValidationError({
            "Error address value": "Must be an existing address",
        })

def validate_dynamic_plans( default_value_description: int ):

    """

        This function validate value plan type

        :param default_value_description: Description value
        :type default_value_description: int

    """

    if not isinstance(default_value_description, int):

        raise ValidationError({
            'Description default_value error: dynamic_plans type': "Description default_value is not valid, must be int type"
        })

    url_plans: str = matrix_url('plans')
    url_plans: str = f'{url_plans}{default_value_description}/'

    response = requests.get(
        url=url_plans,
        verify=False,
        headers={
            "Authorization": get_token()
        }
    )

    if response.status_code != 200:

        raise ValidationError({
            'Error dynamic_plans value': "Must be an existing value"
        })