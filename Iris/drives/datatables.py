import ast
import random
from datetime import datetime
from functools import reduce
from typing import Any, Dict, List, Union
from django.contrib.auth.models import User

from common.dataTables import BaseDatatables
from common.models import BaseModel
from django.db.models.query import QuerySet
from django.http import JsonResponse
from dynamic_preferences.registries import global_preferences_registry
from rest_framework.serializers import ValidationError
from validate_email import validate_email
import requests
import string
from openpyxl.workbook import Workbook
from openpyxl.styles import PatternFill
import os
from common.utilitarian import communications_url, get_token

from .models import DriveDefinitionModel, DriveRegisterModel


class DefinitionTablesDatatable(BaseDatatables):

    FIELDS_SORTABLE: List[str] = ["name"]

    FIELDS_FILTERS: Dict[str, str] = {
        "created":"datetime",
        "creator__pk":"choices",
        "name": "str",
        "description": "str"
    }


    model: BaseModel = DriveDefinitionModel

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:

        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        return qs.filter( name__icontains=self.SEARCH )
        
    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        return {
            "name": instance.name,
            "ID": instance.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "description": instance.description,
            "definition": instance.definition,
        }

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field":"name",
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":600
        }

        data_struct["fields"] = {

            "ID": "int",
            "name": "str",
            "created": "datetime",
            "creator": "str",
            "description": "str",
            "definition": "list",

        }

        data_struct["columns"] = {

            "Nombre":{
                "field": "name",
                "sortable": True,
                "visible": True,
                "position": 1,
                "width":100,
                "fixed":None
            },
            "Descripción":{
                "field": "description",
                "sortable": False,
                "visible": True,
                "position": 2,
                "width":200,
                "fixed":None
            },
            "Modificar":{
                "field": "custom_change",
                "sortable": False,
                "visible": True,
                "position": 3,
                "width":100,
                "fixed":None
            },
            "Eliminar":{
                "field": "custom_delete",
                "sortable": False,
                "visible": True,
                "position": 4,
                "width":100,
                "fixed":None
            },

        }

        data_struct["filters"] = {
            "Fecha de inicio":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Fecha de fin":{
                "type":"datetime",
                "name":"created",
                "format":"%d/%m/%Y %H:%M",
            },
            "Nombre":{
                "type":"str",
                "name":"name"
            },
            "Descripción":{
                "type":"str",
                "name":"description"
            }
        }

        return data_struct


class RegistersDatatable(BaseDatatables):

    def __init__(self, request, pk_definition):

        self.request = request

        try: 

            definition: DriveDefinitionModel = DriveDefinitionModel.objects.get(
                ID=pk_definition,
                deleted=False
            )

        except Exception as e:

            raise ValidationError({
                "Error instance": f"Instance with id == {pk} don`t exist."
            })

        self.DEFINITION: List[Dict[str, any]] = definition

        self.FIELDS_SORTABLE: List[str] = [ item['name'] for item in definition.definition ]
        self.FIELDS_SORTABLE.append("created")
        self.FIELDS_SORTABLE.append("creator")

        self.TYPE_OF_FILTERS: Dict[str, List[str]] = {
            "datetime": [ "equal", "year", "month", "day", "week_day", "week", "quarter", "gt", "gte", "lt", "lte", "different" ],
            "str": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
            "email": [ "equal", "iexact", "icontains", "istartswith", "iendswith", "different" ],
            "int": [ "equal", "gt", "gte", "lt", "lte", "different" ],
            "float": [ "equal", "gt", "gte", "lt", "lte", "different" ],
            "bool": [ "equal", "different" ],
            "choices": [ "equal", "different" ],
            "location": [ "equal", "different"]
        }

        self.COMPARISON: Dict[str, List[str]] = {
            "equal": ["datetime", "str", "int", "bool", "choices", "float", "email"],
            "year": ["int"],
            "month": ["int"],
            "day": ["int"],
            "week_day": ["int"],
            "week": ["int"],
            "quarter": ["int"],
            "gt": ["datetime", "str", "int", "bool", "float"],
            "gte": ["datetime", "str", "int", "bool", "float"],
            "lt": ["datetime", "str", "int", "bool", "float"],
            "lte": ["datetime", "str", "int", "bool", "float"],
            "different": ["datetime", "str", "int", "bool", "float", "email"],
            "iexact": ["str", "email"],
            "icontains": ["str", "email"],
            "istartswith": ["str", "email"],
            "iendswith": ["str", "email"]
        }

        self.FIELDS_FILTERS: Dict[str, str] = {
            definition['name']:self.get_filter(definition)["type"] for definition in self.DEFINITION.definition
        }

        self.FIELDS_FILTERS["creator"] = "choices"
        self.FIELDS_FILTERS["created"] = "datetime"

        position_element: int = 0
        definitions: List[Dict[str, str]] = definition.definition + [
            {"name":"creator"},
            {"name":"created"},
            {"name":"operator"},
            {"name":"ID"},
        ]
        
        length_elements: int = len(definitions)

        self.FIELDS_EXCEL: Dict[str, Dict[str, str]] = {}

        for character_left in ' ' + string.ascii_uppercase:

            for character_right in string.ascii_uppercase:

                cell_name: str = f'{character_right}' if position_element < 25 else f'{character_right}{character_left}'

                self.FIELDS_EXCEL[cell_name] = definitions[position_element]['name']

                position_element = position_element  + 1

                if position_element == length_elements:

                    break

            if position_element == length_elements:

                    break

    def get_qs_to_data(self, qs: QuerySet) -> List[Dict[str, Any]]:

        list_names: List[str] = [ cell_name for cell, cell_name in self.FIELDS_EXCEL.items() ]

        return list(
            map(
                lambda instance: list(
                    map(
                        lambda cell_name: self.get_instance_to_dict_to_file(instance)[cell_name],
                        list_names
                    )
                ),
                qs
            )
        )

    def download_data(self) -> QuerySet:

        """

            This function, return file of data

        """

        def get_locations( self, qs : List[DriveRegisterModel]):

            locations_fields: List[str] = [ definition['name'] for definition in self.DEFINITION.definition if definition['type'] == 'select_dynamic_location' ]

            street_locations: List[int] = []

            if locations_fields:
                
                street_locations: List[int] = list(
                    set(
                        reduce(
                            lambda x, y: x + y,
                            map(
                                lambda x : [ int(value.split(':')[0]) for key, value in x.register.items() if value != None and key in locations_fields ],
                                qs
                            )
                        )
                    )
                )

                global_preferences: Dict[str, Any] = global_preferences_registry.manager()

                pulso_token: str = global_preferences['pulso__pulso_check_token']
                pulso_url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility-iris/'

                _random: int = random.randint(5000, 5000000)

                self.street_locations: Dict[int, str] = []

                if street_locations:

                    street_locations = requests.post(
                        url=f"{pulso_url}?no_cache={_random}",
                        json={
                            "street_location":street_locations,
                        },
                        headers={
                            'Authorization':pulso_token
                        },
                        verify=False,
                    ).json()

                    self.street_locations: Dict[int, str] = { location['street_location_id']:location['street_location'] for location in street_locations}

        def get_plans( self, qs : List[DriveRegisterModel]):

            global_preferences: Dict[str, Any] = global_preferences_registry.manager()

            matrix_token: str = global_preferences['matrix__matrix_token']
            matrix_url: str = global_preferences['general__matrix_domain'] + 'plans/'

            data : Dict[str, any] = {
                'limit': 1000000,
                'offset': 0,
                'fields':'id,name'
            }

            plans_data = requests.get(
                url=matrix_url,
                params=data,
                headers={
                    'Authorization':matrix_token
                },
                verify=False,
            ).json()

            self.plans_info: Dict[int, str] = { customer['id']:customer['name'] for customer in plans_data['results']}

        def get_select_static( self, qs : List[DriveRegisterModel]):

            customers_fields: List[str] = [ definition['name'] for definition in self.DEFINITION.definition if definition['type'] == 'select_static' ]

            self.select_static_info : Dict[str, Dict[int, str]] = {
                definition['name']:{ item['value']:item['name'] for item in definition['values']}
                for definition in self.DEFINITION.definition if definition['name'] in customers_fields
            }

        def get_iris_user(self ):

            self.users_iris : Dict[int, str] = {
                user.pk:user.username for user in User.objects.all()
            }

        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )

        result: List[List[Any]] = []

        if qs:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]

            get_locations( self, qs )
            get_plans( self, qs )
            get_select_static( self, qs )
            get_iris_user( self )

            self.name_type : Dict[str, str] = { item['name']:{'type':item['type'], 'required':item['required']} for item in self.DEFINITION.definition}

            result: List[List[Any]] = self.get_qs_to_data(qs)

        #Define excel
        wb = Workbook()
        ws1 = wb.active
        redFill = PatternFill(
                    start_color='3bd464',
                    end_color='3bd464',
                    fill_type='solid',
                )

        for key, value in self.FIELDS_EXCEL.items():

            ws1[f'{key}1'].fill: PatternFill = redFill
            ws1[f'{key}1']: str = value

        #Create rows
        for row in result:

            ws1.append(row)

        #Save file
        _random: int = random.randint(5000, 5000000)
        path: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        path_file: str = f'{path}/media/files/{_random}.xlsx'
        wb.save(filename=path_file)

        #Send file
        with open(path_file, 'rb') as file_excel:

            sesion = requests.Session()
            sesion.headers.update({"Authorization": get_token()})
            sesion.post(
                url=communications_url('email-list'),
                data={
                    'message': 'Reporte de datos',
                    'subject': 'Reporte',
                    'receiver': self.request.data['email'],
                    'sender': 'reporte@Iris.cl',
                    'operator': int(self.request.data['operator'])
                },
                files={
                    'reporte.xlsx':file_excel,
                },
                verify=False,
            )

    def get_filter(self, definition: Dict[str, any]) -> Dict[str, str]:

        result: Dict[str, str] = {}

        if definition['type'] in ['phone', 'email', 'string']:

            result: Dict[str, str] = {
                "type":"str",
                "name":definition['name'],
            }

        elif definition['type'] == 'logic':

            result: Dict[str, str] = {
                "type":"bool",
                "name":definition['name'],
            }

        elif definition['type'] == 'datetime':

            result: Dict[str, str] = {
                "type":"datetime",
                "name":definition['name'],
                "format":"%Y/%m/%d %H:%M:%S",
            }

        elif definition['type'] in ['float', 'int']:

            result: Dict[str, str] = {
                "type":definition['type'],
                "name":definition['name'],
            }

        elif definition['type'] == 'select_static':

            result: Dict[str, str] = {
                "type":"choices",
                "name":definition['name'],
                "type_choices": "choices",
                "values": {
                    "type": "static",
                    "extra_data":{
                        value['value']: value['name'] for value in definition['values']
                    }
                }
            }

        elif definition['type'] == 'select_dynamic_plans':

            result: Dict[str, str] = {
                "type":"choices",
                "name":definition['name'],
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "matrix/plans/",
                        "read_data":{
                            "value":"id",
                            "human_readable": "name"
                        }
                    }
                }
            }

        elif definition['type'] == 'select_dynamic_location':

            result: Dict[str, str] = {
                "type":"location",
                "name":definition['name'],
            }

        elif definition['type'] == 'select_dynamic_users_iris':

            result: Dict[str, str] = {
                "type":"choices",
                "name":definition['name'],
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "user/data/information/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    }
                }
            }

        elif definition['type'] == 'select_dynamic_rut':

            result: Dict[str, str] = {
                "type":"choices",
                "name":definition['name'],
                "type_choices": "choices",
                "values": {
                    "type": "dinamic",
                    "extra_data":{
                        "url": "api/v1/matrix/customers/",
                        "read_data":{
                            "value":"ID",
                            "human_readable": "name"
                        }
                    },
                    "params":{
                        "rut__icontains":"str"
                    }
                }
            }

        elif definition['type'] == 'select_dynamic_services':

            result: Dict[str, str] = {
                "type":"int",
                "name":definition['name'],
            }

        return result

    def get_struct(self) -> Dict[str, Any]:

        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        def get_type(definition: Dict[str, any]) -> str:

            if definition['type'] == 'logic':

                result: str = 'bool'

            elif definition['type'] in [ 'select_dynamic_rut', 'select_dynamic_users_iris', 'select_dynamic_location', \
                'select_dynamic_plans', 'select_static', 'select_dynamic_services']:

                result: str = 'choices'

            elif definition['type'] == 'string':

                result: str = 'str'

            else:

                result: str = definition['type']

            return result

        def get_width(_type: str) -> int:

            dict_length : Dict[str, int] = {
                'string':150,
                'select_dynamic_location':200,
                'int':100,
                'float':100,
                'phone':150,
                'select_dynamic_services':100,
                'select_dynamic_rut':100,
                'select_dynamic_plans':300,
                'logic':150,
                'datetime':220,
                'email':250,
                'select_dynamic_users_iris':250,
                'select_static':250
            }

            return dict_length[_type]

        data_struct: Dict[str, Any] = {}

        data_struct['comparisons']: Dict[str, Dict[str, any]] = {
            "Igual": {
                "name": "equal",
                "type": ["datetime", "str", "int", "bool", "choices", "location", "float"]
            },
            "Año": {
                "name": "year",
                "type": ["int"]
            },
            "Mes": {
                "name": "month",
                "type": ["int"]
            },
            "Día": {
                "name": "day",
                "type": ["int"]
            },
            "Día de la semana": {
                "name": "week_day",
                "type": ["int"]
            },
            "Semana": {
                "name": "week",
                "type": ["int"]
            },
            "Trimestre": {
                "name": "quarter",
                "type": ["int"]
            },
            "Mayor": {
                "name": "gt",
                "type": ["datetime", "str", "int", "bool", "float"]
            },
            "Mayor o igual": {
                "name": "gte",
                "type": ["datetime", "str", "int", "bool", "float"]
            },
            "Menor": {
                "name": "lt",
                "type": ["datetime", "str", "int", "bool", "float"]
            },
            "Menor o igual": {
                "name": "lte",
                "type": ["datetime", "str", "int", "bool", "float"]
            },
            "Distinto": {
                "name": "different",
                "type": ["datetime", "str", "int", "bool", "choices", "location", "float"]
            },
            "Similar": {
                "name": "iexact",
                "type":"str"
            },
            "Contiene": {
                "name": "icontains",
                "type":"str"
            },
            "Inicia con": {
                "name": "istartswith",
                "type":"str"
            },
            "Termina con": {
                "name": "iendswith",
                "type":"str"
            }
        }
        data_struct['type_of_comparisons']: Dict[str, List[str]] = {
            "datetime": [ "Igual", "Año", "Mes", "Día", "Día de la semana", "Semana", "Trimestre", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto"  ],
            "str": [ "Igual", "Similar", "Contiene", "Inicia con", "Termina con", "Distinto"],
            "int": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
            "float": [ "Igual", "Mayor", "Mayor o igual", "Menor", "Menor o igual", "Distinto" ],
            "bool": [ "Igual", "Distinto" ],
            "choices": [ "Igual", "Distinto" ],
            "location": [ "Igual", "Distinto" ]
        }

        data_struct["defaults"] = {
            "order_field":self.FIELDS_SORTABLE[0],
            "order_type":"desc",
            "start":0,
            "offset":10,
            "filters":[],
            "search":"",
            "scroll":len(self.DEFINITION.definition) * 100,
            "download":True
        }

        data_struct["fields"] = {
            value['name']: get_type( value ) for value in self.DEFINITION.definition
        }

        columns: Dict[str, any] = {}

        for iteration, value in enumerate(self.DEFINITION.definition):

            columns[value['name']] = {
                "field": value['name'],
                "sortable": False if value['type'] == 'select_dynamic_location' else True,
                "visible": True,
                "position": iteration,
                "width":get_width(value['type']),
                "fixed":None
            }

        columns["creator"] = {
            "field": "creator",
            "sortable": True,
            "visible": True,
            "position": len(self.DEFINITION.definition),
            "width":100,
            "fixed":None
        }

        columns["created"] = {
            "field": "created",
            "sortable": True,
            "visible": True,
            "position": len(self.DEFINITION.definition) + 1,
            "width":100,
            "fixed":None
        }

        columns["Modificar"] = {
            "field": "custom_change",
            "sortable": False,
            "visible": True,
            "position": len(self.DEFINITION.definition) + 2,
            "width":100,
            "fixed":"right"
        }

        columns["Eliminar"] = {
            "field": "custom_delete",
            "sortable": False,
            "visible": True,
            "position": len(self.DEFINITION.definition) + 3,
            "width":100,
            "fixed":"right"
        }

        data_struct["columns"] = columns

        data_struct["filters"] = {
            definition['name']:self.get_filter(definition) for definition in self.DEFINITION.definition
        }

        data_struct["filters"]["Creado"] = {
            "type":"datetime",
            "name":"created",
            "format":"%Y/%m/%d %H:%M:%S",
        }

        data_struct["filters"]["Creador"] = {
            "type":"choices",
            "name":"creator",
            "type_choices": "choices",
            "values": {
                "type": "dinamic",
                "extra_data":{
                    "url": "user/data/information/",
                    "read_data":{
                        "value":"ID",
                        "human_readable": "name"
                    }
                }
            }
        }

        return data_struct

    def get_initial_queryset(self) -> QuerySet:
        
        """

            This function, filter queryset by operator

        """

        return self.model.objects.filter(drive=self.DEFINITION, deleted=False).only("operator", "created", "creator","register")

    def get_initial_params(self):

        """

            This function, set initial params

        """

        def scheme_filter(self, _filter: List[str]) -> List[str]:

            """
            
                This funtion, return validate filter or raise exception with errors

            """

            def type_validation(_type: str, value: str, name: str, comparison_filter: str) -> Any:

                """
            
                    This funtion, return validated filter value or raise exception with errors

                """

                def validate_datetime(value: str, _type: str, comparison_filter: str) -> Union[datetime, int]:

                    """
            
                        This funtion, return validated datetime value or raise exception with errors

                    """

                    if comparison_filter in ["year", "month", "day", "week_day", "week", "quarter"]:

                        result: int = int(value)

                        if comparison_filter == "year" and 2100 >= result <= 2000:

                            raise ValidationError({})

                        elif comparison_filter == "month" and 1 >= result <= 12:

                            raise ValidationError({})

                        elif comparison_filter == "day":

                            max_days: int = 366 if calendar( datetime.now().year ) else 365

                            if not 1 >= result <= max_days:

                                raise ValidationError({})

                        elif comparison_filter == "week_day" and 0 >= result <= 6:

                            raise ValidationError({})

                        elif comparison_filter == "quarter" and 1 >= result <= 4:

                            raise ValidationError({})

                    else:

                        result: str = datetime.strptime(value, '%Y/%m/%d %H:%M:%S')

                    return result

                def validate_bool(value: str, _type: str, comparison_filter: str) -> bool:

                    """
                    
                        This function, return validated bool value or raise exception with errors
                    
                    """

                    if not value in ["True", "False"]:

                        raise Exception("Invalid bool value")

                    else:

                        return True if value == "True" else False

                def validate_int(value: str, _type: str, comparison_filter: str) -> int:

                    """
                    
                        This function, return validated int value or raise exception with errors
                    
                    """

                    return int(value)

                def validate_float(value: str, _type: str, comparison_filter: str) -> float:

                    """
                    
                        This function, return validated float value or raise exception with errors
                    
                    """

                    return float(value)

                def validate_str(value: str, _type: str, comparison_filter: str) -> str:

                    """
                    
                        This function, return validated str value or raise exception with errors
                    
                    """

                    return str(value)

                def validate_email(value: str, _type: str, comparison_filter: str) -> str:

                    """
                    
                        This function, return validated str value or raise exception with errors
                    
                    """

                    return str(value) and validate_email(value)

                def validate_choices(value: str, _type: str, comparison_filter: str) -> int:

                    """
                    
                        This function, return validated int value or raise exception with errors
                    
                    """

                    return int(value)

                def validate_location(value: str, _type: str, comparison_filter: str) -> str:

                    """
                    
                        This function, return validated lcoation value or raise exception with errors
                    
                    """

                    result: List[str] = value.split(':')

                    if not ( len(result) == 2  and result[0] in ['region','commune','street','streetLocation'] and result[1].isdigit() ):

                        raise Exception("Invalid location value")

                    return value

                dict_validators: Dict[str, Callable] = {
                    "int":validate_int,
                    "str":validate_str,
                    "datetime":validate_datetime,
                    "bool":validate_bool,
                    "choices":validate_choices,
                    "location":validate_location,
                    "float":validate_float,
                    "email":validate_float,
                }
                
                try:

                    value: Any = dict_validators[_type](value, _type, comparison_filter)

                except Exception as e:

                    raise ValidationError({
                        "Invalid value type": f"Invalid value: {value}, for filter {name}",
                    })

                return value

            if len(_filter) == 3:

                name_filter: str = _filter[0]
                comparison_filter: str = _filter[1]
                value_filter: str = _filter[2]

                #Get filter name or None
                field_type: str = self.FIELDS_FILTERS.get(name_filter)

                if field_type:

                    #Get filter comparison or None
                    comparisons: List[str] = self.TYPE_OF_FILTERS[field_type]

                    if comparison_filter in comparisons:
 
                        value_filter: Any = type_validation(field_type, value_filter, name_filter, comparison_filter)

                    else:

                        raise ValidationError({
                            "Invalid filter comparison": f"Invalid comparison name {comparison_filter}",
                        })

                else:

                    raise ValidationError({
                        "Invalid filter name": f"Invalid filter name {name_filter}",
                    })

            else:

                raise ValidationError({
                    "Invalid filter": "Invalid filter length",
                })

            return [name_filter, comparison_filter, value_filter]

        #Filters list
        filters: List[List[str]] = self.request.data.get('filters', "[]")
        validated_filters: List[list] = [] 

        try:

            #Get filters list
            filters: List[list] = ast.literal_eval(filters)
            
        except Exception as e:

            raise ValidationError({
                "Invalid filter": "Invalid filter struct",
            }) 

        if isinstance(filters, list) :

            for _filter in filters:

                #Filter validation
                validated_filters.append(scheme_filter(self, _filter))

        else:

            raise ValidationError({
                "Invalid filter": "Invalid filter struct",
            })   

        #Save validated filters
        self.REQUEST_FILTERS: List[List[str]] = validated_filters
        
        #Save Slice
        self.START: int = int( self.request.data.get('start', 0) )
        self.OFFSET: int = int( self.request.data.get('offset', 10) )

        #Save column order
        self.COLUMN_ORDER_FIELD: str = self.request.data.get('order_field', None) 

        if not self.COLUMN_ORDER_FIELD in self.FIELDS_SORTABLE:

            raise ValidationError({
                "Invalid sortable column": f"Invalid sortable column called {self.COLUMN_ORDER_FIELD}",
            })

        self.COLUMN_ORDER_TYPE: str = self.request.data.get('order_type', None)
        
        if not self.COLUMN_ORDER_TYPE in ["desc", "asc"]:

            raise ValidationError({
                "Invalid sortable column type": f"Invalid sortable column type called {self.COLUMN_ORDER_TYPE}",
            })

        #Save search value
        self.SEARCH: str = self.request.data.get('search', '')

    def get_filtered_queryset(self) -> QuerySet:

        """

            This function, filter queryset by filters

        """

        def filter_datetime(register: Dict[str, any], _filter: List[str]) -> bool:

            data_datetime: datetime = datetime.strptime(register.register[_filter[0]], '%Y/%m/%d %H:%M:%S:%S')
            data_filter_datetime: datetime = datetime.strptime(_filter[2], '%Y/%m/%d %H:%M:%S:%S')

            filter_datetime: Dict[str, callable] = {
                "year": lambda x, y: x.year == y.year,
                "month": lambda x, y: x.month == y.month,
                "day": lambda x, y: x.day == y.day,
                "week_day": lambda x, y: x.weekday() == y.weekday(),
                "week": lambda x, y: x.isocalendar()[1] == y.isocalendar()[1],
                "quarter": lambda x, y: (x.month-1)//3+1 == (y.month-1)//3+1,
            }

            return filter_datetime[_filter[1]](data_datetime, data_filter_datetime)

        def location_filters_func(LOCATION_FILTER: List[List[str]] , qs: QuerySet, operator: int):

            result : QuerySet = qs

            global_preferences: Dict[str, Any] = global_preferences_registry.manager()

            pulso_token: str = global_preferences['pulso__pulso_check_token']
            pulso_url: str = global_preferences['pulso__domain_url']

            for _filter in LOCATION_FILTER:

                street_location_pulso : List[int] = []

                for instance in result:
                    if instance.register[_filter[0]] == None:
                        continue
                    street_location_pulso.append(instance.register[_filter[0]].split(':')[0])

                location: str = _filter[2]
                _type: str = location.split(':')[0]
                location_search: str = int(location.split(':')[1])

                result_locations : List[int] = []
                    
                _random: int = random.randint(5000, 5000000)
                
                response_locations = requests.post(
                    url=f"{pulso_url}/api/v1/locations-iris/?no_cache={_random}",
                    json={
                        "tipo":_type,
                        "location":location_search,
                        "locations":street_location_pulso,
                    },
                    headers={
                        'Authorization':pulso_token
                    },
                    verify=False,                        
                )

                result_locations : List[int] = list(
                    set(
                        map(
                            lambda x : f'{x}:{operator}',
                            response_locations.json()
                        )
                    )
                )

                if _filter[1] == "equal":

                    result: QuerySet = result.filter(
                        **{
                            f'register__{_filter[0]}__in': result_locations
                        }
                    )

                else:

                    result: QuerySet = result.exclude(
                        **{
                            f'register__{_filter[0]}__in': result_locations
                        }
                    )

            return result

        qs: QuerySet = self.get_initial_queryset()
        filters_dict: Dict[str, Any] = {}
        excludes_dict: Dict[str, Any] = {}

        datatime_filters: List[List[str, str, datetime]] = []
        location_filters: List[List[str, str, str]] = []
        
        for _fitler in self.REQUEST_FILTERS:

            filter_name: str = _fitler[0]
            fitler_comparison: str = _fitler[1]
            value: Any = _fitler[2]

            if filter_name in ["created", "creator"]:

                if  isinstance(value, str) and self.FIELDS_FILTERS[filter_name] == "datetime" \
                    and not fitler_comparison in ["year", "month", "day", "week_day", "week", "quarter"]:

                    value = datetime.strptime(value, '%Y/%m/%d %H:%M:%S')

                if fitler_comparison == "equal":

                    filters_dict[f"{filter_name}"] = value

                elif fitler_comparison == "different":

                    excludes_dict[f"{filter_name}"] = value

                else:

                    filters_dict[f"{filter_name}__{fitler_comparison}"] = value

            elif self.FIELDS_FILTERS[filter_name] == "datetime" \
                and not fitler_comparison in ["year", "month", "day", "week_day", "week", "quarter"] :

                datatime_filters.append(
                    [filter_name, fitler_comparison, value]
                )

            elif self.FIELDS_FILTERS[filter_name] == "location":

                location_filters.append(
                    [filter_name, fitler_comparison, value]
                )

            else:

                if fitler_comparison == "equal":

                    filters_dict[f"register__{filter_name}"] = value

                elif fitler_comparison == "different":

                    excludes_dict[f"register__{filter_name}"] = value

                else:

                    filters_dict[f"register__{filter_name}__{fitler_comparison}"] = value

        if filters_dict:

            qs: QuerySet = qs.filter(**filters_dict)

        if excludes_dict:

            qs: QuerySet = qs.exclude(**excludes_dict)

        qs: List[DriveRegisterModel] = location_filters_func(location_filters, qs, self.request.data['operator'])

        for _filter in datatime_filters:

            qs: List[DriveRegisterModel] = list(
                filter(
                    lambda x: filter_datetime(x, _filter),
                    qs
                )   
            )

        return qs

    def get_instance_to_dict_to_file(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        def get_value(self, key: str, value: any) -> any:

            type_dict_validator: Dict[str, Callable] = {
                'select_dynamic_services': lambda x: x,
                'select_dynamic_rut': lambda x: x,
                'select_dynamic_users_iris': lambda x: self.users_iris[x],
                'select_dynamic_location': lambda x: x if x == None else self.street_locations[int(x.split(':')[0])],
                'select_dynamic_plans': lambda x: self.plans_info[x],
                'select_static': lambda x: x,
                'int': lambda x: x,
                'datetime': lambda x: x,
                'logic': lambda x: "Verdadero" if x else "Falso",
                'string': lambda x: x,
                'phone': lambda x: x,
                'email': lambda x: x,
                'float': lambda x: x
            }

            return type_dict_validator[self.name_type[key]['type']](value)

        result: Dict[str, any] = {
            "operator": instance.operator,
            "created": instance.created,
            "creator": instance.creator.username,
            "ID": instance.ID
        }

        changed_result: Dict[str, any] = { 
            key:get_value(self, key, value) for key, value in instance.register.items()
        }

        result.update(changed_result)

        return result

    def get_instance_to_dict(self, instance: BaseModel) -> Dict[str, Any]:

        """

            This function, return dict of instance

        """

        def get_value(self, key: str, value: any) -> any:

            type_dict_validator: Dict[str, Callable] = {
                'select_dynamic_services': lambda x, y, z: {
                    'type': 'select_dynamic_services',
                    'value': y,
                    'required': z,
                },
                'select_dynamic_rut': lambda x, y, z: {
                    'type': 'select_dynamic_rut',
                    'value': y,
                    'required': z,
                },
                'select_dynamic_users_iris': lambda x, y, z: {
                    'type': 'select_dynamic_users_iris',
                    'value': y,
                    'required': z,
                    'values': [ {'name':nombre, 'value':id} for id, nombre in self.users_iris.items() ]
                },
                'select_dynamic_location': lambda x, y, z: {
                    'type': 'select_dynamic_location',
                    'value': y if y == None else self.street_locations[int(y.split(':')[0])],
                    'required': z
                },
                'select_dynamic_plans': lambda x, y, z: {
                    'type': 'select_dynamic_plans',
                    'value': y,
                    'required': z,
                    'values': [ {'name':nombre, 'value':id} for id, nombre in self.plans_info.items() ]
                },
                'select_static': lambda x, y, z: {
                    'type': 'select_static',
                    'value': y,
                    'required': z,
                    'values': [ {'name':nombre, 'value':id} for id, nombre in self.select_static_info[x].items() ]
                },
                'int': lambda x, y, z: {
                    'type': 'int',
                    'value': y,
                    'required': z
                },
                'datetime': lambda x, y, z: {
                    'type': 'datetime',
                    'value': y,
                    'required': z
                },
                'logic': lambda x, y, z: {
                    'type': 'logic',
                    'value': "Verdadero" if y else "Falso",
                    'required': z
                },
                'string': lambda x, y, z: {
                    'type': 'string',
                    'value': y,
                    'required': z
                },
                'phone': lambda x, y, z: {
                    'type': 'phone',
                    'value': y,
                    'required': z
                },
                'email': lambda x, y, z: {
                    'type': 'email',
                    'value': y,
                    'required': z
                },
                'float': lambda x, y, z: {
                    'type': 'float',
                    'value': y,
                    'required': z
                }

            }

            return type_dict_validator[self.name_type[key]['type']](key, value, self.name_type[key]['required'])

        result: Dict[str, any] = {
            "operator": instance.operator.ID,
            "created": instance.created,
            "creator": instance.creator.username,
            "ID": instance.ID
        }

        changed_result: Dict[str, any] = { 
            key:get_value(self, key, value) for key, value in instance.register.items()
        }

        result.update(changed_result)

        return result

    def get_ordered_queryset(self, qs: QuerySet) -> QuerySet:

        """

            This function, order queryset

        """

        if self.COLUMN_ORDER_FIELD and self.COLUMN_ORDER_TYPE and qs:

            column_order: str = self.COLUMN_ORDER_FIELD

            if column_order in ["operator", "created", "creator", "ID"]:

                if self.COLUMN_ORDER_TYPE == "desc":

                    qs: List[DriveRegisterModel] = sorted(
                        qs,
                        key=lambda x: getattr( x, column_order)
                    )

                else:

                    qs: List[DriveRegisterModel] = sorted(
                        qs,
                        key=lambda x: getattr( x, column_order),
                        reverse=True
                    )

            elif self.FIELDS_FILTERS[column_order] == 'select_dynamic_plans':

                global_preferences: Dict[str, Any] = global_preferences_registry.manager()

                matrix_token: str = global_preferences['matrix__matrix_token']
                matrix_url: str = global_preferences['general__matrix_domain'] + 'pai/v1/plans/'

                headers: Dict[str, str] = {
                    'AUTHORIZATION': matrix_token,
                    'content-type': 'application/json'
                }

                response: List[Dict[str, int]] = requests.get(
                    url=matrix_url,
                    headers=headers,
                    verify=False,
                    params={
                        'fields':'id',
                        'ordering': "-name" if self.COLUMN_ORDER_TYPE == "desc" else "name",
                        'limit': 1000000,
                        'offset': 0
                    }
                ).json()['results']

                plans_position: Dict[int,int] = {
                    value['id']:i for i, value in response
                }

                if self.COLUMN_ORDER_TYPE == "desc":

                    qs: List[DriveRegisterModel] = sorted(
                        qs,
                        key=lambda x: plans_position[x.register[column_order]]
                    )

                else:

                    qs: List[DriveRegisterModel] = sorted(
                        qs,
                        key=lambda x: plans_position[x.register[column_order]],
                        reverse=True
                    )

            else:

                if self.COLUMN_ORDER_TYPE == "desc":

                    qs: List[DriveRegisterModel] = sorted(
                        qs,
                        key=lambda x: x.register[column_order]
                    )

                else:

                    qs: List[DriveRegisterModel] = sorted(
                        qs,
                        key=lambda x: x.register[column_order],
                        reverse=True
                    )

        return qs

    def get_data(self) -> JsonResponse:

        """

            This function, return dict of instance

        """

        def get_locations( self, qs : List[DriveRegisterModel]):

            locations_fields: List[str] = [ definition['name'] for definition in self.DEFINITION.definition if definition['type'] == 'select_dynamic_location' ]

            street_locations: List[int] = []

            if locations_fields:
                
                street_locations: List[int] = list(
                    set(
                        reduce(
                            lambda x, y: x + y,
                            map(
                                lambda x : [ int(value.split(':')[0]) for key, value in x.register.items() if value != None and key in locations_fields ],
                                qs
                            )
                        )
                    )
                )

                global_preferences: Dict[str, Any] = global_preferences_registry.manager()

                pulso_token: str = global_preferences['pulso__pulso_check_token']
                pulso_url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility-iris/'

                _random: int = random.randint(5000, 5000000)

                self.street_locations: Dict[int, str] = []

                if street_locations:

                    street_locations = requests.post(
                        url=f"{pulso_url}?no_cache={_random}",
                        json={
                            "street_location":street_locations,
                        },
                        headers={
                            'Authorization':pulso_token
                        },
                        verify=False,
                    ).json()

                    self.street_locations: Dict[int, str] = { location['street_location_id']:location['street_location'] for location in street_locations}

        def get_plans( self, qs : List[DriveRegisterModel]):

            global_preferences: Dict[str, Any] = global_preferences_registry.manager()

            matrix_token: str = global_preferences['matrix__matrix_token']
            matrix_url: str = global_preferences['general__matrix_domain'] + 'plans/'

            data : Dict[str, any] = {
                'limit': 1000000,
                'offset': 0,
                'fields':'id,name'
            }

            plans_data = requests.get(
                url=matrix_url,
                params=data,
                headers={
                    'Authorization':matrix_token
                },
                verify=False,
            ).json()

            self.plans_info: Dict[int, str] = { customer['id']:customer['name'] for customer in plans_data['results']}

        def get_select_static( self, qs : List[DriveRegisterModel]):

            customers_fields: List[str] = [ definition['name'] for definition in self.DEFINITION.definition if definition['type'] == 'select_static' ]

            self.select_static_info : Dict[str, Dict[int, str]] = {
                definition['name']:{ item['value']:item['name'] for item in definition['values']}
                for definition in self.DEFINITION.definition if definition['name'] in customers_fields
            }

        def get_iris_user(self ):

            self.users_iris : Dict[int, str] = {
                user.pk:user.username for user in User.objects.all()
            }

        self.get_initial_params()
        
        result: List[Dict[str, Any]] = []

        qs: QuerySet = self.get_ordered_queryset( self.get_filtered_search( self.get_filtered_queryset() ) )

        qs_size: int = len(qs)

        if qs:

            qs: QuerySet = qs[self.START: self.START + self.OFFSET]

            get_locations( self, qs )
            get_plans( self, qs )
            get_select_static( self, qs )
            get_iris_user( self )

            self.name_type : Dict[str, str] = { item['name']:{'type':item['type'], 'required':item['required']} for item in self.DEFINITION.definition}

            result: List[Dict[str, Any]] = list( map(lambda instance: self.get_instance_to_dict(instance), qs) )

        result = {"size": qs_size, "result": result}
        return JsonResponse( result, safe=False)

    model: BaseModel = DriveRegisterModel


