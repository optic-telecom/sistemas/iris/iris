

import pytest
import requests
from django.contrib.auth.models import User
from django.urls import reverse
from typing import Any, Dict, List
import json
from dynamic_preferences.registries import global_preferences_registry
from unittest import mock
from django.urls import resolve
from drives.typing_validations import *

class BaseTest():

    def reverse_url(self, url: str, args: List[Any]):

        try:

            url: str = reverse(url, args=args)

        except:

            assert False

        return url

def mock_services_invalid(*args, **kwargs):
    
    class MockResponse:

        status_code: int = 400

        def json(self):

            return {
                'Error dynamic_services value': "Must be an existing value"
            }

    return MockResponse

def mock_customer_invalid(*args, **kwargs):
    
    class MockResponse:

        status_code: int = 400

        def json():

            return {
                'Error dynamic_customers value': "Must be an existing value"
            }

    return MockResponse

def mock_plan_invalid(*args, **kwargs):
    
    class MockResponse:

        status_code: int = 400

        def json():

            return {
                'Error dynamic_plans value': "Must be an existing value"
            }

    return MockResponse

def mock_location_invalid(*args, **kwargs):
    
    class MockResponse:

        status_code: int = 400

        def json():

            return {
                "Error address value": "Must be an existing address"
            }

    return MockResponse

def mock_valid_response(*args, **kwargs):
    
    class MockResponse:

        status_code: int = 200

        def json():

            return {}

    return MockResponse 

@pytest.mark.django_db
@pytest.mark.parametrize(
    'url_base,url_base_detail', [
        (
            'drives:definition-list',
            'drives:definition-detail'
        )
    ]
)
class TestPermissionWriteOrUpdate(BaseTest):

    """

        Check user permissions
    
    """

    
    @pytest.mark.parametrize(
        'data, error_message', [
            (
                {
                    "definition":[
                        {
                            'type':'int',
                            'required':True,
                            'default_value':1,
                            'name':'int_prueba'
                        },
                        {
                            'type':'int',
                            'required':True,
                            'default_value':1,
                            'name':'int_prueba'
                        }
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description repeat error': "Name int_prueba is repeated"
                }
            ),
            (
                {
                    "definition":{},
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Type error': " 'definition' field must be list type "
                }
            ),
            (
                {
                    "definition":[[]],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Value type error': "Value of definition must be dict type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'yyy',
                            'required':True,
                            'default_value':1,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description type error': f"Description type of int_prueba is not valid"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'int',
                            'required':'yyy',
                            'default_value':1,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description required error': f"Description required of int_prueba is not valid"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'int',
                            'required':True,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description error': f"Description of int_prueba no contains any this keys: 'type', 'required', 'default_value', 'name'"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'int',
                            'required':True,
                            'default_value':'f',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error int value': "Value must be int type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'float',
                            'required':True,
                            'default_value':'f',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error float value': "Value must be float type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'logic',
                            'required':True,
                            'default_value':'f',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error logic value': "Value must be bool type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'one',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description field error: static selects': "Field values is required"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'one',
                            'name':'int_prueba',
                            'values':[]
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description error: static selects': "There must be at least one option"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'one',
                            'name':'int_prueba',
                            'values':[{'value':'one'}]
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: static selects': "Description default_value of static is not valid, must be list of dict type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'one',
                            'name':'int_prueba',
                            'values':[{'name':'one'}]
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: static selects': "Description default_value of static is not valid, must be list of dict type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'one',
                            'name':'int_prueba',
                            'values':[[]]
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: static selects': "Description default_value of static is not valid, must be list of dict type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'one',
                            'name':'int_prueba',
                            'values':[{'name':1, 'value':'value'}]
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: static selects': "Description default_value of static is not valid, must be list of dict type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'one',
                            'name':'int_prueba',
                            'values':[{'name':'value', 'value':1}]
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: static selects': "Description default_value of static is not valid, must be list of dict type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'one',
                            'name':'int_prueba',
                            'values':[{'name':'value', 'value':'value'}]
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: static selects': "Default value, must be in the list"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_static',
                            'required':True,
                            'default_value':'value',
                            'name':'int_prueba',
                            'values':[{'name':'value', 'value':'value'}, {'name':'value', 'value':'value'}]
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: static selects': "Invalid repeats values"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'datetime',
                            'required':True,
                            'default_value':1,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error default_value value: datetime type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'datetime',
                            'required':True,
                            'default_value':'2019-11-22 22:22:22',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error default_value format: datetime format': "Description default_valueis not valid, must be datatime this format: %Y/%m/%d %H:%M:%S"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'phone',
                            'required':True,
                            'default_value':1,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error default_value value: phone type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'phone',
                            'required':True,
                            'default_value':'+51dfrs5851fdfere',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error default_value format: phone format': "Description default_value is not valid, Invalid phone format"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'email',
                            'required':True,
                            'default_value':1,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error default_value value: email': "Description default_value is not valid, must be str type and valid email"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'string',
                            'required':True,
                            'default_value':1,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: string': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_dynamic_services',
                            'required':True,
                            'default_value':'f',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_services type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_dynamic_rut',
                            'required':True,
                            'default_value':1,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_customers type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_dynamic_users_iris',
                            'required':True,
                            'default_value':'ff',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_users_iris type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_dynamic_users_iris',
                            'required':True,
                            'default_value':99999999999999,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error dynamic_users_iris value': "Must be an existing value"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_dynamic_location',
                            'required':True,
                            'default_value':99999999999999,
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: location type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_dynamic_location',
                            'required':True,
                            'default_value':'99999999999999',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Error location value': "Must be an a valid format : street_location:operator"
                }
            ),
            (
                {
                    "definition":[
                        {
                            'type':'select_dynamic_plans',
                            'required':True,
                            'default_value':'999999999999g99',
                            'name':'int_prueba'
                        },
                    ],
                    "name":"prueba",
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_plans type': "Description default_value is not valid, must be int type"
                }
            ),
        ]
    )
    def _test_user_with_permissions_write_instance_errors(self, url_base: str, admin_client, url_base_detail: str, data: any, error_message: Dict[str, str]):

        url_base: str = self.reverse_url(url_base, [])

        response = admin_client.post( 
            path=url_base,
            data=json.dumps(data),
            content_type='application/json',
        )

        assert response.status_code == 400 and error_message == response.json()

    def _test_user_with_permissions_write_instance(self, admin_client, url_base: str, url_base_detail: str):

        user = User(username='prueba')
        user.save()

        data: Dict[str, Any] = {
            "definition":[
                {
                    'type':'int',
                    'required':True,
                    'default_value':1,
                    'name':'int'
                },
                {
                    'type':'float',
                    'required':True,
                    'default_value':1.1,
                    'name':'float'
                },
                {
                    'type':'select_static',
                    'required':True,
                    'default_value':'one',
                    'name':'select_static',
                    'values':[{'name':'one', 'value':'one'}]
                },
                {
                    'type':'logic',
                    'required':True,
                    'default_value':True,
                    'name':'logic'
                },
                {
                    'type':'datetime',
                    'required':True,
                    'default_value':'2019/12/22 08:09:05',
                    'name':'datetime'
                },
                {
                    'type':'phone',
                    'required':True,
                    'default_value':'+51235621456',
                    'name':'phone'
                },
                {
                    'type':'email',
                    'required':True,
                    'default_value':'prueba@gmail.com',
                    'name':'email'
                },
                {
                    'type':'string',
                    'required':True,
                    'default_value':'prueba@gmail.com',
                    'name':'string'
                },
                {
                    'type':'select_dynamic_services',
                    'required':True,
                    'default_value':5000,
                    'name':'select_dynamic_services'
                },
                {
                    'type':'select_dynamic_rut',
                    'required':True,
                    'default_value':'6.616.979-0',
                    'name':'select_dynamic_rut'
                },
                {
                    'type':'select_dynamic_users_iris',
                    'required':True,
                    'default_value':User.objects.all()[0].pk,
                    'name':'select_dynamic_users_iris'
                },
                {
                    'type':'select_dynamic_location',
                    'required':True,
                    'default_value':"248064:2",
                    'name':'select_dynamic_location'
                },
                {
                    'type':'select_dynamic_plans',
                    'required':True,
                    'default_value':144,
                    'name':'select_dynamic_plans'
                },
            ],
            "name":"prueba",
            "operator":1
        }

        url_base: str = self.reverse_url(url_base, [])

        response = admin_client.post( 
            path=url_base,
            data=json.dumps(data),
            content_type='application/json',
        )

        assert response.status_code == 201

@mock.patch('requests.get', side_effect=mock_services_invalid)
def _test_user_with_permissions_write_instance_errors_service(self, admin_client):

    try:

        validate_dynamic_services(999999999)
        assert True == False

    except Exception as e:

        pass

@mock.patch('requests.get', side_effect=mock_customer_invalid)
def _test_user_with_permissions_write_instance_errors_customer(self, admin_client):

    try:

        validate_dynamic_customers('999999999')
        assert True == False

    except Exception as e:

        pass

@mock.patch('requests.get', side_effect=mock_location_invalid)
def _test_user_with_permissions_write_instance_errors_location(self, admin_client):

    try:

        validate_location('5632:2')
        assert True == False

    except Exception as e:

        pass

@mock.patch('requests.get', side_effect=mock_plan_invalid)
def test_user_with_permissions_write_instance_errors_plan(self, admin_client):

    data = {
        "definition":[
            {
                'type':'select_dynamic_plans',
                'required':True,
                'default_value':'999999999999g99',
                'name':'int_prueba'
            },
        ],
        "name":"prueba",
        "operator":1
    }

    response = admin_client.post( 
        path='/api/v1/drives/definition/',
        data=json.dumps(data),
        content_type='application/json',
    )

    assert response.status_code == 400


    """try:

        validate_dynamic_plans(9999)
        assert True == False

    except Exception as e:

        pass"""