
import pytest
import requests
from django.contrib.auth.models import User
from django.urls import reverse
from typing import Any, Dict, List
import json
from dynamic_preferences.registries import global_preferences_registry
from unittest import mock
from django.urls import resolve
from drives.typing_validations import *

class BaseTest():

    def reverse_url(self, url: str, args: List[Any]):

        try:

            url: str = reverse(url, args=args)

        except:

            assert False

        return url

@pytest.mark.django_db
@pytest.mark.parametrize(
    'url_base,url_base_detail', [
        (
            'drives:register-list',
            'drives:register-detail'
        )
    ]
)
class TestPermissionWriteOrUpdate(BaseTest):

    """

        Check user permissions
    
    """

    data = [
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2"
                    },
                    "operator":1
                },
                {
                    'required value': "Value of select_dynamic_plans field is required"
                }
            ),
            (
                {
                    "register":{
                        'int':'f',
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error int value': "Value must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':'ff',
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error float value': "Value must be float type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':'True',
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error logic value': "Value must be bool type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':1,
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value value: datetime type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'ddd',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value format: datetime format': "Description default_valueis not valid, must be datatime this format: %Y/%m/%d %H:%M:%S"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':1,
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value value: phone type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'ffff',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value format: phone format': "Description default_value is not valid, Invalid phone format"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':1,
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value value: email': "Description default_value is not valid, must be str type and valid email"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'no es correo',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value value: email': "Description default_value is not valid, must be str type and valid email"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':1,
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: string': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':'5000',
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_services type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':999999,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error dynamic_services value': "Must be an existing value"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':1,
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_customers type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'lll',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error dynamic_customers value': "Must be an existing value"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':'None',
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_users_iris type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':1,
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: location type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':'ppp',
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error location value': "Must be an a valid format : street_location:operator"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':'k'
                    },
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_plans type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':5000
                    },
                    "operator":1
                },
                {
                    'Error dynamic_plans value': "Must be an existing value"
                }
            )
    ]

    @pytest.mark.parametrize(
        'data, error_message', [
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2"
                    },
                    "operator":1
                },
                {
                    'required value': "Value of select_dynamic_plans field is required"
                }
            ),
            (
                {
                    "register":{
                        'int':'f',
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error int value': "Value must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':'ff',
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error float value': "Value must be float type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':'True',
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error logic value': "Value must be bool type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':1,
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value value: datetime type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'ddd',
                        'phone':'+51235621456',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value format: datetime format': "Description default_valueis not valid, must be datatime this format: %Y/%m/%d %H:%M:%S"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':1,
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value value: phone type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'ffff',
                        'email':'prueba@gmail.com',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value format: phone format': "Description default_value is not valid, Invalid phone format"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':1,
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value value: email': "Description default_value is not valid, must be str type and valid email"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'no es correo',
                        'string':'int_prueba',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error default_value value: email': "Description default_value is not valid, must be str type and valid email"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':1,
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: string': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':'5000',
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_services type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':999999,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error dynamic_services value': "Must be an existing value"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':1,
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_customers type': "Description default_value is not valid, must be str type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'lll',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error dynamic_customers value': "Must be an existing value"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':'None',
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_users_iris type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':1,
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Description default_value error: location type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':'ppp',
                        'select_dynamic_plans':144
                    },
                    "operator":1
                },
                {
                    'Error location value': "Must be an a valid format : street_location:operator"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':'k'
                    },
                    "operator":1
                },
                {
                    'Description default_value error: dynamic_plans type': "Description default_value is not valid, must be int type"
                }
            ),
            (
                {
                    "register":{
                        'int':1,
                        'float':1.1,
                        'select_static':'one',
                        'logic':True,
                        'datetime':'2019/12/22 08:09:05',
                        'phone':'+51235621456',
                        'email':'cris@optic.cl',
                        'string':'f',
                        'select_dynamic_services':5000,
                        'select_dynamic_rut':'6.616.979-0',
                        'select_dynamic_users_iris':None,
                        'select_dynamic_location':"248064:2",
                        'select_dynamic_plans':5000
                    },
                    "operator":1
                },
                {
                    'Error dynamic_plans value': "Must be an existing value"
                }
            )
        ]
    )
    def test_user_with_permissions_write_instance(self, admin_client, url_base: str, url_base_detail: str, data: Dict[str,any], error_message: Dict[str, str] ):

        data_definition: Dict[str, Any] = {
            "definition":[
                {
                    'type':'int',
                    'required':True,
                    'default_value':1,
                    'name':'int'
                },
                {
                    'type':'float',
                    'required':True,
                    'default_value':1.1,
                    'name':'float'
                },
                {
                    'type':'select_static',
                    'required':True,
                    'default_value':'one',
                    'name':'select_static',
                    'values':[{'name':'one', 'value':'one'}]
                },
                {
                    'type':'logic',
                    'required':True,
                    'default_value':True,
                    'name':'logic'
                },
                {
                    'type':'datetime',
                    'required':True,
                    'default_value':'2019/12/22 08:09:05',
                    'name':'datetime'
                },
                {
                    'type':'phone',
                    'required':True,
                    'default_value':'+51235621456',
                    'name':'phone'
                },
                {
                    'type':'email',
                    'required':True,
                    'default_value':'prueba@gmail.com',
                    'name':'email'
                },
                {
                    'type':'string',
                    'required':True,
                    'default_value':'prueba@gmail.com',
                    'name':'string'
                },
                {
                    'type':'select_dynamic_services',
                    'required':True,
                    'default_value':5000,
                    'name':'select_dynamic_services'
                },
                {
                    'type':'select_dynamic_rut',
                    'required':True,
                    'default_value':'6.616.979-0',
                    'name':'select_dynamic_rut'
                },
                {
                    'type':'select_dynamic_users_iris',
                    'required':True,
                    'default_value':User.objects.all()[0].pk,
                    'name':'select_dynamic_users_iris'
                },
                {
                    'type':'select_dynamic_location',
                    'required':True,
                    'default_value':"248064:2",
                    'name':'select_dynamic_location'
                },
                {
                    'type':'select_dynamic_plans',
                    'required':True,
                    'default_value':144,
                    'name':'select_dynamic_plans'
                },
            ],
            "name":"prueba",
            "operator":1
        }

        response = admin_client.post( 
            path='/api/v1/drives/definition/',
            data=json.dumps(data_definition),
            content_type='application/json',
        )

        assert response.status_code == 201

        url_base: str = self.reverse_url(url_base, [])

        data['drive'] = response.json()['ID']

        if 'select_dynamic_users_iris' in data['register'] and data['register']['select_dynamic_users_iris'] == None:

            user = User(username='prueba')
            user.save()

            data['register']['select_dynamic_users_iris'] = user.pk

        response = admin_client.post( 
            path=url_base,
            data=json.dumps(data),
            content_type='application/json',
        )

        assert response.status_code == 400 and error_message == response.json()