from typing import Any, Dict, List, Callable, Union
import ast
from django.http import JsonResponse
from rest_framework.decorators import action
from rest_framework.serializers import ValidationError
from common.viewsets import BaseViewSet
from datetime import datetime
from .models import DriveDefinitionModel, DriveRegisterModel
from .serializers import DriveDefinitionSerializer, DriveRegisterSerializer
from rest_framework import serializers
from django.db.models.query import QuerySet
from django.contrib.auth.models import User
from django.contrib.auth.decorators import permission_required  
from django.utils.decorators import method_decorator  
from .datatables import DefinitionTablesDatatable, RegistersDatatable
from common.utilitarian import get_token, matrix_url
from validate_email import validate_email
import requests
from dynamic_preferences.registries import global_preferences_registry
import phonenumbers
from multiprocessing import Pool
import threading

from .documentation import DocumentationDriveDefinition, DocumentationDriveRegister

class DriveDefinitionView(BaseViewSet):

    queryset: QuerySet = DriveDefinitionModel.objects.filter(deleted=False)
    serializer_class = DriveDefinitionSerializer
    documentation_class = DocumentationDriveDefinition()

    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('drives.add_drivedefinitionmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)
    
    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('drives.change_drivedefinitionmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def save_instance(self, request, instance):

        instance.updater: User = request.user
        instance.updated: datetime = datetime.now()
        instance.save()

    @action(detail=False, methods=['get'])
    def datatables_struct(self, request) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class DriveDefinitionView
            :type self: DriveDefinitionView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(DefinitionTablesDatatable(request).get_struct(), safe=True)

    @action(detail=False, methods=['post'])
    def datatables(self, request) -> JsonResponse:

        """
        
            This funtion return struct data

            :param self: Instance of Class DriveDefinitionView
            :type self: DriveDefinitionView
            :param request: data of request
            :type request: request
        
        """
        
        return DefinitionTablesDatatable(request).get_data()

    @action(detail=True, methods=['post'])
    def update_column(self, request, pk) -> JsonResponse:

        """
            This function updated column and registers

            :param self: Instance of Class DriveDefinitionView
            :type self: DriveDefinitionView
            :param request: Instance of Class Request
            :type request: Request
            :param pk: Int, indicate pk of instance
            :type pk: int

            :returns: Response
            :rtype: JsonResponse
        
        """
  
        try:

            instance: DriveDefinitionModel = DriveDefinitionModel.objects.get(ID=pk)

        except:

            raise ValidationError({
                "Error instance": f"Instance with id == {pk} don`t exist."
            })

        #Validate all required fields exist
        post_fields: List[str] = list( set( ['current_name', 'new_name', 'new_required', 'new_default_value'] ) - set( request.data.keys() ) )

        if  not post_fields:

            current_name: str = request.data['current_name']
            new_name: str = request.data['new_name']
            new_required: bool = request.data['new_required']
            new_default_value: Any = request.data['new_default_value']

            #Check if current name exists
            exist_current_name: bool = current_name in [ x['name'] for x in instance.definition]
            #Check if the new name is equal to the current name and new name is not in the definition
            valid_new_name: bool = new_name == current_name or not new_name in [ x['name'] for x in instance.definition]

            if exist_current_name and valid_new_name:

                #Get the definition to update
                old_definition: Dict[str, Any] = list( filter(lambda x: x['name'] == current_name, instance.definition) )[0]

                #Get all definitions except the one to update
                instance.definition: List[ Dict[str, Any] ] = list( filter(lambda x: x['name'] != current_name, instance.definition) )

                new_definition: Dict[str, Any] = {
                    "type": old_definition['type'],
                    "required": new_required, 
                    "default_value": new_default_value,
                    "name":new_name
                }

                if old_definition['type'] == 'select_static':

                    if not 'values' in request.data:

                        raise ValidationError({
                            "Required field": f"Required values field"
                        })

                    new_definition['values'] = request.data['values']

                instance.definition.append(new_definition)

                self.save_instance(request, instance)

                #Update all records of the definition
                if current_name != new_name:

                    for register in DriveRegisterModel.objects.filter(drive=instance):

                        register.register[new_name]: Any = register.register.pop(current_name)

                        self.save_instance(request, register)

            else:

                raise ValidationError({
                    "Error column": f"Column called {current_name} don`t exist"
                })

            pass

        else:

            raise ValidationError({ f"Field {field}":"Required field"  for field in post_fields})

        return JsonResponse({}, safe=True)

    @action(detail=True, methods=['post'])
    def create_column(self, request, pk) -> JsonResponse:

        """
            This function create column and registers

            :param self: Instance of Class DriveDefinitionView
            :type self: DriveDefinitionView
            :param request: Instance of Class Request
            :type request: Request
            :param pk: Int, indicate pk of instance
            :type pk: int

            :returns: Response
            :rtype: JsonResponse
        
        """

        def validate_value(drive_definition: Dict[str, Any]):

            def validate_float(value: Dict[str, Any]):
                
                value: float = value['default_value']

                if not isinstance(value, float):

                    raise ValidationError({
                        'Error float value': "Value must be float type"
                    })

            def validate__email(value: Dict[str, Any]):

                value: str = value['default_value']

                if not ( isinstance(value, str) and validate_email(value) ):

                    raise ValidationError({
                        'Error default_value value: email': "Description default_value is not valid, must be str type and valid email"
                    })

            def validate_phone(value: Dict[str, Any]):

                value: str = value['default_value']

                if not isinstance(value, str):

                    raise ValidationError({
                        'Error default_value value: phone type': "Description default_value is not valid, must be str type"
                    })

                try:

                    phonenumbers.parse(value, None)

                except:

                    raise ValidationError({
                        'Error default_value format: phone format': "Description default_value is not valid, Invalid phone format"
                    })

            def validate_string(value: Dict[str, Any]):

                value: str = value['default_value']

                if not  isinstance(value, str):

                    raise ValidationError({
                        'Description default_value error: string': "Description default_value is not valid, must be str type"
                    })

            def validate_logic(value: Dict[str, Any]):

                value: bool = value['default_value']

                if not isinstance(value, bool):

                    raise ValidationError({
                        'Error logic value': "Value must be bool type"
                    })

            def validate_datetime(value: Dict[str, Any]):

                value: str = value['default_value']

                if not isinstance(value, str):

                    raise ValidationError({
                        'Error datetime value': "Value must be str type"
                    })

                try:

                    datetime.strptime(value, '%Y/%m/%d %H:%M:%S')

                except Exception as e:

                    raise ValidationError({
                        'Error datetime format': "Value must be datatime this format: %Y/%m/%d %H:%M:%S"
                    })

            def validate_int(value: Dict[str, Any]):

                value: int = value['default_value']

                if isinstance(value, bool) or not isinstance(value, int):

                    raise ValidationError({
                        'Error int value': "Value must be int type"
                    })

            def validate_select_static(value: Dict[str, Any]):

                def validate_value( default_value_description: List [ Dict[ Union[int, float, bool, str], str] ] ):


                    for value in default_value_description['values']:

                        if not ( isinstance(value, dict) and "value" in value and "name" in value and isinstance(value["name"], str) and isinstance(value["value"], str ) ):

                            raise ValidationError({
                                'Description default_value error: static selects': "Description default_value of static is not valid, must be list of dict type"
                            })

                    if not [ x for x in default_value_description['values'] if x['value'] == default_value_description['default_value'] ] and default_value_description['default_value'] != None:

                        raise ValidationError({
                            'Description default_value error: static selects': "Default value, must be in the list"
                        })

                    if not len(default_value_description['values']) == len(set([x['value'] for x in default_value_description['values']])):

                        raise ValidationError({
                            'Description default_value error: static selects': "Invalid repeats values"
                        })

                if isinstance(value['values'], list) and validate_value( value ):

                    raise ValidationError({
                        'Description default_value error: static selects': "Description default_value of static is not valid, must be list of dict type"
                    })

            def validate_dynamic_plans(value: Dict[str, Any]):

                value: int = value['default_value']

                if not isinstance(value, int):

                    raise ValidationError({
                        'Description default_value error: dynamic_plans type': "Description default_value is not valid, must be int type"
                    })

                url_plans: str = matrix_url('plans')
                url_plans: str = f'{url_plans}{value}/'

                response = requests.get(
                    url=url_plans,
                    verify=False,
                    headers={
                        "Authorization": get_token()
                    }
                )

                if response.status_code != 200:

                    raise ValidationError({
                        'Error dynamic_plans value': "Must be an existing value"
                    })

            def validate_dynamic_customers(value: Dict[str, Any]):

                value: int = value['default_value']

                if not isinstance(value, str):

                    raise ValidationError({
                        'Description default_value error: dynamic_customers type': "Description default_value is not valid, must be str type"
                    })

                url_customers: str = matrix_url('customers')
                url_customers: str = f'{url_customers}'

                params: Dict[str, str] = {
                    'fields':'id',
                    'rut':value
                }

                response = requests.get(
                    url=url_customers,
                    verify=False,
                    headers={
                        "Authorization": get_token()
                    },
                    params=params
                )

                if response.status_code != 200 and response.json():

                    raise ValidationError({
                        'Error dynamic_customers value': "Must be an existing value"
                    })

            def validate_dynamic_services(value: Dict[str, Any]):

                value: int = value['default_value']

                if not isinstance(value, int):

                    raise ValidationError({
                        'Description default_value error: dynamic_services type': "Description default_value is not valid, must be int type"
                    })

                url_services: str = matrix_url('services')
                url_services: str = f'{url_services}{value}/'
                
                response = requests.get(
                    url=url_services,
                    verify=False,
                    headers={
                        "Authorization": get_token()
                    },
                )

                if response.status_code != 200:

                    raise ValidationError({
                        'Error dynamic_services value': "Must be an existing value"
                    })

            def validate_dynamic_users_iris(value: Dict[str, Any]):

                value: int = value['default_value']

                if not isinstance(value, int):

                    raise ValidationError({
                        'Description default_value error: dynamic_users_iris type': "Description default_value is not valid, must be int type"
                    })

                try:

                    User.objects.get(pk=value)

                except:

                    raise ValidationError({
                        'Error dynamic_users_iris value': "Must be an existing value"
                    })

            def validate_location(value: Dict[str, Any]):

                value: int = value['default_value']

                if not isinstance(value, str):

                    raise ValidationError({
                        'Description default_value error: location type': "Description default_value is not valid, must be int type"
                    })

                global_preferences: Dict[str, Any] = global_preferences_registry.manager()

                #Get headers and token
                headers: Dict[str] = {
                    'AUTHORIZATION': global_preferences['matrix__matrix_token']
                }

                #Get url of pulso
                url: str = global_preferences['pulso__domain_url'] + '/api/v1/factibility/'

                try:

                    street_location = value.split(':')[0]
                    operator = value.split(':')[1]

                except Exception as e:

                    raise ValidationError({
                        'Error location value': "Must be an a valid format : street_location:operator"
                    })

                #Check feasibility
                response_feasibility: Dict[str] = requests.get(
                    url=url,
                    headers=headers,
                    verify=False,
                    params={
                        'street_location':street_location,
                        'operator':operator,
                    },
                )

                response_feasibility_data: Dict[str, str] = response_feasibility.json()
                response_feasibility_status: int = response_feasibility.status_code

                if response_feasibility_status != 200 or response_feasibility_data.get('error', ''):

                    raise ValidationError({
                        "Error address value": "Must be an existing address",
                    })

            type_dict_validator: Dict[str, Callable] = {
                'select_dynamic_services': validate_dynamic_services,
                'select_dynamic_rut': validate_dynamic_customers,
                'select_dynamic_users_iris': validate_dynamic_users_iris,
                'select_dynamic_location': validate_location,
                'select_dynamic_plans': validate_dynamic_plans,
                'select_static': validate_select_static,
                'int': validate_int,
                'datetime': validate_datetime,
                'logic': validate_logic,
                'string': validate_string,
                'phone': validate_phone,
                'email': validate__email,
                'float': validate_float,
            }
            
            type_dict_validator[drive_definition['type']](drive_definition)

        try:

            instance: DriveDefinitionModel = DriveDefinitionModel.objects.get(ID=pk)

        except:

            raise ValidationError({
                "Error instance": f"Instance with id == {pk} don`t exist."
            })

        #Validate all required fields exist
        post_fields: List[str] = list( set( ['name', 'type', 'required', 'default_value'] ) - set( request.data.keys() ) )

        if  not post_fields:

            name: str = request.data['name']
            _type: str = request.data['type']
            required: bool = request.data['required']
            default_value: Any = request.data['default_value']

            if _type == 'select_static':

                if not 'values' in request.data:

                    raise ValidationError({
                        "Required field": f"Required values field"
                    })

                values: str = request.data['values']

            if not list(filter(lambda x: x['name'] == name, instance.definition)):

                if _type == 'select_static':

                    instance.definition.append( 
                        {
                            "type": _type,
                            "required": required,
                            "default_value": default_value,
                            "name": name,
                            "values":values
                        }
                    )
                
                else:

                    instance.definition.append( 
                        {
                            "type": _type,
                            "required": required,
                            "default_value": default_value,
                            "name": name
                        }
                    )
                
                self.save_instance(request, instance)

                #Update all records of the definition
                for register in DriveRegisterModel.objects.filter(drive=instance):

                    register.register[name]: Any = default_value

                    self.save_instance(request, register)

            else:

                raise ValidationError({
                    "Error column": f"Column called {name} exist"
                })

            pass

        else:

            raise ValidationError({ f"Field {field}":"Required field"  for field in post_fields})

        return JsonResponse({}, safe=True)

    @action(detail=True, methods=['post'])
    def delete_column(self, request, pk) -> JsonResponse:

        """
            This function delete column and registers

            :param self: Instance of Class DriveDefinitionView
            :type self: DriveDefinitionView
            :param request: Instance of Class Request
            :type request: Request
            :param pk: Int, indicate pk of instance
            :type pk: int

            :returns: Response
            :rtype: JsonResponse
        
        """

        try:

            instance: DriveDefinitionModel = DriveDefinitionModel.objects.get(ID=pk)

        except:

            raise ValidationError({
                "Error instance": f"Instance with id == {pk} don`t exist."
            })

        column_name: str = request.data.get( 'column_name', None )

        #check if exist definition
        if column_name in [ x['name'] for x in instance.definition]:

            instance.definition = list( filter(lambda x: x['name'] != column_name, instance.definition) )

            self.save_instance(request, instance)

            #Update all records of the definition
            for register in DriveRegisterModel.objects.filter(drive=instance):

                del register.register[column_name]

                self.save_instance(request, register)

        else:

            raise ValidationError({
                "Invalid column": f"Column called {column_name} don`t exist."
            })

        return JsonResponse({}, safe=True)


class DriveRegisterView(BaseViewSet):

    documentation_class = DocumentationDriveRegister()

    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('drives.add_drivedefinitionmodel',raise_exception=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)
    
    @documentation_class.documentation_create_and_update()
    @method_decorator(permission_required('drives.change_drivedefinitionmodel',raise_exception=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['get'])
    def datatables_struct(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request
        
        """

        return JsonResponse(RegistersDatatable(request, pk).get_struct(), safe=True)

    @action(detail=True, methods=['post'])
    def datatables(self, request, pk) -> JsonResponse:

        """
        
            This funtion return struct data

            :param self: Instance of Class CustomerView
            :type self: CustomerView
            :param request: data of request
            :type request: request
        
        """
        
        return RegistersDatatable(request, pk).get_data()

    @action(detail=True, methods=['post'])
    def update_field_value(self, request, pk) -> JsonResponse:

        try:

            instance: DriveRegisterModel = DriveRegisterModel.objects.get(ID=pk)

        except:

            raise ValidationError({
                "Error instance": f"Instance with id == {pk} don`t exist."
            })

        data: Dict[str, any] = request.data

        instance.register[data['fields']] = data['value']
        instance.updater: User = request.user
        instance.save()

        return JsonResponse(data={})

    @action(detail=True, methods=['post'])
    def download_data(self, request, pk) -> JsonResponse:

        """
        
            This funtion return datatables data

            :param self: Instance of Class DriveRegisterView
            :type self: DriveRegisterView
            :param request: data of request
            :type request: request
        
        """

        def send_data(self, request, pk):

            RegistersDatatable(request, pk).download_data()

        threading.Thread(
            target=send_data,
            args=[ self, request, pk]
        ).start()

        return JsonResponse(data={})

    queryset = DriveRegisterModel.objects.filter(deleted=False)
    serializer_class = DriveRegisterSerializer