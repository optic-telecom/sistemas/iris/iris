from typing import Union, List

from drf_yasg.openapi import Response as Response_Openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers

from .models import DriveDefinitionModel, DriveRegisterModel
from .serializers import DriveDefinitionSerializer, DriveRegisterSerializer

class DocumentationDriveDefinition(object):

    """

        Class define documentation of DriveDefinitionModel
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of DriveDefinitionView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []

    serializer_class_body = DriveDefinitionSerializer
    serializer_class_response = DriveDefinitionSerializer

    def documentation_create_and_update(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationDriveDefinition
            :type self: DocumentationDriveDefinition

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create or update, drive definition',
            operation_description="""

                This endpoint creates a new drive definition

            """,
            responses = {
                200:Response_Openapi(
                    'Create or update drive definition',
                    self.serializer_class_response,
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )

    def documentation_get_companies(self):

        """

            This function returns information of documentation view 'get_channel'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Get companies and operators',
            operation_description="""

                This endpoint returns the companies and their assigned operators

            """,
            responses = {
                200:Response_Openapi(
                    description = 'Companies and operators',
                    examples = {
                        "application/json":[
                            {
                                "name": "Multifiber",
                                "ID": 1,
                                "operators": [
                                    {
                                        "ID": 4,
                                        "name": "a"
                                    },
                                    {
                                        "ID": 1,
                                        "name": "Optic"
                                    },
                                    {
                                        "ID": 3,
                                        "name": "Multifiber"
                                    },
                                    {
                                        "ID": 2,
                                        "name": "Bandaancha"
                                    }
                                ]
                            }
                        ]
                    },
                )
            }
        )

class DocumentationDriveRegister(object):

    """

        Class define documentation of DriveRegisterModel
    
    """

    class DocumentationSerializerVoid(serializers.Serializer):

        """

            Class define void serializer of NewContactView.
    
        """
        
        class Meta:

            fields: Union[list,str] = []

    serializer_class_body = DriveRegisterSerializer
    serializer_class_response = DriveRegisterSerializer

    def documentation_create_and_update(self):

        """

            This function return information of documentation view 'create'

            :param self: Instance of Class DocumentationTicket
            :type self: DocumentationTicket

            :returns: Function, obtains documentation information
            :rtype: function
        
        """

        return swagger_auto_schema(
            operation_id='Create or update, drive register',
            operation_description="""

                This endpoint creates a new drive register

            """,
            responses = {
                200:Response_Openapi(
                    'Create or update drive register',
                    self.serializer_class_response,
                )
            },
            query_serializer = self.serializer_class_response,
            request_body = self.serializer_class_body,
        )
