from django.db import models
from common.models import BaseModel
from django.contrib.postgres.fields import JSONField
from rest_framework.serializers import ValidationError
from functools import reduce
from typing import List, Any, Dict, Callable, Union
from .typing_validations import *


class DriveDefinitionModel(BaseModel):

    def clean(self):

        """

            This function validate DriveDefinitionModel model instance

            :param self: Instance of Class DriveDefinitionModel
            :type self: DriveDefinitionModel
        
        """

        def definition_validate(value: Dict[ str, Dict[ str, Any]]):

            """

                This function validate value

                :param value: Instance of Class dict
                :type value: dict
        
            """

            def validate_name( value: str ):

                """

                    This function validate value is only alfanumeric and '_'

                    :param value: Instance of Class str
                    :type value: str
        
                """

                alpha_and_underscore: str = 'abcdefghijklmnñopqrstuvwxyz_0123456789'

                if not isinstance(value, str):

                    raise ValidationError({
                        'Name type error': " 'name' field must be str "
                    })

                if not reduce( lambda x,y: x and y, map( lambda x: x in alpha_and_underscore, value.lower() ) ):

                    raise ValidationError({
                        'Invalid name': "The name must be alphanumeric or the character '_'",
                    })

                if value in ["operator", "created", "creator", "ID"]:

                    raise ValidationError({
                        'Invalid name': f"Reserved words"
                    })

            def validate_default_value( type_description: str, default_value_description: Any ):

                """

                    This function validate description

                    :param type_description: Description name
                    :type type_description: str
                    :param default_value_description: Description value
                    :type default_value_description: dict
        
                """

                type_dict_validator: Dict[str, Callable] = {
                    'select_dynamic_services': validate_dynamic_services,
                    'select_dynamic_rut': validate_dynamic_customers,
                    'select_dynamic_users_iris': validate_dynamic_users_iris,
                    'select_dynamic_location': validate_location,
                    'select_dynamic_plans': validate_dynamic_plans,
                    'select_static': validate_select_static,
                    'int': validate_int,
                    'datetime': validate_datetime,
                    'logic': validate_logic,
                    'string': validate_string,
                    'phone': validate_phone,
                    'email': validate__email,
                    'float': validate_float,
                }

                if default_value_description != None:

                    type_dict_validator[type_description](default_value_description)

            type_list: List[str] = [
                'select_dynamic_services', 
                'select_dynamic_rut',
                'select_dynamic_users_iris',
                'select_dynamic_location',
                'select_dynamic_plans',
                'select_static',
                'int',
                'datetime',
                'logic',
                'string',
                'phone',
                'email',
                'float',
            ]

            result_dict: List[Dict[str, Any]] = []
            
            #Validate type
            if isinstance(value, list):

                names: List[str] = []

                #Validate all definitions
                for value_dict in value:

                    #Validation type of value_dict
                    if isinstance( value_dict, dict ):

                        validate_dict_description: Dict[str, Any] = {}
                        name_description: str = value_dict['name']

                        #Validation name don't repeated
                        if name_description in names:

                            raise ValidationError({
                                'Description repeat error': f"Name {name_description} is repeated"
                            })

                        else:

                            names.append(name_description)

                        #Validation name of field
                        validate_name(name_description)

                        #Validate exist required fields
                        if  not set(['type', 'required', 'default_value', 'name']) - set(value_dict.keys()):

                            type_description: str = value_dict['type']
                            
                            #Validate type
                            if type_description in type_list:

                                validate_dict_description['type']: str = type_description

                            else:

                                raise ValidationError({
                                    'Description type error': f"Description type of {name_description} is not valid"
                                })

                            required_description: bool = value_dict['required']

                            #Validate required
                            if isinstance(required_description, bool):

                                validate_dict_description['required']: bool = required_description

                            else:

                                raise ValidationError({
                                    'Description required error': f"Description required of {name_description} is not valid"
                                })

                            if type_description == 'select_static':

                                if not 'values' in value_dict:

                                    raise ValidationError({
                                        'Description field error: static selects': "Field values is required"
                                    })

                                default_value_description: Dict[ str, List[Dict[str,any]] ] = {
                                    'values': value_dict['values'],
                                    'default_value': value_dict['default_value']
                                }

                                validate_default_value(type_description, default_value_description)

                                result_dict.append( 
                                    {
                                        "type": type_description,
                                        "required": required_description,
                                        "default_value": value_dict['default_value'],
                                        "name": name_description,
                                        "values": value_dict['values']
                                    }
                                )

                            else:

                                default_value_description: Any = value_dict['default_value']

                                validate_default_value(type_description, default_value_description)

                                result_dict.append( 
                                    {
                                        "type": type_description,
                                        "required": required_description,
                                        "default_value": default_value_description,
                                        "name": name_description
                                    }
                                )

                        else:

                            raise ValidationError({
                                'Description error': f"Description of {name_description} no contains any this keys: 'type', 'required', 'default_value', 'name'"
                            })

                    else:

                        raise ValidationError({
                            'Value type error': "Value of definition must be dict type"
                        })

            else:

                raise ValidationError({
                    'Type error': " 'definition' field must be list type "
                })

            return result_dict

        self.definition: Dict[str, Dict[str, Any]] = definition_validate(self.definition)

    name: str = models.CharField(max_length=50, null=False, blank=False)
    description: str = models.CharField(max_length=200, default='')
    definition: dict = JSONField(null=False)


class DriveRegisterModel(BaseModel):

    def clean(self):

        def validate_value(drive_definition: Dict[str, Any]):

            type_dict_validator: Dict[str, Callable] = {
                'select_dynamic_services': validate_dynamic_services,
                'select_dynamic_rut': validate_dynamic_customers,
                'select_dynamic_users_iris': validate_dynamic_users_iris,
                'select_dynamic_location': validate_location,
                'select_dynamic_plans': validate_dynamic_plans,
                'select_static': validate_select_static,
                'int': validate_int,
                'datetime': validate_datetime,
                'logic': validate_logic,
                'string': validate_string,
                'phone': validate_phone,
                'email': validate__email,
                'float': validate_float,
            }
            
            if not ( drive_definition['required'] == False and drive_definition['value'] == None ):

                if drive_definition['type'] == 'select_static':

                    type_dict_validator[drive_definition['type']](drive_definition)

                else:

                    type_dict_validator[drive_definition['type']](drive_definition['value'])

        result: Dict[str, Any] = {}

        for value_definition in self.drive.definition:

            required: bool = value_definition['required']
            default_value: Any = value_definition['default_value']
            key_definition: str = value_definition['name']

            value_definition_register: Any = self.register.get(key_definition, None)

            if value_definition_register == None and required:

                raise ValidationError({
                    'required value': f"Value of {key_definition} field is required"
                })

            value_definition['value']: Any = value_definition_register

            #Validate value of register
            validate_value( value_definition )

            result[key_definition]: Any = value_definition_register

        self.register: Dict[str, Any] = result

    drive: BaseModel = models.ForeignKey( DriveDefinitionModel, on_delete=models.PROTECT, null=False )
    register: dict = JSONField(null=False)

