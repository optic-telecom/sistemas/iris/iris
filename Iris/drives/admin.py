from django.contrib import admin
from typing import List
from .models import DriveDefinitionModel, DriveRegisterModel

class DriveDefinitionAdmin(admin.ModelAdmin):

    fields: List[str] = ["name", "description", "definition","operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["name", "description","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["name", "description"]

    #Define model data list ordening
    ordering: List[str] = ["name"]

class DriveRegisterAdmin(admin.ModelAdmin):

    fields: List[str] = ["drive", "register","operator"]

    #List of fields to display in django admin
    list_display: List[str] = ["drive","register","operator"]

    #If you want django admin to show the search bar, just add this line
    search_fields: List[str] = ["register"]

    #Define model data list ordening
    ordering: List[str] = ["drive"]

admin.site.register(DriveDefinitionModel, DriveDefinitionAdmin)
admin.site.register(DriveRegisterModel, DriveRegisterAdmin)
